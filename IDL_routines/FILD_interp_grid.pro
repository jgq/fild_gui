function FILD_interp_grid,xpixel,ypixel,gyr_grid,pitch_grid,colfac,incidence_angle,initial_gyrophase,image

;=====================================================================================================
;                   J. Galdon               12/09/2014
;=====================================================================================================
;
; This function interpolates the values of gyroradius, pitch angle and collimating factor along 
; all the pixels of a given image. To interpolate, it uses the gyroradius, pitch angle and collimating factor
; given at each point of the grid. This way we can associate a gyroradius, pitch angle and collimating factor
; to each pixel of the frame.
;
; The inputs are:
; XPIXEL: 1-d array--> x coordinate in pixels of the grid points (given by get_pixel_coord.pro).
; YPIXEL: 1-d array--> y coordinate in pixels of the grid points (given by get_pixel_coord.pro).
; GYR_GRID:  1-d array--> Gyroradius associated to each grid point (given by read_grid.pro).
; PITCH_GRID:  1-d array--> Pitch angle associated to each grid point (given by read_grid.pro).
; COLFAC:  1-d array--> Collimating factor associated to each grid point (given by read_grid.pro).
; IMAGE:  2-d array--> The image for which you want to do the interpolation.
;
; Outputs is a structure containing the following variables:
; GINTERP: 2-d array with the interpolated gyroradius.
; PINTERP: 2-d array with the interpolated pitch angles.
; CINTERP: 2-d array with the interpolated collimating factor.
;
;=====================================================================================================

s=size(image,/dimensions)
xsize=s(0)
ysize=s(1)


;=========================================== 1 METHOD OF TRIANGULATION====================================================================

;TRIANGULATE, xpixel, ypixel, tr,b

;ginterp=griddata(xpixel,ypixel,gyr_grid,dimension=[xsize,ysize],method='quintic',triangles=tr,start=[0,0],delta=[1,1])

;pinterp=griddata(xpixel,ypixel,pitch_grid,dimension=[xsize,ysize],method='quintic',triangles=tr,start=[0,0],delta=[1,1])

;cinterp=griddata(xpixel,ypixel,colfac,dimension=[xsize,ysize],method='quintic',triangles=tr,start=[0,0],delta=[1,1])

;=====================================================================================================================

;=============================================2 METHOD OF TRIANGULATION===================================================================

;ginterp=tri_surf(gyr_grid,xpixel,ypixel,missing=0,gs=[1.,1.])
;pinterp=tri_surf(pitch_grid,xpixel,ypixel,nx=xsize,ny=ysize)

;cinterp=tri_surf(colfac,xpixel,ypixel,nx=xsize,ny=ysize,bounds=[min(xpixel),min(ypixel),max(xpixel),max(ypixel)])

;=====================================================================================================================

;=============================================3 METHOD OF TRIANGULATION===================================================================

TRIANGULATE, xpixel, ypixel, tr,b

ginterp=trigrid(xpixel,ypixel,gyr_grid,tr,[1,1],[0,0,xsize,ysize],nx=xsize,ny=ysize,/quintic,extra=b) ;extra=b
pinterp=trigrid(xpixel,ypixel,pitch_grid,tr,[1,1],[0,0,xsize,ysize],nx=xsize,ny=ysize,/quintic,extra=b)
cinterp=trigrid(xpixel,ypixel,colfac,tr,[1,1],[0,0,xsize,ysize],nx=xsize,ny=ysize,/quintic)      ; care with extrapolating the colimator factor
anginterp=trigrid(xpixel,ypixel,colfac,tr,[1,1],[0,0,xsize,ysize],nx=xsize,ny=ysize,/quintic)      ; care with extrapolating the incidence angle
gyrophaseinterp=trigrid(xpixel,ypixel,colfac,tr,[1,1],[0,0,xsize,ysize],nx=xsize,ny=ysize,/quintic)      ; care with extrapolating the initial gyrophase


ginterp>=0   ; REMOVE SPUREOUS VALUES LOWER THAN 0
pinterp>=0
cinterp>=0

;=====================================================================================================================



gstructure=create_struct('ginterp',ginterp,'pinterp',pinterp,'cinterp',cinterp,'anginterp',anginterp,'gyrophaseinterp',gyrophaseinterp)

return,gstructure
end
