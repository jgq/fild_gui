pro get_FILD_bfields,r_fild,z_fild,shot,time,br,bz,bt,b_total,$
    INTERP=INTERP

spawn,'module load /afs/ipp-garching.mpg.de/common/usr/modules/.sun/com_sun.mod/libs/nag_flib/mk22'

if (!VERSION.MEMORY_BITS eq 32) then $
_libkk = '/afs/ipp/aug/ads/lib/@sys/libkk8.so' $
else $
_libkk = '/afs/ipp/aug/ads/lib64/@sys/libkk8.so'
defsysv, '!LIBKK', _libkk

print,'****************************************************************'
print,'Calculating B field at FILD position...'
print,'---> Remember to load NAG libraries. If not errors can occurr...'
print,'****************************************************************'

shot = long(shot)
time = float(time)
exp='AUGD'
diag='EQH'

fpedit = 1L
error  = 0L

fpf    = fltarr(100)
rhozlib= fltarr(100)
r_sep  = 0.0
z_sep = 0.024
rzlib = 1.9 + 0.4 * indgen(100)/100.
zlib  = replicate(z_sep, 100)

;s = call_external(!libkk, 'kkidl', 'kkrzpfn', $
;                  error,exp,diag,shot,fpedit,time, $
;                  rzlib, zlib, 100L, $
;                  fpf, rhozlib )


;r_sep          = interpol(rzlib, rhozlib, 1.0) ; get R of separatrix at z=zlib

;print, 'R_sep/EQH/z=zLIB=', r_sep


rFILD=r_fild
zFILD=z_fild

Br=fltarr(n_elements(rFILD))
Bz=fltarr(n_elements(rFILD))
Bt=fltarr(n_elements(rFILD))
fPF=fltarr(n_elements(rFILD))
FJP=fltarr(n_elements(rFILD))


IF KEYWORD_SET(INTERP) THEN BEGIN
    isw=41L
    i=call_external(!libkk,'kkidl','kkeqints',error,isw) ;;; REQUEST INTERPOLATION ;;;; JGQ 19/12/2014
ENDIF


IF error NE 0 THEN BEGIN

	print,'An error ocurred...Returning'
	return

ENDIF

s = call_external(!libkk, 'kkidl', 'kkrzBrzt', $
                  error,exp,diag,shot,fpedit,time, $
                  rFILD, zFILD, n_elements(rFILD), $
                  Br,Bz,Bt,fPF,fJp)

IF error NE 0 THEN BEGIN

	print,'An error ocurred...Returning'
	return

ENDIF

b_total=sqrt(br^2.+bz^2.+bt^2.)

print,'*******************************************************************'
print,'Time point used to calculate B field (in s): ',time
print,'Magnetic field [Br,Bz,Bt]=[',Br,Bz,Bt,'] at [R,Z]=[',rfild,zfild,']'
print,'Total Magnetic field (in T): ',b_total
print,'Poloidal tilt angle wrt horizontal plane [in degs] (for FILD probe head, as looking from outside): ',atan(bz/bt)*180./!pi
print,'*******************************************************************'

;stop
return
end
