function FILD_get_pixel_coord,xgrid,ygrid,xscale,yscale,xshift,yshift,deg

;=====================================================================================================
;                   J. Galdon               16/07/2014
;=====================================================================================================
;
; This function gets the coordinates in pixels of the strike points of the grid for a certain image.
; The inputs are the coordinates of the grid points and the parameters given by the cal_grid.pro.
; The ouput is a structure containing the variables XPIXEL and YPIXEL
; 
;=====================================================================================================

x=xgrid
y=ygrid
xscale=xscale
yscale=yscale
xshift=xshift
yshift=yshift
deg=deg

alfa=deg*!pi/180

xpixel=(cos(alfa)*x-sin(alfa)*y)*xscale+xshift
ypixel=(sin(alfa)*x+cos(alfa)*y)*yscale+yshift

gstructure=create_struct('xpixel',xpixel,'ypixel',ypixel)

return,gstructure
end