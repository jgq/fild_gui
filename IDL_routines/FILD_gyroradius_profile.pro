pro FILD_gyroradius_profile,o,REF_FRAME=REF_FRAME,STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME,$
			GRID_PARAMS=GRID_PARAMS,REMAP_set=REMAP_set

;==========================================================================
;                   J. Galdon               10/07/2014
;==========================================================================
;
; 
;==========================================================================

REF_FRAME=REF_FRAME
GRID_PARAMS=GRID_PARAMS
REMAP_SET=REMAP_SET
STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME

s=size(ref_frame,/dimensions)

;*********************
;GET GRID INFORMATION
;*********************

FILDSIM_read_strike_map,strike_map,filename=strike_map_filename

IF n_tags(strike_map) EQ 0 THEN return

w=where(strike_map.collimator_factor GT 0,count)
ygrid=strike_map.yy(w)
zgrid=strike_map.zz(w)
gyr_grid=strike_map.gyroradius(w)
pitch_grid=strike_map.pitch_angle(w)
colfac=strike_map.collimator_factor(w)
incidence_angle=strike_map.average_incidence_angle(w)
initial_gyrophase=strike_map.average_initial_gyrophase(w)


;***************************
;GET CALIBRATION PARAMETERS
;***************************

cal_params_pix=grid_params

IF n_tags(cal_params_pix) LT 2 THEN return

;************************************************
; LOOK FOR THE POSITION IN PIXELS OF THE POINTS
;************************************************

grid_pixel=FILD_get_pixel_coord(ygrid,zgrid,$
                               cal_params_pix.xscale_pix,cal_params_pix.yscale_pix,$
                               cal_params_pix.xshift_pix,cal_params_pix.yshift_pix,$
                               cal_params_pix.deg_pix)



;********************************************************
;INTERPOLATION TO ASSOCIATE GYR AND PITCH TO EACH PIXEL
;********************************************************

grid_interp=FILD_interp_grid(grid_pixel.xpixel,grid_pixel.ypixel,gyr_grid,pitch_grid,colfac,incidence_angle,initial_gyrophase,ref_frame)

ginterp=grid_interp.ginterp
pinterp=grid_interp.pinterp
cinterp=grid_interp.cinterp


maxgir=remap_set.profile_gyro_maxgyr
mingir=remap_set.profile_gyro_mingyr         				
dg=remap_set.dgyr            					
maxpitch=remap_set.profile_gyro_maxpitch
minpitch=remap_set.profile_gyro_minpitch					
dp=remap_set.dpitch


selec_pitch=where(pinterp LT maxpitch AND pinterp GT minpitch,count) 

IF count EQ 0 THEN BEGIN

	print,'Pitch angle range to integrate is not valid...'
	o=0
	return

ENDIF
                                	      
gyro=findgen((maxgir-mingir)/dg+1)*dg+mingir
gyro_intensity=fltarr(n_elements(gyro))

im=fltarr(s(0),s(1))
im(selec_pitch)=ref_frame(selec_pitch)

FOR ii=mingir,maxgir-dg,dg   DO BEGIN

	w1=where(ginterp GT ii-dg/2 AND ginterp LT ii+dg/2,count)

        IF count GT 0 THEN BEGIN
                 wp=where(gyro GT ii-dg/10 AND gyro LT ii+dg/10,count)                  
                 tmp_im=im(w1)
                 gyro_intensity(wp)=total(tmp_im)
			
        ENDIF

		  
ENDFOR

;n_smooth=ROUND(2/dg)
;order=0
;savgol_filter=savgol(n_smooth,n_smooth,order,4)               ;;;*(FACTORIAL(order)/(dp^order))  ; this is to normalize in case order>0   
;gyro_intensity=convol(gyro_intensity,savgol_filter,/edge_truncate)


o={gyro:gyro,gyro_intensity:gyro_intensity}


return
end
