pro FILD_video_phantom,PULSE=PULSE,TIME=TIME,DATA=DATA,RES_PATH=RES_PATH,$
	xshift,yshift,xscale,yscale,alfa,grid_image

;==========================================================================
;                   J. Galdon               13/03/2016
;==========================================================================
;
; Routine to generate a .mpeg video from the frames read from the phantom 
; camera .CIN files 
; 
; KEYWORDS:
; PULSE: Shotnumber just to give a name to the output file
; TIME: 1D float array. Timepoints to overlay in the video frames
; DATA: 3D array (XSIZE,YSIZE,NFRAMES). Frames read from a .CIN file
; RES_PATH: Path where to save the video 
;==========================================================================

PULSE=PULSE
TIME=TIME
DATA=DATA
OUTPUT_PATH=RES_PATH

;**************
;LOAD FRAMES
;**************

data=bytscl(data,max=max(data))     
s=size(data,/dimensions)
xsize=s(0)
ysize=s(1)
frames=s(2)

;*****************
;GRID PARAMETERS
;*****************

gridImage=grid_image
gridImage=read_bmp(gridImage)

gridSize=size(gridImage)

xGRIDSize=gridsize[1]
yGRIDsize=gridsize[2]

gridInd=max(gridImage)


;*************************
;CALIBRATION PARAMETERS
;*************************

xshift=xshift
yshift=yshift
xscale=xscale
yscale=yscale
alfa=alfa*!pi/180

;*************
;CREATE MPEG
;*************

Filename_save=OUTPUT_PATH+strtrim(pulse,2)+'_'+strtrim(time(0),2)+'_'+strtrim(time(n_elements(time)-1),2)+'_phantom.mpeg'
mpegID = MPEG_Open([xsize, ysize], Filename=filename_save,quality=90)


set_plot,'X'

FOR j=0,frames-1 DO BEGIN
      print,j,' out of ',frames,'  frames loaded.'

;**************
;OVERLAY GRID
;**************

gridvalue=make_array(1,1,/UINT)
gridvalue[0,0]=!d.table_size-1

      for x=0,xGRIDsize-1 do begin
          for y=0,yGRIDsize-1 do begin
               if gridImage[x,y] eq gridInd then begin

			xx=ROUND((cos(alfa)*x-sin(alfa)*y)*xscale+xshift)
			yy=ROUND((sin(alfa)*x+cos(alfa)*y)*yscale+yshift)

			IF xx GE 0 AND xx LE xsize AND yy GE 0 AND yy LE ysize THEN BEGIN

                    		 DATA(ROUND((cos(alfa)*x-sin(alfa)*y)*xscale+xshift),$
                         	 ROUND((sin(alfa)*x+cos(alfa)*y)*yscale+yshift),j)=gridvalue
              		ENDIF
                     
              endif
          endfor
      endfor

data_plot=reform(data(*,*,j))


;*******************
;ROTATE WHOLE IMAGE
;*******************

IF abs(alfa) GT 140 THEN BEGIN

	data_plot=rotate(data_plot,2)

ENDIF

;*******************************
; USE Z BUFFER TO OVERLAY TIME
;*******************************

set_plot,'Z'
device,set_pixel_depth=24,decomposed=0,set_resolution=[xsize,ysize],set_colors=256          ; resolution sets the size of the buffer
tv,data_plot

XYOUTS, xsize*0.5, 10, 't='+strtrim(string(time[j],format='(f6.3)'))+' s', color=255,charsize=1 ,/device       ; here we can write anything to add to the frames
XYOUTS,xsize*0.1,10,STRING('Pulse: ',pulse,format='(A,I0)'),color=255,charsize=1 ,/device

data_2=TVRD(/true)                        


MPEG_Put, mpegID, image=data_plot, Frame=j,/ORDER,/color   


ENDFOR

set_plot,'X'

print,'Saving Mpeg video into '+OUTPUT_PATH+'      ... can take a while'
MPEG_Save, mpegID
MPEG_Close, mpegID



return
end
