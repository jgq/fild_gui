function rz2rhopol,rr,zz,shot,time,$
        EXP=EXP,DIAG=DIAG,ED=ED

;=====================================================================================================
;                   J. Galdon               22/07/2020
;=====================================================================================================
;
; Routine to convert R-Z coordinates to rhopol for a given AUG shot
;
;=====================================================================================================

if (!VERSION.MEMORY_BITS eq 32) then $
_libkk = '/afs/ipp/aug/ads/lib/@sys/libkk8.so' $
else $
_libkk = '/afs/ipp/aug/ads/lib64/@sys/libkk8.so'
defsysv, '!LIBKK', _libkk


IF KEYWORD_SET(EXP) THEN EXP=EXP ELSE EXP='AUGD'
IF KEYWORD_SET(DIAG) THEN DIAG=DIAG ELSE DIAG='EQH'
IF KEYWORD_SET(ED) THEN edition=ED ELSE edition=0L

error=0L

rin=rr
zin=zz
lin=n_elements(rin)


fpf=fltarr(lin)
rhop=fltarr(lin)
fpt=fltarr(lin)
rhot=fltarr(lin)

i=call_external(!libkk,'kkidl','kkrzptfn',$
    error,exp,diag,shot,edition,time,$
    rin,zin,lin,$
    fpf,rhop,fpt,rhot)

IF error NE 0 THEN BEGIN

    print,'There was an error in the calculation'
    return,-1

ENDIF

return,rhop
end
