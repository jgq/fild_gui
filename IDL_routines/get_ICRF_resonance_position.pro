@get_FILD_bfields.pro

pro get_ICRF_resonance_position,shot,time,A,Z,l,f_ICRF=f_ICRF

;=====================================================================================================
;                   J. Galdon
;=====================================================================================================
;
; f_ICRF=qB/(2!pi*m)
; l : harmonic
;
; Example: get_ICRF_resonance_position,38010,2.,[1,2],1,[1,3]
;
;=====================================================================================================

amu=1.661e-27   ; in kg
echarge=1.6022e-19  ; in Coulombs

q=echarge*Z
mass=amu*A

IF KEYWORD_SET(f_ICRF) THEN f_ICRF=f_ICRF ELSE f_ICRF=36.5e6    ; in Hz

B_res=(2.*!pi*mass*f_ICRF)/(q*l)

; Read shot equilibrium

g=readg(shot,time*1.e3)

; Read shot Bt

nr=1.e2
minr=1.0
maxr=2.3

rr=findgen(nr)/(nr-1)*abs(maxr-minr)+minr
zz=fltarr(nr)

get_FILD_bfields,rr,zz,shot,time,br,bz,bt,b_total

bt*=-1.

; Plot

int_r=interpol(rr,bt,b_res)
int_z=fltarr(n_elements(int_r))

window,0

plot,rr,bt,color=0,background=255,xtitle='R (m)',ytitle='Btor (T)',charsize=2.
    plots,int_r,b_res,psym=4,color=50,symsize=2.

window,1

plot,g.bdry(0,*),g.bdry(1,*),/iso,color=0,background=255,xtitle='R (m)',ytitle='Z (m)',charsize=2.,$
    xrange=[1.,2.3],xstyle=1,title='Shot: '+strtrim(shot,1)+'; B_axis (T): '+strtrim(g.bcentr,1)
    plots,int_r,int_z,psym=4,symsize=2.,color=50.
    plots,g.rmaxis,g.zmaxis,psym=1,symsize=2.,color=0

    FOR kk=0,n_elements(int_r)-1 DO BEGIN

        nn=1.e2
        dumm_r=fltarr(nn)+int_r(kk)

        minz=-1.0
        maxz=1.0

        dumm_z=findgen(nn)/(nn-1)*abs(maxz-minz)+minz

        oplot,dumm_r,dumm_z,psym=-3,color=100.

    ENDFOR

print,b_res

stop
return
end
