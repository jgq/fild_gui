pro read_artifitial_distribution,filename,output

;=====================================================================================================
;                   J. Galdon               20/05/2015
;=====================================================================================================
;
;=====================================================================================================

filename=filename

ON_IOERROR,err

openr,lun,filename,/get_lun


Energy=double(0)
Pitch=double(0)
A=double(0)
Q=double(0)
WEIGHT=double(0)
header=''

readf,lun,header
readf,lun,header

WHILE NOT EOF(lun) DO BEGIN
	readf,lun,Energyo,Pitcho,ao,qo,weighto             ;format='()'

	Energy=[Energy,Energyo]
	Pitch=[Pitch,Pitcho]
	A=[A,ao]
	Q=[Q,qo]
	Weight=[Weight,weighto]

ENDWHILE

close,lun
free_lun,lun

ON_IOERROR,NULL

Energy=Energy(1:*)
Pitch=Pitch(1:*)
A=A(1:*)
Q=Q(1:*)
Weight=Weight(1:*)


; Pitch is given in v||/v. Transform to deg

raw_pitch=Pitch
Pitch=abs(Pitch)
Pitch=acos(pitch)*180./!pi

output=create_struct('Energy',Energy,'Pitch',Pitch,'raw_pitch',raw_pitch,'A',A,'Q',Q,'Weight',weight)

return

err: BEGIN

	print,'Error reading ASCOT filename: '+filename
	print,!ERR_STRING
	output='ERROR'	
	
	return

END


end
