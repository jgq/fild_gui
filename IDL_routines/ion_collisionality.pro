function ion_collisionality,ni,ti,q,R,epsilon,z_eff

;==========================================================================
;                   J. Galdon               03/05/2016
;==========================================================================
; Computes ion collisionality following formula 
; from [Sauter O., Angioni C. and Lin-Liu Y. 1999, Phys. Plasmas 6 2834]
;
; INPUT PARAMETERS
;
; ni --> Ion density in m-3 ; Can be a scalar or 1D array
; Ti --> Ion temperature in eV ; Can be a scalar or 1D array
; q --> safety factor (is it the rho_pol dependent or should we just consider q95?) ; Can be scalar or 1D array
; R --> Major radius in m
; epsilon --> tokamak inverse aspect ratio
; Z-eff --> Z effective
;==========================================================================
 

s_ni=size(ni,/dimensions)
s_ti=size(ti,/dimensions)
s_q=size(q,/dimensions)

IF s_ni EQ s_ti AND s_ti EQ s_q THEN BEGIN

	

ENDIF ELSE BEGIN

	print,'Ne Te and q must have the same dimensions... returning'
	return,-1

ENDELSE


coulomb_log_ion=30.-alog(Z_eff^3*sqrt(ni)/Ti^(3/2))

;print,'Coulomb log. ion=',coulomb_log_ion

nu_ion=4.90e-18*(q*R*ni*Z_eff^4*coulomb_log_ion)/((Ti^2)*(epsilon^(3/2)))

return,nu_ion
end
