function int,input 

;=====================================================================================================
;                   J. Galdon               16/01/2024
;=====================================================================================================
;
; Routine to define a variable type "INT". 
; This routine substitutes former function in old AFS system. Who knows where this function was ... 
;
;=====================================================================================================

output=fix(input)

return, output 
end 