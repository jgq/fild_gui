function FILDSIM_read_std_set,FILD=FILD

;=====================================================================================================
;                   J. Galdon               6/07/2015
;=====================================================================================================
;
;=====================================================================================================

IF keyword_set(FILD) THEN BEGIN
	FILD=FILD
ENDIF ELSE BEGIN
	FILD=0
ENDELSE

filename='/afs/ipp-garching.mpg.de/home/j/jgq/IDL/FILD/AUG/FILD_ROUTINES/GUI/STD_FILD_DATA/std_filenames.txt'

openr,lun,filename,/GET_LUN

dummy=''
file_path=''

readf,lun,dummy
readf,lun,dummy
readf,lun,file_path
readf,lun,dummy
readf,lun,dummy

FILD_NAME=''
FILD_ID=0
STRIKE_MAP_FILENAME=''
STRIKE_POINTS_FILENAME=''
PLATE_FILENAME=''

kk=0

WHILE NOT(EOF(LUN)) DO BEGIN
	
	dummy=''
	readf,lun,dummy
	dumm=strsplit(dummy,/EXTRACT)
	
	FILD_NAME=[FILD_NAME,dumm(0)]
	FILD_ID=[FILD_ID,dumm(1)]
	STRIKE_MAP_FILENAME=[STRIKE_MAP_FILENAME,str(file_path)+dumm(2)]
	STRIKE_POINTS_FILENAME=[STRIKE_POINTS_FILENAME,str(file_path)+dumm(3)]
	PLATE_FILENAME=[PLATE_FILENAME,str(file_path)+dumm(4)]
	
	kk+=1
ENDWHILE

close,lun
FREE_LUN,lun

FILD_NAME=FILD_NAME(1:*)
FILD_ID=FILD_ID(1:*)
STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME(1:*)
STRIKE_POINTS_FILENAME=STRIKE_POINTS_FILENAME(1:*)
PLATE_FILENAME=PLATE_FILENAME(1:*)


CASE FILD OF
	1:  BEGIN
		w=where(FILD_ID EQ FILD,c)
		fild_name=fild_name(w)
		fild_id=fild_id(w)
		strike_map_filename=strike_map_filename(w)
		strike_points_filename=strike_points_filename(w)
		plate_filename=plate_filename(w)
		;print,'Reading filenames for FILD 1 in AUG'
	END

	11:  BEGIN
		w=where(FILD_ID EQ FILD,c)
		fild_name=fild_name(w)
		fild_id=fild_id(w)
		strike_map_filename=strike_map_filename(w)
		strike_points_filename=strike_points_filename(w)
		plate_filename=plate_filename(w)
		;print,'Reading filenames for FILD 1 in AUG'
	END
	
	2:  BEGIN
		w=where(FILD_ID EQ FILD,c)
		fild_name=fild_name(w)
		fild_id=fild_id(w)
		strike_map_filename=strike_map_filename(w)
		strike_points_filename=strike_points_filename(w)
		plate_filename=plate_filename(w)
		;print,'Reading filenames for FILD 1 in AUG'
	END


	55:  BEGIN
		w=where(FILD_ID EQ FILD,c)
		fild_name=fild_name(w)
		fild_id=fild_id(w)
		strike_map_filename=strike_map_filename(w)
		strike_points_filename=strike_points_filename(w)
		plate_filename=plate_filename(w)
		;print,'Reading filenames for JT60-SA FILD _v6'
	END

	56:  BEGIN
		w=where(FILD_ID EQ FILD,c)
		fild_name=fild_name(w)
		fild_id=fild_id(w)
		strike_map_filename=strike_map_filename(w)
		strike_points_filename=strike_points_filename(w)
		plate_filename=plate_filename(w)
		;print,'Reading filenames for JT60-SA FILD _v9'
	END


	
	ELSE: BEGIN

		print,'FILD number not valid'
		print,'Please set one of the following values to the FILD keyword: '
		print,'1---> FILD 1 in AUG'
		print,'2---> FILD 2 in AUG'
		print,'22---> FILD in TJ-II'
		print,'33---> FILD in ITER'
		print,'3---> FILD 3 in AUG'
		print,'55---> FILD in JT60-SA'
		return,'error'
	END
ENDCASE

output={FILD_NAME:FILD_NAME,FILD_ID:FILD_ID,STRIKE_MAP_FILENAME:STRIKE_MAP_FILENAME,$
	STRIKE_POINTS_FILENAME:STRIKE_POINTS_FILENAME,PLATE_FILENAME:PLATE_FILENAME}

return,output
end
