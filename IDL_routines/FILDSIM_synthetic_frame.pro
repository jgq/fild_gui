pro FILDSIM_synthetic_frame,input,output,$
	STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME,SCINTILLATOR_FILE=SCINTILLATOR_FILE,$
	SCINTILLATOR_YIELD=SCINTILLATOR_YIELD,GEOMETRICAL_FACTOR=GEOMETRICAL_FACTOR,$
	EXPOSURE_TIME=EXPOSURE_TIME,CAMERA_PROPERTIES=CAMERA_PROPERTIES,$
	OPTICS_TRANSMISSION=OPTICS_TRANSMISSION,OPTICS_SOLID_ANGLE=OPTICS_SOLID_ANGLE,$
	OPTICS_BETA=OPTICS_BETA,GAUSSIAN_NOISE_CENTROID=GAUSSIAN_NOISE_CENTROID,GAUSSIAN_NOISE_WIDTH=GAUSSIAN_NOISE_WIDTH,$
	PRINT_INFO=PRINT_INFO

;==========================================================================
;                   J. Galdon               27/02/2018
;==========================================================================
;
; Routine to generate a synthetic camera frame from a given FILDSIM simulation
;
; input: it is the output provided by FILD_synthetic_matrix_method --> Should run it including scintillator yield!!
;
; Geometrical factor: Correction factor to get real number of ions reaching the pinhole.
; 		It is essentially Area_pinhole/Area_FILD_code (i.e: in the simulation we get 10^6 ions/s reaching the whole FILD, 
;								but in reality the number of ions reaching the pinhole is smaller, lets say 10^2 ions/s)
;
; Exposure Time [in s]
; Optics Transmission [normalized to 1]
; Optics solid angle [in sr]
; Optics beta [normalized to 1]
;
; Gaussian noise centroid & width [in counts]
; 
;==========================================================================


;***************************
; SET INPUTS
;***************************

;IF keyword_set(exposure_time) THEN exposure_time=exposure_time ELSE exposure_time=0.001
;IF keyword_set(optics_transmission) THEN optics_transmission=optics_transmission ELSE optics_transmission=0.8
;IF keyword_set(optics_solid_angle) THEN optics_solid_angle=optics_solid_angle ELSE optics_solid_angle=1.e-5

;IF keyword_set(gaussian_noise_centroid) THEN gaussian_noise_centroid=gaussian_noise_centroid ELSE gaussian_noise_centroid=200
;IF keyword_set(gaussian_noise_width) THEN gaussian_noise_width=gaussian_noise_width ELSE gaussian_noise_width=50



;************************
; READ STRIKE MAP
;************************

FILDSIM_read_strike_map,strike_map,filename=strike_map_filename

IF n_tags(strike_map) EQ 0 THEN BEGIN

	print,'Strike map file not read ... Returning...'
	return

ENDIF

w=where(strike_map.collimator_factor GT 0)
ygrid=strike_map.yy(w)
zgrid=strike_map.zz(w)
gyr_grid=strike_map.gyroradius(w)
pitch_grid=strike_map.pitch_angle(w)
colfac=strike_map.collimator_factor(w)


;************************
; READ SCINTILLATOR
;************************

FILDSIM_read_plate,plate,filename=scintillator_file

size_plate_x=abs(max(plate.y)-min(plate.y))
size_plate_y=abs(max(plate.z)-min(plate.z))

IF keyword_set(optics_beta) THEN BEGIN 
	optics_beta=optics_beta 
ENDIF ELSE BEGIN

	print,'Optics magnification not set; Calculating proxy based on ratio chip-to-scintillator size'
	optics_beta=max([camera_properties.chip_xsize,camera_properties.chip_ysize])/max([size_plate_x,size_plate_y])*0.9

	print,'Optics magnification: ',optics_beta
ENDELSE


;************************
; READ YIELD
; not needed in principle
;************************


;************************
; SET RESIZE PARAMETERS
;************************

xscale=optics_beta/camera_properties.pixel_xsize
yscale=optics_beta/camera_properties.pixel_ysize

x0_scint_chip=camera_properties.nx_pixels/2-xscale*size_plate_x/2
y0_scint_chip=camera_properties.ny_pixels/2-yscale*size_plate_y/2

xshift=x0_scint_chip-xscale*min(plate.y)
yshift=y0_scint_chip-yscale*min(plate.z)

alpha=0.

print,'Size plate'
print,'x: ',size_plate_x
print,'Y: ',size_plate_y
print,'xscale,yscale,xshift,yshift'
print,xscale,yscale,xshift,yshift

;*************************
; LOAD INPUT DISTRIBUTION
;*************************

restore,input,/verb

input_gyr=fildsim_gyr
input_pitch=fildsim_pitch
input_dist=fildsim_output_dist

synthetic_frame=lonarr(camera_properties.nx_pixels,camera_properties.ny_pixels) ; in counts
synthetic_scintillator=dblarr(camera_properties.nx_pixels,camera_properties.ny_pixels)	; in photons/s
synthetic_scintillator_transmitted=dblarr(camera_properties.nx_pixels,camera_properties.ny_pixels)	; in photons/s

;*************************************
; MAP SCINTILLATOR AND GRID TO FRAME
;*************************************

grid_pixel=FILD_get_pixel_coord(ygrid,zgrid,xscale,yscale,xshift,yshift,alpha)

plate_pixel=FILD_get_pixel_coord(plate.y,plate.z,xscale,yscale,xshift,yshift,alpha)

xplate=plate_pixel.xpixel
yplate=plate_pixel.ypixel

dumm=colfac

grid_interp=FILD_interp_grid(grid_pixel.xpixel,grid_pixel.ypixel,gyr_grid,pitch_grid,colfac,dumm,dumm,synthetic_frame)

ginterp=grid_interp.ginterp
pinterp=grid_interp.pinterp
cinterp=grid_interp.cinterp

;**********************************
; MAP INPUT DISTRIBUTION TO FRAME
;**********************************

dg=abs(input_gyr(1)-input_gyr(0))
dp=abs(input_pitch(1)-input_pitch(0))

ss=size(input_dist,/dimensions)
n_gyr=ss(1)
n_pitch=ss(0)

FOR jj=0,n_gyr-1 DO BEGIN
	FOR ii=0,n_pitch-1 DO BEGIN	

		w=where(ginterp LE input_gyr(jj)+dg/2 AND ginterp GT input_gyr(jj)-dg/2 $
			AND pinterp LE input_pitch(ii)+dp/2 AND pinterp GT input_pitch(ii)-dp/2,npixel)

		IF npixel GT 0 THEN BEGIN

		synthetic_frame(w)+=ROUND(input_dist(ii,jj)/npixel*dg*dp*camera_properties.quantum_efficiency/camera_properties.f_analog_digital*exposure_time*optics_transmission/(4*!pi)*geometrical_factor)		
		synthetic_scintillator(w)+=input_dist(ii,jj)*dg*dp*geometrical_factor/npixel
		synthetic_scintillator_transmitted(w)+=input_dist(ii,jj)*dg*dp*geometrical_factor/npixel*optics_transmission/(4*!pi)
		
		ENDIF
	ENDFOR
ENDFOR


total_photons_transmitted=total(synthetic_scintillator_transmitted)
total_input_photons=total(input_dist)*dg*dp
total_input_photons_corrected=total_input_photons*geometrical_factor


;************************
; CALCULATE NOISE
;************************

; GAUSSIAN NOISE
; Still have to include dependence with exposure time !!!! 

random_dist_frame=randomu(seed,camera_properties.nx_pixels,camera_properties.ny_pixels,/normal)

gaussian_noise_frame=gaussian_noise_width*random_dist_frame+gaussian_noise_centroid

gaussian_noise_frame=LONG(gaussian_noise_frame)
gaussian_noise_frame>=0

;************************
; ADD NOISE
;************************

synthetic_frame+=gaussian_noise_frame

synthetic_frame<=camera_properties.saturation_level

synthetic_frame>=0

;************************

output={synthetic_frame:synthetic_frame,synthetic_scintillator:synthetic_scintillator,synthetic_scintillator_transmitted:synthetic_scintillator_transmitted,$
	plate_pixel:plate_pixel,grid_pixel:grid_pixel,pitch_grid:pitch_grid,gyr_grid:gyr_grid,optics_beta:optics_beta}

return
end

