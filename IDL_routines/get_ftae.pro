function calculate_ftae,btf,rho,rmag,q_tae

    mu0 = 4.*!pi*1e-7 ; Vacuum permeability [T*m/A]

    ; Alfven velocity

    vA0 = abs(btf)/sqrt(mu0*rho)

    ; TAE frequency

    f_tae=va0/(4.*!pi*rmag*q_tae)

    output={f_tae:f_tae,vA0:vA0}
return,output
end

function get_ftae,shot,ti,tf,tae_rhopol,PLOT=PLOT,PROF=PROF,NTOR=NTOR,YRANGE=YRANGE,EXPORT_PS=EXPORT_PS,$
    HtoHD=HtoHD

;=====================================================================================================
;                   J. Galdon               28/11/2019
;=====================================================================================================
;
; Routine to get the TAE frequency
; Simple formula from Heidbrink et al., PoP 2008. Formula #4
; V_alfven and q are evaluated at "guess" radial location of TAE
; Inputs:
; time: one point. In seconds.
; tae_rhopol: guess position in rho_poloidal of the TAE
;=====================================================================================================

if (!VERSION.MEMORY_BITS eq 32) then $
_libkk = '/afs/ipp/aug/ads/lib/@sys/libkk8.so' $
else $
_libkk = '/afs/ipp/aug/ads/lib64/@sys/libkk8.so'
defsysv, '!LIBKK', _libkk

IF KEYWORD_SET(NTOR) THEN ntor=NTOR ELSE ntor=4.
IF KEYWORD_SET(PROF) THEN prof_=prof ELSE prof_=0
IF KEYWORD_SET(YRANGE) THEN yr=yrange ELSE yr=[0.,200.]

IF KEYWORD_SET(HtoHD) THEN htohd=htohd ELSE htohd=0.05


CASE PROF_ OF

    1: BEGIN

        diag_ne='IDA'
        signal_ne='ne'

        diag_q='EQH'
        signal_q=''

        print,'not yet Implemented'
        print,'Returning...'
        return,-1

    END

    2: BEGIN

        diag_ne='IDA'
        signal_ne='ne'

        diag_q='FPG'
        signal_q='q50'  ; Also q0, q25, q50, q75, q95

        diag_vrot='IDI'
        signal_vrot='vt'

    END

    3: BEGIN

        print,'Vtor mapping not yet implemented'
        print,'Do not trust lab frame frequency... Use at your own risk'
        print,'.c to cotinue'
        stop

        diag_ne='IDA'
        signal_ne='ne'

        diag_q='FPG'
        signal_q='q50'  ; Also q0, q25, q50, q75, q95

        diag_vrot='CEZ'
        signal_vrot='vr_c'

    END

    ELSE: BEGIN

        print,'Need to define shotfiles to evaluate n_e and safety factor'
        print,'PROF=1 --> IDA & EQH'
        print,'PROF=2 --> IDA & FPG & IDI'
        print,'PROF=2 --> IDA & FPG & CEZ'

        print,'Returning ...'
        return,-1
    END

ENDCASE


print,'Using diag: ',diag_ne,diag_q
print,'Using signal: ',signal_ne,signal_q


nt=1.e3
time=findgen(nt)/(nt-1)*abs(tf-ti)+ti

;************************
; READ SIGNALS
;************************

; Read q

read_signal,ier,shot,diag_q,signal_q,t_q,d_q,pp,edition=0L


; Read density

read_signal,ier,shot,diag_ne,signal_ne,t_ne,d_ne,pp,edition=0L,area_base=rhop

dummy_rhop=reform(rhop(0,*))
dumm=min(abs(tae_rhopol-dummy_rhop),rind)

d_ne=d_ne(*,rind)  ;Take values in the channel closer to tae_rhopol

; Read vtor

read_signal,ier,shot,diag_vrot,signal_vrot,t_vrot,d_vrot,pp,edition=0L,area_base=rhop

dummy_rhop=reform(rhop(0,*))
dumm=min(abs(tae_rhopol-dummy_rhop),rind)

d_vrot=d_vrot(*,rind)  ; Rad/s.Take values in the channel closer to tae_rhopol

; Read B0 (on axis)

read_signal,ier,shot,'MAI','BTF',t_btf,btf,pp,edition=0L

; Read Magnetic axis position

read_signal,ier,shot,'FPG','Rmag',t_rmag,rmag,pp,edition=0L

; Read Zeff

read_signal,ier,shot,'IDZ','Zeff',t_zeff,zeff,pp,edition=0L


; Interpolate signals to same time basis

q_tae=interpol(d_q,t_q,time)
n_e=interpol(d_ne,t_ne,time)
vtor=interpol(d_vrot,t_vrot,time)
btf=interpol(btf,t_btf,time)
rmag=interpol(rmag,t_rmag,time)
zeff=interpol(zeff(*,0),t_zeff,time)


q_tae=abs(q_tae)    ; Change sign due to Bt/Ip convention

;***************************
; Calculate f_TAE
;***************************

mD  = 3.3435e-27; Deuterium mass [kg]
mH = 1.67355e-27

nD=n_e*(1.-HtoHD)
nH=n_e*(HtoHD)

rho=(nD*mD+nH*mH)*1./Zeff

dummy=calculate_ftae(btf,rho,rmag,q_tae)

f_tae=dummy.f_tae
vA0=dummy.vA0

; Add doppler shift correction due to plasma rotation

; vtor needs to be in rad/s
doppler_shift = ntor*vtor/(2.*!pi) ; % Doppler shift [Hz].

f_tae_doppler=f_tae+doppler_shift    ; This is the frequency in the lab frame

print,'Using H2HD: ',htohd
print,'Mean Alfven velocity  [m/s]: ',mean(va0)

;***************
; PLOT
;***************

sm=2

IF KEYWORD_SET(PLOT) THEN BEGIN

    IF KEYWORD_SET(EXPORT_PS) THEN BEGIN

            set_plot,'ps'
            device,filename='Analytical_ftae_'+strtrim(shot,1)+'_tmp.eps',/color

    ENDIF

    loadct,5

    window,/free

    plot,time,f_tae*1.e-3,color=0,background=255,xtitle='Time (s)',ytitle='TAE Frequency (kHz)',$
        charsize=2.,yrange=yr,ystyle=1,title='Shot: '+strtrim(shot,1)+'; n_TAE:'+strtrim(ntor,1)+' ; Guessed rhop TAE: '+strtrim(tae_rhopol,1),/nodata

    oplot,time,smooth(f_tae*1.e-3,sm),color=50
    oplot,time,smooth(f_tae_doppler*1.e-3,sm),color=100

    xyouts,0.8,0.8,'Plasma Frame',color=50,/norm,charsize=2.
    xyouts,0.8,0.7,'Lab frame',color=100,/norm,charsize=2.


    IF KEYWORD_SET(EXPORT_PS) THEN BEGIN

        device,/close
        set_plot,'X'

    ENDIF

ENDIF

output={time:time,f_tae:f_tae,f_tae_doppler:f_tae_doppler,$
    n_e:n_e,vtor:vtor,rmag:rmag,btf:btf,$
    q_tae:q_tae,tae_rhopol:tae_rhopol,$
    htohd:htohd,vA0:vA0}

stop
return,output
end
