pro FILD_pitch_profile,o,REF_FRAME=REF_FRAME,STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME,GRID_PARAMS=GRID_PARAMS,REMAP_set=REMAP_set

;==========================================================================
;                   J. Galdon               10/07/2014
;==========================================================================
;
; 
;==========================================================================

REF_FRAME=REF_FRAME
GRID_PARAMS=GRID_PARAMS
REMAP_SET=REMAP_SET
STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME

s=size(ref_frame,/dimensions)

;***********************
;GET GRID INFORMATION
;***********************

FILDSIM_read_strike_map,strike_map,filename=strike_map_filename

IF n_tags(strike_map) EQ 0 THEN return

w=where(strike_map.collimator_factor GT 0,count)
ygrid=strike_map.yy(w)
zgrid=strike_map.zz(w)
gyr_grid=strike_map.gyroradius(w)
pitch_grid=strike_map.pitch_angle(w)
colfac=strike_map.collimator_factor(w)
incidence_angle=strike_map.average_incidence_angle(w)
initial_gyrophase=strike_map.average_initial_gyrophase(w)

;****************************
;GET CALIBRATION PARAMETERS
;****************************

cal_params_pix=grid_params

IF n_tags(cal_params_pix) LT 2 THEN return

;************************************************
; LOOK FOR THE POSITION IN PIXELS OF THE POINTS
;************************************************

grid_pixel=FILD_get_pixel_coord(ygrid,zgrid,$
                               cal_params_pix.xscale_pix,cal_params_pix.yscale_pix,$
                               cal_params_pix.xshift_pix,cal_params_pix.yshift_pix,$
                               cal_params_pix.deg_pix)

;*******************************************************
;INTERPOLATION TO ASSOCIATE GYR AND PITCH TO EACH PIXEL
;*******************************************************

grid_interp=FILD_interp_grid(grid_pixel.xpixel,grid_pixel.ypixel,gyr_grid,pitch_grid,colfac,incidence_angle,initial_gyrophase,ref_frame)

ginterp=grid_interp.ginterp
pinterp=grid_interp.pinterp
cinterp=grid_interp.cinterp



maxgir=remap_set.profile_pitch_maxgyr
mingir=remap_set.profile_pitch_mingyr         				
dg=remap_set.dgyr            					
maxpitch=remap_set.profile_pitch_maxpitch
minpitch=remap_set.profile_pitch_minpitch					
dp=remap_set.dpitch


selec_gyro=where(ginterp LT maxgir AND ginterp GT mingir,count) 

IF count EQ 0 THEN BEGIN

	print,'Gyroradius range to integrate is not valid...'
	o=0
	return

ENDIF
                                	      
pitch=findgen((maxpitch-minpitch)/dp+1)*dp+minpitch
pitch_intensity=fltarr(n_elements(pitch))

im=fltarr(s(0),s(1))
im(selec_gyro)=ref_frame(selec_gyro)

FOR ii=minpitch,maxpitch-dp,dp   DO BEGIN

	w1=where(pinterp GT ii-dp/2 AND pinterp LT ii+dp/2,count)

        IF count GT 0 THEN BEGIN
                 wp=where(pitch GT ii-dp/10 AND pitch LT ii+dp/10,count)                  
                 tmp_im=im(w1)
                 pitch_intensity(wp)=total(tmp_im)
			
        ENDIF

		  
ENDFOR

n_smooth=ROUND(2/dp)
order=0
savgol_filter=savgol(n_smooth,n_smooth,order,4)               ;;;*(FACTORIAL(order)/(dp^order))  ; this is to normalize in case order>0   
pitch_intensity=convol(pitch_intensity,savgol_filter,/edge_truncate)


o={pitch:pitch,pitch_intensity:pitch_intensity}


return
end