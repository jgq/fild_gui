pro FILDSIM_save_standard_namelist,fn,result_dir,geometry_dir,backtrace,save_orbits,$
	n_gyroradius,n_pitch,n_ions,step,length,gyrophase,$
	start_x,start_y,start_z,alpha,beta,$
	gyroradius,pitch,$
	n_scintillator,n_slits,$
	scintillator_files,slit_files

;*****************************************
; J. Galdon               19/01/2016
; University of Seville
; Email: jgaldon@us.es
;*****************************************

setup={result_dir:result_dir,geometry_dir:geometry_dir,backtrace:backtrace,save_orbits:save_orbits,$
	N_gyroradius:N_gyroradius,N_pitch:N_pitch,N_ions:N_ions,$
	step:step,length:length,gyrophase:gyrophase,$
	start_x:start_x,start_y:start_y,start_z:start_z,$
	alpha:alpha,beta:beta,$
	gyroradius:gyroradius,pitch:pitch,$
	N_scintillator:N_scintillator,N_slits:N_slits,$
	scintillator_files:scintillator_files,slit_files:slit_files}

save,FILENAME=fn,setup

print,'Standard setup for FILDSIM.F90 saved as: '+fn


return
end  
