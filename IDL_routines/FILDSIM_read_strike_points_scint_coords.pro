pro FILDSIM_read_strike_points_scint_coords,output,filename=filename

;*****************************************
; J. Galdon               19/01/2016
; University of Seville
; Email: jgaldon@us.es
;*****************************************

;*****************************************************************
; Routine to read the strike-points filename given by fildsim.f90
;*****************************************************************

nheader=9
origin=fltarr(3)
vec_base_1=fltarr(3)
vec_base_2=fltarr(3)

IF keyword_set(filename) EQ 0 THEN BEGIN
	print,'You must set an input filename... Returning'
	return
ENDIF

dumm=''
gyroradius=0.
pitch_angle=0.
initial_gyrophase=0.
xx=0.
yy=0.
zz=0.
xscint=0.
yscint=0.
remap_gyro=0.
remap_pitch=0.
incidence_angle=0.
xini=0.
yini=0.
zini=0.


openr,lun,filename,/get_lun

FOR kk=0,2 DO readf,lun,dumm

readf,lun,origin
readf,lun,dumm
readf,lun,vec_base_1
readf,lun,dumm
readf,lun,vec_base_2
readf,lun,dumm

WHILE NOT EOF(lun) DO BEGIN

    dummy=''
    readf,lun,dummy

    vars=strsplit(dummy,/extract)
    vars=float(vars)

    gyroradius=[gyroradius,vars(0)]
    pitch_angle=[pitch_angle,vars(1)]
    initial_gyrophase=[initial_gyrophase,vars(2)]
    xx=[xx,vars(3)]
    yy=[yy,vars(4)]
    zz=[zz,vars(5)]
    xscint=[xscint,vars(6)]
    yscint=[yscint,vars(7)]
    remap_gyro=[remap_gyro,vars(8)]
    remap_pitch=[remap_pitch,vars(9)]
    incidence_angle=[incidence_angle,vars(10)]

    IF n_elements(vars) GT 11 THEN BEGIN
        xini=[xini,vars(11)]
        yini=[yini,vars(12)]
        zini=[zini,vars(13)]
    ENDIF ELSE BEGIN
        xini=[xini,0.]
        yini=[yini,0.]
        zini=[zini,0.]
    ENDELSE


;stop
ENDWHILE

close,lun
free_lun,lun

gyroradius=gyroradius(1:*)
pitch_angle=pitch_angle(1:*)
initial_gyrophase=initial_gyrophase(1:*)
xx=xx(1:*)
yy=yy(1:*)
zz=zz(1:*)
xscint=xscint(1:*)
yscint=yscint(1:*)
remap_gyro=remap_gyro(1:*)
remap_pitch=remap_pitch(1:*)
incidence_angle=incidence_angle(1:*)
xini=xini(1:*)
yini=yini(1:*)
zini=zini(1:*)


output={gyroradius:gyroradius,pitch_angle:pitch_angle,initial_gyrophase:initial_gyrophase, $
	xx:xx,yy:yy,zz:zz,xscint:xscint,yscint:yscint,remap_gyro:remap_gyro,remap_pitch:remap_pitch,$
    incidence_angle:incidence_angle,$
    xini:xini,yini:yini,zini:zini,$
    origin:origin,vec_base_1:vec_base_1,vec_base_2:vec_base_2}

return
end
