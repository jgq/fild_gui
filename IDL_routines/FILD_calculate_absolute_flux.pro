pro FILD_calculate_absolute_flux,output,raw_frame,calibration_frame,$
	efficiency_energy,efficiency_yield,b_field,band_pass_filter,$
	interpolated_gyro,interpolated_fcol,roi,$
	PINHOLE_AREA=PINHOLE_AREA,EXPOSURE_TIME=EXPOSURE_TIME,A=A,Z=Z,$
	CALIB_EXPOSURE_TIME=CALIB_EXPOSURE_TIME,PIXEL_AREA_COVERED=PIXEL_AREA_COVERED,$
	INT_PHOTON_FLUX=INT_PHOTON_FLUX,METHOD=METHOD,IGNORE_FCOL=IGNORE_FCOL
;*****************************************
; J. Galdon               15/03/2017
; University of Seville
; Email: jgaldon@us.es
;*****************************************
;****************************************************************
; Routine to estimate absolute flux of losses
; efficiency energy --> Given in eV
; efficiency yield --> Given in photons/ion
; b_field --> Given in T
; band_pass_filter -->
; interpolated_gyro --> given in cm
; interpolated_fcol --> given in Nscint/Npinhole*100
; pinhole_area --> given in m^2
; exposure_time --> given in s
;
; Output:
; absolute flux --> should be ions/s/m^2 in the pinhole
;****************************************************************

IF keyword_set(IGNORE_FCOL) THEN BEGIN

	interpolated_fcol=interpolated_fcol*0.+1.
	print,'Ignoring collimator factor --> Calculating Ion Flux at the scintillator not the pinhole!!!'

ENDIF ELSE BEGIN

	interpolated_fcol=interpolated_fcol/100.

	ww=where(interpolated_fcol LT 0.005,count)

	IF count GT 0 THEN interpolated_fcol(ww)=0.		; We remove interpolated values being lower than 0.5%. Since fcol goes in the denominator of the formula,
								; very low values lead to diverging heat loads which are nonsense
ENDELSE

;***************************
; CHECK FRAMES SIZE
;***************************
s1=size(raw_frame,/dimensions)
s2=size(calibration_frame,/dimensions)

IF s1(0) NE s2(0) OR s1(1) NE s1(1) THEN BEGIN

	print,'Size of data frame and calibration frame not matching!!'
	print,'Artifitially resizing the calibration frame!!'

;	print,'Returning...'
	calbration_frame=congrid(calibration_frame,s1(0),s1(1))

;	output={absolute_flux_frame:raw_frame*0.,absolute_flux:0.,$
;		absolute_heat_load_frame:raw_frame*0.,absolute_heat_load:0.,$
;		a:a,z:z,exposure_time:exposure_time,pinhole_Area:pinhole_area,$
;		b_field:b_field}
;	return

ENDIF


IF max(interpolated_gyro) EQ 0 THEN BEGIN

	print,'Interpolated matrix is calculated ONLY after a remap operation...Returning'

	output={absolute_flux_frame:raw_frame*0.,absolute_flux:0.,$
		absolute_heat_load_frame:raw_frame*0.,absolute_heat_load:0.,$
		a:a,z:z,exposure_time:exposure_time,pinhole_Area:pinhole_area,$
		b_field:b_field}

	return

ENDIF


;*******************************
; CALCULATE INTERPOLATED ENERGY
;*******************************
echarge=1.602e-19
mass_proton=938.272e6			; in eV/c^2
c_light=3.e8
mass=A*mass_proton

interpolated_energy=0.5*(interpolated_gyro/100)^2*b_field^2*Z^2/mass*c_light^2

;***********************************
; CALCULATE INTERPOLATED EFFICIENCY
;***********************************

efficiency_frame=interpol(efficiency_yield,efficiency_energy,interpolated_energy)
efficiency_frame>=1.

print,'WARNING!!! EFFICIENCY FRAME IS BEING FAKED CURRENTLY!!'

;*******************************
; GET THE SPOTS REGIONS
;*******************************

spots_frame=raw_frame
dumm_spots=bpass(raw_frame,1,band_pass_filter)
ww=where(dumm_spots EQ 0,count)

IF count GT 0 THEN spots_frame(ww)=0.


IF keyword_set(SPOT_TRACKING) THEN BEGIN

	spot_track=feature(spots_frame,band_pass_filter,iterate=1)

ENDIF ELSE BEGIN

	spot_track=0.

ENDELSE

;*******************************
; CALIBRATION FRAME
;*******************************

calibration_frame=calibration_frame/(calib_exposure_time*int_photon_flux*pixel_area_covered)

;*******************************
; APPLY MODEL
;*******************************


CASE method OF

	1: BEGIN

	;*******************************
	; GET THE SPOTS REGIONS
	;*******************************

	spots_frame=raw_frame
	dumm_spots=bpass(raw_frame,1,band_pass_filter)
	ww=where(dumm_spots EQ 0,count)

	IF count GT 0 THEN spots_frame(ww)=0.

	absolute_flux_frame=spots_frame/(efficiency_frame*calibration_frame*exposure_time*pinhole_area)
	fcol_0_pos=where(interpolated_fcol EQ 0,c1)
	fcol_finite_pos=where(interpolated_fcol GT 0,c2)
	absolute_flux_frame(fcol_finite_pos)=absolute_flux_frame(fcol_finite_pos)/interpolated_fcol(fcol_finite_pos)
	IF c1 GT 0 THEN absolute_flux_frame(fcol_0_pos)=0.


	absolute_flux=total(absolute_flux_frame,/DOUBLE,/NAN)
	absolute_heat_load_frame=absolute_flux_frame*interpolated_energy*echarge
	absolute_heat_load=total(absolute_heat_load_frame,/DOUBLE,/NAN)

	spot_track=0.

	END


	2:BEGIN


	;*******************************
	; GET THE SPOTS REGIONS
	;*******************************

	spots_frame=raw_frame
	dumm_spots=bpass(raw_frame,1,band_pass_filter)
	ww=where(dumm_spots EQ 0,count)

	IF count GT 0 THEN spots_frame(ww)=0.
	spot_track=feature(spots_frame,band_pass_filter,iterate=1)
	dumm=max(spot_track(2,*),bright_spot)

	print,'Using spot tracking algorithm to find mean collimator factor'

	absolute_flux_frame=spots_frame/(efficiency_frame*calibration_frame*exposure_time*pinhole_area)
	absolute_flux_frame=absolute_flux_frame/interpolated_fcol(spot_track(0,bright_spot),spot_track(1,bright_spot))

	print,'Mean Collimator Factor: ',interpolated_fcol(spot_track(0,bright_spot),spot_track(1,bright_spot))

	absolute_flux=total(absolute_flux_frame,/DOUBLE,/NAN)
	absolute_heat_load_frame=absolute_flux_frame*interpolated_energy*echarge
	absolute_heat_load=total(absolute_heat_load_frame,/DOUBLE,/NAN)

	END


	3:BEGIN

	print,'Using ROI method'

	roi_region=ROI
	help,roi_region

	spots_frame=raw_frame
	absolute_flux_frame=raw_frame*0.
	absolute_flux_frame(roi_region)=spots_frame(roi_region)/(efficiency_frame(roi_region)*calibration_frame(roi_region)*exposure_time*pinhole_area)
	
	absolute_flux_frame=absolute_flux_frame/mean(interpolated_fcol(roi_region))

	print,'Mean Collimator Factor: ',mean(interpolated_fcol(roi_region))

	absolute_flux=total(absolute_flux_frame,/DOUBLE,/NAN)
	absolute_heat_load_frame=absolute_flux_frame*interpolated_energy*echarge
	absolute_heat_load=total(absolute_heat_load_frame,/DOUBLE,/NAN)

	spot_track=0.


	END

	ELSE: BEGIN

		print,'Select a method for the spot detection:'
		print,'1 --> Integrate whole frame'
		print,'2 --> Spot tracking method'
		print,'3 --> ROI method'
	END

ENDCASE

;*******************************


output={absolute_flux_frame:absolute_flux_frame,absolute_flux:absolute_flux,$
	absolute_heat_load_frame:absolute_heat_load_frame,absolute_heat_load:absolute_heat_load,$
	spots_frame:spots_frame,$
	a:a,z:z,exposure_time:exposure_time,pinhole_Area:pinhole_area,$
	b_field:b_field,spot_track:spot_track,roi:roi}

return
end
