@ion_collisionality.pro
@electron_collisionality.pro
@get_IDX_profile.pro
@LIBP_experiments_database.pro

pro calculate_collisionality_timetrace,SHOT,output,PLOT=PLOT,$
	Q_DIAG=Q_DIAG,Q_SIGNAL=Q_SIGNAL,TARGET_RHOPOL=TARGET_RHOPOL

;==========================================================================
;                   J. Galdon               03/05/2016
;==========================================================================
; Routine to plot the time-trace of the collisionality during a shot.
; The diagnostics used to calculate ne,te and ti have to be changed inside the routine
; by hand as well as the channels used.
;
; As a reference one can use the .csv standard settings from cview used by W. Suttrop
; elm_mitig_nu.csv
;==========================================================================

a=0.5 		; AUG minor plasma radius
R= 1.65		; AUG major radius
epsilon=a/R	; Inverse aspect ratio (a/R)

If KEYWORD_SET(TARGET_RHOPOL) THEN target_rhopol=target_rhopol ELSE target_rhopol=0.9

print,'Target rho_pol: ',target_rhopol
;**************************
; Get Zeff
;**************************

zeff_prof=get_IDX_profile(shot,prof=8)

IF type(zeff_prof) EQ 2 THEN BEGIN

    print,'No Zeff available from IDZ. Setting Zeff to 1.5 ...'

    nt=1.e3
    nrhopol=10.

    dumm_time=findgen(nt)/nt*10.
    dumm_prof=fltarr(nt)
    dumm_prof+=1.5

    dumm_data=fltarr(nt,nrhopol)
    dumm_rhopol=findgen(nrhopol)/nrhopol*1.2

    FOR jj=0,nrhopol-1 DO BEGIN

        dumm_data(*,jj)=dumm_prof

    ENDFOR

    zeff_prof={time:dumm_time,data:dumm_data,rhop:dumm_rhopol}

ENDIF

dumm=min(abs(zeff_prof.rhop(0,*)-target_rhopol),rpos)
zeff_raw=zeff_prof.data(*,rpos)

;**************************
; Get ne
;**************************

ne_prof=get_IDX_profile(shot,prof=1)

IF type(ne_prof) EQ 2 THEN BEGIN

    print,'No IDA profile available. Returning ...'

    output=-1
    return

ENDIF


dumm=min(abs(ne_prof.rhop(0,*)-target_rhopol),rpos)
ne_raw=ne_prof.data(*,rpos)

;**************************
; Get Te
;**************************

te_prof=get_IDX_profile(shot,prof=2)


dumm=min(abs(te_prof.rhop(0,*)-target_rhopol),rpos)
te_raw=te_prof.data(*,rpos)

;**************************
; Get Ti
;**************************

ti_prof=get_IDX_profile(shot,prof=5)

IF type(ti_prof) EQ 2 THEN BEGIN

    ti_prof=te_prof
    print,'WARNING!!! NO Ti from IDI available --> continue assuming Ti=Te?? '
    stop

ENDIF

dumm=min(abs(ti_prof.rhop(0,*)-target_rhopol),rpos)
ti_raw=ti_prof.data(*,rpos)

;**************************
; Get safety factor q
;**************************

; From FPG --> 'FPG'  'q95' // 'q0'

;***************************

q_diagname='FPG'
q_signalname='q95'

IF keyword_set(Q_DIAG) THEN q_diagname=q_diag
IF keyword_set(Q_SIGNAL) THEN q_signalname=q_signal

edition=0
ier=0

read_signal,ier,shot,q_diagname,q_signalname,time_q,q_raw,phys_unit,edition=0,time=[0.,10.]

IF ier NE 0 THEN BEGIN

	print,'Error:',ier
	print,'Returning...'
	return
ENDIF


;*********************************
; Set timebase to IDA time base
;*********************************

time_base=ne_prof.time


n_e=interpol(ne_raw,ne_prof.time,time_base)
ti=interpol(ti_raw,ti_prof.time,time_base)
te=interpol(te_raw,te_prof.time,time_base)
zeff=interpol(zeff_raw,zeff_prof.time,time_base)

n_i=n_e

q=interpol(q_raw,time_q,time_base)
q=abs(q)

;**********************************
; Calculate collisinalities
;***********************************

nu_ion=ion_collisionality(n_i,ti,q,R,epsilon,zeff)
nu_electron=electron_collisionality(n_e,te,q,R,epsilon,zeff)

nu_electron_suttrop=1.5*1.026e-15*(q)*n_e/te^2
nu_ion_suttrop=1.5*1.026e-15*(q)*n_e/ti^2


;*******************
; PLOT OPTIONS
;*******************

xr=[0.2,7.]
yr=[0.,3.]

IF keyword_set(PLOT) THEN BEGIN

loadct,5

window,0

plot,time_base,nu_electron,color=0,background=255,charsize=2.,$
	xtitle='Time (s)', ytitle='Collisionality',psym=4,$
	yrange=yr,ystyle=1,xrange=[0.,6.],position=[0.3,0.2,0.9,0.8],$
	title='SHOT #'+strtrim(SHOT,1),/nodata

oplot,time_base,nu_electron,color=50,psym=1
oplot,time_base,nu_ion,color=100,psym=1


oplot,time_base,nu_electron_suttrop,color=50,psym=4
oplot,time_base,nu_ion_suttrop,color=100,psym=4


xyouts,0.02,0.9,'nu_electron',color=50,/norm,charsize=1.5
xyouts,0.02,0.85,'nu_ion',color=100,/norm,charsize=1.5

window,1

!P.Multi=[0,1,3]

plot,ne_prof.time,ne_raw,color=0,background=255,$
	xtitle='Time (s)',ytitle='Electron density (m-3)',charsize=2.,$
	xrange=xr,title='SHOT #'+strtrim(SHOT,1)

plot,te_prof.time,te_raw,color=0,background=255,$
	xtitle='Time (s)',ytitle='Electron Temperature (eV)',charsize=2.,$
	xrange=xr

plot,ti_prof.time,ti_raw,color=0,background=255,$
	xtitle='Time (s)',ytitle='Ion Temperature (eV)',charsize=2.,$
	xrange=xr


!P.Multi=0

ENDIF

;******************
; OUTPUT
;******************

output={shot:shot,a:a,R:R,epsilon:epsilon,target_rhopol:target_rhopol,$
	time:time_base,nu_ion:nu_ion,nu_electron:nu_electron}

;******************

return
end
