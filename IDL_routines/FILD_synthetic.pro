pro FILD_synthetic,output,DEFAULT_PATH=DEFAULT_PATH,A=A,Z=Z,B_FILD=B_FILD,SIM_CODE=SIM_CODE,$
	GRID_PARAM=GRID_PARAM,PHYSIC_CONSTANTS=PHYSIC_CONSTANTS,INPUT_FILENAME=INPUT_FILENAME,$
	PRECOMPUTED_RESOLUTION=PRECOMPUTED_RESOLUTION,ASCOT_pitch=ASCOT_pitch

;==========================================================================
;                   J. Galdon               22/12/2015
;==========================================================================
;
; This routine calculates the distribution of ions that would reach the
; scintillator given a loss distribution in the first Wall, and taking 
; into account the colimator factor and the resolution of the detector.
; The routine uses the results obtained with the EFIPDESIGN code. It uses 
; the collimating factor at each point of the strike-map and the resolution
; in gyrorradius and pitch angle.
;
; output: Structure containing output variables
; A,Z: KEYWORDS to set the mass and charge of the ions by force. If not set it
;	must be included in the input distribution parameters
; B_FILD: Magnetic field at FILD position
; SIM_CODE: INTEGER. Indicates the code that was used to give the input distribution.
;	Each code has a particular format.
; GRID_PARAM: Structure containing the parameters of the grid where the 
;	calculation will be performed.
; PHYSIC_CONSTANTS: Structure containing some physics constants necessary
;	for the calculation. i.e.: speed of light
; INPUT_FILENAME: Filename containing the input distribution
; PRECOMPUTED_RESOLUTION: resolutions.sav file with the resolution information (NOT IMPLEMENTED YET)
;==========================================================================

DEVICE=DEVICE
MASS=MASS
B_FILD=B_FILD
SIM_CODE=SIM_CODE
GRID_PARAM=GRID_PARAM
PHYSIC_CONSTANTS=PHYSIC_CONSTANTS

IF keyword_set(DEFAULT_PATH) THEN BEGIN

	def_path=default_path

ENDIF ELSE BEGIN

	def_path='~/'

ENDELSE

strike_map_filename = DIALOG_PICKFILE(/READ,PATH=def_path,FILTER='*strike_map.dat')

;*************
; MAIN GRID
;*************
maxgyr=grid_param.maxgyr
mingyr=grid_param.mingyr
dg=grid_param.dg

maxpitch=grid_param.maxpitch
minpitch=grid_param.minpitch
dp=grid_param.dp

gyr=findgen((maxgyr-mingyr)/dg+1)*dg+mingyr
pitch=findgen((maxpitch-minpitch)/dp+1)*dp+minpitch

;******************
;READ RESOLUTIONS
;******************

IF keyword_set(PRECOMPUTED_RESOLUTION) THEN BEGIN

	print,'Reading resolutions from file: ',precomputed_resolution
	restore,precomputed_resolution

ENDIF ELSE BEGIN

	strike_points_file = DIALOG_PICKFILE(/READ,PATH=def_path,FILTER='*strike_points.dat')

	print,'Computing resolutions from file: ',strike_points_file
	
	nm_resolutions=FILDSIM_calculate_all_resolutions(strike_points_file)

	resolution_info={Gyroradius:reform(nm_resolutions(0,*)),Pitch_angle:reform(nm_resolutions(1,*)),$
		Sigma_gyroradius:reform(nm_resolutions(2,*)),Sigma_pitch_angle:reform(nm_resolutions(3,*)),$
		Centroid_gyroradius:reform(nm_resolutions(4,*)),Centroid_pitch_angle:reform(nm_resolutions(5,*)),$
		Amplitude_gyroradius:reform(nm_resolutions(6,*)),Amplitude_pitch_angle:reform(nm_resolutions(7,*)),$
		filename:strike_points_file}

ENDELSE

u_pitch=resolution_info.pitch_angle[UNIQ(resolution_info.pitch_angle,SORT(resolution_info.pitch_angle))]
u_gyroradius=resolution_info.gyroradius[UNIQ(resolution_info.gyroradius,SORT(resolution_info.gyroradius))]

m_gyro_resolution=fltarr(n_elements(u_gyroradius),n_elements(u_pitch))
m_pitch_resolution=fltarr(n_elements(u_gyroradius),n_elements(u_pitch))


FOR kk=0,n_elements(u_gyroradius)-1 DO BEGIN
	FOR jj=0,n_elements(u_pitch)-1 DO BEGIN

		w=where(resolution_info.gyroradius EQ u_gyroradius(kk) AND resolution_info.pitch_angle EQ u_pitch(jj),count)

		IF count GT 0 THEN BEGIN

			m_gyro_resolution(kk,jj)=resolution_info.sigma_gyroradius(w)
			m_pitch_resolution(kk,jj)=resolution_info.sigma_pitch_angle(w)
		ENDIF

	ENDFOR
ENDFOR

m_gyro_resolution_int=tri_surf(m_gyro_resolution,xvalues=u_gyroradius,yvalues=u_pitch,/regular,nx=n_elements(gyr),ny=n_elements(pitch),$
                 bounds=[mingyr,minpitch,maxgyr,maxpitch])

m_pitch_resolution_int=tri_surf(m_pitch_resolution,xvalues=u_gyroradius,yvalues=u_pitch,/regular,nx=n_elements(gyr),ny=n_elements(pitch),$
                 bounds=[mingyr,minpitch,maxgyr,maxpitch])


print,'-> RESOLUTIONS READ AND INTERPOLATED'

;************************
;READ COLIMATOR FACTOR 
;************************

print,'Reading collimator factor from: ',strike_map_filename

FILDSIM_read_strike_map,grid,filename=strike_map_filename

u_pitch=grid.pitch_angle[UNIQ(grid.pitch_angle, SORT(grid.pitch_angle))]
u_gyroradius=grid.gyroradius[UNIQ(grid.gyroradius, SORT(grid.gyroradius))]

m_colfac=fltarr(n_elements(u_gyroradius),n_elements(u_pitch))

FOR kk=0,n_elements(u_gyroradius)-1 DO BEGIN
	FOR jj=0,n_elements(u_pitch)-1 DO BEGIN

	w=where(grid.gyroradius EQ u_gyroradius(kk) AND grid.pitch_angle EQ u_pitch(jj),count)
	
	IF count GT 0 THEN BEGIN
		m_colfac(kk,jj)=grid.collimator_factor(w)
	ENDIF

	ENDFOR
ENDFOR

m_colfac=m_colfac/100. 			; colfac in strike_map.dat is given in %

m_colfac_int=tri_surf(m_colfac,xvalues=u_gyroradius,yvalues=u_pitch,/regular,nx=n_elements(gyr),ny=n_elements(pitch),$
                 bounds=[mingyr,minpitch,maxgyr,maxpitch])



print,'-> COLIMATOR FACTOR READ AND INTERPOLATED'

;**********************************************


C_LIGHT=physic_constants.c	
B=B_FILD	
FILENAME=INPUT_FILENAME

CASE sim_code OF
	1: BEGIN
		print,'Using ASCOT distribution...'

		IF keyword_set(ASCOT_pitch) THEN ascot_pitch=1 ELSE ascot_pitch=0

		read_ASCOT,filename,input_dist_params,pitch_sign=ascot_pitch

		IF n_tags(input_dist_params) LT 2 THEN BEGIN

			output='ERROR'
			return

		ENDIF
			
		IF keyword_set(A) THEN BEGIN
			M=A*physic_constants.proton_mass
		ENDIF ELSE BEGIN
			M=input_dist_params.A*physic_constants.proton_mass
			A=input_dist_params.A	
		ENDELSE

		IF keyword_set(Z) THEN BEGIN
			Z=Z
		ENDIF ELSE BEGIN
			Z=input_dist_params.Q
		ENDELSE

		;print,'A and Z are: ',M,Z

		ehist=input_dist_params.energy
		phist=input_dist_params.pitch
		weight=input_dist_params.weight

		rhist= sqrt(2*M*ehist)/(Z*c_light*B)*100 ; in cm
		
		
	   END

	2: BEGIN

		print,'Using artifitial distribution generated by artifitial_dist_generator.pro'
		read_artifitial_distribution,filename,input_dist_params

		IF n_tags(input_dist_params) LT 2 THEN BEGIN

			output='ERROR'
			return

		ENDIF
			
		IF keyword_set(A) THEN BEGIN
			M=A*physic_constants.proton_mass
		ENDIF ELSE BEGIN
			M=input_dist_params.A*physic_constants.proton_mass
			A=input_dist_params.A	
		ENDELSE

		IF keyword_set(Z) THEN BEGIN
			Z=Z
		ENDIF ELSE BEGIN
			Z=input_dist_params.Q
		ENDELSE

		;print,'A and Z are: ',M,Z

		ehist=input_dist_params.energy
		phist=input_dist_params.pitch
		weight=input_dist_params.weight

		rhist= sqrt(2*M*ehist)/(Z*c_light*B)*100 ; in cm

	END	

	ELSE: BEGIN
		
		print,'Only ASCOT code available at the moment...'
		output='ERROR'
		return
	      END

ENDCASE

print,'-> INPUT FILENAME READ: '+filename

;**************************
; 2D HIST COMPUTATION
;**************************

h=fltarr(n_elements(pitch),n_elements(gyr))
heat_flux=fltarr(n_elements(pitch),n_elements(gyr))

FOR kk=0,n_elements(pitch)-1 DO BEGIN
	FOR jj=0,n_elements(gyr)-1 DO BEGIN

		ww=where(rhist GT gyr(jj)-dg/2. AND rhist LT gyr(jj)+dg/2. AND $
			phist GT pitch(kk)-dp/2. AND phist LT pitch(kk)+dp/2.,count)
		
		IF (count GT 0) THEN BEGIN

			h(kk,jj)=total(weight(ww))/dg/dp
			heat_flux(kk,jj)=total(weight(ww)*ehist(ww))
		ENDIF
	ENDFOR	
ENDFOR

print,'Total Input ions (weight): ',total(weight)
print,'Total Input ions: ',total(h)*dp*dg
print,'Heat Rate (in eV/s): ',total(heat_flux)
print,'Heat Rate (in J/s): ',total(heat_flux)*1.60218e-19

area=0.0042	; FILD area in ASCOT in m^2

print,'To calculate heat flux divide heat rate by the area (A) defined in the simulation code'
print,'A is the surface defined in the orbit tracing code where we computed the distribution function'
print,'Using default FILD area in ASCOT'
print,'Heat Flux (in J/(s m^2)): ',total(heat_flux)*1.60218e-19/area

;*************************
;FINAL DISTRIBUTION
;*************************

ztotal=fltarr(n_elements(pitch),n_elements(gyr))

;=======================MAIN LOOP====================================

print,'Performing calculations... May take a few minutes...'


nx=n_elements(gyr)
ny=n_elements(pitch)

FOR kk=0L,n_elements(rhist)-1 DO BEGIN

	dummy=min(abs(phist(kk)-pitch),p0mat)
	dummy=min(abs(rhist(kk)-gyr),g0mat)

	sp=m_pitch_resolution_int(g0mat,p0mat)
	sg=m_gyro_resolution_int(g0mat,p0mat)
	sp=sp(0)
	sg=sg(0)
	colfac=m_colfac_int(g0mat,p0mat)
	colfac=colfac(0)

	ax=(2*!pi*sg^2)^(-0.5)
	ay=(2*!pi*sp^2)^(-0.5)
	
	z=fltarr(n_elements(pitch),n_elements(gyr))
	

	IF colfac GT 0 AND sp GT 0 AND sg GT 0 THEN BEGIN

		FOR j=0,ny-1 DO BEGIN
			FOR i=0,nx-1 DO BEGIN
				gausx=exp(-0.5*((rhist(kk)-gyr(i))/sg)^2)*ax
				gausy=exp(-0.5*((phist(kk)-pitch(j))/sp)^2)*ay
				z(j,i)=gausx*gausy/ax/ay*(2*!pi*sp*sg)^(-1)*weight(kk)*colfac

			ENDFOR
		ENDFOR

	ENDIF

	ztotal=ztotal+z
	

ENDFOR

;=====================================================================

print,'Total output ions: ',total(ztotal)*dg*dp

output={gyr:gyr,pitch:pitch,output_dist:ztotal,input_dist:h,strike_map_file:strike_map_filename,strike_points_file:strike_points_file}

return
end

