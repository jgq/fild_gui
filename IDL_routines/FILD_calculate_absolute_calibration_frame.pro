function   FILD_calculate_absolute_calibration_frame,calibration_frame,exposure_time,$
		pinhole_area,calib_exposure_time,int_photon_flux,pixel_area_covered


;=====================================================================================================
;                   J. Galdon               02/04/2018
;=====================================================================================================
;
; Routine to calculate FILD absolute calibration frame
;
; Int_photon_flux --> Photons/(s*m^2)
; Pixel_area_covered --> m^2
; Calib_exposure_time --> s
; exposure_time --> s
; pinhole_area --> m^2
; Calibration frame (input) --> Counts
;
; Raw Frame --> Counts
; Calibration frame (output) --> Counts*s*m^2/photons
; Photon flux frame --> Photons/(s*m^2)

;=====================================================================================================


output=calibration_frame*exposure_time*pinhole_area/(calib_exposure_time*int_photon_flux*pixel_area_covered)


return,output
end
