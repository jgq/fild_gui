@read_ASCOT_birth_profile.pro
@orb_gc_aug.pro
@vrzphi_2_Epitch.pro
@plot_aug_walls.pro
@rz2rhopol.pro

pro get_FILD_back_orbit,energy,ipitch,output,Anum=Anum,Znum=Znum,$
	SHOT=SHOT,TIME=TIME,DIAG=DIAG,EXPERIMENT=EXPERIMENT,ED=ED,NSTEPS=NSTEPS,RFILD=RFILD,ZFILD=ZFILD,PHIFILD=PHIFILD,$
	EXPORT_PS=EXPORT_PS,WRITE_ORBIT_FILES=WRITE_ORBIT_FILES,GET_RHOPOL=GET_RHOPOL,$
    NO_PLOT=NO_PLOT,FORWARD_ORBIT=FORWARD_ORBIT

;==========================================================================
;                   J. Galdon               16/01/2017
;==========================================================================
;
; Routine to calculate the birth position of FILD prompt losses.
; 	1. Calculate overlap between FILD orbit and Beam deposition in Z-Phi plane.
;	2. Limited to this region, calculate overlap between beam birth pitch and the orbit pitch.
;
;	Energy in keV
;	Pitch in v||/v
;	phi in rad
;
; calling sequence
; > get_FILD_birth_position,93.,cos(65.*!pi/180.),2.18,0.32,236.5*!pi/180.
;
; WARNING!! If you load the NAG module then the gfile for the equilibrium is not correctly restored!
;==========================================================================

echarge=1.6022e-19

; FILD 1: phi=236.5

IF keyword_set(Anum) then Anum=Anum ELSE Anum=2
If keyword_set(Znum) then Znum=Znum ELSE Znum=1

If keyword_set(SHOT) THEN shot=shot ELSE shot=34697L
If keyword_set(TIME) THEN time=time ELSE time=2400.		; in ms
If keyword_set(DIAG) THEN diag=diag ELSE diag='EQH'
If keyword_set(EXPERIMENT) THEN exp=experiment ELSE exp='AUGD'
If keyword_set(ED) THEN ed=ed ELSE ed=0L
If keyword_set(NSTEPS) THEN NST=NSTEPS ELSE NST=1.e3

If keyword_set(RFILD) THEN RFILD=RFILD ELSE rfild=2.189
If keyword_set(ZFILD) THEN ZFILD=ZFILD ELSE zfild=0.32
If keyword_set(PHIFILD) THEN PHIFILD=PHIFILD ELSE phifild=236.5*!pi/180.

IF KEYWORD_SET(FORWARD_ORBIT) THEN rev_time=0 ELSE rev_time=1

print,'*********************'
print,'A:',Anum
print,'Z:',Znum
print,'SHOT:',shot
print,'TIME (ms):',time
print,'DIAG:',diag
print,'EXP:',exp
print,'EDITION:',ed
print,'NSTEPS:',nst
print,'R FILD (m):',rfild
print,'Z FILD (m):',zfild
print,'PHI FILD (rad):',phifild
print,'*********************'


;***************************
; Calculate orbit backwards
;***************************

g=readg(shot,time,exp=exp,diag=diag,ed=ed)
rphiz_part=[rfild,phifild,zfild]

; Calculate in full-orbit

orb_gc_aug,g,rphiz_part,ipitch,energy,fnsteps=nst,INPUTSATGC=0,$
			 mm=Anum,z=znum,checkfull=1,orbinteg=0,reverse_time=rev_time,plott=1,printinfo=1,outs=orbit

; orbit.fout contains particle orbit info: [vr,vphi,vz,r,phi,z]


ww=where(orbit.fout(4,*) LT 0,c)

IF c GT 0 THEN orbit.fout(4,ww)+=2.*!pi

;************************
; PLOT
;************************

IF KEYWORD_SET(NO_PLOT) THEN GOTO,jump_noplot

!P.Multi=[0,1,1]

window,0

plot,orbit.fout(4,*),orbit.fout(5,*),psym=3,color=0,background=255,xtitle='Phi',ytitle='Z',charsize=2.,$
	xrange=[0.,2.*!pi],xstyle=1,yrange=[-1.,1.],/nodata

oplot,orbit.fout(4,*),orbit.fout(5,*),psym=3,color=50
plots,orbit.fout(4,0),orbit.fout(5,0),psym=1,color=100,thick=2.

!P.Multi=[0,2,1]

window,2

plot,g.bdry(0,*),g.bdry(1,*),psym=-3,color=0,background=255,xtitle='R(m)',ytitle='Z(m)',/nodata,xrange=[1.0,2.5],yrange=[-1.2,1.2],/iso

oplot,orbit.fout(3,*),orbit.fout(5,*),psym=-3,color=50
oplot,orbit.rcont0,orbit.zcont0,color=150,psym=-3,thick=2.
oplot,g.bdry(0,*),g.bdry(1,*),psym=-3,color=100

plot_aug_walls,g,col=0

plots,orbit.fout(3,0),orbit.fout(5,0),psym=1,color=100,thick=2.,symsize=2.




plot,g.bdry(0,*),g.bdry(1,*),psym=-3,color=0,background=255,xtitle='R(m)',ytitle='Z(m)',/nodata,xrange=[1.9,2.3],yrange=[0.1,0.5],/iso

oplot,orbit.fout(3,*),orbit.fout(5,*),psym=-3,color=50
oplot,g.bdry(0,*),g.bdry(1,*),psym=-3,color=100

plot_aug_walls,g,col=0

plots,orbit.fout(3,0),orbit.fout(5,0),psym=1,color=100,thick=2.,symsize=2.


!P.Multi=0

jump_noplot:

;************************
; EXPORT PS
;************************

IF keyword_set(EXPORT_PS) THEN BEGIN

    fn='FILD_backwards_RZ_'+strtrim(shot,1)+'_'+strtrim(time,1)+'ms_'+$
    	strtrim(energy,1)+'keV_'+strtrim(ipitch,1)+'_A='+strtrim(Anum,1)+$
    	'_Z='+strtrim(Znum,1)+'.ps'

    set_plot,'ps'
    device,filename=fn,/color

    !P.Multi=[0,2,1]


    plot,g.bdry(0,*),g.bdry(1,*),psym=-3,color=0,background=255,xtitle='R(m)',ytitle='Z(m)',/nodata,xrange=[1.0,2.5],yrange=[-1.2,1.2],/iso

    oplot,orbit.fout(3,*),orbit.fout(5,*),psym=-3,color=50
    oplot,orbit.rcont0,orbit.zcont0,color=150,psym=-3,thick=2.
    oplot,g.bdry(0,*),g.bdry(1,*),psym=-3,color=100

    plot_aug_walls,g,col=0

    plots,orbit.fout(3,0),orbit.fout(5,0),psym=1,color=100,thick=2.,symsize=2.

    plot,g.bdry(0,*),g.bdry(1,*),psym=-3,color=0,background=255,xtitle='R(m)',ytitle='Z(m)',/nodata,xrange=[1.9,2.3],yrange=[0.1,0.5],/iso

    oplot,orbit.fout(3,*),orbit.fout(5,*),psym=-3,color=50
    oplot,g.bdry(0,*),g.bdry(1,*),psym=-3,color=100

    plot_aug_walls,g,col=0

    plots,orbit.fout(3,0),orbit.fout(5,0),psym=1,color=100,thick=2.,symsize=2.


    !P.Multi=0

	device,/close
	set_plot,'X'

ENDIF


IF KEYWORD_SET(WRITE_ORBIT_FILES) THEN BEGIN


    print,'Writing file with orbit coordinates'
    print,'Format is: '
    print,'R(m) Z(m) Phi (rad)'

    fn_dir=WRITE_ORBIT_FILES

    fn=strtrim(fn_dir,1)+'FILD_backwards_RZ_'+strtrim(shot,1)+'_'+strtrim(time,1)+'ms_'+$
    	strtrim(energy,1)+'keV_'+strtrim(ipitch,1)+'_A='+strtrim(Anum,1)+$
    	'_Z='+strtrim(Znum,1)+'_ORBIT_COORDS.txt'

    openw,lun,fn,/get_lun

    printf,lun,'Orbit coordinates calculated on: ',systime()
    printf,lun,'Shot: ',shot
    printf,lun,'Time (ms): ',time
    printf,lun,'Initial energy (keV): ',energy
    printf,lun,'Initial pitch (vpara/vtot): ',ipitch
    printf,lun,'A: ',Anum
    printf,lun,'Z: ',Znum
    printf,lun,'DIAG:',diag
    printf,lun,'EXP:',exp
    printf,lun,'EDITION:',ed
    printf,lun,'NSTEPS:',nst
    printf,lun,'R FILD (m):',rfild
    printf,lun,'Z FILD (m):',zfild
    printf,lun,'PHI FILD (rad):',phifild

    printf,lun,'-----------------------------'
    printf,lun,'Format is: R (m) Z(m) Phi (rads)'

    FOR kk=0L,n_elements(orbit.fout(3,*))-1 DO BEGIN

    	printf,lun,orbit.fout(3,kk),orbit.fout(5,kk),orbit.fout(4,kk)

    ENDFOR

    close,lun
    free_lun,lun

    ON_IOERROR,NULL

ENDIF

IF KEYWORD_SET(GET_RHOPOL) THEN BEGIN

    print,'Calculating rhopol explored by orbit'
    rhopol_orbit=rz2rhopol(orbit.fout(3,*),orbit.fout(5,*),shot,time,exp=exp,diag=diag,ed=ed)

ENDIF ELSE BEGIN

    rhopol_orbit=-1

ENDELSE

output={orbit:orbit,rhopol_orbit:rhopol_orbit,gfile:g}

return
end
