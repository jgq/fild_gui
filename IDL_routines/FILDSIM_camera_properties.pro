function FILDSIM_camera_properties,cam_type

;==========================================================================
;                   J. Galdon               18/05/2016
;==========================================================================
;  Function to call the properties of a given camera.
;
;	NX_PIXELS: Number of pixels of the chip in the X direction
;	NY_PIXELS: Number of pixels of the chip in the Y direction
;	PIXEL_XSIZE: Size of the pixel in the X direction (in cm)
;	PIXEL_YSIZE: Size of the pixel in the Y direction (in cm)
;	QUANTUM_EFFICIENCY: Quantum efficiency of the camera ( < 1)
;	F_ANALOG_DIGITAL: Analog to digital conversion factor (electrons/count)
;	DYNAMIC_RANGE: Dynamic range of the camera (in bits)
;==========================================================================
 

CASE cam_type OF 

	1: BEGIN

		camera_name='VGA Pixelfly'
		nx_pixels=640L
		ny_pixels=480L
		pixel_xsize=9.9e-4
		pixel_ysize=9.9e-4
		quantum_efficiency=0.40
		f_analog_digital=6.5
		dynamic_range=12

	END



	2: BEGIN

		camera_name='QE Pixelfly'
		nx_pixels=1392L
		ny_pixels=1024L
		pixel_xsize=6.45e-4
		pixel_ysize=6.45e-4
		quantum_efficiency=0.62
		f_analog_digital=3.8
		dynamic_range=12


	END



	ELSE: BEGIN

		print,'Camera Type is not defined. Please select: '
		print,'1 --> VGA Pixelfly'
		print,'2 --> QE Pixelfly'
		
		return,-1
	END

ENDCASE


properties={nx_pixels:nx_pixels,ny_pixels:ny_pixels,$
	pixel_xsize:pixel_xsize,pixel_ysize:pixel_ysize,$
	quantum_efficiency:quantum_efficiency,f_analog_digital:f_analog_digital,$
	dynamic_range:dynamic_range,camera_name:camera_name}

return,properties
end
