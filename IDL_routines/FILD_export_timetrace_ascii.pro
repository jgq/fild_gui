pro FILD_export_ascii_timetrace,output_filename,time,data

;=====================================================================================================
;                   J. Galdon               20/05/2015
;=====================================================================================================
;
; FILD in counts
; Time in s
;=====================================================================================================

print,'Writing file with FILD timetrace'

openw,lun,output_filename,/get_lun

printf,lun,'Time (s) ','FILD (counts)'

FOR kk=0L,n_elements(time)-1L DO BEGIN

	printf,lun,time(kk),data(kk)

ENDFOR

close,lun
free_lun,lun

ON_IOERROR,NULL


return
end
