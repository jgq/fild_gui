@plot_aug_walls.pro

pro FILD_plot_plasma_shape_evolution,shot,ti,tf,output

;=====================================================================================================
;                   J. Galdon               14/06/2021
;=====================================================================================================
;
; Routine to plot the plasma shape evolution and compare to FILD positions
;
;=====================================================================================================

; Define timing for plasma shapes and ranges

dt=0.1  ; in s
ntime=ROUND(abs(tf-ti)/dt)   ; # of time points to evaluate

times=findgen(ntime)/(ntime-1)*abs(tf-ti)+ti
times*=1.e3 ; convert into ms

xr=[1.0,2.4]
yr=[-1.0,1.0]

; Define FILD positions
; Index order is: FILD1, FILD4, FILD5
; FILD4 and 5 are measurements of pinhole in parking position with FARO

FILD_id=[1,4,5]
FILD_r=[2.181,2.082,1.795]   ; in m
FILD_z=[0.32,-0.437,-0.794]    ; in m
FILD_insertion=[-14.,10.,18.]     ; in mm ; FILD1 is wrt limiter shadow. FILD4 and FILD5 are manipulator units

FILD_insertion*=1.e-3   ; convert into m

nfild=n_elements(FILD_r)

; Get Plasma Shapes

fild_to_plasma=fltarr(nfild,ntime)
fild_plasma_min_r=fltarr(nfild,ntime)
fild_plasma_min_z=fltarr(nfild,ntime)

FOR kk=0,ntime-1 DO BEGIN

    col=kk*200./ntime

    g=readg(shot,times(kk))

    IF kk EQ 0 THEN BEGIN

        window,0
        plot,g.bdry(0,*),g.bdry(1,*),color=0,background=255,xtitle='R (m)',ytitle='Z (m)',charsize=2.,/iso,/nodata,$
            xrange=xr,yrange=yr,position=[0.2,0.2,0.8,0.8],/norm,title='SHOT #: '+strtrim(shot,1)

        plot_aug_walls,g,col=0

    ENDIF

    oplot,g.bdry(0,*),g.bdry(1,*),color=col

    ; Find FILD distance to separatrix

    FOR JJ=0,nfild-1 DO BEGIN

    plots,FILD_r(jj),FILD_z(jj),psym=1,symsize=2.,color=50+jj*50.

    dumm_dist=reform(sqrt((FILD_r(jj)-g.bdry(0,*))^2.+(FILD_z(jj)-g.bdry(1,*))^2.))
    dumm_dist-=FILD_insertion(jj)
    fild_to_plasma(jj,kk)=min(dumm_dist,ipos)
    fild_plasma_min_r(jj,kk)=g.bdry(0,ipos)
    fild_plasma_min_z(jj,kk)=g.bdry(1,ipos)

    plots,fild_plasma_min_r(jj,kk),fild_plasma_min_z(jj,kk),color=50+jj*50.,psym=4

    ENDFOR

ENDFOR

; Add colorbar

colorbar,max=tf,min=ti,/vert,position=[0.9,0.2,0.93,0.8],color=0,format='(f7.3)',title='Time (s)'

; Plot distance to plasma

window,/free
plot,times*1.e-3,fild_to_plasma(0,*)*1.e2,psym=-4,color=0,background=255,$
    yrange=[0.,15.],xtitle='Time [s]',ytitle='FILD distance to plasma [cm]',charsize=2.,/nodata,$
    title='SHOT #: '+strtrim(shot,1)

FOR kk=0,nfild-1 DO BEGIN

    oplot,times*1.e-3,fild_to_plasma(kk,*)*1.e2,color=50+kk*50.,psym=-4
    xyouts,0.2,0.2+kk*0.05,'FILD'+strtrim(fild_id(kk),1)+' insertion [cm]: '+strtrim(FILD_insertion(kk)*1.e2,1),color=50+kk*50.,charsize=2.,/norm

ENDFOR


; Outputs

output={shot:shot,times:times,fild_to_plasma:fild_to_plasma,fild_plasma_min_r:fild_plasma_min_r,fild_plasma_min_z:fild_plasma_min_z,$
    FILD_id:FILD_id,FILD_r:FILD_r,FILD_z:FILD_z}

return
end
