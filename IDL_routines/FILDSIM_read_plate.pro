pro FILDSIM_read_plate,output,filename=filename

;*****************************************
; J. Galdon               19/01/2016
; University of Seville
; Email: jgaldon@us.es
;*****************************************

;******************************************************************
; Routine to read the scintillator geometry used for fildsim.f90
; The geometry files are .pl
;******************************************************************


IF keyword_set(filename) EQ 0 THEN BEGIN
	print,'You must set an input filename... Returning'
	return
ENDIF

dumm=''
n_vertices=0
openr,lun,filename,/get_lun

readf,lun,dumm
readf,lun,dumm
readf,lun,dumm,n_vertices,format='(A11,I3)'


x=fltarr(n_vertices+1)
y=fltarr(n_vertices+1)
z=fltarr(n_vertices+1)

FOR kk=0,n_vertices-1 do begin

	readf,lun,xx,yy,zz,format='(3f11.7)'
	x(kk)=xx
	y(kk)=yy
	z(kk)=zz
ENDFOR

close,lun
free_lun,lun

x(n_vertices)=x(0)
y(n_vertices)=y(0)
z(n_vertices)=z(0)

output={n_vertices:n_vertices,x:x,y:y,z:z}

return
end
