@get_energy_FILD.pro

pro FILD_plot_energy_vs_gyro,bfield,EXPORT_PS=EXPORT_PS

;=====================================================================================================
;                   J. Galdon               02/07/2020
;=====================================================================================================
;
; Routine to map FILD gyroradius into energy depending on the species
;
;=====================================================================================================

mingyro=0.
maxgyro=12.
npoints=1.e3

gyro=findgen(npoints)/(npoints-1)*abs(maxgyro-mingyro)+mingyro

;***********************
; DEUTERIUM CURVE
;***********************

deut=get_energy_FILD(gyro,bfield,A=2,Z=1)

;***********************
; HYDROGEN CURVE
;***********************

hyd=get_energy_FILD(gyro,bfield,A=1,Z=1)

;***********************
; alpha CURVE
;***********************

alpha=get_energy_FILD(gyro,bfield,A=4,Z=2)

;***********************

IF keyword_set(EXPORT_PS) THEN BEGIN

	set_plot,'ps'
	ff='Energy_vs_gyro_bfield_'+strtrim(bfield,1)+'.ps'
	device,filename=ff,/color

ENDIF

loadct,5
plot,gyro,deut/1.e3,xtitle='Gyroradius (cm)',ytitle='Energy (keV)',charsize=2.,color=0,background=255,/nodata,title='Bfield (T):'+strtrim(bfield,1),$
	xrange=[0.,maxgyro]

oplot,gyro,deut/1.e3,color=50
oplot,gyro,hyd/1.e3,color=100
;oplot,gyro,alpha/1.e3,color=150

xyouts,0.2,0.8,'Deuterium',color=50,/norm
xyouts,0.2,0.7,'Hydrogen',color=100,/norm
;xyouts,0.2,0.6,'Alpha',color=150,/norm



IF keyword_set(EXPORT_PS) THEN BEGIN

	device,/close
	set_plot,'X'
	print,'Figure saved as: ',ff

ENDIF


stop
return
end
