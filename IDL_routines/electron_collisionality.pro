function electron_collisionality,n_e,te,q,R,epsilon,z_eff

;==========================================================================
;                   J. Galdon               03/05/2016
;==========================================================================
; Computes electron collisionality following formula 
; from [Sauter O., Angioni C. and Lin-Liu Y. 1999, Phys. Plasmas 6 2834]
;
; INPUT PARAMETERS
;
; ne --> Electron density in m-3 ; Can be scalar or 1D array
; Te --> Electron temperature in eV ; Can be scalar or 1D array
; q --> safety factor (is it the rho_pol dependent or should we just consider q95?) ; Can be scalar or 1D array
; R --> Major radius in m
; epsilon --> tokamak inverse aspect ratio
; Z-eff --> Z effective
;
;==========================================================================

s_ne=size(n_e,/dimensions)
s_te=size(te,/dimensions)
s_q=size(q,/dimensions)

IF s_ne EQ s_te AND s_te EQ s_q THEN BEGIN

	

ENDIF ELSE BEGIN

	print,'Ne Te and q must have the same dimensions... returning'
	return,-1

ENDELSE


coulomb_log_electron=31.3-alog(sqrt(n_e)/Te)

;print,'Coulomb log. electron=',coulomb_log_electron

nu_electron=6.921e-18*(q*R*n_e*Z_eff*coulomb_log_electron)/((Te^2)*(epsilon^(3/2)))

return,nu_electron
end
