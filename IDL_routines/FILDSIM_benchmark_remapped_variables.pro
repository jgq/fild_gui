@FILDSIM_read_strike_points.pro

pro FILDSIM_benchmark_remapped_variables,reference_strike_points_file,new_strike_points_file,var


;=====================================================================================================
;                   J. Galdon               27/05/2021
;=====================================================================================================
;
; Routine to perform benchmarks on FILDSIM runs. Target is the strike_points file
;
;=====================================================================================================

FILDSIM_read_strike_points,ref,filename=reference_strike_points_file
FILDSIM_read_strike_points,new,filename=new_strike_points_file

; Compare number of points, just for statistics

n_strikes_ref=n_elements(ref.xx)
n_strikes_new=n_elements(new.xx)

print,'Reference # strike points: ',n_strikes_ref
print,'New # strike points: ',n_strikes_new

CASE VAR of

1: BEGIN
ref_var=ref.remap_gyro
new_var=new.remap_gyro
bs_hist=0.1

END

2: BEGIN
ref_var=ref.remap_pitch
new_var=new.remap_pitch
bs_hist=1.

END

ELSE: BEGIN

    print,'Set VAR to:'
    print,'1 --> remapped gyro'
    print,'2 --> remapped pitch'
    return

END

ENDCASE



; Compare Variable remap

window,0

contour,ref_var,ref.yy,ref.zz,/irregular,/fill,nlevels=50,$
    xtitle='X (cm)',ytitle='Y (cm)',charsize=2.,color=0,background=255,position=[0.2,0.2,0.8,0.8],/norm,$
    title=reference_strike_points_file

colorbar,max=max(ref_var),position=[0.95,0.2,0.98,0.85],color=0,format='(f8.3)',/vert

window,2

contour,new_var,new.yy,new.zz,/irregular,/fill,nlevels=50,$
    xtitle='X (cm)',ytitle='Y (cm)',charsize=2.,color=0,background=255,position=[0.2,0.2,0.8,0.8],/norm,$
    title=new_strike_points_file

colorbar,max=max(new_var),position=[0.95,0.2,0.98,0.85],color=0,format='(f8.3)',/vert

; Compare histograms

hist_ref=histogram(ref_var,locations=xx_ref,binsize=bs_hist)
hist_new=histogram(new_var,locations=xx_new,binsize=bs_hist)

window,3

plot,xx_ref,hist_ref,psym=10,color=0,background=255,xtitle='Var (a.u.)',ytitle='Counts',charsize=2.
oplot,xx_new,hist_new,psym=10,color=50

stop
return
end
