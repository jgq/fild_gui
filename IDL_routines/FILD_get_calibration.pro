function   FILD_get_calibration,PULSE,CALIB_FILE=CALIB_FILE,CAMERA=CAMERA,CALIBRATION=CALIBRATION,FILD_ID=FILD_ID

;=====================================================================================================
;                   J. Galdon               29/09/2014
;=====================================================================================================
;
; This function read the calibration parameters from a .txt database.
; INPUTS:
;       -PULSE: NUMBER OF THE PULSE.
;       -CAMERA:IS 0 ('CCD') OR 1 ('PHANTOM').
;       -CALIBRATION: IS 0 ('IMAGE') OR 1 ('PIXELS'). 
;=====================================================================================================

pulse=LONG(pulse)
camera=LONG(camera)
calibration=LONG(calibration)
fild_id=INT(fild_id)
file=calib_file

CASE camera of
0: cam_type='CCD'
1: cam_type='PHANTOM'
else: BEGIN
          print,'ERROR: Select camera type: 0--> CCD ; 1--> PHANTOM'
          return,0
      ENDELSE
ENDCASE

CASE calibration of 
0: calibration='IMAGE'
1: calibration='PIX'
else: BEGIN
         print,'ERROR: Select calibration type: 0--> IMAGE ; 1--> PIXEL'
         return,0
      ENDELSE
ENDCASE

CASE fild_id of 
1: fild_id=1
2: fild_id=2
3: fild_id=3
4: fild_id=4
5: fild_id=5
else: BEGIN
         print,'ERROR: Select FILD : 1--> FILD1 ; 2--> FILD2 ; 3--> FILD3 ; 4 --> FILD4 ; 5 --> FILD5'
         return,0
      ENDELSE
ENDCASE


data=FILD_read_calibration_database(file)

cal_id=where(data.pulse_i LE pulse AND data.pulse_f GE pulse AND data.cam_type EQ cam_type AND data.cal_type EQ calibration AND data.fild_id EQ fild_id,count)
cal_id=cal_id(0)

IF count LT 1 THEN BEGIN
 print,'There is no STRIKE-MAP calibration for your requirements!!'
 print,'To calibrate use FILD_overlay_image.pro or FILD_overlay_pixel.pro'
return,0
ENDIF


cal_params={xshift:data.xshift(cal_id),yshift:data.yshift(cal_id),$
            xscale:data.xscale(cal_id),yscale:data.yscale(cal_id),deg:data.deg(cal_id)}

return,cal_params
end
