function get_VRT_FILD,shot,NOPLOT=NOPLOT



;=====================================================================================================
;                   J. Galdon               18/06/2021
;=====================================================================================================
;
; Routine to retrieve VRT settings for FILD ROIs
;
;=====================================================================================================


loadct,5


print,'********************************************************************************************************'
print,'DISCLAIMER!!!'
print,'The routine is just using dummy signal names now, for testing. Need to check the right names with VRT team'
print,'You can continue, but dont trust the outputs!'
print,'********************************************************************************************************'

stop


;*********************
; DEFINE SIGNAL NAMES
;*********************

diagname='VRT'

fild1='Cr-06ME'

fild2='Ed-012H'

fild4='Ed-01Ho'

fild5='Ed-07Ho'

fild5_2='Ed-07Sp'

FILD_id=[1,2,4,5,5]

FILD_signals=[fild1,fild2,fild4,fild5,fild5_2]
VRT_signal_names=strtrim(FILD_signals,1)+'i'
VRT_threshold_names=strtrim(FILD_signals,1)+'v'
VRT_VPE=fltarr(n_elements(FILD_signals))

;******************
; READ SIGNALS
;******************

FOR kk=0,n_elements(FILD_signals)-1 DO BEGIN

    read_signal,ier,shot,diagname,VRT_signal_names(kk),t,ss,pp
    read_signal,ier,shot,diagname,VRT_threshold_names(kk),t,tt,pp

    IF kk EQ 0 THEN BEGIN

        VRT_signal=fltarr(n_elements(FILD_signals),n_elements(t))
        VRT_threshold=fltarr(n_elements(FILD_signals),n_elements(t))
        Time=t

        stop
    ENDIF

    VRT_signal(kk,*)=ss
    VRT_threshold(kk,*)=tt

    ; Check if threshold was surpassed
    
    IF max(ss) GT max(tt) THEN VRT_VPE(kk)=1.

ENDFOR

;*******************
; PLOT
;*******************

IF KEYWORD_SET(NOPLOT) THEN GOTO, NOP

window,0

!P.Multi=[0,1,n_elements(FILD_signals)]

    FOR kk=0,n_elements(FILD_signals)-1 DO BEGIN

        plot,time,VRT_signal(kk,*),color=0,background=255,xtitle='Time (s)',ytitle=VRT_signal_names(kk),charsize=2.
        oplot,time,VRT_threshold(kk,*),color=50

    ENDFOR


!P.Multi=0


NOP:

; Define Output

output={FILD_ID:FILD_ID,FILD_signals:FILD_signals,VRT_signal_names:VRT_signal_names,VRT_threshold_names:VRT_threshold_names,$
    VRT_signal:VRT_signal,VRT_threshold:VRT_threshold,time:time,$
    VRT_VPE:VRT_VPE}

return,output
end
