;****************************************************************************************
function derivsgc,t,y
;****************************************************************************************

;gets drift + parallel motion velocities for gc integrator
;
common vars1,vrdrift,vzdrift,vtdrift,gg

vrd=interpolatexy(gg.r,gg.z,vrdrift,y(0,*),y(2,*))
vtd=interpolatexy(gg.r,gg.z,vtdrift,y(0,*),y(2,*))
vzd=interpolatexy(gg.r,gg.z,vzdrift,y(0,*),y(2,*))

;print, vrd,' ',vtd,' ',vzd
;return,[[vrd],[vtd/y(0)],[vzd]]
return,[vrd,vtd/y(0),vzd]

end

;****************************************************************************************
function getvels,r,z
;****************************************************************************************
;gets drift + parallel motion velocities given vector of r,z
;same thing as derivsgc except can take sep. vectors of r,z
common vars1,vrdrift,vzdrift,vtdrift,gg

vrd=interpolatexy(gg.r,gg.z,vrdrift,r,z)
vtd=interpolatexy(gg.r,gg.z,vtdrift,r,z)
vzd=interpolatexy(gg.r,gg.z,vzdrift,r,z)

;print, vrd,' ',vtd,' ',vzd
;return,[[vrd],[vtd/y(0)],[vzd]]
return,[[vrd],[vtd/r],[vzd]]

end

;****************************************************************************************
function getdrift,r,z,vpar,vperp,gci
;****************************************************************************************

;returns [ur,ut,uz] drift*(z*ee/mm/mi)
;interpolates drifts to r,z for a given vpar vperp

ucurvr=interpolatexy(gci.g.r,gci.g.z,gci.ucurvr*vpar^2,r,z)
ucurvt=interpolatexy(gci.g.r,gci.g.z,gci.ucurvt*vpar^2,r,z)
ucurvz=interpolatexy(gci.g.r,gci.g.z,gci.ucurvz*vpar^2,r,z)

ugradr=interpolatexy(gci.g.r,gci.g.z,gci.ugradr*vperp^2/2.,r,z)
ugradt=interpolatexy(gci.g.r,gci.g.z,gci.ugradt*vperp^2/2.,r,z)
ugradz=interpolatexy(gci.g.r,gci.g.z,gci.ugradz*vperp^2/2.,r,z)

return,[ucurvr+ugradr,ucurvt+ugradt,ucurvz+ugradz]
end

;****************************************************************************************


function orbclassify, pitchcont, rcont0,zcont0,g,printinfo
;*****************************************************************************************************
;Classifying particle
;;ORBIT CLASSIFICATION
;0=SOME ERROR IN CONTOURING  ;1=PASSING axis encircling, 2 = PASSING NOT-axis encircling
; 3=TRAPPED axis encircling (rare), 4=TRAPPED NOT-axis encircling,
;5=CLOSED ORBIT ON EFIT GRID BUT HITS WALL, 6 = OPEN ORBIT ON EFIT GRID

;*****************************************************************************************************
;made into a function so we could easily just call it with the integrated gc results

GROSSCLASS=0
temp=where(pitchcont gt 0.0,ncountg)  ;seeing if sign of pitch flipped
temp=where(pitchcont lt 0.0,ncountl)  ;seeing if sign of pitch flipped
;anywhere on contour, if it did, then went through v||=0 and is trapped
ncount=0
if ncountg ge 1 and ncountl ge 1 then  ncount=1

if ncount gt 0 then begin

if printinfo then print,'TRAPPED ORBIT'
;if (min(rcont0) lt g.rmaxis) and (max(rcont0) gt g.rmaxis) then begin   ;doesnt work for trapped
;GROSSCLASS=3 ;axis encircling
;if printinfo then print,'AXIS ENCIRCLING'
;endif else begin
GROSSCLASS=4
;if printinfo then print,'NOT-AXIS ENCIRCLING'

endif else begin
if printinfo then print,'PASSING ORBIT'
if (min(rcont0) lt g.rmaxis) and (max(rcont0) gt g.rmaxis) then begin
GROSSCLASS=1 ;axis encircling
if printinfo then print,'AXIS ENCIRCLING'
endif else begin
GROSSCLASS=2
if printinfo then print,'NOT-AXIS ENCIRCLING'

endelse
endelse

if max(rcont0) lt max(g.lim(0,*)) then begin  ;FIRST SIMPLE TEST IF ANY POINTS ARE OUTSIDE LIMITER
;test for hitting wall
inout=check_lim_inoutorb(rcont0,zcont0,g=g,usefinewall=0) ;IF PASSES FIRST TEST THEN TEST ALL POINTS ON CONTOUR
;for aug must use finewall=0
temp=where(inout lt 1,nino)

if nino gt 0 then begin
GROSSCLASS=5
if printinfo then  print,'HITS WALL'
endif

endif else begin
GROSSCLASS=5
if printinfo then print,'HITS WALL'
endelse


return,GROSSCLASS
end

;*****************************************************************************************************

;****************************************************************************************
;MAIN  Program
;****************************************************************************************
pro orb_gc_aug,g,rtz,pitch0i,energy,mm=mm,z=z,inputsatgc=inputsatgc,orbinteg=orbinteg,$
reverse_time=reverse_time,calcdrifts=calcdrifts,plott=plott,integrator2use=integrator2use,$
fild_rtz=fild_rtz,gci=gci,printinfo=printinfo,outs=outs,nmin=nmin,nsteps=nsteps,$
checkfull=checkfull,fnsteps=fnsteps,skip_lostint=skip_lostint,signfud=signfud
;****************************************************************************************
;SOME EXAMPLES
;an example to do everything including full orbit check is below
;IDL> orb_gc,g,[2.2,0.,0.],.4,60,nsteps=3.E3,checkfull=1,inputsatgc=0,fnsteps=1.E4

;an example just to plot an orbit from gc inputs and follow gc w/ integration
;IDL> orb_gc,g,[2.2,0.,0.],.4,60

   ;an example just to calc an orbit from gc inputs and NOT follow gc w/ integration and NOT plot or print, anything
   ;outs will have all the information in in
;IDL> orb_gc,g,[2.2,0.,0.],.4,60, orbinteg=0,plott=0,printinfo=0,outs=outs

;***ADDING GCI=GCI  WILL HELP SPEED IT UP SOME IF NOT CHANGING EQUILIBRIUM
;*****************************************************************************************************
;Classifying particle is in outs.grossclass
;Classifying particle
;;ORBIT CLASSIFICATION
;0=SOME ERROR IN CONTOURING  ;1=PASSING axis encircling, 2 = PASSING NOT-axis encircling
; 3=TRAPPED axis encircling (rare), 4=TRAPPED NOT-axis encircling,
;5=CLOSED ORBIT ON EFIT GRID BUT HITS WALL, 6 = OPEN ORBIT ON EFIT GRID


;EXPLANATION OF OUTPUT STRUCTURE
;*****************************************************************************************************

;outs = {error:error, r0:r0, z0:z0, pitch0:ci.pitch0, phi0:phi0, $
;ee:ee,mi:mi,mm:mm,ejou:ejou,v0:v0,eqovm:eqovm,$
;gci:gci, ci:ci, rcont0:rcont0, zcont0:zcont0, mu0:mu0, pphi0:pphi0, velvec0:ci.velvec0, pitchrz:pitchrz, murz:murz, pitchcont:pitchcont, $
;grossclass:grossclass, dfild:dfild, dfild_rz:dfild_rz, pitch_dfild:pitch_dfild, bfild:bfild, circumrcont:circumrcont, $
;inputs:{energy:energy, pitch0i:pitch0i, R0i:r0i, Z0i:z0i, phi0i:phi0, mm:mm, z:z, ejou:ejou, $
;v0:v0, inputsatgc:inputsatgc, reverse_time:reverse_time, $
;orbinteg:orbinteg, calcdrifts:calcdrifts, integrator2use:integrator2use}, $
;;below here will be all -1 if no orbit integration
;drifts:{vparrz:vparrz, vperprz:vperprz, vrdrift:vrdrift, vzdrift:vzdrift, vpdrift:vpdrift, vtdrift:vtdrift}, $
;;these are from orbit integration
;grossclass_i:grossclass_i,tstep:tstep, thets:thets, timearr:timearr, yout:yout, phtot:phtot, timeorb:timeorb, dphitor:dphitor, dpoltime:dpoltime, $
;omegatoroidal:omegatoroidal, omegapoloidal:omegapoloidal, pitchyout:pitchyout, vd_tot:vd_tot, vd_o:vd_o, $
;dfild_i:dfild_i, dfild_rz_i:dfild_rz_i, pitch_dfild_i:pitch_dfild_i, dfild_it:dfild_it, dfild_rz_it:dfild_rz_it, $
;pitch_dfild_it:pitch_dfild_it,$
;fout:fout}

;error = 1 if bad
;r0,z0,pitch0 = initial R, z, and pitch
;phi0=initial toroidal angle
;v0 = inital speed
;gci = structure with magnetic fields and drift arrays
;ci = inputs to com calcs.
;rcont0 = major radius of orbit contour from idl contour
;zcont0= z of orbit contour from idl contour
;pitchcont = pitch along idl contour
;dfild = minim. distance to fild along contour
;bfild = mag. field at fild
;pitch_dfild = pitch at fild min. distance
;mu0= initial mag. moment
;pphi0 = initial toroidal can. ang. mom.
;velvec0=inital velocity vector for full orbit calc only valid if inputsatgc = 0
;pitchrz = pitches on rz grid satisfying toroidal can. ang. momen. cons.
;murz = mag. momen. satisf. toroidal. can ang. momen. cons. but not mu cons. except at = mu0
;grossclass = orbit classification

;IF INTEGRATED ORBIT
;yout = (3,nt) array of R,phi,z positions along integrated orbit
;timearr = time in s.
;omegatoroidal = toroidal transit freq.
;omegapoloidal = poloidal transit freq.
; ;drifts = structure of all drifts
;dpoltime = time to go one poloidal transit in closed orbit
;all dfild***_i = same as before except evaluated using integrated orbit and _it means including toroidal position, i.e. not just r,z




common vars1,vrdrift,vzdrift,vtdrift,gg

;****************************************************************************************
if n_elements(fild_rtz) lt 1 then fild_rtz=[2.165,45.*!pi/180,.3]  ;r,phi (in radians),z of fild

;example lost part = 80.,0.62,2.23,-.695 to fild
if n_elements(energy) lt 1 then energy=double(50.) ;kev
if n_elements(pitch0i) lt 1 then pitch0i=double(.3) ;61955 ;  ;v_||/v   0.7,0.65,.6,.55,.5,.4,.3,.2
if pitch0i eq 1.0 then begin
print, 'USING PTICH = 0.99'
pitch0i=0.99
ENDIF
if pitch0i eq -1.0 then begin
print, 'USING PTICH = -0.99'
pitch0i=-0.99
ENDIF


if n_elements(mm) lt 1 then mm=2.  ;proton masses
if n_elements(z) lt 1 then z=1.  ;charge in proton chg ;can be negative

if n_elements(rtz) lt 3 then begin
R0i=2.0 & Z0i=0.0 & phi0=0.0
phi0=0.
endif else begin
r0i=rtz(0) & z0i=rtz(2) & phi0=rtz(1)
endelse
if n_elements(inputsatgc) lt 1 then inputsatgc=1  ;if input is at guiding center then =1,
;if instantaneous position then =0.  Note, to compare to orb calculations
;this should be set to 0 and then that position can be also used in orb
if n_elements(orbinteg) lt 1 then orbinteg=1  ;to integrate orbit or not
if n_elements(reverse_time) lt 1 then reverse_time=0 ;to reverse time from initial position = 1
if n_elements(calcdrifts) lt 1 then calcdrifts=1   ;will calculate drifts at all positions on integrated orbit
if n_elements(plott) lt 1 then plott=1  ;if gt 0 then will plott orbit
if n_elements(integrator2use) lt 1 then integrator2use='rk4'  ;'ddeabm'  or 'rk4' the integrator for actually following gc.  Only
    	    	    ;used if orbinteg gt 0
if n_elements(g) lt 1 then g=readg('/u/vanzee/TRANSP/142111/TRY1/HRSEFITS/g142111.00525_x257')
if n_elements(printinfo) lt 1 then printinfo=1  ;print, orbit info to terminal
if n_elements(nmin) lt 1 then nmin=20.  ;increase to do more steps in following
    	    	    	                 ;orbit around poloidal transit if integrating = finger stepsize
;gci is from get_gcinputs(g)
if n_elements(nsteps) lt 1 then nsteps=1E3  ;maximum number of timesteps in integration
if n_elements(checkfull) lt 1 then checkfull=0  ;do full orbit calculation ***note, inputsatgc must be 0
if n_elements(fnsteps) lt 1 then fnsteps=1.E4   ;number of steps for full orbit if desired
if n_elements(skip_lostint) lt 1 then skip_lostint=0  ;wont integrate lost orbits or wall hits
if n_elements(signfud) lt 1 then signfud=-1  ;some equil. have flux sign difft. and bfield calc. is wrong
    	    	    	    	    	    	;this should be -1 for d3d sense and i think for aug too
;****************************************************************************************

skipduetolost=0
error=0

if n_elements(gci) lt 1 then begin
gci=get_gcinputs(g,driftterms=1)  ;made separate routine from
;com inputs because equil. may not change for many calcs but
;interesting pitch energy etc. may more often
endif else begin
if g.source ne gci.g.source then begin
print,'DOING GCI CALC EVEN THOUGH GCI WAS SENT BECAUSE DIDNT MATCH G'
gci=get_gcinputs(g,driftterms=1)
endif
endelse

gg=g

ee=double(1.60E-19)
mi=double(1.67E-27)
ejou=energy*ee*1.E3
v0=(ejou/mi/mm*2.)^.5

eqovm=ee*z/mm/mi

;taking position, energ. and pitch and getting inputs for coms at
;gc. trivial if already starting at gc, i.e. just an interpolation of f0,psi0,b0,etc.
ci=get_com_ins(eqovm,ejou,v0,pitch0i,r0i,z0i,gci,inputsatgc=inputsatgc)

z0=ci.z0
r0=ci.r0
if printinfo then begin
print, 'r0,z0,phi0=',r0,',',z0,',',phi0
print,'R_li=',ci.rli0,' m'
print,'E=',energy,' keV'
print,'Pitch=',ci.pitch0
endif

;****************************************************************************************
;Getting original COM
;****************************************************************************************
mu0=ejou/ci.b0*(1.-ci.pitch0^2.)  ;magnetic moment using pitch rel to guiding center??
;mu0=mm*mi*(vperp0)^2/2/b0  ;original magnetic moment at gc  ;should this include the perp dfrift??

;signfud=-1. ;dont yet know for sure about this -1., may cause
;an error for difft current directions or something

;this is the initial toroidal canonical angular momentum
pphi0=ci.psi0-signfud*ci.f0*ci.pitch0/z/ee/ci.b0*(2.*mm*mi*ejou)^.5

pitchrz=-(pphi0-gci.psirz)/(gci.frz/z/ee/gci.btot*(2.*mm*mi*ejou)^.5 )
;this is the pitch at all r,z that have same canonical ang. momentum
;and energy

murz=ejou/gci.btot*(1.-pitchrz^2.)/mu0 ;magnetic moments from pitchrz scaled to mu0
;not all possible though because murz must = mu0

;***********************************************************************************

;Plotting below here
;***********************************************************************************
dev0=!D
if plott eq 0 then begin
set_plot,'null'
endif ;cant just skip plotting commands even if you dont want a plot because contour needs to run


prebin=0
rebinrz=1 ;factor to upsample the efit dimensions for calculating contour - might be useful for 65x65 efits
;to do anything it has to be >1 and an integer
;allowed pitches and contour
jumprebin:   ;will jump back up to here with rebinrz increased and redo contour
;if only a few points on contour
conmurz=murz
conr=g.r
conz=g.z
if rebinrz gt 1 then begin
nrg=n_elements(g.r)*1.
nzg=n_elements(g.z)*1.
conmurz=rebin(conmurz,long(nrg*rebinrz),long(nzg*rebinrz))
conr=rebin(conr,long(nrg*rebinrz))
conz=rebin(conz,long(nzg*rebinrz))
endif

contour,conmurz,conr,conz,levels=1.0,path_xy=paths,path_data_coords=1,$
path_info=pathinf,closed=0

if plott eq 0 then begin
set_plot,dev0.name  ;setting back original !D
endif



;Errors and gotos can happen after this point so defining all struc elements now
rcont0=-1 & zcont0=-1 & pitchcont=-1
 dfild=-1 & dfild_rz=-1 & pitch_dfild=-1 & circumrcont=-1 & pitchsignflip=-1 & currentsignflip=-1 &
bfild=interpolatexy(g.r,g.z,gci.btot,fild_rtz(0),fild_rtz(2)) ;magnetic field at fild
;drifts
vparrz=-1 & vperprz=-1 & vrdrift=-1 & vzdrift=-1 & vpdrift=-1 & vtdrift=-1
tstep=-1 & thets=-1 & timearr=-1 & yout=-1 & phtot=-1 & timeorb=-1 & timearr=-1 & dphitor=-1 &
dpoltime=-1 & omegatoroidal=-1 & omegapoloidal=-1 & pitchyout=-1
vd_tot=-1 & vd_o=-1 & dfild_i=-1 & dfild_rz_i=-1 & $
pitch_dfild_i=-1 & dfild_it=-1 & dfild_rz_it=-1 & pitch_dfild_it=-1 & grossclass_i=0 & fout=-1


;DETERMINING ORBIT INFO FROM CONTOUR OUTPUT
;*****************************************************************************************************
GROSSCLASS=0
if n_elements(paths) eq 0 then begin
;had a problem with contour. could iterate here using rebin above and might help
error=1
goto,jumpath
endif

;THIS PART COULD BE USELESS BUT ITS AN ATTEMPT TO INCREASE THE NUMBER
;OF POINTS CONTOUR SPITS OUT. IT HELPS IF THERE IS ALREADY A GOOD CONTOUR
;BUT MAY NOT IF ONLY A FEW POINTS
nctmin=4  ;minimum number of points on a contour to accept
nct=n_elements(paths(0,*))
if nct lt nctmin then begin
if prebin lt 2 then begin  ;going to increase number of points  in murz so contour
;adds points. will try this up to 2x
prebin=prebin+1
rebinrz=rebinrz+1.  ;doubles size then quadruples size of murz grid
goto,jumprebin
endif
error=1
goto,jumpath
endif


rcont=reform(paths(0,0:nct-1)) ;could have be double valued in mu0..so will look for closest to original position
zcont=reform(paths(1,0:nct-1))
ncont=n_elements(pathinf.level) ;number of contours drawn
ind0=0
pioff=pathinf.offset
pin=pathinf.n
ptyp=pathinf.type

if ncont gt 1 then begin
temp=min((rcont-r0)^2.+(zcont-z0)^2,mind)  ;finding contour that goes through initial gc location
indemp=mind-pioff
ind0=max(where(indemp ge 0))
endif

ii0=pioff(ind0)
ii1=ii0+pin(ind0)-1

rcont0=rcont(ii0:ii1)
zcont0=zcont(ii0:ii1)

temp=min((rcont0-r0)^2.+(zcont0-z0)^2,mind0) ;contour doesnt always start at r0,z0
rcont0=shift(rcont0,-mind0)
zcont0=shift(zcont0,-mind0)
nc0=n_elements(rcont0)
tempr0=rcont0
tempz0=zcont0
for i=0,nc0-1 do begin
rcont0(i)=tempr0(nc0-1-i)
zcont0(i)=tempz0(nc0-1-i)
endfor

nsrc=0
;rcont0=smooth(rcont0,nsrc,/edge_truncate)   ;These are the r,z coordinates of the contour evaluated gc orbit
;zcont0=smooth(zcont0,nsrc,/edge_truncate)


pitchcont=interpolatexy(g.r,g.z,pitchrz,rcont0,zcont0)

temps0=sign(pitchcont(0))
temps1=sign(pitch0i)
if temps0 eq temps1 then pitchsignflip=1
if temps0 ne temps1 then pitchsignflip=-1
if temps1 eq 0 then pitchsignflip=1

 currentSign = -1.0 * g.cpasma / abs(g.cpasma)

 ;pitchrz *= pitchsignflip * currentSign
 ;pitchrz *= pitchsignflip


;CLASSIFYING ORBIT NOW

if ptyp(ind0) gt 0 then begin   ;checking whether its closed or not using pathinf.type
grossclass=orbclassify(pitchcont,rcont0,zcont0,g,printinfo)
endif else begin
if printinfo then print,'OPEN ORBIT'
GROSSCLASS=6
endelse

;****************now know whether passing trapped or lost


circumrcont=total( ((rcont0(1:*)-rcont0(0:*))^2+(zcont0(1:*)-zcont0(0:*))^2)^.5 )
;rough circumference of r,z contour

;***********************************************************************************
;NOW FINDING CLOSEST POINT OF APPROACH TO FILD  only in R,z using contour
dfild=min(((fild_rtz(0)-rcont0)^2+(fild_rtz(2)-zcont0)^2)^.5,fin0)  ;closest in R,z it gets to fild
dfild_rz=[rcont0(fin0),zcont0(fin0)] ;R,z at that position
pitch_dfild=interpolatexy(g.r,g.z,pitchrz,rcont0(fin0),zcont0(fin0))  ;pitch at position of closest approach


orbinteg0=orbinteg

if (skip_lostint gt 0) and (grossclass ge 5) then begin
skipduetolost=1
orbinteg=0
endif

;NOW WILL ITNEGRATE GC ORBIT AND GET TRANSIT TIMES IF REQUESTED
if orbinteg gt 0 then begin
;***********************************************************************************
;finding a point inside of enclosed orbit at average z location
;this defines the position that is used to evaluate if orbit has gone
;around 2pi in integrator
z_orb=avg(zcont0)  ;finding approx. center of midplane banana
zlt1=where(abs(zcont0) lt 0.015*max(abs(zcont0)),nlt1)  ;could do something else looking at avg. z heigh
if nlt1 lt 2 then zlt1=where(abs(zcont0) lt 0.1*max(abs(zcont0)),nlt1) ;pretty stupid way to find the avg. r of banana midplane
if nlt1 lt 2 then zlt1=where(abs(zcont0) lt 0.5*max(abs(zcont0)),nlt1)
if nlt1 lt 2 then zlt1=indgen(n_elements(rcont0))
r_orb=avg(rcont0(zlt1))

;going to follow orbit for a full poloidal transit
;doing orbit gc integration
;***********************************************************************************

;***********************************************************************************

;NOW CALCULATING ALL VELOCITY COMPONENTS - ONLY NECESSARY IF REQUESTING
;TRANSIT TIMES / ORBITAL FREQUENCIES
;;***********************************************************************************

omegatoroidal=0.0 ;will be toroidal passing+precession if passing or just prece. if trapped
omegapoloidal=0.0

vparrz=(ejou/mi/mm*2.)^.5*pitchrz
vperprz=vparrz*0.
wgtv=where(vparrz^2. lt ejou/mi/mm*2.)
vperprz(wgtv)=(ejou/mi/mm*2.-vparrz(wgtv)^2.)^.5
;vperprz=(ejou/mi/mm*2.-vparrz^2.)^.5  ;was causing error because could be sqrt(-)

vpartrz=vparrz*gci.bt/gci.btot
vparrrz=vparrz*gci.br/gci.btot
vparzrz=vparrz*gci.bz/gci.btot

ucurvr=gci.ucurvr*mm*mi*vparrz^2/z/ee
ucurvt=gci.ucurvt*mm*mi*vparrz^2/z/ee
ucurvz=gci.ucurvz*mm*mi*vparrz^2/z/ee

ugradr=gci.ugradr*mm*mi*vperprz^2/2/z/ee
ugradt=gci.ugradt*mm*mi*vperprz^2/2/z/ee
ugradz=gci.ugradz*mm*mi*vperprz^2/2/z/ee

vrdrift=(ucurvr+ugradr)+vparrrz
vzdrift=(ucurvz+ugradz)+vparzrz
vpdrift=(vrdrift^2+vzdrift^2)^.5
vtdrift=(ucurvt+ugradt)+vpartrz

;estimating transit times based on contour only ...could be an option if other one takes too long
;boils down to what is in that thesis
;vcont=getvels(rcont0,zcont0)
;vcontp=(vcont(*,0)^2+vcont(*,2)^2.)^.5
;dtpolcont= circumrcont/avg(vcontp)  ;
;omegapolcont=2.*!pi/dtpolcont


y=dblarr(3) ;
y=[r0,phi0,z0]

;t0s=systime(1)

i=0L

yout=y ;dblarr(3,nsteps)

der0=derivsgc(0,y)

if grossclass eq 0 then begin  ;trying to get timestep appropriate to transit velocity
dtmin=2.*!pi*r0/abs(der0(2))/nmin ;20./ci.q0
endif else begin
dtmin=circumrcont/(der0(0)^2+der0(1)^2)^.5/nmin
endelse
h=min([dtmin,70./ci.wci0])
if reverse_time gt 0 then h=-h
;h=40./ci.wci0 ;timestep

nfeval=0.
thets=atan(y(0)-r_orb,y(2)-z_orb)
timearr=0D
tstep=h
on_ioerror,bad_num
while i lt nsteps-1 do begin   ;Beginning main integration

dydx=derivsgc(0,y)
  i=long(i*1. + 1L)
if integrator2use eq 'rk4' then begin
  y=rk4(y,dydx,0.,h,'derivsgc',/double)
  tempnf=1.
endif else begin
ddeabm,'derivsgc',0.,y,h,nfev=tempnf,epsabs=epsabs,epsrel=epsrel; ,tgrid=tgrid,ygrid=ygrid,/intermediate
endelse


 nfeval=nfeval+tempnf

  ;*****COULD IMPROVE ALL THIS BY KEEPING ALL EVALUATIONS FROM DDEABM
  thets=[thets,atan(y(0)-r_orb,y(2)-z_orb)]
  yout=[[yout],[y]]

 timearr=[timearr,i]
  if i gt 2 then begin
 circsofar= total(((yout(0,1:*)-yout(0,*))^2+(yout(2,1:*)-yout(2,*))^2)^.5)

 ;checks to make sure you went around at least one
if (circsofar gt 0.8*circumrcont) then begin
phtot=phunwrap(thets)  ;thets can have 2pi flips so this unwraps it
 npht=n_elements(phtot)
 dphtot=phtot(npht-1)-phtot(0)

if (abs(dphtot) gt 2.*!pi) then goto,jumpi
if circsofar gt 2.*circumrcont then begin
bad_num:
if printinfo then print,'LIKELY ERROR'
error=1
goto,jumpi
endif

endif

endif

endwhile
jumpi:


temp=where(finite(yout) eq 0,nyfinit)
if nyfinit gt 0 then begin
error=1
if printinfo then print,'ERROR in integration'
goto,jumpath
endif

pitchyout=reform(interpolatexy(g.r,g.z,pitchrz,yout(0,*),yout(2,*)))

;t1s=systime(1)
;print,'t1s-t0s',t1s-t0s
;***********************************************************************************

;DONE WITH ORBIT INTEGRATION NOW GETTING TRANSIT FREQUENCIES
timeorb=i*h ;seconds
timearr=timearr*h ;seconds
if n_elements(timearr) gt nsteps-3 then begin
error=1
if printinfo then print,'ERROR hit max number of steps in integration'
goto,jumpath
endif

;print,'saved steps:',n_elements(timearr)
;print,'actual steps:',nfeval
if printinfo then print,integrator2use
phtot0=phunwrap(thets)
phtot0=abs(phtot0-phtot0(0))
dphitor=interpol(reform(yout(1,*)-yout(1,0)),phtot0,2.*!pi) ;interpolating phi vs theta data and finding exact change in toroidal
;angle after 1 poloidal orbit transit
dpoltime=interpol(timearr,phtot0,2.*!pi)  ;poloidal dt, interpolating to 2pi using poloidal angle vs. time to
;get exact time at 1 poloidal transit

omegatoroidal = dphitor/dpoltime  ;angular toroidal velocity avg. over pol. transit

vdth=avg(deriv(phunwrap(thets)))  ;vdth will have the sign of dtheta around poloidal orbit
if vdth eq 0 then begin
vdth=1.
endif else begin
vdth=vdth/abs(vdth)
endelse
omegapoloidal = -2.*!pi/dpoltime*vdth   ;angular poloidal transit  veloc.
;****added negative sign on 10/28/10 because it wasnt rotating in correct
;direction for counter clockwise theta increasing

;to find out exactly how far toroidally it went in 1 poloidal transit

;orbm_adams,g,[r0i,0,z0i],vel2ui,rv0,e0=energy,nsteps=30.E3,time_reverse=0,$
;epsabs=1.E-8,epsrel=1.E-8

;endif

;CLASSIFYING ORBIT NOW ON INTEGRATED PATH
grossclass_i=orbclassify(pitchyout,reform(yout(0,*)),reform(yout(2,*)),g,printinfo)
;****************now know whether passing trapped or lost



vd_tot=0.
vd_o=0.
if calcdrifts gt 0 then begin
vd_tot=derivsgc(0,yout)
vd_o=getdrift(yout(0,*),yout(2,*),vparrz,vperprz,gci)*mm*mi/z/ee
endif


if printinfo then print, 'TOROIDAL ANGULAR ROTAT. FREQ IS:',OMEGATOROIDAL*1.E-3,' rad*kHz'
if printinfo then print, 'POLOIDAL ANGULAR ROTAT. FREQ IS:',OMEGAPOLOIDAL*1.E-3,' rad*kHz'


;NOW FINDING CLOSEST POINT OF APPROACH TO FILD using RZ alone from integrated orbit
dfild_i=min(((fild_rtz(0)-yout(0,*))^2+(fild_rtz(2)-yout(2,*))^2)^.5,fin0)  ;closest in R,z it gets to fild
dfild_rz_i=[yout(0,fin0),yout(2,fin0)] ;R,z at that position
pitch_dfild_i=interpolatexy(g.r,g.z,pitchrz,yout(0,fin0),yout(2,fin0))  ;pitch at position of closest approach

;NOW FINDING CLOSEST POINT OF APPROACH TO FILD using RZ,phi from integrated orbit
dfild_it=min(((fild_rtz(0)*cos(fild_rtz(1))-yout(0,*)*cos(yout(1,*)))^2+$
(fild_rtz(0)*sin(fild_rtz(1))-yout(0,*)*sin(yout(1,*)))^2+$
(fild_rtz(2)-yout(2,*))^2)^.5,fin0)  ;closest in R,phi,z it gets to fild
dfild_rz_it=[yout(0,fin0),yout(1,fin0),yout(2,fin0)] ;R,phi,z at that position
pitch_dfild_it=interpolatexy(g.r,g.z,pitchrz,yout(0,fin0),yout(2,fin0))  ;pitch at position of closest approach

ENDIF ;END OF ORBIT INTEGRATION AND ORBITAL TIME CALC
;***********************************************************************************

;Will do full orbit if desired
if checkfull gt 0 then begin
if inputsatgc ne 0 then begin
print,'IF YOU WANT FULL ORBIT THEN SET INPUTSATGC=0 ALSO'
goto,jumpfullo
endif
print,'DOING FULL ORBIT'

orbm_adams,g,[ci.r0i,phi0,ci.z0i],ci.velvec0,fout,e0=energy,amu=mm,z=z,nsteps=fnsteps,time_reverse=reverse_time,$
epsabs=1.E-8,epsrel=1.E-8

endif
jumpfullo:




;***********************************************************************************
;JUST PLOTTING OUTPUT BELOW HERE IF REQUESTED
;***********************************************************************************
jumpath:

if plott gt 0 then begin
!p.multi=0

if (orbinteg gt 0) and (plott gt 0) then !p.multi=[0,2,1]

cz=1.5
;yr=[-1.3,1.3]
yr=[min(g.z),max(g.z)]
xr=[min(g.r),max(g.r)] ;0 ;[.8,2.4]
cc=254

plot,g.lim(0,*),g.lim(1,*),/iso,xrange=xr,yrange=yr,xstyle=1,ystyle=1,xtitle='R (m)',ytitle='z (m)',charsize=cz,/nodata
drlim=((g.lim(0,*)-shift(g.lim(0,*),1))^2+(g.lim(1,*)-shift(g.lim(1,*),1))^2)^.5
wglim=where(drlim gt 0.5,ncc)

if ncc gt 2 then begin   ;asdex limiter is defined in noncontiguous pieces so plotting each individually
for i=1,ncc-1 do begin

oplot,g.lim(0,wglim(i-1):wglim(i)-1),g.lim(1,wglim(i-1):wglim(i)-1)

endfor
endif

;
;
; rbdry=where(g.bdry(0,*) gt 1.)
; oplot,g.bdry(0,rbdry),g.bdry(1,rbdry),thick=2,color=100  ;this defines the surface


if checkfull gt 0 and n_elements(fout) gt 1 then begin
oplot,fout(3,*),fout(5,*),thick=.5

endif


;where it would reflect if trapped, i.e. v|| = 0
plots,r0,z0,psym=4,thick=2,color=100
plots,g.rmaxis,g.zmaxis,psym=4,thick=3,symsize=1,color=45


dzf=0.05
drf=dzf
oplot,fild_rtz(0)+[-drf,drf,drf,-drf,-drf],fild_rtz(2)+[-dzf,-dzf,dzf,dzf,-dzf],color=100,thick=2

if n_elements(rcont0) gt 1 then begin
oplot,rcont0,zcont0,psym=4,symsize=.5
endif

plots,r0,z0,psym=4,thick=2,color=100

mxnn=n_elements(yout(0,*))-1
if mxnn gt 0 then begin
plots,yout(0,mxnn),yout(2,mxnn),psym=5,thick=2,color=180
endif

if orbinteg gt 0 and error eq 0 then begin

if n_elements(g.lim(0,*)) gt 1 then begin
limmx=max(g.lim(0,*))
endif else begin
limmx=2.5
endelse

oplot,yout(0,*),yout(2,*),color=45
plots,r_orb,z_orb,psym=5,thick=2,color=150

;plot,yout(0,*)*sin(yout(1,*)),yout(0,*)*cos(yout(1,*)),thick=.5,psym=0,xtitle='x(m)',ytitle='y(m)',charsize=cz,/iso,$
;xrange=[-limmx,limmx],yrange=[-limmx,limmx],ystyle=1,xstyle=1,symsize=.25

plot,yout(0,*)*cos(yout(1,*)),yout(0,*)*sin(yout(1,*)),thick=.5,psym=0,xtitle='x(m)',ytitle='y(m)',charsize=cz,/iso,$
xrange=[-limmx,limmx],yrange=[-limmx,limmx],ystyle=1,xstyle=1,symsize=.25



if checkfull gt 0 and n_elements(fout) gt 1 then begin
oplot,fout(3,*)*sin(fout(4,*)),fout(3,*)*cos(fout(4,*)),thick=.5
endif

oplot,yout(0,*)*cos(yout(1,*)),yout(0,*)*sin(yout(1,*)),thick=.5,psym=-4
;plots,r0*sin(phi0),r0*cos(phi0),color=100,psym=4,thick=2
plots,r0*cos(phi0),r0*sin(phi0),color=100,psym=4,thick=2


plots,yout(0,mxnn)*cos(yout(1,mxnn)),yout(0,mxnn)*sin(yout(1,mxnn)),psym=5,thick=2,color=180


endif



endif
;***********************************************************************************

if skipduetolost gt 0 then orbinteg=orbinteg0

;jumpath:
;**********************this is a kluge to make all pitch signs correct again
pitchrz=pitchrz*pitchsignflip
pitchcont=pitchcont*pitchsignflip
pitch_dfild=pitch_dfild*pitchsignflip
pitch_dfild_i=pitch_dfild_i*pitchsignflip
pitchyout=pitchyout*pitchsignflip
;*****************


outs = {error:error, r0:r0, z0:z0, pitch0:ci.pitch0, phi0:phi0, pitchsignflip:pitchsignflip, currentsignflip:currentsignflip, $
ee:ee,mi:mi,mm:mm,ejou:ejou,v0:v0,eqovm:eqovm,$
gci:gci, ci:ci, rcont0:rcont0, zcont0:zcont0, mu0:mu0, pphi0:pphi0, velvec0:ci.velvec0, pitchrz:pitchrz, murz:murz, pitchcont:pitchcont, $
grossclass:grossclass, dfild:dfild, dfild_rz:dfild_rz, pitch_dfild:pitch_dfild, bfild:bfild, circumrcont:circumrcont, $
inputs:{energy:energy, pitch0i:pitch0i, R0i:r0i, Z0i:z0i, phi0i:phi0, mm:mm, z:z, ejou:ejou, $
v0:v0, inputsatgc:inputsatgc, reverse_time:reverse_time, $
orbinteg:orbinteg, calcdrifts:calcdrifts, integrator2use:integrator2use}, $
;below here will be all -1 if no orbit integration
drifts:{vparrz:vparrz, vperprz:vperprz, vrdrift:vrdrift, vzdrift:vzdrift, vpdrift:vpdrift, vtdrift:vtdrift}, $
;these are from orbit integration
grossclass_i:grossclass_i,tstep:tstep, thets:thets, timearr:timearr, yout:yout, phtot:phtot, timeorb:timeorb, dphitor:dphitor, dpoltime:dpoltime, $
omegatoroidal:omegatoroidal, omegapoloidal:omegapoloidal, pitchyout:pitchyout, vd_tot:vd_tot, vd_o:vd_o, $
dfild_i:dfild_i, dfild_rz_i:dfild_rz_i, pitch_dfild_i:pitch_dfild_i, dfild_it:dfild_it, dfild_rz_it:dfild_rz_it, $
pitch_dfild_it:pitch_dfild_it,$
fout:fout}

;
end
