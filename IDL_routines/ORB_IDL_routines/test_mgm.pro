PRO test_mgm

;gfil = '/u/mgmunoz/idl/ORB/ORB_GC/'

;g=readg(gfil)

;g=readg(28061,3.2)

;g=readg(26823,2.4)

g = readg(28061,1.615)

;restore,'g.idl'   ;This is a file for MAST

;help,g,/str

fild_rpz=[2.17,1.32,0.34]

pit = .9

energy = 1e6

nmin=20

ngrr = 20

Rrange=[1.6,2.2] ; R range 
zrange=[0.,0.9] ; z range

nr = 20

nz = 20

R=findgen(nr)/(nr-1.)*(max(Rrange)-min(Rrange))+min(Rrange) ;
z=findgen(nz)/(nr-1.)*(max(zrange)-min(zrange))+min(zrange) ;

rgc = findgen(25)
zgc = findgen(25)
phgc = findgen(25)

checkfull=0


for jj=0,nr-1 do begin
for ii=0,nz-1 do begin

Rinit=R(ii)

zinit=z(jj)

;orb_gc_aug,g,Rinit,0.,zinit,pit,energy,plot=1,orbinteg=1,outs=o, gci=gci,nsteps=nmin/20.*1.E3 ; 


;orb_gc_aug,g,Rinit,0.,zinit,.4,60, orbinteg=0,plott=1,printinfo=0,outs=o

orb_gc_aug,g,[Rinit,0.,zinit],0.9,90, INPUTSATGC=0, mm=1,checkfull=checkfull,orbinteg=0,plott=1,printinfo=0,outs=o

;help,o,/str

;rgc=o.yout(0,*) ;rcont0
rgc = o.rcont0(*)
zgc = o.zcont0(*)


;!p.multi=[0,2,1]

plot,g.lim(0,*),g.lim(1,*),/iso,xrange=xr,yrange=yr,xstyle=1,ystyle=1,xtitle='R (m)',ytitle='z (m)',charsize=cz,/nodata
drlim=((g.lim(0,*)-shift(g.lim(0,*),1))^2+(g.lim(1,*)-shift(g.lim(1,*),1))^2)^.5
wglim=where(drlim gt 0.5,ncc)

if ncc gt 2 then begin   ;asdex limiter is defined in noncontiguous pieces so plotting each individually
for i=1,ncc-1 do begin

oplot,g.lim(0,wglim(i-1):wglim(i)-1),g.lim(1,wglim(i-1):wglim(i)-1)

endfor
endif

rbdry=where(g.bdry(0,*) gt 1.)

oplot,g.bdry(0,rbdry),g.bdry(1,rbdry),thick=2,color=100  ;this defines the surface








;plot,g.lim(0,*),g.lim(1,*),/iso
;oplot,g.bdry(0,*),g.bdry(1,*)

oplot,rgc,zgc,color=200

;ths=indgen(100)/99.*2.*!pi
;plot,2.5*sin(ths),2.5*cos(ths),/iso
;oplot,xgc,ygc,color=200

if checkfull gt 0 and n_elements(fout) gt 1 then begin
plot,fout(3,*),fout(5,*),thick=.5

endif

wk=get_kbrd(1)


endfor
endfor

END
