

nparts=200000L

;function read_transp_birth, fil,plott=plott,n2use=n2use
;Will read in transp birth profiles
fil='/u/vanzee/TRANSP/OUTPUTFILES/142111M30/142111M30_birth_7.dat'
g=readg('/u/vanzee/TRANSP/142111/TRY1/HRSEFITS/g142111.00725_x257')

pts=read_transp_birth(fil,n2use=nparts)
orbinteg=0


orbcarr=intarr(nparts)    ;will have orbit classification
orbcarr_i=intarr(nparts)  ;will have orbit classification based on integrated orbit= less reliable because
    	    	    	    	    ;it croaks on unconfined orbits
tangarr=fltarr(nparts)  ;will have the toroidal transit frequency (rad/s)
pangarr=tangarr     	    ;will have the poloidal transit frequency (rad/s)
filddistarr=tangarr 	    ;will have the closest dist. to fild from contour alone
fildpitcharr=tangarr	    ;will have pitch at point of closest approach to fild
errorarr=pangarr    	;will have either 0=good 1=error
phi0arr=tangarr     	;original toroidal canonical angular momentum
mu0arr=tangarr	    ;original magnetic moment
maxrho=tangarr  ;will have maximum rho attained
minrho=maxrho


t0=systime(1)
pp=0
for kr=0L,nparts-1 do begin

if pp eq 1000 then begin
print,kr,'nparts',nparts
pp=0
endif

;print,kr
;orb_gc_mv,g,[radarr(kr),phi0,z0],pitcharr(jj),energyarr(ii),plot=0,$
;gci=gci,outs=outs,printinfo=0,orbinteg=orbinteg,nmin=10,skip_lostint=1
;took 114 s w plotting vs 109 with no plotting=negligble overhead
rpz=reform([pts(0,kr)/100.,pts(4,kr)*!pi/180.,pts(1,kr)/100.])

orb_gc_mv,g,rpz,pts(2,kr),pts(3,kr)*1.E-3,plot=0,$
gci=gci,outs=outs,printinfo=0,orbinteg=orbinteg,nmin=10,skip_lostint=1

rho0=calculate_rhog(outs.rcont0,outs.zcont0,g)

maxrho(kr)=max(rho0)
minrho(kr)=min(rho0)

orbcarr(kr)=outs.grossclass 
errorarr(kr)=outs.error
filddistarr(kr)=outs.dfild
fildpitcharr(kr)=outs.pitch_dfild
phi0arr(kr)=outs.pphi0
mu0arr(kr)=outs.mu0

if orbinteg gt 0 then begin
tangarr(kr)=outs.omegatoroidal ;fltarr(ner,npit)
pangarr(kr)=outs.omegapoloidal ;=tangarr
filddistarr(kr)=outs.dfild_i
fildpitcharr(kr)=outs.pitch_dfild_i
orbcarr_i(kr)=outs.grossclass_i
endif

pp=pp+1
endfor
t1=systime(1)
print,'IT TOOK: ',t1-t0,' s'


;
end
