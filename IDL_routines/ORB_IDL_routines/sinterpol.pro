function sinterpol,v,x,u,sortt=sortt,_extra=_extra
if n_elements(sortt) lt 1 then sortt=0

if sortt then begin
ind=sort(X)
endif else begin
ind=lindgen(n_elements(x))
endelse


return,interpol(v[ind],x[ind],u,_extra=_extra)
end
