
nnrad=5
ner=80
npit=80
;radarr=findgen(nnrad)/(nnrad-1.)*(2.25-1.8)+1.8
radarr=findgen(nnrad)/(nnrad-1.)*(2.25-2.05)+2.05

phi0=0.0
z0=0.0
orbinteg=1

energyarr=findgen(ner)/(ner-1.)*70.+10.
pitcharr=findgen(npit)/(npit-1.)*1.85-1.85/2.
orbcarr=intarr(nnrad,ner,npit)
orbcarr_i=intarr(nnrad,ner,npit)

tangarr=fltarr(nnrad,ner,npit)
pangarr=tangarr
filddistarr=tangarr
fildpitcharr=tangarr
errorarr=pangarr
phi0arr=tangarr
mu0arr=tangarr

g=readg('/u/vanzee/efits/141076/HRS/g141076.01835_x257')

t0=systime(1)
for kr=0,nnrad-1 do begin
print, kr,' ',nnrad-1
for ii=0,ner-1 do begin
for jj=0,npit-1 do begin

orb_gc,g,[radarr(kr),phi0,z0],pitcharr(jj),energyarr(ii),plot=0,$
gci=gci,outs=outs,printinfo=0,orbinteg=orbinteg,nmin=10
;took 114 s w plotting vs 109 with no plotting=negligble overhead

orbcarr(kr,ii,jj)=outs.grossclass 
errorarr(kr,ii,jj)=outs.error
filddistarr(kr,ii,jj)=outs.dfild
fildpitcharr(kr,ii,jj)=outs.pitch_dfild
phi0arr(kr,ii,jj)=outs.pphi0
mu0arr(kr,ii,jj)=outs.mu0

if orbinteg gt 0 then begin
tangarr(kr,ii,jj)=outs.omegatoroidal ;fltarr(ner,npit)
pangarr(kr,ii,jj)=outs.omegapoloidal ;=tangarr
filddistarr(kr,ii,jj)=outs.dfild_i
fildpitcharr(kr,ii,jj)=outs.pitch_dfild_i
orbcarr_i(kr,ii,jj)=outs.grossclass_i
endif


endfor
endfor
endfor
t1=systime(1)
print,'IT TOOK: ',t1-t0,' s'


;
end
