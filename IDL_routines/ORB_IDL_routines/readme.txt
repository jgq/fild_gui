DIIID IDL  code to follow some particle trajectories (guiding center and full orbit)

The COM based code was written by M. A. Van Zeeland

M. Garcia-Munoz running it in for AUG.

To run  the code for a given  equilibrium  and time point you have to  produce a g-file and read it.

1) g = readg(28061,3.2)

Once you have read the g-file corresponding to  the equilibrium  that you are interested in  you can proceed with  the orbit following code

2) orb_gc_aug,g,[2.2,0.,0.],.4,60,nsteps=3.E3,checkfull=1,inputsatgc=0,fnsteps=1.E4
