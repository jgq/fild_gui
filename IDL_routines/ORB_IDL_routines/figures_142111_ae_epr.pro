
;;ORBIT CLASSIFICATION
;0=SOME ERROR IN CONTOURING  ;1=PASSING axis encircling, 2 = PASSING NOT-axis encircling
; 3=TRAPPED axis encircling (rare), 4=TRAPPED NOT-axis encircling, 
;5=CLOSED ORBIT ON EFIT GRID BUT HITS WALL, 6 = OPEN ORBIT ON EFIT GRID

restore,'epr_scan_t525_142111.sav'
loadct,5
!p.multi=[0,2,3]
loadct,5
xt='Energy (kev)'
yt='Pitch (V||/V)'
cz=2.5
czs=1.0 ;cz
sz=.25
dfld=.05
xrr=[0,80]
yr=[-1.,1.]
mm=2.
z=1.
ee=double(1.60E-19)
mi=double(1.67E-27)
ejou=energyarr*ee*1.E3
v0=(ejou/mi/mm*2.)^.5
valf=va_calc(2.5,g.bcentr*g.rzero/g.rmaxis,mm)
vpararr=orbcarr*0.
v0arr=vpararr
for j=0,nnrad-1 do begin
for i=0,ner-1 do begin
v0arr(j,i,*)=v0(i)
vpararr(j,i,*)=v0(i)*pitcharr
endfor
endfor


bfss=10
modef=60. ;7.1 ;15.; 21. ;15.0
moden=1.
modem=4.
;pvals=[-2,-1,0,1,2] ;indgen(4)-2 ;reverse(modef/(1.+findgen(3))) ;findgen(10)*15.
;pvals=indgen(4)-5
pvals=indgen(40)-20 ;reverse(modef/(1.+findgen(3))) ;findgen(10)*15.

clabs='p='+stringc(fix(pvals)) ;reverse(['p=1','p=2','p=3'])


;**********************************************************************************************************
set_plot,'ps'
device,filename='figure_epr_142111_t525.eps',/color,xsize=21,$
ysize=24,xoffset=.5,yoffset=1$
,font_size=bfss,bits_per_pixel=6,bold=1,scale=1
!x.thick=5
!y.thick=5
tt=4



for i=0,4 do begin
loadct,5
sig=reform(orbcarr(i,*,*))
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

xyouts,30,.975,'R='+stringc(radarr(i)),charsize=czs,color=0,font=0
wl1=where(abs(filddistarr(i,*,*)) le dfld,nwl1)
wn1=where(abs(filddistarr(i,*,*)) gt dfld)
if nwl1 gt 0 then x=array_indices(reform(filddistarr(0,*,*)),wl1)
temp=where(errorarr(i,*,*) gt 0,nce)
if nce gt 0 then ai=array_indices(reform(errorarr(i,*,*)),temp)
;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=ztt,color=113
loadct,5
if nce gt 0 then plots,energyarr(ai(0,*)),pitcharr(ai(1,*)),psym=5,symsize=sz,thick=ztt,color=255
endif
;if w-nw_phi+pw_theta=0
;omeg=-reform(modef*1.E3*2.*!pi-moden*abs(tangarr(i,*,*)))/reform(abs(pangarr(i,*,*)))
;if  w-nw_phi + (m+l)w_theta=0
;omeg=reform(modef*1.E3*2.*!pi+moden*abs(tangarr(i,*,*)))/reform(abs(pangarr(i,*,*)))-modem

;Todo for just p
omeg=reform(modef*1.E3*2.*!pi-moden*(tangarr(i,*,*)))/reform(abs(pangarr(i,*,*)))



sigo=omeg ;*1.E-3
loadct,4
;contour,sigo,energyarr,pitcharr,levels=pvals,/overplot,c_thick=7,c_colors=200,$
;c_labels=pvals*0+1,closed=0,c_ann=clabs,c_chars=czs*1.2,c_charthick=3,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1
loadct,5

endfor

device,/close_file

set_plot,'x'








;NOW 2nd TRANSP FIGURE  
;*****************************

!p.multi=[0,3,4]
bfss=8


restore,'epr_scan_t525_142111_50x50x50.sav'

;set_plot,'ps'
;device,filename='figure_transp_142111_t325_fineres.eps',/color,xsize=20,$
;ysize=25,xoffset=.5,yoffset=2$
;,font_size=bfss,bits_per_pixel=6,bold=0,scale=1
;!x.thick=5
;!y.thick=5
tt=4
czsc=2
ymm=[2,2]

;contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=7,c_colors=255,$
;c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1


;OUTTIM= 0.316500 , 0.321500 , 0.326500 , 0.331500 , 0.336500 , 0.341500 , 0.346
;500 , 0.356500 , 0.366500
;a1=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M18/142111M18_fi_4.cdf')
nrr=51
nzz=101
;f=tdf_rzmap_full(a1,nr=nrr,nz=nzz)


for i=26,48,2 do begin
modei=i
print, radarr(modei)
wl1=where(abs(filddistarr(modei,*,*)) le dfld,nwl1)
wn1=where(abs(filddistarr(modei,*,*)) gt dfld)
if nwl1 gt 0 then xw=array_indices(reform(filddistarr(0,*,*)),wl1)
sigorbc=reform(orbcarr(modei,*,*))

zarr=0
nir=findloc(f.r_rz/100.,radarr(modei))
niz=findloc(f.z_rz/100.,zarr)

sigdf=reform(f.distrz(nir,niz,*,*))
stemp=max(sigdf)*.3
shade_surf,sigdf*0,f.energy,f.pitch,ax=90,az=0,shades=bytscl((sigdf),max=stemp),$
xtitle='Energy (keV)',zstyle=4,ytitle='Pitch',charsize=cz,xstyle=1,$
font=0,xrange=xrr,yrange=yrr,ystyle=1

contour,sigorbc,energyarr,pitcharr,levels=[5,6],/overplot,xstyle=1,$
xrange=xrr,yrange=yrr,ystyle=1
loadct,4
print,nwl1
if nwl1 gt 0 then oplot,energyarr(xw(0,*)),pitcharr(xw(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5

endfor

ener2p=36.


!p.multi=0
e2p=findloc(f.energy,ener2p)
sigdf2=reform(f.distrz(*,niz,e2p,*))
stemp=max(sigdf2)*.3
xrr2=[1.2,2.3]
yrr2=[-1,1]
shade_surf,sigdf2*0,f.r_rz/100.,f.pitch,ax=90,az=0,shades=bytscl((sigdf2),max=stemp),$
xtitle='Radius',zstyle=4,ytitle='Pitch',charsize=cz,xstyle=1,$
xrange=xrr2,yrange=yrr2,ystyle=1
e2pr=findloc(energyarr,ener2p)
sigorbc2=reform(orbcarr(*,e2pr,*))
muo2=reform(mu0arr(*,e2pr,*))
contour,sigorbc2,radarr,pitcharr,levels=[5],/overplot,xstyle=1,$
xrange=xrr2,yrange=yrr2,ystyle=1,/fill
muo2=muo2/max(abs(muo2))
contour,muo2,radarr,pitcharr,nlevels=10,/overplot,xstyle=1,$
xrange=xrr2,yrange=yrr2,ystyle=1


pvals2=pvals ;[-2,-1,1,2] ;indgen(4)-2 ;reverse(modef/(1.+findgen(3))) ;findgen(10)*15.

clabs2='p='+stringc(fix(pvals2)) ;reverse(['p=1','p=2','p=3'])


modefa=[57.,60.,62.,73.,80,85]
modena=[2.,3.,4.,3.,4.,5.]
modema=[8.,12.,16.,13.,17.,21.]
nnm=n_elements(modefa)

ccls=modefa-min(modefa)
;ccls=fltarr(nnm)+180 ;[100,100] ;ccls/(max(ccls))*180+30
ccls=ccls/(max(ccls))*180+30
;for i=0,nnm-1 do begin
;for i=4,4 do begin

i=1
;omeg2=reform(modefa(i)*1.E3*2.*!pi+modena(i)*abs(tangarr(*,e2pr,*)))/reform(abs(pangarr(*,e2pr,*)))-modema(i)
omeg2=reform(modefa(i)*1.E3*2.*!pi-modena(i)*(tangarr(*,e2pr,*)))/reform(abs(pangarr(*,e2pr,*)))

contour,omeg2,radarr,pitcharr,levels=pvals2,/overplot,c_thick=2,c_colors=100,$
c_labels=pvals2*0+1,closed=0,c_ann=clabs2,c_chars=czs*2,c_charthick=2,xrange=xrr2,xstyle=1,thick=1,yrange=yrr2,ystyle=1

;endfor

end




