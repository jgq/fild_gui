

function getdrift,r,z,vpar,vperp,gci
;returns [ur,ut,uz] drift*(z*ee/mm/mi)
;interpolates drifts to r,z for a given vpar vperp

ucurvr=interpolatexy(gci.g.r,gci.g.z,gci.ucurvr*vpar^2,r,z)
ucurvt=interpolatexy(gci.g.r,gci.g.z,gci.ucurvt*vpar^2,r,z)
ucurvz=interpolatexy(gci.g.r,gci.g.z,gci.ucurvz*vpar^2,r,z)

ugradr=interpolatexy(gci.g.r,gci.g.z,gci.ugradr*vperp^2/2.,r,z)
ugradt=interpolatexy(gci.g.r,gci.g.z,gci.ugradt*vperp^2/2.,r,z)
ugradz=interpolatexy(gci.g.r,gci.g.z,gci.ugradz*vperp^2/2.,r,z)

return,[ucurvr+ugradr,ucurvt+ugradt,ucurvz+ugradz]
end



function get_com_ins,eqovm,ejou,v0,pitch0i,r0i,z0i,gci,inputsatgc=inputsatgc

if n_elements(inputsatgc) lt 1 then inputsatgc=1 
;input can be at gc or instantaneous position = 0

if inputsatgc lt 1 then begin
;to find gyrocenter and larmor radius
;****************************************************************************************
brp0=interpolatexy(gci.g.r,gci.g.z,gci.br,r0i,z0i)
btp0=interpolatexy(gci.g.r,gci.g.z,gci.bt,r0i,z0i)
bzp0=interpolatexy(gci.g.r,gci.g.z,gci.bz,r0i,z0i)

bunit0=[brp0,btp0,bzp0]/(brp0^2.+btp0^2.+bzp0^2.)^.5
bunitperp=[bzp0-btp0,brp0-bzp0,btp0-brp0]
bunitperp=bunitperp/(total(bunitperp^2.))^.5

sipitch=1.
if gci.g.bcentr*gci.g.cpasma lt 0 then sipitch=-1.

vtot0=1.
vpar=(vtot0*pitch0i*sipitch)
vperp=(vtot0^2.-vpar^2.)^.5

vel2ui0=vpar*bunit0+vperp*bunitperp

vd0i=getdrift(r0i,z0i,vpar*v0,vperp*v0,gci)/(eqovm)
vel2ui=vel2ui0*v0-vd0i  ;not 100% sure this is the proper sign for vd0i

vxbi=crossp(vel2ui/total(vel2ui^2)^.5,bunit0)  ;vxb unit
bveci=[brp0,btp0,bzp0]   ;bfield vector at r0i,z0i

b0i=total(bveci^2)^.5

wci0i=eqovm*b0i   
rli0i=v0*(1.-pitch0i^2.)/wci0i

rlvec=vxbi*rli0i ;*eqovm/abs(eqovm)   ;larmor radius vector from particle position to guiding center

;make the new r0,z0 at the gyrocenter to calculate mu and momentum
r0=rlvec(0)+r0i
z0=rlvec(2)+z0i

vpar=vpar*v0
vperp=vperp*v0

endif else begin

r0=r0i
z0=z0i
sipitch=1.
if gci.g.bcentr*gci.g.cpasma lt 0 then sipitch=-1.
vpar=v0*pitch0i*sipitch
vperp=(v0^2.-vpar^2.)^.5

endelse


;****************************************************************************************


psi0=interpolatexy(gci.g.r,gci.g.z,gci.psirz,r0,z0)  ;now evaluate psi0, f0, and b0 at GC
f0=interpolatexy(gci.g.r,gci.g.z,gci.frz,r0,z0)
b0=interpolatexy(gci.g.r,gci.g.z,gci.btot,r0,z0)
bt0=interpolatexy(gci.g.r,gci.g.z,gci.bt,r0,z0)
br0=interpolatexy(gci.g.r,gci.g.z,gci.br,r0,z0)
bz0=interpolatexy(gci.g.r,gci.g.z,gci.bz,r0,z0)
rho0=calculate_rhog(r0,z0,gci.g)
rho0=rho0(0)

if n_elements(gci.g.qpsi) eq n_elements(gci.g.rhovn) then begin
q0=interpol(gci.g.qpsi, gci.g.rhovn,rho0)
endif else begin
print,'Nuber of elements in g.qpsi NE g.rhovn)
q0=1.
endelse

;dont know whether or not to evaluate larmor radius at gc b0
wci0=eqovm*b0
rli0=v0*(1.-pitch0i^2.)/wci0

;print,'R_li0=',rli0

pitch0=pitch0i  ;will change pitch0 to gc if input was at instant. position

;if pitch at gc should include drift then need to re-evaluate
outs={r0:r0,z0:z0,r0i:r0i,z0i:z0i,vpar0:vpar,vperp0:vperp,velvec0:[0.0,0.0,0.0],pitch0:pitch0,wci0:wci0,rli0:rli0,vpar0i:vpar,vperp0i:vperp,$
pitch0i:pitch0i,wci0i:wci0,rli0i:rli0,psi0:psi0,f0:f0,b0:b0,bt0:bt0,br0:br0,bz0:bz0,psi0i:psi0,f0i:f0,b0i:b0,$
bt0i:bt0,br0i:br0,bz0i:bz0,rho0:rho0,q0:q0}


if inputsatgc eq 0 then begin  ;need to adjust pitch ifinputs werent at gc
;************************************************************

;dont know whether to use total velocity vector including drift to calculate pitch at gc
;would also affect inputsatgc=1 if evaluated new pitch including drifts
;vpar0=total(vel2ui*[br0,bt0,bz0]/b0) ;parallel velocity including drift at gc
;vperp0=(total(vel2ui^2)-vpar0^2)^.5  ;perp vel including drift
vpar0=total(vel2ui0*v0*[br0,bt0,bz0]/b0) ;par. veloc. at gc with only original vel no
;drift
vperp0=(v0^2-vpar0^2)^.5 ;perp veloc. at gc

signpitch=1.0
if pitch0i ne 0.0 then signpitch=pitch0i/abs(pitch0i)
pitch0=abs(vpar0/total(vpar0^2+vperp0^2)^.5)*signpitch

outs={r0:r0,z0:z0,r0i:r0i,z0i:z0i,vpar0:vpar,vperp0:vperp,velvec0:vel2ui0*v0,pitch0:pitch0,wci0:wci0,rli0:rli0,vpar0i:vpar,vperp0i:vperp,$
pitch0i:pitch0i,wci0i:wci0,rli0i:rli0,psi0:psi0,f0:f0,b0:b0,bt0:bt0,br0:br0,bz0:bz0,psi0i:psi0,f0i:f0,b0i:b0,$
bt0i:btp0,br0i:brp0,bz0i:bzp0,rho0:rho0,q0:q0}


;************************************************************
endif


return,outs



end
