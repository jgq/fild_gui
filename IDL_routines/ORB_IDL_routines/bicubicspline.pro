;+
; NAME:
;     BICUBICSPLINE
;
; PURPOSE:
;     Calculate the 2-D spline representation of a function given on a 2-D
;     grid.
;
; CALLING SEQUENCE:
;
;     data = bicubicspline(gridxy,x,y,xin,yin,ierr=ierr [,grid=grid])
;
; INPUT PARAMETERS:
;
;   GRIDXY(F(X,Y)) -- function values
;   X              -- x values at grid points (first dimension)
;   Y              -- y values at grid points (second dimension)
;   XIN            -- x values for desired spline interpolated result
;   YIN            -- y values for desired spline interpolated result
;
; OPTIONAL INPUT PARAMETERS:
;
; KEYWORDS:
;
;   IERR=ier       -- returned error code:
;                       0=ok
;                       -1 = not all parameters were supplied.
;                       -2 = dim(xin) .ne. dim(yin) with grid=0
;                       -3 = dim(x) or dim(y doesn't match gridxy
;			-4 =  x dim must be .ge. 2
;			-5 =  y dim must be .ge. 2
;			-6 =  x not in ascending order.
;			-7 =  y not in ascending order.
;
;
;   GRID=grid      -- specify /GRID or GRID=1 if you want the result interpolated
;                      onto a grid of the given xin and yin values.  Don't
;                      specify this keyword, or use GRID=0 to return the
;                      interpolated results as a vector using xin and yin as
;	               ordered pairs.
;
; OUTPUTS:
;
; COMMON BLOCKS:
;
;     NONE
;
; SIDE EFFECTS:
;
;     NONE
;
; RESTRICTIONS:
;
;     NONE
;
; PROCEDURE:
;
; CODE TYPE: modeling, analysis
;
; CODE SUBJECT:  other
;
; EASE OF USE: can be used with existing documentation
;
; OPERATING SYSTEMS:  UNIX of all flavor
;
; EXTERNAL CALLS:  NONE
;
; RESPONSIBLE PERSON: Ray Jong
;
; DATE OF LAST MODIFICATION:  10/15/98
;
; MODIFICATION HISTORY:
;     Created 1996.06.18 by Michael D. Brown, LLNL;
;-


function bicubicspline,gridxy,x,y,xin,yin,ierr=ierr,grid=grid

compile_opt defint32,strictarr,strictarrsubs

RESULT=[0.0]
IERR = -1L
IF N_ELEMENTS(GRIDXY) EQ 0 OR $
   N_ELEMENTS(X) EQ 0 OR N_ELEMENTS(Y) EQ 0 OR $
   N_ELEMENTS(XIN) EQ 0 OR N_ELEMENTS(YIN) EQ 0 THEN RETURN,RESULT

; @@whereami

IF N_ELEMENTS(GRID) EQ 0 THEN GRID=0

; Check that x and y are the same size (unless /grid was given).
IERR = -2L
IF GRID EQ 0 AND N_ELEMENTS(XIN) NE N_ELEMENTS(YIN) THEN RETURN,RESULT

SZ=SIZE(GRIDXY) & IFD=SZ[1]
NX=N_ELEMENTS(X)
NY=N_ELEMENTS(Y)

; Check that grid size matches given X and Y sizes.
IERR=   -3L
IF NX NE SZ[1] OR NY NE SZ[2] THEN RETURN,RESULT

; Check that dim of X is .ge. 2
IERR = -4
IF NX LT 2 THEN RETURN,RESULT

; Check that dim of Y is .ge. 2
IERR = -5
IF NY LT 2 THEN RETURN,RESULT

; Check that X is in ascending order.
IERR= -6L
IF MIN(  [X,X[NX-1]] - [X[0],X] ) LT 0  THEN RETURN,RESULT

; Check that Y is in ascending order.
IERR= -7L
IF MIN(  [Y,Y[NY-1]] - [Y[0],Y] ) LT 0  THEN RETURN,RESULT

XL=[FLOAT(XIN)]    & NXL=N_ELEMENTS(XL)
YL=[FLOAT(YIN)]    & NYL=N_ELEMENTS(YL)

; print,'aqui  estamos'
; print,NXL,NYL
;
; print,NX,NY


FL=FLTARR(NXL,NYL)
IERR=0L

  savefxcoeff=fltarr(NX,NY)
  savefygrid=fltarr(NXL,NY)

  ; save fxcoeff's to reduce repetitive calls.
  FOR i=0L,NY-1 DO BEGIN
    savefxcoeff[*,i] = nr_spline(x,gridxy[*,i])
    savefygrid[*,i]  = nr_splint(x,gridxy[*,i],savefxcoeff[*,i],xl)
  ENDFOR

  ;
  FOR J = 0L, NXL-1 DO BEGIN
      fygrid = reform(savefygrid[j,*])
      fycoeff = nr_spline(y,fygrid)
      IF grid eq 0 THEN					$  ; diagonal only.
        FL[j,j] = nr_splint(y,fygrid,fycoeff,yl[j])	$
      ELSE 						$
        FL[j,*] = nr_splint(y,fygrid,fycoeff,yl)
  ENDFOR

; Return only a vector if xin and yin are ordered pairs (grid=0).
IF GRID EQ 0 THEN BEGIN
  SZXIN=SIZE(XIN) & SZYIN=SIZE(YIN)
  IF SZXIN[0] EQ 0 AND SZYIN[0] EQ 0 THEN BEGIN
     RESULT=FL[0,0]  ; Scaler xin and yin results in scaler result.
  ENDIF ELSE BEGIN
     I=LINDGEN(NXL)   ; NXL AND NYL ARE EQUAL WITH GRID=0
     RESULT=FL[I,I]  ; Result is vector (the diagonal of the matrix.
  ENDELSE
ENDIF ELSE BEGIN
  RESULT=FL[*,0:NYL-1]  ; Result is a matrix.
ENDELSE

RETURN,RESULT

END
