
;;ORBIT CLASSIFICATION
;0=SOME ERROR IN CONTOURING  ;1=PASSING axis encircling, 2 = PASSING NOT-axis encircling
; 3=TRAPPED axis encircling (rare), 4=TRAPPED NOT-axis encircling, 
;5=CLOSED ORBIT ON EFIT GRID BUT HITS WALL, 6 = OPEN ORBIT ON EFIT GRID

restore,'epr_scan_t1835_141076.sav'
loadct,5
!p.multi=[0,2,3]
loadct,5
xt='Energy (kev)'
yt='Pitch (V||/V)'
cz=2.5
czs=1.5 ;cz
sz=.25
dfld=.05
xrr=[10,80]
yr=[-1.,1.]
ee=double(1.60E-19)
mi=double(1.67E-27)
mm=2.
z=1.
bfss=10
modef=7.1-.3 ;15.; 21. ;15.0
moden=1.  ;tornum
modem=2.  ;polnum
modei=3  ;for individual images use this of the 5 radii

;pvals=[-2,-1,0,1,2] ;indgen(4)-2 ;reverse(modef/(1.+findgen(3))) ;findgen(10)*15.
pvals=[0]
;pvals=[0,1,2] ;indgen(4)-2 ;reverse(modef/(1.+findgen(3))) ;findgen(10)*15.

clabs='p='+stringc(fix(pvals)) ;reverse(['p=1','p=2','p=3'])


;**********************************************************************************************************
set_plot,'ps'
device,filename='figure_epr_141076_t1835.eps',/color,xsize=21,$
ysize=24,xoffset=.5,yoffset=1$
,font_size=bfss,bits_per_pixel=6,bold=1,scale=1
!x.thick=5
!y.thick=5
tt=4



for i=0,4 do begin
loadct,5
sig=reform(orbcarr(i,*,*))
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

xyouts,30,.975,'R='+stringc(radarr(i)),charsize=czs,color=0,font=0
wl1=where(abs(filddistarr(i,*,*)) le dfld,nwl1)
wn1=where(abs(filddistarr(i,*,*)) gt dfld)
if nwl1 gt 0 then x=array_indices(reform(filddistarr(0,*,*)),wl1)
temp=where(errorarr(i,*,*) gt 0,nce)
if nce gt 0 then ai=array_indices(reform(errorarr(i,*,*)),temp)
;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=ztt,color=113
loadct,5
if nce gt 0 then plots,energyarr(ai(0,*)),pitcharr(ai(1,*)),psym=5,symsize=sz,thick=ztt,color=255
endif

omeg=-reform(modef*1.E3*2.*!pi-moden*abs(tangarr(i,*,*)))/reform(abs(pangarr(i,*,*)))
;omeg=reform(modef*1.E3*2.*!pi/moden/abs(tangarr(i,*,*)))
sigo=omeg ;*1.E-3
loadct,0
contour,sigo,energyarr,pitcharr,levels=pvals,/overplot,c_thick=7,c_colors=00,$
c_labels=pvals*0+1,closed=0,c_ann=clabs,c_chars=czs*1.2,c_charthick=3,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1
loadct,5

endfor

device,/close_file

set_plot,'x'


;**********************************************************************************************************

;wk=get_kbrd(1)

set_plot,'ps'
device,filename='figure_1r_res_141076_t1835.eps',/color,xsize=21,$
ysize=22,xoffset=.5,yoffset=1$
,font_size=bfss,bits_per_pixel=6,bold=0,scale=1
!x.thick=5
!y.thick=5
tt=4


loadct,5

sz=.7


;from n*w_phi-p*w_theta=w
;omeg=-reform(modef*1.E3*2.*!pi-moden*abs(tangarr(i,*,*)))/reform(abs(pangarr(i,*,*)))
omeg=-reform(modef*1.E3*2.*!pi-moden*abs(tangarr(modei,*,*)))/reform(abs(pangarr(modei,*,*)))
;omeg=reform(modef*1.E3*2.*!pi/moden/abs(tangarr(i,*,*)))
sigo=omeg ;*1.E-3
wlost=where(orbcarr(modei,*,*) eq 6)
;sigo(wlost)=-999
plotomeg=1
!p.multi=0 ;[0,1,1]
if plotomeg then begin




wl1=where(abs(filddistarr(modei,*,*)) le dfld,nwl1)
wn1=where(abs(filddistarr(modei,*,*)) gt dfld)
if nwl1 gt 0 then x=array_indices(reform(filddistarr(0,*,*)),wl1)
temp=where(errorarr(modei,*,*) gt 0,nce)
if nce gt 0 then ai=array_indices(reform(errorarr(modei,*,*)),temp)

sig=reform(orbcarr(modei,*,*))
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

xyouts,40,.995,'R='+stringc(radarr(modei)),charsize=czs*1.5,color=0,font=0

;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5
if nce gt 0 then plots,energyarr(ai(0,*)),pitcharr(ai(1,*)),psym=5,symsize=sz,thick=1,color=255

endif

contour,sigo,energyarr,pitcharr,levels=pvals,/overplot,c_thick=7,c_colors=255,$
c_labels=pvals*0+1,closed=0,c_ann=clabs,c_chars=czs*1.5,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1

device,/close
set_plot,'x'


;eq56=where(orbcarr(i,*,*) eq 5 or orbcarr(i,*,*) eq 6)
;xeq56=array_indices(reform(orbcarr(i,*,*)),eq56)

;**********************************************************************************************************


;NOW TRANSP FIGURE
;*****************************

!p.multi=[0,1,2]
bfss=8
set_plot,'ps'
device,filename='figure_transp_res_141076_t1835.eps',/color,xsize=21,$
ysize=25,xoffset=.5,yoffset=2$
,font_size=bfss,bits_per_pixel=6,bold=0,scale=1
!x.thick=5
!y.thick=5
tt=4
czsc=2
ymm=[2,2]
sig=reform(orbcarr(modei,*,*))
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1,ymargin=ymm
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

xyouts,40,.975,'R='+stringc(radarr(modei)),charsize=czs*2,color=0,font=0

;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5
endif

contour,sigo,energyarr,pitcharr,levels=pvals,/overplot,c_thick=7,c_colors=255,$
c_labels=pvals*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1
endif

a1=transp_cdf_restore('/u/heidbrin/FISHBONE/141069B04_fi_3.cdf')

f1=a1.f_d_nbi


enedf=1.e-3*a1.e_d_nbi
pitchdf=a1.a_d_nbi

nf=findloc(enedf,xrr(0))
nenn=findloc(enedf,xrr(1))

distrztr=((radarr(modei)-a1.r2d/100.)^2.+(z0-a1.z2d/100.)^2)^.5
temp=min(distrztr,nin2)
sdis=sort(distrztr)

loadct,5
n2avtr=4
sigt=total((f1(nf:nenn-1,*,sdis(0:n2avtr-1))),3)/n2avtr

stemp=.5E7
shade_surf,sigt,enedf(nf:nenn-1),pitchdf,ax=90,az=0,shades=bytscl((sigt),$
min=min(sigt)),$
xtitle='Energy (keV)',zstyle=4,ytitle='Pitch',charsize=cz,xstyle=1,$
font=0,xrange=xrr,yrange=yr,ystyle=1,ymargin=ymm
contour,sigo,energyarr,pitcharr,levels=pvals,/overplot,c_thick=7,c_colors=255,$
c_labels=pvals*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1

loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5


device,/close_file
set_plot,'x'

;**********************************************************************************************************



end




