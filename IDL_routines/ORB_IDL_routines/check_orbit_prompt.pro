gfil='/u/vanzee/efits/146121/KEFIT/AUTOKNOT/UPLOAD/HIGHRES/g146121.00555'
g=readg(gfil)
b=get_beam_pitch(g,plott=0,ndrays=300)


phifildintransp=90.-(165.+360.)
fild_rpz=[2.372,phifildintransp*!pi/180.,-0.134]


;grr=findgen(8)*.5+1.5
ngrr=20
erange=[25.,80.] ;[75.,80.]
prange=[0.1,0.9]
nprr=20

nmin=20 ;default is 20

dpitch=0.08  ;must be within this pitch
drzmin=0.15  ;
;dxymin=0.10   ;must be within this distance in x,y
grr=findgen(ngrr)/(ngrr-1.)*(max(erange)-min(erange))+min(erange) ;findgen(7)*10.+20.
prr=findgen(nprr)/(nprr-1.)*(max(prange)-min(prange))+min(prange) ;cos((findgen(12)*5.+30.)*!pi/180.)

fildhit=intarr(8,ngrr,nprr)

for ii=0,ngrr-1 do begin
for jj=0,nprr-1 do begin


energ=grr(ii) ;74.5 ;45 ;74.5
pit=prr(jj) ;0.5 ;0.7 ;0.5



;need to consider correct toroidal angles
;orb_gc_mv,g,fild_rpz,pit,energ,plot=1,orbinteg=0,outs=o, gci=gci; 149
orb_gc_mv,g,fild_rpz,pit,energ,plot=1,orbinteg=1,outs=o, gci=gci,reverse_time=1,nmin=nmin,nsteps=nmin/20.*1.E3 ; 149


;if o.error gt 0 then goto,jump1

rgc=reform(o.yout(0,*)) ;rcont0
zgc=reform(o.yout(2,*)) ;zcont0
phgc=reform(o.yout(1,*))
xgc=rgc*cos(o.yout(1,*))
ygc=rgc*sin(o.yout(1,*))
prz=o.pitchyout*o.pitchsignflip ;o.pitchcont*o.pitchsignflip

dzbeam=1.5*b.heightbeam/2.*1.E-2  ;within this of beam positions in R,z
match=0
wmatch=0
wrz=where(abs(zgc) lt dzbeam,ncz)
if ncz gt 0 then begin
drz=fltarr(ncz,n_elements(b.beam_pitch_dz(*,0,0)),n_elements(b.beam_pitch_dz(0,*,0)),n_elements(b.beam_pitch_dz(0,0,*)))
dprz=drz
dxy=drz
for i=0,ncz-1 do begin  ;replace with vector operation eventually
dprz(i,*,*,*)=abs(prz(wrz(i))-b.beam_pitch_dz)
;drz(i,*,*,*)=((rgc(wrz(i))-reform(b.beam_rays_dz(*,3,*,*)))^2.+(zgc(wrz(i))-reform(b.beam_rays_dz(*,2,*,*)))^2.)^.5

drz(i,*,*,*)=((xgc(wrz(i))-reform(b.beam_rays_dz(*,0,*,*)))^2.+$
(ygc(wrz(i))-reform(b.beam_rays_dz(*,1,*,*)))^2.+$
(zgc(wrz(i))-reform(b.beam_rays_dz(*,2,*,*)))^2.)^.5

;dxy(i,*,*,*)=((xgc(wrz(i))-reform(b.beam_rays_dz(*,0,*,*)))^2.+$   ;just checking in xy plane since already constrained z
;(ygc(wrz(i))-reform(b.beam_rays_dz(*,1,*,*)))^2.)^.5


endfor


wmatch=where(abs(dprz lt dpitch) and abs(drz lt drzmin),match)
;wmatch=where(abs(dprz lt dpitch) and abs(dxy lt dxymin),match)

if match gt 0 then begin
wrmatch=array_indices(dprz,wmatch)

rmatch=rgc(wrz(wrmatch(0,*)))
zmatch=zgc(wrz(wrmatch(0,*)))
xmatch=xgc(wrz(wrmatch(0,*)))
ymatch=ygc(wrz(wrmatch(0,*)))

pmatch=prz(wrz(wrmatch(0,*)))
beammatch=reform(wrmatch(1,*))

!p.multi=[0,2,1]
plot,g.lim(0,*),g.lim(1,*),/iso
oplot,g.bdry(0,*),g.bdry(1,*)
oplot,rgc,zgc,color=200
oplot,rmatch,zmatch,psym=4,color=100,symsize=1.

ths=indgen(100)/99.*2.*!pi
plot,2.5*sin(ths),2.5*cos(ths),/iso
oplot,xgc,ygc,color=200

oplot,b.beam_rays(*,0,*),b.beam_rays(*,1,*),psym=5,symsize=0.5
oplot,xmatch,ymatch,psym=4,color=100,symsize=1.1

;wk=get_kbrd(1)
for fh=0,7 do begin
temp=where(beammatch eq fh,ntemp)
if ntemp gt 0 then fildhit(fh,ii,jj)=ntemp
endfor

endif

endif

jump1:

endfor
endfor

contour,total(fildhit(*,*,*),1),grr,prr,/fill,nlevels=255

;orb_gc_mv,g,fild_rpz,0.75,45.,plot=1,orbinteg=1,reverse_time=1,nsteps=92,outs=o ;149



end
