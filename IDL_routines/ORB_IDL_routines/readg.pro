;+ 
; NAME: 
;	READG_AUG
;
; PURPOSE: 
;
;	Retrieves GEQDSK data from file or MDSplus for DIII-D
;	and via libkk from the AUG CLISTE type equilibia
;
; CATEGORY: 
;
;	DIII-D development 
;	AUG addition for reading AUG CLISTE,FPP equilibria as g-structure
;
; CALLING SEQUENCE: 
;
;	a = READG_AUG(arg1 [,arg2] [,MODE=mode] [,RUNID=runid] [,INFO=info] 
;                              [,SOURCE=source] [,EXACT_TIME=exact_time]
;			       [,VERBOSE=verbose] [,SERVER=server]
;                              [,DEBUG=debug] [,STATUS=status] )
;	AUG extensions:
;				[,exp=exp] [,diag=diag] [,edition=edition
;				[,no_interpolation = no_interpolation]
;				[,opt_time=opt_time]
;				[,stop=stop] [,plot=plot]
;
;
; INPUT PARAMETERS: 
;
;	arg1:  	Either a string specifying the filename to read, or a long 
;               integer specifying the shot number (if the latter, arg2 must 
;               be present) to read.
;
;	arg2:	Float or double specifying the EFIT time to read and return.  
;               If arg1 specifies the shot number, arg2 must be used.
;
; OPTIONAL INPUT PARAMETERS: 
;
;	none 
;
; KEYWORDS: 
;
;	(all are optional)
;	
;	MODE:  If "FILE", will restrict READG to retrieving EFIT data from
;	       files only, not from MDSplus.  If "MDSPLUS", will restrict 
;	       READG to retrieving EFIT data from MDSplus only, not files.  If
;	       not specified, READG will first attempt to retrieve the data 
;	       from a file, and then from MDSplus.
;	
;	RUNID:  EFIT "run ID" to use in MDSplus.  This defaults to "EFIT01" - 
;		the non-MSE automatic control room EFIT.
;	
;	INFO:  A structure with the following form:
;	
;		{mode:'', file:'', shot:0l, time:0.0d0, runid:''}
;	
;	       If specified as an input to READG, INFO will superceed the 
;	       arguments specified in arg1, arg2, and the keyword values of 
;              MODE and RUNID.
;	
;	       INFO is also returned from READG to indicate the values it used
;              to find the EFIT.
;	
;	SOURCE:  Either "FILE" or "MDSPLUS" - specifies the data source from
;	         where the EFIT data were retrieved.  Note that this information
;		 is also available in the returned structure as G.SOURCE.
;	
;	EXACT_TIME:  If set, forces READG to match the time specified, rather
;		     than using the default behavior of returning the nearest
;		     time.  
;
;	VERBOSE:  If set, READG will print out informational messages on its 
;                 progress.
;
;       SERVER:  If set to a string containing a valid IP address for
;                an MDSplus data server, READG will read the EFITs
;                from the specified server instead of the default for DIII-D.
;	
;	DEBUG:  If set, READG will print out additional debugging information,
;	        as well as turn off all error handling.  This will allow READG 
;	        to crash if there is an unexpected error.
;	
;	STATUS:  TRUE if READG was able to retrieve the data successfully, 
;                FALSE if not.  This information is also provided in the 
;		 output of the function (see below).
;
;	EXP:	AUG experiment specification
;
;	DIAG:	AUG diagnostic specification (FPP,EQI,EQE at present)
;
;	EDITION:AUG edition specification
;
;	NO_INTERPOLATION:	suppres the interpolation of the radial
;		coordinates to the grid of MW numbers. This makes the
;		g-structure a invalid g-structure.
;
;	OPT_TIME:	option for times:
;		0, default - return specified time points
;		1 - return time points from shotfile between
;		    time(0) and time(end)
;		2 - return all time points from shotfile,
;		    ignoring input time
;		In all cases an array of g-structures is given back.
;
;	STOP:	stop the programme after the work for debugging purposes.
;
;	PLOT:	do a plot of the grid and the data with PLOT_G,G
;
;
; OUTPUTS: 
;
;	Structure(s) containing the GEQDSK data retrieved for the shot and
;	time(s) specified.
;
;	Note that the structure contains the tag ERROR, which is 0 if the
;	data was read successfully, and 1 if not.
;
;	The returned structure also contains the tag SOURCE, a string that
;	describes the source from which the data was obtained (MDSplus or
;	File, which shot, EFIT run, and time).
;
; COMMON BLOCKS: 
;
;       COMMON EFIT_READG_CACHE,info_cache,data_cache
;
;	This common block caches GEQDSK data read from MDSplus.  READG reads 
;	the data for the entire time history, and then subscripts it at the 
;	time of interest. Subsequent references to data from the same shot and
;	EFIT run but different time will retrieve the data from the cache 
;	rather than reading it again from MDSplus.
;
; SIDE EFFECTS: 
;
;	Calls function EFIT_READ to handle read logic - as this logic is the 
;	same as for READA.
;
; RESTRICTIONS:
;
;	None.
;
; PROCEDURE: 
;
;	READG retrieves GEQDSK data from an EFIT run for a particular shot and
;	timeslice.  READG uses the following logic to locate the EFIT:
;	
;	- If arg1 specifies a file, READG attempts to determine the 6 digit
;	  shot number and time from the filename, assuming it has the format
;	  .../gSSSSSS.TTTTT_TTT.  _TTT is optional - used for specifying
;	  sub-millisecond timeslices.  If it cannot, it will still attempt to 
;	  read the file specified, but if the file attempt fails, the MDSplus
;	  attempt will also fail (see below).  NOTE THAT if arg1 specifies a
;	  file, READG will act as if the EXACT_TIME keyword is set - that is
;	  it will not attempt to find the nearest time if it cannot find the
;	  exact time.
;	
;	- If arg1 specifies the shot and arg2 the time, the filename
;	  gSSSSSS.TTTTT_TTT is composed, where SSSSSS is the 6 digit shot 
;	  number and TTTTT_TTT is the time, optionally using the _TTT for 
;	  sub-ms timeslices.
;	
;	- If the filename contains a directory specification, READG will look 
;	  for the file specified in the place specified.  If it does not, 
;	  READG will search  the following locations (in order) for the file:
;	
;	  1) The current directory ./  (VMS: [])
;	  2) The subdirectory ./shotSSSSSS  (VMS: [.shotSSSSSS])
;	  3) The subdirectory ./shotSSSSS  (VMS: [.shotSSSSS]) 
;					(SSSSS = 5 digit shot number)
;	
;	- If the file is found in one of these places, an attempt is made to 
;	  read it.
;	
;	- If the file is not found in one of these three places, and the 
;	  keyword EXACT_TIME is *NOT* set, the same locations are searched 
;	  for an GEQDSK file with a time *nearest* the time specified.  
;	
;	- If the read attempt fails, or if the file is not found, READG will 
;	  attempt to read the data from MDSplus, using the shot number and 
;	  time specified (or determined from the filename).  Data from the 
;	  time *nearest* the time specified will be returned if the MDSplus 
;	  read attempt is successful (unless the keyword EXACT_TIME is set).
;	
;	- If the value of the keyword MODE is "MDSPLUS", READG will not
;	  attempt to read the data from a file, instead proceeding directly to
;	  the MDSplus read attempt. 
;	
;	- If the value ofthe keyword MODE is "FILE", READG will not attempt to
;	  read the data from MDSplus if the file read attempt is unsuccessful.
;	
;	- Any other value of MODE will have no effect on the read logic.
;	
; EASE OF USE: Can be used with existing documentation
;
; OPERATING SYSTEMS:  HP-UX, OSF/Unix, OpenVMS, MacOS
;
; EXTERNAL CALLS:  MDSplus only from sfaug1.aug.ipp.mpg.de to atlas.gat.com
;
; RESPONSIBLE PERSON: Jeff Schachter
;
; CODE TYPE: modeling, analysis, control  
;
; CODE SUBJECT:  handling, equilibrium
;
; DATE OF LAST MODIFICATION: 06/04/01
;
; MODIFICATION HISTORY:
;
;	Version 1.0: Released by Marc Maraschek, 13.06.2008

;-	




function readg,shot,time, $
	exp=exp,diag=diag,edition=edition, $
	no_interpolation = no_interpolation, $
	spline=spline, $
	opt_time=opt_time, $
	stop=stop, plot=plot, $
; DIII-D readg options:
	mode=mode, runid=runid, info=info, source=source, $
	exact_time=exact_time, time_range=time_range, $
	server=server, $
	verbose=verbose, debug=debug, status=status 


if (keyword_set(mode)) then begin
case mode of
 'mdsplus': begin
	g = readg_d3d(shot,time, $
		mode=mode, runid=runid, info=info, source=source, $
		exact_time=exact_time, time_range=time_range, $
		server=server, $
		verbose=verbose, debug=debug, status=status)
	if (keyword_set(plot)) then begin
		plot_g,g
	endif
	return,g
	end
 'file:`': begin
	g = readg_d3d(shot,time, $
		mode=mode, runid=runid, info=info, source=source, $
		exact_time=exact_time, time_range=time_range, $
		server=server, $
		verbose=verbose, debug=debug, status=status)
	if (keyword_set(plot)) then begin
		plot_g,g
	endif
	return,g
	end
  else: begin
	end
endcase
endif


case n_params() of
    1: begin
	; use standard DIII-D reading routine for g-file:
	mode = 'file'
	g = readg_d3d(strtrim(shot,2), $
		mode=mode, runid=runid, info=info, source=source, $
		exact_time=exact_time, time_range=time_range, $
		server=server, $
		verbose=verbose, debug=debug, status=status)
	if (keyword_set(plot)) then begin
		plot_g,g
	endif
	return,g
	end
    2: begin
	; do the AUG shotfile reading below, for shot,time pair:
	end
    else: begin
	print,'usage:    g = readg_aug(shot,time[ms],exp=exp,diag=diag,edition=edition,plot=plot,status=status)
	print,'          g = readg_aug(g-filename,DIII-D-options)'
	return,{error:-1l}
	end
endcase



status = 1

defsysv, '!libddww', exists=exists
if exists ne 1 then begin
	;defsysv, '!libddww', '/usr/ads/lib/libddww.so'
	if (!version.memory_bits eq 64) then $
		lib_path='/usr/ads/lib64' $
	else $
		lib_path='/usr/ads/lib'
	libddww = lib_path+'/libddww.so'
endif else begin
	libddww = !libddww
endelse
;libddww = '/usr/ads/lib/libddww.so'

defsysv, '!libkk', exists=exists
if exists ne 1 then begin
	;defsysv, '!libddww', '/usr/ads/lib/libddww.so'
	if (!version.memory_bits eq 64) then $
		lib_path='/usr/ads/lib64' $
	else $
		lib_path='/usr/ads/lib'
	libddww = lib_path+'/libkk.so'
endif else begin
	libkk = !libkk
endelse
;libkk = '/usr/ads/lib/libkk.so'

if (not keyword_set(exp)) then exp='AUGD'
if (not keyword_set(diag)) then diag='EQI'
if (not keyword_set(edition)) then edition=0l
if (not keyword_set(opt_time)) then opt_time=0l


exp = strupcase(exp)
diag = strupcase(diag)
shot = long(shot)
edition = long(edition)
ctrl = 3l



ier	= 0l
diaref	= 0l
date	= '                  '
physdim	= '            '
s = call_external(libddww,'ddgetaug','ddopen', $
	ier,exp,diag,shot,edition,diaref,date)
if (ier ne 0) then begin
	s = call_external(libddww,'ddgetaug','xxerror',ier,ctrl,'ddopen')
	status = 0
	return,{error,ier}
endif
source = exp+':'+diag+strtrim(shot,2)+'.'+strtrim(edition,2)+':'+date
translate_date,date,year,month,day,hour,min,sec,/makestring


type	= 1L
leng	= 1L
M	= 0L
N	= 0L
NTIME	= 0L
ier	= 0L
s = CALL_EXTERNAL (libddww,'ddgetaug','ddparm', $
	ier, diaref,'PARMV','M',type,leng,M,physdim)
if (ier ne 0) then begin
	s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier, ctrl, 'ddparm: PARMV:M')
endif
ier	= 0L
s = CALL_EXTERNAL (libddww,'ddgetaug','ddparm', $
	ier, diaref,'PARMV','N',type,leng,N,physdim)
if (ier ne 0) then begin
	s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier, ctrl, 'ddparm: PARMV:N')
endif
ier	= 0L
s = CALL_EXTERNAL (libddww,'ddgetaug','ddparm', $
	ier, diaref,'PARMV','NTIME',type,leng,NTIME,physdim)
if (ier ne 0) then begin
	s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier, ctrl, 'ddparm: PARMV:NTIME')
endif


ier	= 0l
s = call_external(libddww,'ddgetaug','ddclose', $
	ier,diaref)
if (ier ne 0) then begin
	s = call_external(libddww,'ddgetaug','xxerror',ier,ctrl,'ddclose')
	message,'Could not close shotfile, continue anyway.',/cont
endif


;
; Read vessel data directly with kk-routines
;
;Ndim	= 700L
; mrm, 30.08.2011: extended buffer length for 2010/2011 campaign:
Ndim	= 1000L
NdGC	= 45L
xyGC	= fltarr(Ndim,2)
NGC	= 0L
ixbeg	= lonarr(NdGC)
lenix	= lonarr(NdGC)
valix	= lonarr(NdGC)
GCnam	= replicate(STRING(' ', FORMAT = '(A8)'),NdGC)
ier	= 0L
YGCed	= 0L
s = call_external(libkk,'kkidl','kkGCd0', $
	ier, exp, 'YGC', shot, YGCed, $
	Ndim, xyGC, NdGC, NGC, ixbeg, lenix, valix, GCnam)
if (ier gt 0) then begin
    message, 'error in reading vessel structures from kk-routines',/cont
endif
ndim_all = ixbeg(ngc-1)+lenix(ngc-1)-1

; remove all parts which are invalid in the DIII-D structure:
lim = transpose([[xyGC[*,0]],[xyGC[*,1]]])
index = 0l
for j=0,NGC-1 do begin
	if (valix(j) gt 0) then begin
		lim[*,index:lenix(j)+index-1] = $
			lim[*,(ixbeg(j)-1):(ixbeg(j)+lenix(j)-2)]
		index = index + lenix(j)
	endif
endfor

;
; read valid time points in the shotfile:
;
nprnt	= 0l
Ntipts	= 1000l
timpts	= fltarr(Ntipts)
ixtipt	= lonarr(Ntipts)
ier	= 0l
s = call_external(libkk,'kkidl','kkEQtpx', $
	ier, exp, diag, shot, edition, nprnt, $
	Ntipts, timpts, ixtipt)
timpts	= timpts[0:Ntipts-1]
ixtipt	= ixtipt[0:Ntipts-1]



time	= float(time/1000.)
case opt_time of
    0: begin
	end
    1: begin
	; should now create array of sf time values from window
	if (n_elements(time) lt 2) then begin
		print,'no time window provided, but window requested, continue with singel time point'
	endif else begin
		ind = where((timpts ge time[0]) and (timpts le time[n_elements(time)-1]))
		if (ind[0] ne -1) then time = timpts[ind] else begin
			time = timpts
			print,'time window not in valid time range, taking all time points'
		endelse
	endelse
	end
    2: begin
	; take all time points from the shotfile:
	time = timpts
	end
    else: begin
	end
endcase




for i=0, n_elements(time)-1 do begin

my_time = time[i]
ecase = [diag,month+'/'+day,strmid(date,7,2),'#'+strtrim(shot,2),strtrim(my_time*1000,2),'']

;
; read special points (magn. axis, 1st X-point, 1st limiter, 2nd X-point, 2nd limiter):
;
ier	= 0L
tsf	= my_time
LPFx	= 4L
PFxx	= fltarr(LPFx+1)
RPFx	= fltarr(LPFx+1)
zPFx	= fltarr(LPFx+1)
s = call_external(libkk, 'kkidl', 'kkEQpfx', $
	ier, exp, diag, shot, edition, tsf, $
	LPFx, PFxx, RPFx, zPFx )
ssimag	= PFxx[0]
Rmag	= RPFx[0]
zmag	= zPFx[0]
ssibry	= PFxx[1]
R_xp	= RPFx[1]
z_xp	= zPFx[1]
ssilim	= PFxx[2]
R_lim	= RPFx[2]
z_lim	= zPFx[2]
ssi2bry	= PFxx[3]
R_2xp	= RPFx[3]
z_2xp	= zPFx[3]
ssi2lim	= PFxx[4]
R_2lim	= RPFx[4]
z_2lim	= zPFx[4]
R_0	= 1.65
Z_0	= 0.0
if (keyword_set(debug)) then print,PFxx,RPFx,zPFx


;
; read grid ad poloidal flux matrix :
;
tsfh	= 0.0
N_R	= m+1
N_Z	= n+1
Mdim	= m+1
Ri	= fltarr(N_R)
Zj	= fltarr(N_Z)
PFM	= fltarr(N_R,N_Z)
ier	= 0l
s = call_external(libkk,'kkidl','kkEQPFM', $
	ier,exp,diag,shot,edition,tsf, $
	Mdim,N_R,N_Z,Ri,Zj,PFM)

MW	= N_R+1
MH	= N_Z+1


;
; read q-profile and psi-profile:
;
length	= 600l
LPF	= length
PFL	= fltarr(LPF)
Qpl	= fltarr(LPF)
ier	= 0l
s = call_external(libkk,'kkidl','kkEQqpl', $
	ier,exp,diag,shot,edition,tsf, $
	LPF,PFL,Qpl)
PFL	= PFL[0:LPF]
psi_n	= (ssimag-reverse(PFL))/(ssimag-ssibry)
Qpl	= Qpl[0:LPF]
LPF	= LPF+1

;
; read p-profile and pprime-profile:
;
LPF_	= length
pres	= fltarr(LPF_)
ppres	= fltarr(LPF_)
PFL_	= fltarr(LPF_)
ier	= 0l
s = call_external(libkk,'kkidl','kkEQPres', $
	ier,exp,diag,shot,edition,tsf, $
	LPF_,PFL_,pres,ppres)
pres	= pres[0:LPF_]
ppres	= ppres[0:LPF_]

;
; read j-profile and jprime-profile:
;
LPF_	= length
Jpol	= fltarr(LPF_)
Jpolp	= fltarr(LPF_)
PFL_	= fltarr(LPF_)
ier	= 0l
s = call_external(libkk,'kkidl','kkEQJpol', $
	ier,exp,diag,shot,edition,tsf, $
	LPF_,PFL_,Jpol,Jpolp)
Jpol	= Jpol[0:LPF_]
Jpolp	= Jpolp[0:LPF_]

;
; read f-profile and fprime-profile:
;
LPF_	= length
F	= fltarr(LPF_)
FP	= fltarr(LPF_)
PFL_	= fltarr(LPF_)
typ	= 11l
ier	= 0l
s = call_external(libkk,'kkidl','kkEQFFs', $
	ier,exp,diag,shot,edition,tsf, $
	typ,LPF_,PFL_,F,FP)
if (ier ne 0) then begin
	print,'error reading F, FP'
endif
F	= F[0:LPF_]
FP	= FP[0:LPF_]

;
; read f-profile and fprime-profile:
;
LPF_	= length
FFP	= fltarr(LPF_)
PFL_	= fltarr(LPF_)
typ	= 11l
ier	= 0l
s = call_external(libkk,'kkidl','kkEQFFP', $
	ier,exp,diag,shot,edition,tsf, $
	typ,LPF_,PFL_,FFP)
if (ier ne 0) then begin
	print,'error reading FFP'
endif
FFP	= FFP[0:LPF_]


if (keyword_set(no_interpolation)) then begin
	MR = LPF
endif else begin
	MR = MW
endelse
	psi_n_efit = findgen(MR)/(MR-1)
	if(keyword_set(spline)) then begin
		qpl = interpol(qpl,psi_n,psi_n_efit,/spline)
		pfl = interpol(pfl,psi_n,psi_n_efit,/spline)
		pres = interpol(pres,psi_n,psi_n_efit,/spline)
		ppres = interpol(ppres,psi_n,psi_n_efit,/spline)
		Jpol = interpol(Jpol,psi_n,psi_n_efit,/spline)
		Jpolp = interpol(Jpolp,psi_n,psi_n_efit,/spline)
		FFP = interpol(FFP,psi_n,psi_n_efit,/spline)
		F = interpol(F,psi_n,psi_n_efit,/spline)
		FP = interpol(FP,psi_n,psi_n_efit,/spline)
	endif else begin
		qpl = interpol(qpl,psi_n,psi_n_efit)
		pfl = interpol(pfl,psi_n,psi_n_efit)
		pres = interpol(pres,psi_n,psi_n_efit)
		ppres = interpol(ppres,psi_n,psi_n_efit)
		Jpol = interpol(Jpol,psi_n,psi_n_efit)
		Jpolp = interpol(Jpolp,psi_n,psi_n_efit)
		FFP = interpol(FFP,psi_n,psi_n_efit)
		F = interpol(F,psi_n,psi_n_efit)
		FP = interpol(FP,psi_n,psi_n_efit)
	endelse

;
; Read scalar quantities from FPP corresponding to FPG:
;
;lssq	= 120
;lssq_len= lssq
;ssq	= fltarr(lssq)
;ssqnam	= replicate(string('',format='(a8)'),lssq)
;ier	= 0L
;s = call_external(libkk, 'kkidl', 'kkEQssq', $
;	ier, exp, diag, shot, edition, tsf, $
;	lssq, ssq, ssqnam )
;if (ier ne 0) then begin
;	print, 'Error reading scalar quantities!'
;endif
;if (keyword_set(debug)) then begin
;	print,'lssq = ',lssq
;	for i=0,lssq_len-1 do begin
;		print, strtrim(i,2)+': '+strtrim(ssqnam(i),2)+'	= '+strtrim(ssq(i),2)
;	endfor
;endif


;
; read surface for plasma edge:
;
ndxy	= 900l
xysp	= fltarr(ndxy,2)
nxy	= 0l
work	= fltarr(ndxy+1,ndxy+1)
psip	= ssibry
ier	= 0l
s = call_external(libkk,'kkidl','kkEQpsp', $
	ier,exp,diag,shot,edition,tsf, $
	psip,ndxy,xysp,nxy,work,tsfh)


;
; read surface for last flux surface, typically limiter:
;
ndxy	= 900l
xylim	= fltarr(ndxy,2)
nxy_lim	= 0l
psip	= ssilim
ier	= 0l
s = call_external(libkk,'kkidl','kkEQpsp', $
	ier,exp,diag,shot,edition,tsf, $
	psip,ndxy,xylim,nxy_lim,work,tsfh)


;
; Read magnetic field at (RZERO,0.0) = (1.65,0.0) from FPP:
;
Br      = 0.0
Bz      = 0.0
Bt      = 0.0
fPF     = 0.0
fJp     = 0.0
ier	= 0L
x	= R_0
y	= Z_0
s = call_external(libkk, 'kkidl', 'kkrzBrzt', $
	ier, exp, diag, shot, edition, tsf, $
	x, y, 1L, $
	Br, Bz, Bt, fPF, fJp )
if (ier ne 0) then begin
	print, 'Error reading magnetic field!'
endif
if (keyword_set(debug)) then begin
	B_p = sqrt(Br^2+Bz^2)
	print, 'B ('+strtrim(R_0,2)+','+strtrim(Z_0,2)+')_r,z,p = ('+ $
		strtrim(Br,2)+','+strtrim(Bz,2)+','+strtrim(Bt,2)+')'
	print, 'B_t = '+strtrim(Bt,2)+', B_p = '+strtrim(B_p,2)+ $
		', pitch angle = '+strtrim(atan(B_p/Bt)*180.0/!pi,2)
endif


;
; Read plasma current from FPP:
;
ier	= 0L
Ipi	= 0.0
fppkat	= 0l
s = call_external(libkk, 'kkidl', 'kkEQIpka', $
	ier, exp, diag, shot, edition, tsf, $
	Ipi, fppkat )
if (ier ne 0) then begin
	print, 'Error reading plasma current!'
endif




g = { $
	shot:	shot, $
	time:	double(tsf*1000.), $
	error:	0l, $
	source:	source, $
	ecase:	ecase, $

	mw:	MW, $
	mh:	MH, $
	xdim:	Ri[n_elements(Ri)-1]-Ri[0], $	;max(R,min=min)-min, $
	zdim:	Zj[n_elements(Zj)-1]-Zj[0], $	;max(Z,min=min)-min, $
	rzero:	R_0, $	; major radius for design value of the machine
	rgrid1:	Ri[0], $
	zmid:	(Zj[n_elements(Zj)-1]+Zj[0])/2, $
	rmaxis:	Rmag, $
	zmaxis:	Zmag, $
	ssimag:	-ssimag/2/!pi, $	;PFL[n_elements(PFL)-1], $
	ssibry:	-ssibry/2/!pi, $	;PFL[0], $
	bcentr:	Bt, $	; on design radius 1.65m from AUG
	cpasma:	Ipi, $	; directly from FPP

	fpol:	reverse(F), $
	pres:	reverse(pres), $
	ffprim:	-reverse(FFP), $	; ======================= sign correct ???
	pprime:	-reverse(ppres), $	; ======================= sign correct ???
	psirz:	-PFM/2/!pi, $
	qpsi:	abs(reverse(Qpl)), $

;	nbdry:	nxy, $	; AUG equilibria have a closed boundary !!!
	nbdry:	nxy-1, $
	limitr:	ndim_all, $ $	;ndim, $
	bdry:	transpose([[xysp[0:nxy-2,0]],[xysp[0:nxy-2,1]]]), $
;	bdry:	transpose([[xysp[0:nxy-1,0]],[xysp[0:nxy-1,1]]]), $
;	bdry:	transpose([[xysp[*,0]],[xysp[*,1]]]), $
;	lim:	transpose([[xyGC[0:ndim_all-1,0]],[xyGC[0:ndim_all-1,1]]]), $
	lim:	lim, $

	r:	Ri, $
	z:	Zj, $
;	rhovn:	(PFL[n_elements(PFL)-1]-reverse(PFL))/(PFL[n_elements(PFL)-1]-PFL[0]), $
;	rhovn:	sqrt((ssimag-reverse(PFL))/(ssimag-ssibry)), $	; rho_pol = sqrt(psi_n)
	rhovn:	sqrt(psi_n), $	; rho_pol = sqrt(psi_n)
	epoten:	fltarr(MR), $	; =======================

	; AUG extensions start here:
	rtime:	float(tsf), $	; time from sfh
	mr:	MR, $		; number of radial points
	psi:	reverse(PFL), $	; poloidal flux
	jpol:	reverse(Jpol), $
	jpolp:	reverse(Jpolp), $

	fppkat:	fppkat, $
	rxp:	R_xp, $
	zxp:	Z_xp, $
	ssilim:	ssilim, $
	rlim:	R_lim, $
	zlim:	Z_lim, $
	nlim:	nxy_lim, $
;	lim_sf:	transpose([[xylim[0:nxy_lim-1,0]],[xylim[0:nxy_lim-1,1]]]), $
	lim_sf:	transpose([[xylim[*,0]],[xylim[*,1]]]), $

	; AUG vessel structure:
	xyGC:	xyGC, $
	NdGC:	NdGC, $
	NGC:	NGC, $
	ixbeg:	ixbeg, $
	lenix:	lenix, $
	valix:	valix, $
	GCnam:	GCnam $
}


if (keyword_set(plot)) then plot_g,g

; concatenate different time points:
if (i eq 0) then begin
	my_g = [g]
endif else begin
	my_g = [my_g,g]
endelse

endfor


if (keyword_set(stop)) then stop

return,my_g
end


