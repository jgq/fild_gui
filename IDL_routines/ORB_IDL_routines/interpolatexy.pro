function interpolatexy,x,y,z,xu,yu,_extra=_extra

;takes in vectors of x and y coordinates for z(x,y) data set
;interpolates z onto (xu,yu) coordinates has same keywords as 
;interpolate


indx=sinterpol(indgen(n_elements(x)),x,xu,sortt=1)
indy=sinterpol(indgen(n_elements(y)),y,yu,sortt=1)

int=interpolate(z,indx,indy,_extra=_extra)

return,int

end
