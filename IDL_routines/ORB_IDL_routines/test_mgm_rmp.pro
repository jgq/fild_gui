PRO test_mgm_rmp

device,decompose=0
loadct,5

checkfull=1

g = readg(28061,1.615)

;g = readg('/u/mgmunoz/g28061.00129_x129')

;g = readg('/u/mgmunoz/idl/EQUIL/g28061.01615_x513')

;g = readg('/u/mgmunoz/idl/EQUIL/g028061.01615')

;g = readg('/u/mgmunoz/idl/EQUIL/g028061.01615')

;g = readg('/u/vanzee/idl/ORBM/ORB_GC/g028104.02000')

g = readg('/u/mgmunoz/idl/EQUIL/TARDINI/28061.eqdsk')

fild_rpz=[2.17,1.32,0.34]

pit = .9

energy = 1e6

nmin=20




ngrr = 20

;Rrange=[1.6,2.2] ; R range

;zrange=[0.,0.9] ; z range

Rinit = 2.14

zinit = 0.2

nr = 20

orb_gc_aug,g,[Rinit,0.,zinit],-0.9,100, INPUTSATGC=0, mm=2,checkfull=1,orbinteg=0,plott=0,printinfo=0,outs=outs

rgc = outs.rcont0(*)
zgc = outs.zcont0(*)

contour,g.psirz,g.r,g.z,/noerase,xrange=xr,yrange=yr,/iso,xstyle=1,ystyle=1,xtitle='R (m)',ytitle='z (m)' $
     ,/fill,nlevels=50
;plot,g.lim(0,*),g.lim(1,*),/iso,xrange=xr,yrange=yr,xstyle=1,ystyle=1,xtitle='R (m)',ytitle='z (m)',charsize=cz,/nodata
drlim=((g.lim(0,*)-shift(g.lim(0,*),1))^2+(g.lim(1,*)-shift(g.lim(1,*),1))^2)^.5
wglim=where(drlim gt 0.5,ncc)

if ncc gt 2 then begin   ;asdex limiter is defined in noncontiguous pieces so plotting each individually
for i=1,ncc-1 do begin

oplot,g.lim(0,wglim(i-1):wglim(i)-1),g.lim(1,wglim(i-1):wglim(i)-1)

endfor
endif


rbdry=where(g.bdry(0,*) gt 1.)

; separatrix
oplot,g.bdry(0,rbdry),g.bdry(1,rbdry),thick=2,color=100  ;this defines the surface


; plots the trajectory
oplot,rgc,zgc,color=200

if checkfull gt 0 and n_elements(fout) gt 1 then begin
window,1
plot,fout(3,*),fout(5,*),thick=.5
endif


window,2

contour,-1*g.psirz,g.r,g.z,/noerase,xrange=[0.9,2.6],yrange=[-1.5,1.5],/iso,xstyle=1,ystyle=1,xtitle='R (m)',ytitle='z (m)' $
   ,/fill,nlevels=50



oplot,outs.fout(3,*),outs.fout(5,*),thick=.5

oplot,g.bdry(0,rbdry),g.bdry(1,rbdry),thick=2,color=100  ;this defines the surface

; plots the trajectory

oplot,rgc,zgc,color=200,thick=2

for i=1,ncc-1 do begin
oplot,g.lim(0,wglim(i-1):wglim(i)-1),g.lim(1,wglim(i-1):wglim(i)-1)
endfor

print,'llega al final'

END
