
nnrad=20     ;number of radial points
;40x40x40 took 18715 s on venus w/ orbit integration

ner=50     ;number of energy points
npit=50    ;number of pitch points
phi0=0.0   ;initial toroidal angle
z0=0.0    ;initial z position
orbinteg=1   ;will integrate orbits to get transit frequencies

radarr=findgen(nnrad)/(nnrad-1.)*(2.25-1.7)+1.7  ;initial major radius position array
energyarr=findgen(ner)/(ner-1.)*70.+10.       ;energy array
pitcharr=findgen(npit)/(npit-1.)*1.85-1.85/2.	    ;pitch array - dies on abs(pitch)~>0.96
orbcarr=intarr(nnrad,ner,npit)    ;will have orbit classification
orbcarr_i=intarr(nnrad,ner,npit)  ;will have orbit classification based on integrated orbit= less reliable because
    	    	    	    	    ;it croaks on unconfined orbits
tangarr=fltarr(nnrad,ner,npit)  ;will have the toroidal transit frequency (rad/s)
pangarr=tangarr     	    ;will have the poloidal transit frequency (rad/s)
filddistarr=tangarr 	    ;will have the closest dist. to fild from contour alone
fildpitcharr=tangarr	    ;will have pitch at point of closest approach to fild
filddistarr_i=tangarr 	    ;will have the closest dist. to fild from contour alone
fildpitcharr_i=tangarr	    ;will have pitch at point of closest approach to fild

errorarr=pangarr    	;will have either 0=good 1=error
phi0arr=tangarr     	;original toroidal canonical angular momentum
mu0arr=tangarr	    ;original magnetic moment

g=readg('/u/vanzee/TRANSP/142111/TRY1/HRSEFITS/g142111.00325_x257')

t0=systime(1)
for kr=0,nnrad-1 do begin
print, kr,' ',nnrad-1
for ii=0,ner-1 do begin

print,ii,' ',ner-1
for jj=0,npit-1 do begin


ppin=pitcharr(jj)
enerin=energyarr(ii)
orb_gc_mv,g,[radarr(kr),phi0,z0],ppin,enerin,plot=0,$
gci=gci,outs=outs,printinfo=0,orbinteg=orbinteg,nmin=10,skip_lostint=1
;took 114 s w plotting vs 109 with no plotting=negligble overhead

orbcarr(kr,ii,jj)=outs.grossclass 
errorarr(kr,ii,jj)=outs.error
filddistarr(kr,ii,jj)=outs.dfild
fildpitcharr(kr,ii,jj)=outs.pitch_dfild
phi0arr(kr,ii,jj)=outs.pphi0
mu0arr(kr,ii,jj)=outs.mu0

if orbinteg gt 0 then begin
tangarr(kr,ii,jj)=outs.omegatoroidal ;fltarr(ner,npit)
pangarr(kr,ii,jj)=outs.omegapoloidal ;=tangarr
filddistarr_i(kr,ii,jj)=outs.dfild_i
fildpitcharr_i(kr,ii,jj)=outs.pitch_dfild_i
orbcarr_i(kr,ii,jj)=outs.grossclass_i
endif


endfor
endfor
endfor
t1=systime(1)
print,'IT TOOK: ',t1-t0,' s'


;
end
