g=readg(142111,525,runid='EFIT04')
pit=.6 ;.62 ;.7 ;0.55
e0=60.

rr0=2.05
zz0=g.zmaxis
vel2u=calculate_pitchv(g,rr0,zz0,pit)
step=.01

sys0=systime(1)
orbm_adams,g,[rr0,0,zz0],vel2u,rv,e0=e0,nsteps=nsteps,step=step,epsabs=1.E-6,$
epsrel=1.E-5,timestep_s=timestep_s,omega=omega,v0=v0,z=1,hstep=hstep ;, /time_reverse
print,systime(1)-sys0

end
