;Just a copy of calculate_bfield but with added f, btot,gradients, etc.

;function get_gcinputs,g,driftterms=driftterms

driftterms=1
;if n_elements(driftterms) lt 1 then driftterms=0  ;will return
;all curvature and gradient drift terms

mw=g.mw & mh=g.mh
bp=fltarr(mw,mh) & bt=fltarr(mw,mh) & br=fltarr(mw,mh) & bz=fltarr(mw,mh)
dpsidx = fltarr(mw,mh)
dpsidy = fltarr(mw,mh)

; calculate vertical derivative of psi
for i = 0,mw-1 do begin
 dpsidy(i,*) = Deriv(g.z(0:mh-1),g.psirz(i,0:mh-1))
endfor

; calculate horizontal derivative of psi
for j = 0,mh-1 do begin
  dpsidx(*,j) = Deriv(g.r(0:mw-1),g.psirz(0:mw-1,j))
endfor

; calculate array of Br, Bz, and Bp
for j = 0,mh-1 do begin
   br(*,j) = dpsidy(0:mw-1,j)/g.r(0:mw-1)
   bz(*,j) = -dpsidx(0:mw-1,j)/g.r(0:mw-1)
endfor
bp = sqrt(br*br+bz*bz)

; WWH get right sign
if g.cpasma lt 0. then begin
  br=-br & bz=-bz
end

; Calculate toroidal field
; Original coding was from gfield.for by Peter Politzer,
;   translated to IDL by Gary Porter (see BFIELD.PRO).
; The code below has be optimized for IDL by Michael D. Brown, 2/16/94

dpsi = (g.ssibry-g.ssimag)/float(mw-1)
; first order Bt value.
for j=0,mh-1 do bt(0:mw-1,j)=g.bcentr*g.rzero/g.r(0:mw-1)  
k = long((g.psirz - g.ssimag)/dpsi) 
iw=where(k ge 0 and k lt mw-1,n)  ; 1-d indexes where k is a valid index.
if n gt 0 then begin
  iwr = iw mod mw  ; map matrix 1-d selected indexes to an refit row index.
  bt(iw) = ( g.fpol(k(iw))+(g.fpol(k(iw)+1)-g.fpol(k(iw)))* $
             (g.psirz(iw)-(k(iw)*dpsi+g.ssimag))/dpsi ) / g.r(iwr)
endif


;below here is the only part that is dfft from calculate_bfield used in the orb code

btot=(bp^2+bt^2)^.5
ngf=n_elements(g.fpol)
psigf=findgen(ngf)/(ngf-1.)*(g.ssibry-g.ssimag)+g.ssimag
frz=interpol(g.fpol,psigf,g.psirz)
;psig=g.psirz



ucurvr=-1
ucurvt=-1
ucurvz=-1
ugradr=-1
ugradt=-1
ugradz=-1

rarr=bp*0
zarr=rarr
for i=0,mw-1 do begin
rarr(i,*)=g.r(i)
zarr(i,*)=g.z
endfor



;spline alpha(psi) onto psirz grid
;alpha = alpha(psi)*cos(-m theta + n phi - wt - phz)
;
;will probably need high res grid for most m

psiinp=indgen(51)/50.
alphapsi=exp(-(psiinp-.5)^2/.1^2)
modem=12.
moden=4.
phi0=0.
wwtt=0.
equalarc=0  ;if =1 then equalarc otherwise boozer/pest angle


psirzn=g.psirz
psirzn=psirzn+sign(g.ssimag)*g.ssimag ;(max(psirzn)-min(psirzn))   ;*(g.ssibry-g.ssimag)+g.ssimag
psirzn=psirzn/(g.ssibry-g.ssimag)
alpharz=interpol(alphapsi,psiinp,psirzn)
wgt=where(psirzn gt 1.0)
alpharz(wgt)=0.
pa=get_efit_pestangle(g)

if equalarc gt 0 then begin
thetarpzp=pa.thetageorz
endif else begin
thetarpzp=pa.thetapestrz
endelse

;thetarpzp=atan(smooth(sin(thetarpzp),10),smooth(cos(thetarpzp),10))
rpest=pa.rtheta
zpest=pa.ztheta
psipest=pa.psipest+sign(g.ssimag)*g.ssimag
psipest=psipest/(g.ssibry-g.ssimag)
alphapest=interpol(alphapsi,psiinp,psipest)
brpest=psipest*0.
bzpest=psipest*0.
btpest=psipest*0.

nrpest=n_elements(rpest)
nzpest=n_elements(zpest)
for i=0,nrpest-1 do begin
brpest(i,*)=interpolatexy(g.r,g.z,br,rpest(i)+zpest*0.,zpest)
bzpest(i,*)=interpolatexy(g.r,g.z,bz,rpest(i)+zpest*0.,zpest)
btpest(i,*)=interpolatexy(g.r,g.z,bt,rpest(i)+zpest*0.,zpest)
endfor


btotpest=(brpest^2.+btpest^2.+bzpest^2.)^.5
alco=alphapest*cos(-modem*thetarpzp+moden*phi0-wwtt)

alsi=alphapest*sin(-modem*thetarpzp+moden*phi0-wwtt)

dtalp=psipest*0.
dzalp90=psipest*0.
dtalp90=psipest*0.
dralp90=psipest*0.
dzalp=dtalp
dzbtp=dtalp
dzbrp=dtalp
dralp=dtalp
drbzp=dtalp
drbtp=dtalp
dtbrp=dtalp
rpesta=dtalp

dtalp=-alsi*moden
dtalp90=alco*moden

for i=0,nrpest-1 do begin
dzbrp(i,*)=deriv(zpest,brpest(i,*))
dzbtp(i,*)=deriv(zpest,btpest(i,*))
dzalp(i,*)=deriv(zpest,alco(i,*))
dzalp90(i,*)=deriv(zpest,alsi(i,*))
endfor
for i=0,nzpest-1 do begin
drbtp(*,i)=deriv(rpest,btpest(*,i))
drbzp(*,i)=deriv(rpest,bzpest(*,i))
dralp(*,i)=deriv(rpest,alco(*,i))
dralp90(*,i)=deriv(rpest,alsi(*,i))

rpesta(*,i)=rpest
endfor

;delta b - major radius
dbr=1./rpesta*(dtalp*bzpest)-(dzalp*btpest+alco*dzbtp)
dbt=dzalp*brpest+alco*dzbrp-(dralp*bzpest+alco*drbzp)
dbz=1./rpesta*(alco*btpest+rpesta*dralp*btpest+rpesta*alco*drbtp)-1./rpesta*(dtalp*brpest)

dbr90=1./rpesta*(dtalp90*bzpest)-(dzalp90*btpest+alsi*dzbtp)
dbt90=dzalp90*brpest+alsi*dzbrp-(dralp90*bzpest+alsi*drbzp)
dbz90=1./rpesta*(alsi*btpest+rpesta*dralp90*btpest+rpesta*alsi*drbtp)-1./rpesta*(dtalp90*brpest)


outs={bp:bp,br:br,bt:bt,bz:bz,g:g,btot:btot,frz:frz,psirz:g.psirz,$
ucurvr:ucurvr,ucurvt:ucurvt,ucurvz:ucurvz,ugradr:ugradr,$
ugradt:ugradt,ugradz:ugradz,rarr:rarr,zarr:zarr}

;return,outs
end
