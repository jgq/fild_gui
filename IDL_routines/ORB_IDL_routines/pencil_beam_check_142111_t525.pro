dir0='/u/vanzee/TRANSP/142111/TRY1/'   ;where profile fits are can change call to fida_atten_wz to use zipfits
shot=142111	              ;basically need all profiles
time=525

widthbeam=(8.8*!pi^.5)
heightbeam=(21.1*!pi^.5) ;
beamarea=widthbeam*heightbeam ; ;in cm^2

nr=50  ;number of radii u want

rrang=[1.15,2.35]    ;Range in R major along beam


rmaj=indgen(nr)/(nr-1.)*(rrang(1)-rrang(0))+rrang(0)

 pini=rmaj*0+2  ;0=30L
 ;********pini fixes what beam you look at
;  [30L,30R,150L,150R,210L,210R,330L,330R], e.g., [0,1,2,3,4,5,6,7]
;  

;fixnbv=fltarr(8)+80.E3

 ;nb_total=fida_atten_wz(shot,time,rmaj,pini,path=dir0,nb_all=nb_all,locprofs=locprofs,$
 ;zloc=0.0,nb_atten=nb_atten,fixnbvolts=fixnbv)/beamarea/1.E-4   ;nb_total is the total density of neutrals at each rmaj
nb_total=fida_atten_wz(shot,time,rmaj,pini,path=dir0,nb_all=nb_all,locprofs=locprofs,$
 zloc=0.0,nb_atten=nb_atten)/beamarea/1.E-4   ;nb_total is the total density of neutrals at each rmaj

 ;zloc lets u do off midplane horizontal lines
 
 nb_all=nb_all*1.E-6/(beamarea*1.E-4)  ;nb_all is the density of full, half, and third vs rmaj = (nr,3) size array
 
 ll=2
 nb_tot=total(nb_all,2)
 th=2
 plot,rmaj*100.,nb_tot,linestyle=ll,xtitle='Distance from R=2.35 m',ytitle='N in 7.5 cm of Centerline',thick=th
 oplot,rmaj*100.,nb_all(*,0),linestyle=ll,color=100,thick=th
 oplot,rmaj*100.,nb_all(*,1),linestyle=ll,color=150,thick=th
 oplot,rmaj*100.,nb_all(*,2),linestyle=ll,color=45,thick=th
 ;oplot,rmaj*100.,nb_atten(*,3),linestyle=ll
 
 dlt=where(tt lt 170.)
 scc=max(nb_tot)/max(npray)
 oplot,rvals(dlt),npray*scc  
oplot,rvals(dlt),nprayf*scc,color=100
 oplot,rvals(dlt),nprayt*scc,color=45
oplot,rvals(dlt),nprayh*scc,color=150
 
 end
