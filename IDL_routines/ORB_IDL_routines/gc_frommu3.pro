
;example lost part = 80.,0.62,2.23,-.695 to fild
energy=60. ;kev
pitch0=0.41955 ;  ;v_||/v   0.7,0.65,.6,.55,.5,.4,.3,.2
mm=2.  ;proton masses
z=1.  ;charge in proton chg ;can be negative
R0i=2.23 ;23
Z0i=-0.10

orbinteg=1  ;to integrate orbit or not
driftterms=1  ;do drift orbit calculation

topp=1
if topp gt 0 then begin
;g=readg(142111,525,runid='EFIT04')
g=readg('/u/vanzee/idl/lib/LINETRACE/g142111.00525_e257') ;129,257,65

gci=get_gcinputs(g,driftterms=driftterms)
gci.psirz=gci.psirz

endif

ee=1.60E-19
mi=1.67E-27
ejou=energy*ee*1.E3
v0=(ejou/mi/mm*2.)^.5

;to find gyrocenter and larmor radius
;****************************************************************************************

brp0=interpolatexy(g.r,g.z,gci.br,r0i,z0i)
btp0=interpolatexy(g.r,g.z,gci.bt,r0i,z0i)
bzp0=interpolatexy(g.r,g.z,gci.bz,r0i,z0i)

bunit0=[brp0,btp0,bzp0]/(brp0^2.+btp0^2.+bzp0^2.)^.5
bunitperp=[bzp0-btp0,brp0-bzp0,btp0-brp0]
bunitperp=bunitperp/(total(bunitperp^2.))^.5

;pitch=.75 ;defined as v_par/vtot, where we'll say
sipitch=1.
if g.bcentr*g.cpasma lt 0 then sipitch=-1.

vtot0=1.
vpar=(vtot0*pitch0*sipitch)
vperp=(vtot0^2.-vpar^2.)^.5

vel2ui=vpar*bunit0+vperp*bunitperp

vxbi=crossp(vel2ui/abs(vtot0),bunit0)  ;vxb unit
bveci=[brp0,btp0,bzp0]   ;bfield vector at r0i,z0i

b0i=total(bveci^2)^.5

wci0i=ee*z*b0i/mi/mm   ;9.58E3*z/mm*b0i*1.E4  ;check mi used here
rli0i=v0*(1.-pitch0^2.)/wci0i

rlvec=vxbi*rli0i*z/abs(z)   ;larmor radius vector from particle position to guiding center

;make the new r0,z0 at the gyrocenter to calculate mu and momentum
r0=rlvec(0)+r0i
z0=rlvec(2)+z0i
;****************************************************************************************



psi0=interpolatexy(g.r,g.z,gci.psirz,r0,z0)  ;now evaluate psi0, f0, and b0 at GC
f0=interpolatexy(g.r,g.z,gci.frz,r0,z0)
b0=interpolatexy(g.r,g.z,gci.btot,r0,z0)

wci0=ee*z*b0/mi/mm   ;9.58E3*z/mm*b0*1.E4
rli0=v0*(1.-pitch0^2.)/wci0

print,'R_li0=',rli0

nsmrli=fix(rli0*2./avg(deriv(g.r))+.5)

mu0=ejou/b0*(1.-pitch0^2.)  ;original magnetic moment

signfud=-1. ;dont yet know for sure about this -1.
;this is the initial toroidal canonical angular momentum
;in straight field line coordinates
pphi0=psi0-signfud*f0*pitch0/z/ee/b0*(2.*mm*mi*ejou)^.5 ;


pitchrz=-(pphi0-gci.psirz)/(gci.frz/z/ee/gci.btot*(2.*mm*mi*ejou)^.5)
;this is the pitch at all r,z that have same canonical ang. momentum
;and energy

murz=ejou/gci.btot*(1.-pitchrz^2.)/mu0 ;magnetic moments from pitchrz
;not all possible though because murz must = mu0

vparrz=pitchrz*v0
vperprz=(v0^2.-vparrz^2.)^.5
vpartrz=vparrz*gci.bt/gci.btot
vparrrz=vparrz*gci.br/gci.btot
vparzrz=vparrz*gci.bz/gci.btot

ucurvr=gci.ucurvr*mm*mi*vparrz^2/z/ee
ucurvt=gci.ucurvt*mm*mi*vparrz^2/z/ee
ucurvz=gci.ucurvz*mm*mi*vparrz^2/z/ee

ugradr=gci.ugradr*mm*mi*vperprz^2/2/z/ee
ugradt=gci.ugradt*mm*mi*vperprz^2/2/z/ee
ugradz=gci.ugradz*mm*mi*vperprz^2/2/z/ee

srz=1
sdc=1
vrdrift=sdc*(ucurvr+ugradr)+vparrrz*srz
vzdrift=sdc*(ucurvz+ugradz)+vparzrz*srz
vpdrift=(vrdrift^2+vzdrift^2)^.5
vtdrift=sdc*(ucurvt+ugradt)+vpartrz*srz

ng1=where(abs(pitchrz) gt 1.0)
pitchrzgt1=pitchrz
pitchrzgt1(ng1)=999.   ;set all unphysical pitches to 999.



;Plotting below here
;***********************************************************************************
!p.multi=0

if orbinteg then begin
;vel2u=calculate_pitchv(g,r0,z0,pitch0)
vel2u=vel2ui ;not sure which yet
;orbm_adams,g,[r0i,0,z0i],vel2u,rv0,e0=energy,nsteps=10.E3,time_reverse=0,$
;epsabs=1.E-8,epsrel=1.E-8
endif

if driftterms gt 0 then !p.multi=[0,2,1]

cz=1.5
yr=[-1.3,1.3]
xr=[.8,2.37]
cc=254
;shade_surf,shhs*0,g.r,g.z,ax=90,az=0,shades=shhs,$
;max_value=254,color=254,xtitle='R (m)',ytitle='z (m)',$
;yrange=yr,xrange=xr,xstyle=1,ystyle=1,charsize=cz

plot,g.lim(0,*),g.lim(1,*),/iso,xrange=xr,yrange=yr,xstyle=1,ystyle=1,xtitle='R (m)',ytitle='z (m)',charsize=cz



if orbinteg then begin
nds=0
if nds gt 0 then begin
oplot,dsamp(rv0(3,*),nds),dsamp(rv0(5,*),nds),color=100,thick=.5
endif else begin
loadct,4
oplot,rv0(3,*),rv0(5,*),color=100,thick=.5
endelse
endif

loadct,5

oplot,g.bdry(0,*),g.bdry(1,*),thick=2,color=100  ;this defines the surface
;where it would reflect if trapped, i.e. v|| = 0
plots,r0,z0,psym=4,thick=2,color=100
plots,g.rmaxis,g.zmaxis,psym=4,thick=3,symsize=1,color=45
contour,gci.psirz,g.r,g.z,levels=psi0,color=cc,c_linestyle=2,$
thick=2,/overplot,$
yrange=yr,xrange=xr,xstyle=1,ystyle=1 ;this is the 
;original flux surface
contour,murz,g.r,g.z,levels=1.0,color=cc,c_linestyle=3,thick=2,/overplot,$  ;scaled murz to mu0
yrange=yr,xrange=xr,xstyle=1,ystyle=1 ,path_xy=paths,path_data_coords=1,$
path_info=pathinf  ,closed=0



;DETERMINING ORBIT INFO FROM CONTOUR OUTPUT
;*****************************************************************************************************
nct=n_elements(paths(0,*))
rcont=reform(paths(0,0:nct-1)) ;could have be double valued in mu0..
zcont=reform(paths(1,0:nct-1))
ncont=n_elements(pathinf.level) ;number of contours drawn
ind0=0
pioff=pathinf.offset
pin=pathinf.n
ptyp=pathinf.type

if ncont gt 1 then begin
temp=min((rcont-r0)^2.+(zcont-z0)^2,mind)  ;finding contour that goes through initial gc location
indemp=mind-pioff
ind0=max(where(indemp ge 0))
endif

ii0=pioff(ind0)
ii1=ii0+pin(ind0)-1

;oplot,rcont,zcont,psym=5,color=45
rcont0=rcont(ii0:ii1)
zcont0=zcont(ii0:ii1)

temp=min((rcont0-r0)^2.+(zcont0-z0)^2,mind0) ;contour doesnt always start at r0,z0
rcont0=shift(rcont0,-mind0)
zcont0=shift(zcont0,-mind0)
nc0=n_elements(rcont0)
tempr0=rcont0
tempz0=zcont0
for i=0,nc0-1 do begin
rcont0(i)=tempr0(nc0-1-i)
zcont0(i)=tempz0(nc0-1-i)
endfor

nsrc=0
rcont0=smooth(rcont0,nsrc,/edge_truncate)
zcont0=smooth(zcont0,nsrc,/edge_truncate)


oplot,rcont0,zcont0,psym=-4,symsize=.5
plots,r0,z0,psym=4,thick=2,color=100

;thet=atan(rcont0-g.rmaxis,zcont0-g.zmaxis)
;thet=phunwrap(thet)
;dthet=thet-thet(1:*)
;encir=total(thet(1:*)*dthet)
;print,encir

;CALCULATING TOROIDAL POSITION
dtrz=rcont0*0.
dlphi=dtrz
if driftterms gt 0 then begin
updri0=interpolatexy(g.r,g.z,vpdrift,rcont0,zcont0)
utdri0=interpolatexy(g.r,g.z,vtdrift,rcont0,zcont0)
vrdri0=interpolatexy(g.r,g.z,vrdrift,rcont0,zcont0)
vzdri0=interpolatexy(g.r,g.z,vzdrift,rcont0,zcont0)
vzav0=.5*(vzdri0(1:*)+vzdri0(0:*))
vrav0=.5*(vrdri0(1:*)+vrdri0(0:*))
cvecr0=rcont0(1:*)-rcont0(0:*)
cvecz0=zcont0(1:*)-zcont0(0:*)

upav0=.5*(updri0(1:*)+updri0(0:*))
utav0=.5*(utdri0(1:*)+utdri0(0:*))
rav0=(rcont0(1:*)+rcont0(0:*))*.5
pathrz=( (rcont0(1:*)-rcont0(0:*))^2+(zcont0(1:*)-zcont0(0:*))^2 )^.5
dtrz(0:n_elements(dtrz)-2)=pathrz/upav0
dlphi(0:n_elements(dtrz)-2)=0.5*utav0*dtrz
phitor=total(dlphi/rav0,/cum)
phitor=phitor-phitor(0)
time=total(dtrz,/cum)

plot,rav0*cos(phitor),rav0*sin(phitor),psym=4,xtitle='x(m)',ytitle='y(m)',charsize=cz,/iso,$
xrange=[-2.5,2.5],yrange=[-2.5,2.5],ystyle=1,xstyle=1,symsize=.25
plots,rav0(0)*cos(phitor(0)),rav0(0)*sin(phitor(0)),psym=5,color=100
endif


if orbinteg then begin
if nds gt 0 then begin
oplot,dsamp(rv0(3,*)*cos(rv0(4,*)),nds),dsamp(rv0(3,*)*sin(rv0(4,*)),nds),color=100,thick=.5
endif else begin
loadct,4
oplot,rv0(3,*)*cos(rv0(4,*)),rv0(3,*)*sin(rv0(4,*)),color=100,thick=.5
endelse
plots,rv0(3,0)*cos(rv0(4,0)),rv0(3,0)*sin(rv0(4,0)),psym=4,color=45
endif




;

if ptyp(ind0) then begin   ;checking whether its closed or not using pathinf.type
print,'CLOSED ORBIT'
endif else begin
print,'OPEN ORBIT HITS WALL'
endelse
;Before finding transit times etc. need to determine lost, trapped, and if contour is more than one enclosed region
;*****************************************************************************************************

end
