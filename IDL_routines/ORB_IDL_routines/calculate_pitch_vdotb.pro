function calculate_pitch_vdotb,g,rr0,zz0,velr,velphi,velz;(where vel is (r,phi,z)   ;;,pitch
;Given a g file and r,z location and r,phi,z vel it calcualtes the 
; pitch

;g=readg(122117,365.,runid='EFIT04')
;rr0=2.
;zz0=

nxx=n_elements(rr0)

calculate_bfield,bp,br,bt,bz,g


;pitch=.75 ;defined as v_par/vtot, where we'll say
sipitch=1.
if g.bcentr*g.cpasma lt 0 then sipitch=-1.

pitch=fltarr(nxx)
for i=0,nxx-1 do begin

brp0=interp2d(br,g.r,g.z,rr0(i),zz0(i))
btp0=interp2d(bt,g.r,g.z,rr0(i),zz0(i))
bzp0=interp2d(bz,g.r,g.z,rr0(i),zz0(i))


bunit0=[brp0,btp0,bzp0]/(brp0^2.+btp0^2.+bzp0^2.)^.5
bunitperp=[bzp0-btp0,brp0-bzp0,btp0-brp0]
bunitperp=bunitperp/(total(bunitperp^2.))^.5

vel=[velr(i),velphi(i),velz(i)]   ;
vpar=vel*bunit0  
;vperp=(vtot0^2.-vpar^2.)^.5
pitch(i)=total(vpar*sipitch)/total(vel^2)^.5*sipitch
endfor

;print,'pitch=',pitch
;print,'vr=',vel(0),' vtor=',vel(1),' vz',vel(2)
;print, 'Ang w/ B=',acos(total(vel/abs(vtot0)*bunit0))*180./!pi,' Degrees'
;print,'V0 dot B is:',total(vel/abs(vtot0)*bunit0)

return,pitch
end
