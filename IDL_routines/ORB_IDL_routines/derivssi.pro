FUNCTION derivssi,x,y
; r0, y(3:5) have units of meters
; y(0:2) are the velocities in units of meters
; x is the dimensionless time Omega*t
; b is the field in units of Tesla/(rad/sec)

b=magfld(y(3:5))
qovm=double(1.6/1.67E-8/2.)

;return,[y(1)^2/y(3)+y(1)*b(2)-y(2)*b(1), $
;  -y(0)*y(1)/y(3)+y(2)*b(0)-y(0)*b(2), $
;  y(0)*b(1)-y(1)*b(0), $
;  y(0), $
;  y(1)/y(3), $
;  y(2)]
return,[(y(1)^2*y(3)+(y(3)*y(1)*b(2)-y(2)*b(1)))*qovm, $
  (-2.*y(0)*y(1)/y(3)+(y(2)*b(0)-y(0)*b(2))/y(3))*qovm, $
  (y(0)*b(1)-y(1)*b(0)/y(3))*qovm, $
  y(0), $
  y(1)*y(3), $
  y(2)]

;return,[y(1)^2/y(3)+y(1)*bz-y(2)*bp, $
;  -y(0)*y(1)/y(3)+y(2)*br-y(0)*bz, $
;  y(0)*bp-y(1)*br, $
;  y(0), $
;  y(1)/y(3), $
;  y(2)]

end
