PRO orbm,g,InitialPosition,ivel,randv, $
      nsteps=nsteps,step=step,time_reverse=time_reverse, $
      e0=e0,amu=amu,z=z, $
      chk_conserved=chk_conserved,detail=detail,hc=hc, $
      orb_dat=orb_dat,econs=econs,epsrel=epsrel,epsabs=epsabs,onlyplot=onlyplot,omega=omega,timestep=h

; Cleaned up (a bit) on 6/1/2007 by WWH
;randv is dr,dphi,dz,r,phi,z
; WWH 4/6/2001
; SI units
; Program to calculate fast-ion orbits in efit equilibrium using rk4
;
; INPUTS
;	InitialPosition	  [r,phi,z] vector (m)
;	ivel		[v_r,v_phi,v_z] initial vector (a.u.)
;	g		eqdsk saved as idl structure

; OUTPUT	elevation and plan view plots
; KEYWORDS
;	nsteps	orbit length is step*nsteps (m)
;	step	how often to record orbit coordinates (m)
;		(not used in actual integration)
;   time_reverse    set to run orbit backward in time
;	e0	particle energy (keV)--default is 80
;	amu	particle atomic mass (default=2)
;	z	particle charge (default=1)
;   chk_conserved  plots constants of motion
;   detail	plots detail of orbit near starting point
;   hc		writes postscript file of the graphs 
;   orb_dat	writes orbit coordinates to a local file
;		'orb.dat'
; yout(*,nactual) is the saved 6-vector coordinates of the orbit
; in cylindrical coordinates (v_r,v_phi,v_z,r,phi,z)

common bcom,b0,r0,br,bphi,bz,gr0,gz0,dr,dz

if keyword_set(hc) then begin
  set_plot,'ps'
  device,color=1
  fname='orb.ps'
  device,filename=fname
end

; Ion orbit parameters
if not keyword_set(nsteps) then nsteps=5000
if not keyword_set(step) then step=.01	; step length in m
if not keyword_set(e0) then e0=80.			; keV
if not keyword_set(amu) then amu=2. & mp=1.67e-27
if not keyword_set(z) then z=1.
if not keyword_set(time_reverse) then time_reverse=0

if n_elements(econs) lt 1 then econs=0

;InitialPosition=[2.15,0.,-.74]
;ivel=[.3,0.,-1.]

if not keyword_set(onlyplot) then onlyplot=0


; Use eqdsk to get wall location and magnetic field grid
b0=abs(g.bcentr) & r0=g.rmaxis
finewall,g,rwall,zwall
calculate_bfield,bp,br,bphi,bz,g
br=double(br) & bphi=double(bphi) & bz=double(bz)
gr0=double(g.r(0)) & gz0=double(g.z(0)) 
dr=double(g.r(1)-g.r(0)) & dz=double(g.z(1)-g.z(0))
br=double(br) & bphi=double(bphi) & bz=double(bz)
pphisgn=-g.cpasma/abs(g.cpasma)
qovm=double(1.6/1.67E-8/amu)

; Normalization constants
omega=z*1.6e-19*b0/(amu*mp)
v0=sqrt(2*e0*1.e3*1.6e-19/(amu*mp))
vconstant=v0;/omega
ivel=float(ivel)
Velocity=vconstant*ivel/sqrt(ivel(0)^2 + ivel(1)^2 + ivel(2)^2)

y=dblarr(6) & y(0:2)=Velocity & y(3:5)=InitialPosition
h=1./omega/10. ;step*omega/v0 & if time_reverse then h=-h
yout=dblarr(6,nsteps)
;dydx=derivssi(0.,y)
yout(*,0)=y(*)
;*********************************************************************************
;*********************************************************************************
; Main loop
;*********************************************************************************
;*********************************************************************************

nfeval=0L
tempnf=0
i=0L
lwall=1		; logical to stop if hits wall
;while i lt nsteps-1 and check_bdry(g,rwall,zwall,y(3),y(5)) do begin

if onlyplot le 0 then begin  ;checking to see if we only want to replot
while i lt nsteps-1 do begin

  i=long(i*1. + 1L)
  ;y=rk4(y,dydx,0.,h,'derivs',/double)
  ddeabm,'derivssi',0.,y,h,nfev=tempnf,epsabs=epsabs,epsrel=epsrel
 
 nfeval=nfeval+tempnf  ;how many times the 'derivs' has been evaluated
  ;pro DDEABM, DF, T, Y, TOUT0, PRIVATE
  
; force energy conservation
 if econs then   y(0:2)=y(0:2)*vconstant/sqrt(y(0)^2+y(1)^2+y(2)^2)  
 
  yout(*,i)=y(*)
 ; dydx=derivs(0.,y)
end

 print,'EPSABS, EPSREL'
 print,epsabs,epsrel
 
nactual=i
print,'Requested points and Actual evaluations required'
print,nactual+1L,nfeval

endif else begin
yout=randv
nactual=nsteps
endelse
;*********************************************************************************
;*********************************************************************************
; End Main loop
;*********************************************************************************
;*********************************************************************************

;-------------------------------
; The calculation is complete here.  Everything else is just plotting options

if keyword_set(orb_dat) then begin
  orbcoords=yout(*,0:nactual-1)
  save,filename='orb_dat',orbcoords
end

; Check conserved quantities
if keyword_set(chk_conserved) then begin
vsq=dblarr(nactual) & mu=dblarr(nactual) & pphi=dblarr(nactual)
for i=0,nactual-1 do begin
  vperpsq=yout(0,i)^2+yout(2,i)^2
  vphisq=yout(1,i)^2   ;***************this isn't right?...it needs rmajor ;mvz
  vsq(i)=vperpsq+vphisq
  b=magfld(yout(3:5,i))
  bmag=sqrt(b(0)^2+b(1)^2+b(2)^2)
  mu(i)=vperpsq/bmag   ;***************this isn't right...it isn't really vperp to b ;mvz
  pphi(i)=yout(3,i)*yout(1,i) + $
     pphisgn*find_psi(g,[yout(3,i)],[yout(5,i)])/b0
;  pphi(i)=yout(3,i)*yout(1,i)+abs(find_psi(g,[yout(3,i)],[yout(5,i)]))/b0
end
end

if keyword_set(chk_conserved) then begin
;window,0
!p.multi=[0,2,0]
; (R,z) elevation
contour,g.psirz,g.r,g.z,nlevels=10,color=155
oplot,g.lim(0,*),g.lim(1,*),color=100
oplot,yout(3,0:nactual-1),yout(5,0:nactual-1)
oplot,g.bdry(0,0:g.nbdry-1),g.bdry(1,0:g.nbdry-1),color=60

; (R,phi) plan
theta=2.*!pi*findgen(31)/30. 
plot,max(rwall)*cos(theta),max(rwall)*sin(theta),color=100
oplot,min(rwall)*cos(theta),min(rwall)*sin(theta),color=100
oplot,yout(3,0:nactual-1)*cos(yout(4,0:nactual-1)),$
      yout(3,0:nactual-1)*sin(yout(4,0:nactual-1))

;window,1
!p.multi=[0,3,0]

if vsq(0) ne 0 then plot,(vsq-vsq(0))/vsq(0),ytitle='Energy' else plot,vsq
if pphi(0) ne 0 then plot,(pphi-pphi(0))/pphi(0),ytitle='P_phi' else $
   plot,pphi
if mu(0) ne 0 then begin 
  plot,(mu-mu(0))/mu(0),ytitle='mu'
  if nactual gt 100 then oplot,(smooth(mu,101)-mu(0))/mu(0),color=100
end else begin
  plot,mu-mu(0)
  if nactual gt 100 then oplot,smooth(mu,101)-mu(0),color=100
end

end else begin
if not keyword_set(hc) then print,'hi'
!p.multi=[0,2,0]
; (R,z) elevation
if keyword_set(detail) then begin
  xmin=yout(3,0) - detail & xmax=yout(3,0) + detail
  ymin=yout(5,0) - detail & ymax=yout(5,0) + detail
end else begin
  xmin=min(g.r) & xmax=max(g.r) & ymin=min(g.z) & ymax=max(g.z)
end
contour,g.psirz,g.r,g.z,nlevels=10,color=155, $
  xrange=[xmin,xmax],yrange=[ymin,ymax]
oplot,g.lim(0,*),g.lim(1,*),color=100
oplot,yout(3,0:nactual-1),yout(5,0:nactual-1)
oplot,g.bdry(0,0:g.nbdry-1),g.bdry(1,0:g.nbdry-1),color=60

; (R,phi) plan
theta=2.*!pi*findgen(31)/30. 
if keyword_set(detail) then begin
  xmin=yout(3,0)*cos(yout(4,0)) - detail 
  xmax=yout(3,0)*cos(yout(4,0)) + detail
  ymin=yout(3,0)*sin(yout(4,0)) - detail  
  ymax=yout(3,0)*sin(yout(4,0)) + detail
end else begin
  xmin=-max(g.r) & xmax=max(g.r) & ymin=xmin & ymax=xmax
end
plot,max(rwall)*cos(theta),max(rwall)*sin(theta),color=100, $
  xrange=[-2.5,2.5],yrange=[-4,4],xstyle=1
  oplot,2.35*cos(theta),2.35*sin(theta),color=170
 
  
;  xrange=[xmin,xmax],yrange=[ymin,ymax]
oplot,min(rwall)*cos(theta),min(rwall)*sin(theta),color=100
oplot,yout(3,0:nactual-1)*cos(yout(4,0:nactual-1)),$
      yout(3,0:nactual-1)*sin(yout(4,0:nactual-1))
end
;print,'Rmax is:',max(rwall)

if keyword_set(hc) then begin
  !p.multi=[0,0,1]
  contour,g.psirz,g.r,g.z,nlevels=10
  oplot,g.lim(0,*),g.lim(1,*),color=100
  oplot,yout(3,0:nactual-1),yout(5,0:nactual-1)
  oplot,g.bdry(0,0:g.nbdry-1),g.bdry(1,0:g.nbdry-1)
;  plot,g.lim(0,*),g.lim(1,*),color=100,xrange=[2,2.3],yrange=[-.9,-.6]
;  oplot,yout(3,0:nactual-1),yout(5,0:nactual-1)
  device,/close
end

randv=yout

end
