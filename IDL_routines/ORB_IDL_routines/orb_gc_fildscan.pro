

ner=20
npit=20
phi0=0.0
z0=0.0

;fild_rtz=[2.25,0.,-.66]  ;r,phi (in radians),z of fild
fild_rtz=[2.27,0.,-.15]  ;r,phi (in radians),z of fild

energyarr=findgen(ner)/(ner-1.)*70.+10.
pitcharr=findgen(npit)/(npit-1.)*1.85-1.85/2.


g=readg('/u/vanzee/TRANSP/142111/TRY1/HRSEFITS/g142111.00525_x257')

t0=systime(1)

for ii=0,ner-1 do begin
for jj=0,npit-1 do begin

orb_gc,g,fild_rtz,pitcharr(jj),energyarr(ii),plot=1,$
gci=gci,outs=outs,printinfo=0,orbinteg=0,inputsatgc=1 ;,nmin=10
;took 114 s w plotting vs 109 with no plotting=negligble overhead

;outs.rcont0 and outs.zcont0 have the R,z positions along the contour so you 
;can make a big pointer array with all of those if you like for each energy and pitch
;You need a pointer because each energy and pitch are not always the same number of elements
;print, pitcharr(jj)
;wk=get_kbrd(1)
endfor
endfor
t1=systime(1)
print,'IT TOOK: ',t1-t0,' s'


;
end
