
;Will read in transp birth profiles
fils=['/u/vanzee/TRANSP/OUTPUTFILES/142111M18/142111M18_birth_1.out',$
'/u/vanzee/TRANSP/OUTPUTFILES/142111M18/142111M18_birth_7.out']

topp=1
if topp gt 0 then begin
plott=1
n2use=200000L
if n_elements(n2use) lt 1 then n2use=0
if n_elements(plott) lt 1 then plott=0

!p.multi=[0,2,2]
for jj=0,1 do begin

fil=fils(jj)

a='a'
openr,1,fil


for i=0,1000 do begin
readf,1,a
print,a
sn=strmatch(a,'*N= *')
if sn eq 1 then begin
a2=strsplit(a,/extract)
npart=long(a2(1))
print, 'Npart=',npart
endif

sa=strmatch(a,'*start-of-data*')
if sa eq 1 then goto,jump1
endfor
jump1:
; '  <start-of-data>'


if n2use eq 0 then n2use=npart
partinfo=fltarr(5,n2use)


for i=0L,n2use-1 do begin
temp=fltarr(5)
readf,1,temp
partinfo(*,i)=temp
endfor


close,1
pts=partinfo

;************
xbeams=findgen(501)-250.
us_nb=fltarr(8)-150.
vs_nb=fltarr(8)
vs_nb_s=fltarr(8)
us_nb_s=-us_nb

   u = (findgen(101)-50)*3
for i=0,7 do begin
nn=fltarr(8)
nn(i)=1
beam_geometry_d3d,nn,i,nb
vs_nb(i)= nb.vs + (us_nb(i)-nb.us) * tan(nb.alpha)
vs_nb_s(i)= nb.vs + (us_nb_s(i)-nb.us) * tan(nb.alpha)


endfor
   
   
beamslopes=us_nb*0.
beamyints=us_nb*0.
for i=0,7 do begin
beamslopes(i)=(vs_nb(i)-vs_nb_s(i))/(us_nb(i)-us_nb_s(i))
beamyints(i)=vs_nb(i)-beamslopes(i)*us_nb(i)
endfor
;************

wco=where(pts(2,*) ge 0.0)
wcn=where(pts(2,*) lt 0.0)

if plott gt 0 then begin


sz=.03
;plot,partinfo(0,*),partinfo(1,*),psym=4,xtitle='R (cm)',ytitle='z (cm)',$
;charsize=2,/iso,symsize=sz
theta=findgen(201)/200.*2.*!pi


titsrs=['t=325','t=725']
plot,2.3*100.*cos(theta),2.3*100.*sin(theta),$
xtitle='x (cm)',ytitle='y (cm)',charsize=2,/iso,title='All Particles '+titsrs(jj)
oplot,100.*cos(theta),100.*sin(theta)

if n_elements(wco) gt 0 then begin
oplot,partinfo(0,wco)*cos(partinfo(4,wco)*!pi/180.),partinfo(0,wco)*sin(partinfo(4,wco)*!pi/180.),$
psym=4,symsize=sz,color=45
endif

if n_elements(wcn) gt 0 then begin
oplot,partinfo(0,wcn)*cos(partinfo(4,wcn)*!pi/180.),partinfo(0,wcn)*sin(partinfo(4,wcn)*!pi/180.),$
psym=4,symsize=sz,color=100
endif


;*******

thets=findgen(201)/200.*2.*!pi
;plot,250*cos(thets),250*sin(thets)

for i=0,7 do begin

oplot,xbeams,xbeams*beamslopes(i)+beamyints(i)
endfor

;***********


;plot,2.3*100.*cos(theta),2.3*100.*sin(theta),$
;xtitle='x (cm)',ytitle='y (cm)',charsize=2,/iso,title='Co Only'
;oplot,100.*cos(theta),100.*sin(theta)

;if n_elements(wco) gt 0 then begin
;oplot,partinfo(0,wco)*cos(partinfo(4,wco)*!pi/180.),partinfo(0,wco)*sin(partinfo(4,wco)*!pi/180.),$
;psym=4,symsize=sz,color=45
;endif

;for i=0,7 do begin

;oplot,xbeams,xbeams*beamslopes(i)+beamyints(i)
;endfor

;plot,2.3*100.*cos(theta),2.3*100.*sin(theta),$
;xtitle='x (cm)',ytitle='y (cm)',charsize=2,/iso,title='Counter Only'
;oplot,100.*cos(theta),100.*sin(theta)


;if n_elements(wcn) gt 0 then begin
;oplot,partinfo(0,wcn)*cos(partinfo(4,wcn)*!pi/180.),partinfo(0,wcn)*sin(partinfo(4,wcn)*!pi/180.),$
;psym=4,symsize=sz,color=100
;endif

;for i=0,7 do begin

;oplot,xbeams,xbeams*beamslopes(i)+beamyints(i)
;endfor

endif

;      whichsource     array of sources that inject viewed neutrals
; [30L,30R,150L,150R,210L,210R,330L,330R], e.g., [0,0,0,0,0,0,1,0] for 330L
beamin=2

xf00=xbeams(500)
yf00=xf00*beamslopes(beamin)+beamyints(beamin)
xl0=xbeams(0)
yl0=xl0*beamslopes(beamin)+beamyints(beamin)
zf00=0.
zl0=0.

totlen=((xf00-xl0)^2.+(yf00-yl0)^2.+(zf00-zl0)^2.)^.5 

dpath=1.
ntt=long(totlen/dpath)
tt=findgen(ntt)/(ntt-1.)*totlen  ;/totlen

;equations for lines in x,y,z are
xvals=fltarr(ntt)
yvals=xvals
zvals=xvals


xvals=xf00+tt*(xl0-xf00)/totlen
yvals=yf00+tt*(yl0-yf00)/totlen
zvals=zf00+tt*(zl0-zf00)/totlen

rvals=(xvals^2+yvals^2)^.5
wltr=where(rvals lt 2.35E2)
xvals=xvals(wltr)
yvals=yvals(wltr)
zvals=zvals(wltr)
rvals=rvals(wltr)
ntt=n_elements(rvals)
tt=findgen(ntt)*dpath

oplot,xvals,yvals,psym=5

rminrv=min(rvals)
inrv=!C


t0=systime(1)


dr=7.5  ;look for particles within dr cm
fe=75.5E3

;********************************dong the particle search on each ray

finparts=1
if finparts gt 0 then begin
nprayf=fltarr(ntt)
npray=nprayf
nprayh=fltarr(ntt)
nprayt=fltarr(ntt)

for i=0L,ntt-1 do begin
nct=0

idr=where(((xvals(i)-pts(0,wco)*cos(pts(4,wco)*!pi/180.))^2+(yvals(i)-pts(0,wco)*sin(pts(4,wco)*!pi/180.))^2+$
(zvals(i)-pts(1,wco))^2)^.5 lt dr, nct)
if nct gt 0 then begin
 npray(i)=float(nct)

idrf=where(abs(pts(3,wco(idr)) - fe) lt 5.E3,ncf)
idrh=where(abs(pts(3,wco(idr)) - fe/2.) lt 5.E3,nch)
idrt=where(abs(pts(3,wco(idr)) - fe/3.) lt 5.E3,ncth)

if ncth gt 0 then nprayt(i)=float(ncth)
if ncf gt 0 then nprayf(i)=float(ncf)
if nch gt 0 then nprayh(i)=float(nch)
endif
endfor
endif
t1=systime(1)

print,t1-t0,' seconds'

if jj eq 0 then begin
tt0=tt
npray0=npray
nprayf0=nprayf
nprayt0=nprayt
nprayh0=nprayh
endif

endfor
endif



plot,tt,npray  ,xtitle='Distance from R=2.35 m',ytitle='N in 7.5 cm of Centerline',charsize=1.5
oplot,tt,nprayf,color=100
 oplot,tt,nprayt,color=45
oplot,tt,nprayh,color=150

oplot,tt0,nprayf0,color=100,linestyle=2
 oplot,tt0,nprayt0,color=45,linestyle=2
oplot,tt0,nprayh0,color=150,linestyle=2
oplot,tt0,npray0,linestyle=2


gadat,tn,nn,'density',142111

plot,tn,nn,xrange=[0,1000],ytitle='Density cm!A-3!N',charsize=1.5,xtitle='Time (ms)'

oplot,nn*0+325,nn,linestyle=2
oplot,nn*0+725,nn,linestyle=0




end
