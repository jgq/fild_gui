function derivsgc,t,y
common vars1,vrdrift,vzdrift,vtdrift,g

vrd=interpolatexy(g.r,g.z,vrdrift,y(0),y(2))
vtd=interpolatexy(g.r,g.z,vtdrift,y(0),y(2))
vzd=interpolatexy(g.r,g.z,vzdrift,y(0),y(2))


return,[vrd,vtd/y(0),vzd]

end

function getdrift,r,z,vpar,vperp,gci
;returns [ur,ut,uz] drift*(z*ee/mm/mi)
;interpolates drifts to r,z for a given vpar vperp

ucurvr=interpolatexy(gci.g.r,gci.g.z,gci.ucurvr*vpar^2,r,z)
ucurvt=interpolatexy(gci.g.r,gci.g.z,gci.ucurvt*vpar^2,r,z)
ucurvz=interpolatexy(gci.g.r,gci.g.z,gci.ucurvz*vpar^2,r,z)

ugradr=interpolatexy(gci.g.r,gci.g.z,gci.ugradr*vperp^2/2.,r,z)
ugradt=interpolatexy(gci.g.r,gci.g.z,gci.ugradt*vperp^2/2.,r,z)
ugradz=interpolatexy(gci.g.r,gci.g.z,gci.ugradz*vperp^2/2.,r,z)

return,[ucurvr+ugradr,ucurvt+ugradt,ucurvz+ugradz]
end




;****************************************************************************************

common vars1,vrdrift,vzdrift,vtdrift,g
;****************************************************************************************

;example lost part = 80.,0.62,2.23,-.695 to fild
energy=double(60.) ;kev
pitch0=double(.1) ;61955 ;  ;v_||/v   0.7,0.65,.6,.55,.5,.4,.3,.2
mm=2.  ;proton masses
z=1  ;charge in proton chg ;can be negative
R0i=2.0 ;23
Z0i=0
phi0=0.
inputsatgc=0  ;if input is at guiding center then =1, if instant. then =0

orbinteg=1  ;to integrate orbit or not
driftterms=1
;****************************************************************************************



topp=1
if topp gt 0 then begin
;g=readg(142111,525,runid='EFIT04')
;g=readg('/u/vanzee/idl/lib/LINETRACE/g142111.00525_e257') ;129,257,65
g=readg('/u/vanzee/TRANSP/142111/TRY1/HRSEFITS/g142111.00325_x257')
gci=get_gcinputs(g,driftterms=driftterms)
endif

ee=double(1.60E-19)
mi=double(1.67E-27)
ejou=energy*ee*1.E3
v0=(ejou/mi/mm*2.)^.5


if inputsatgc eq 0 then begin   ;checking to see if inputs were at gc or inst

;to find gyrocenter and larmor radius
;****************************************************************************************
brp0=interpolatexy(g.r,g.z,gci.br,r0i,z0i)
btp0=interpolatexy(g.r,g.z,gci.bt,r0i,z0i)
bzp0=interpolatexy(g.r,g.z,gci.bz,r0i,z0i)

bunit0=[brp0,btp0,bzp0]/(brp0^2.+btp0^2.+bzp0^2.)^.5
bunitperp=[bzp0-btp0,brp0-bzp0,btp0-brp0]
bunitperp=bunitperp/(total(bunitperp^2.))^.5

sipitch=1.
if g.bcentr*g.cpasma lt 0 then sipitch=-1.

vtot0=1.
vpar=(vtot0*pitch0*sipitch)
vperp=(vtot0^2.-vpar^2.)^.5

vel2ui0=vpar*bunit0+vperp*bunitperp

vd0i=getdrift(r0i,z0i,vpar*v0,vperp*v0,gci)/(z*ee/mm/mi)
vel2ui=vel2ui0*v0-vd0i  ;not 100% sure this is the proper sign for vd0i

vxbi=crossp(vel2ui/total(vel2ui^2)^.5,bunit0)  ;vxb unit
bveci=[brp0,btp0,bzp0]   ;bfield vector at r0i,z0i

b0i=total(bveci^2)^.5

wci0i=ee*z*b0i/mi/mm   ;9.58E3*z/mm*b0i*1.E4  ;check mi used here
rli0i=v0*(1.-pitch0^2.)/wci0i

rlvec=vxbi*rli0i ;*z/abs(z)   ;larmor radius vector from particle position to guiding center

;make the new r0,z0 at the gyrocenter to calculate mu and momentum
r0=rlvec(0)+r0i
z0=rlvec(2)+z0i

endif else begin
r0=r0i
z0=z0i
endelse

;****************************************************************************************


psi0=interpolatexy(g.r,g.z,gci.psirz,r0,z0)  ;now evaluate psi0, f0, and b0 at GC
f0=interpolatexy(g.r,g.z,gci.frz,r0,z0)
b0=interpolatexy(g.r,g.z,gci.btot,r0,z0)
bt0=interpolatexy(g.r,g.z,gci.bt,r0,z0)
br0=interpolatexy(g.r,g.z,gci.br,r0,z0)
bz0=interpolatexy(g.r,g.z,gci.bz,r0,z0)

;dont know whether or not to evaluate larmor radius at gc b0
wci0=ee*z*b0/mi/mm  ;9.58E3*z/mm*b0*1.E4
rli0=v0*(1.-pitch0^2.)/wci0

print,'R_li0=',rli0

if inputsatgc eq 0 then begin  ;need to adjust pitch ifinputs werent at gc
;************************************************************
;mu0=ejou/b0*(1.-pitch0^2.)  ;original magnetic moment
vpar0=total(vel2ui*[br0,bt0,bz0]/b0) ;parallel velocity including drift at gc
vperp0=(total(vel2ui^2)-vpar0^2)^.5  ;perp vel including drift
;vpar0=total(vel2ui0*v0*[br0,bt0,bz0]/b0) ;par. veloc. at gc with only original vel no
;drift
;vperp0=(v0^2-vpar0^2)^.5 ;perp veloc. at gc
pitch0i=pitch0
signpitch=1.0
if pitch0 ne 0.0 then signpitch=pitch0/abs(pitch0)
pitch0=abs(vpar0/total(vpar0^2+vperp0^2)^.5)*signpitch
ejouwdrift = total(vel2ui^2)*mi*mm/2. ; energy w/ drift ;v0=(ejou/mi/mm*2.)^.5
mu0i=ejou/b0*(1.-pitch0i^2.)  ;original magnetic moment
;************************************************************
endif


mu0=ejou/b0*(1.-pitch0^2.)  ;magnetic moment using pitch rel to guiding center??
;mu0=mm*mi*(vperp0)^2/2/b0  ;original magnetic moment at gc  ;should this include the perp dfrift??


signfud=-1. ;dont yet know for sure about this -1.
;this is the initial toroidal canonical angular momentum
;in straight field line coordinates

pphi0=psi0-signfud*f0*pitch0/z/ee/b0*(2.*mm*mi*ejou)^.5

pitchrz=-(pphi0-gci.psirz)/(gci.frz/z/ee/gci.btot*(2.*mm*mi*ejou)^.5 )
;this is the pitch at all r,z that have same canonical ang. momentum
;and energy

murz=ejou/gci.btot*(1.-pitchrz^2.)/mu0 ;magnetic moments from pitchrz scaled to mu0
;not all possible though because murz must = mu0

vparrz=(ejou/mi/mm*2.)^.5*pitchrz  
vperprz=(ejou/mi/mm*2.-vparrz^2.)^.5

vpartrz=vparrz*gci.bt/gci.btot
vparrrz=vparrz*gci.br/gci.btot
vparzrz=vparrz*gci.bz/gci.btot

ucurvr=gci.ucurvr*mm*mi*vparrz^2/z/ee
ucurvt=gci.ucurvt*mm*mi*vparrz^2/z/ee
ucurvz=gci.ucurvz*mm*mi*vparrz^2/z/ee

ugradr=gci.ugradr*mm*mi*vperprz^2/2/z/ee
ugradt=gci.ugradt*mm*mi*vperprz^2/2/z/ee
ugradz=gci.ugradz*mm*mi*vperprz^2/2/z/ee

vrdrift=(ucurvr+ugradr)+vparrrz
vzdrift=(ucurvz+ugradz)+vparzrz
vpdrift=(vrdrift^2+vzdrift^2)^.5
vtdrift=(ucurvt+ugradt)+vpartrz

iii=1

;***********************************************************************************

;Plotting below here
;***********************************************************************************
!p.multi=0


if driftterms gt 0 then !p.multi=[0,2,1]

cz=1.5
yr=[-1.3,1.3]
xr=[.8,2.37]
cc=254
;shade_surf,shhs*0,g.r,g.z,ax=90,az=0,shades=shhs,$
;max_value=254,color=254,xtitle='R (m)',ytitle='z (m)',$
;yrange=yr,xrange=xr,xstyle=1,ystyle=1,charsize=cz

plot,g.lim(0,*),g.lim(1,*),/iso,xrange=xr,yrange=yr,xstyle=1,ystyle=1,xtitle='R (m)',ytitle='z (m)',charsize=cz



loadct,5

oplot,g.bdry(0,*),g.bdry(1,*),thick=2,color=100  ;this defines the surface
;where it would reflect if trapped, i.e. v|| = 0
plots,r0,z0,psym=4,thick=2,color=100
plots,g.rmaxis,g.zmaxis,psym=4,thick=3,symsize=1,color=45
contour,gci.psirz,g.r,g.z,levels=psi0,color=cc,c_linestyle=2,$
thick=2,/overplot,$
yrange=yr,xrange=xr,xstyle=1,ystyle=1 ;this is the 
;original flux surface
contour,murz,g.r,g.z,levels=1.0,color=cc,c_linestyle=3,thick=2,/overplot,$  ;scaled murz to mu0
yrange=yr,xrange=xr,xstyle=1,ystyle=1 ,path_xy=paths,path_data_coords=1,$
path_info=pathinf  ,closed=0



;DETERMINING ORBIT INFO FROM CONTOUR OUTPUT
;*****************************************************************************************************
nct=n_elements(paths(0,*))
rcont=reform(paths(0,0:nct-1)) ;could have be double valued in mu0..
zcont=reform(paths(1,0:nct-1))
ncont=n_elements(pathinf.level) ;number of contours drawn
ind0=0
pioff=pathinf.offset
pin=pathinf.n
ptyp=pathinf.type

if ncont gt 1 then begin
temp=min((rcont-r0)^2.+(zcont-z0)^2,mind)  ;finding contour that goes through initial gc location
indemp=mind-pioff
ind0=max(where(indemp ge 0))
endif

ii0=pioff(ind0)
ii1=ii0+pin(ind0)-1

;oplot,rcont,zcont,psym=5,color=45
rcont0=rcont(ii0:ii1)
zcont0=zcont(ii0:ii1)

temp=min((rcont0-r0)^2.+(zcont0-z0)^2,mind0) ;contour doesnt always start at r0,z0
rcont0=shift(rcont0,-mind0)
zcont0=shift(zcont0,-mind0)
nc0=n_elements(rcont0)
tempr0=rcont0
tempz0=zcont0
for i=0,nc0-1 do begin
rcont0(i)=tempr0(nc0-1-i)
zcont0(i)=tempz0(nc0-1-i)
endfor

nsrc=0
rcont0=smooth(rcont0,nsrc,/edge_truncate)
zcont0=smooth(zcont0,nsrc,/edge_truncate)

;Before finding transit times etc. need to determine lost, trapped, and if contour is more than one enclosed region
;*****************************************************************************************************
pitchcont=interpolatexy(g.r,g.z,pitchrz,rcont0,zcont0)
temp=where(pitchcont gt 0.0,ncountg)  ;seeing if sign of pitch flipped
temp=where(pitchcont lt 0.0,ncountl)  ;seeing if sign of pitch flipped
ncount=0
if ncountg ge 1 and ncountl ge 1 then  ncount=1
;anywhere on contour, if it did, then went through v||=0 and is trapped
;ORBIT CLASSIFICATION
;GROSSCLASS=0  ;0=PASSING, 1=TRAPPED, 2 = OPEN ORBIT

if ptyp(ind0) then begin   ;checking whether its closed or not using pathinf.type
GROSSCLASS=0 
if ncount gt 0 then begin
GROSSCLASS=1  ;BOUNCED SO ITS TRAPPED
print,'TRAPPED ORBIT'
endif else begin
print,'PASSING ORBIT'
endelse

endif else begin
print,'OPEN ORBIT'
GROSSCLASS=2
endelse

;****************now know whether passing trapped or lost
omegatoroidal=0.0 ;will be toroidal passing+precession if passing or just prece. if trapped
omegapoloidal=0.0 


;***********************************************************************************
if grossclass lt 2 then begin

z_orb=avg(zcont0)  ;finding approx. center of midplane banana
zlt1=where(abs(zcont0) lt 0.05*max(abs(zcont0)),nlt1) 
if nlt1 lt 2 then zlt1=where(abs(zcont0) lt 0.1*max(abs(zcont0)),nlt1) 
r_orb=avg(rcont0(zlt1))

circumrcont=total( ((rcont0(1:*)-rcont0(0:*))^2+(zcont0(1:*)-zcont0(0:*))^2)^.5 )
;rough circumference of r,z contour
;going to follow orbit for a full poloidal transit
;doing orbit gc integration
;***********************************************************************************
y=dblarr(3) ;
y=[r0,phi0,z0]

; Main loop
i=0L
nsteps=1E4  ;maximum number of times
yout=y ;dblarr(3,nsteps)
h=10./wci0 ;timestep   ;could evaluate perp. velocity and do some approx. steps
;for circumference

nfeval=0.
thets=atan(y(0)-r_orb,y(2)-z_orb)
timearr=0D
while i lt nsteps-1 do begin

  i=long(i*1. + 1L)
  ;y=rk4(y,dydx,0.,h,'derivs',/double)
 ;ddeabm,'derivs',i*h,y,(i+1.)*h
  ddeabm,'derivsgc',0.,y,h,nfev=tempnf,epsabs=epsabs,epsrel=epsrel
   ;y=rk4(y,dydx,0.,h,'derivs',/double)
 nfeval=nfeval+tempnf
 
  ;*****COULD IMPROVE ALL THIS BY KEEPING ALL EVALUATIONS FROM DDEABM
  thets=[thets,atan(y(0)-r_orb,y(2)-z_orb)]
  yout=[[yout],[y]]
 
 timearr=[timearr,i]
  if i gt 2 then begin
 circsofar= total(((yout(0,1:*)-yout(0,*))^2+(yout(2,1:*)-yout(2,*))^2)^.5)
 
 ;checks to make sure you went around at least one
if (circsofar gt 0.8*circumrcont) then begin
phtot=phunwrap(thets)  ;thets can have 2pi flips so this unwraps it
 npht=n_elements(phtot)
 dphtot=phtot(npht-1)-phtot(0)
 
if (abs(dphtot) gt 2.*!pi) then goto,jumpi
if circsofar gt 1.2*circumrcont then begin
print,'LIKELY ERROR'
error=1
goto,jumpi
endif

endif

endif

endwhile
jumpi:
timeorb=i*h ;seconds
timearr=timearr*h ;seconds
print,'actual steps:',nfeval

phtot0=phunwrap(thets)
phtot0=abs(phtot0-phtot0(0))
dphitor=interpol(reform(yout(1,*)),phtot0,2.*!pi) ;interpolating phi vs theta data
dpoltime=interpol(timearr,phtot0,2.*!pi)  ;poloidal dt

omegatoroidal = dphitor/dpoltime  ;angular toroidal velocity avg. over pol. transit
omegapoloidal = 2.*!pi/dpoltime   ;angular poloidal transit  veloc.

;to find out exactly how far toroidally it went in 1 poloidal transit

orbm_adams,g,[r0i,0,z0i],vel2ui,rv0,e0=energy,nsteps=30.E3,time_reverse=0,$
epsabs=1.E-8,epsrel=1.E-8

endif

print, 'TOROIDAL ANGULAR ROTAT. FREQ IS:',OMEGATOROIDAL/2./!PI*1.E-3,' kHz'
print, 'POLOIDAL ANGULAR ROTAT. FREQ IS:',OMEGAPOLOIDAL/2./!PI*1.E-3,' kHz'

oplot,rcont0,zcont0,psym=4,symsize=.5
plots,r0,z0,psym=4,thick=2,color=100
oplot,yout(0,*),yout(2,*),color=45
plots,r_orb,z_orb,psym=5,thick=2,color=150
plot,yout(0,*)*cos(yout(1,*)),yout(0,*)*sin(yout(1,*)),thick=.5,psym=0,xtitle='x(m)',ytitle='y(m)',charsize=cz,/iso,$
xrange=[-2.5,2.5],yrange=[-2.5,2.5],ystyle=1,xstyle=1,symsize=.25

oplot,yout(0,*)*cos(yout(1,*)),yout(0,*)*sin(yout(1,*)),thick=.5,psym=-4



;
end
