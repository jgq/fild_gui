!p.multi=[0,2,2]
loadct,5
xt='Energy (kev)'
yt='Pitch (V||/V)'
cz=1.5
czs=cz
sz=.5
sig=orbcarr
ne3=where(sig eq 3)
ne2=where(sig ne 2)
eq2=where(sig eq 2)
;sig(ne3)=2
clabs=reverse(['p=1','p=2','p=3'])
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig),xtitle=xt,ytitle=yt,charsize=cz
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]
wl1=where((filddistarr le 0.05))
wn1=where(filddistarr gt 0.05)
x=array_indices(reform(filddistarr(0,*,*)),wl1)
badi=0 ;array_indices(tangarr,where(finite(tangarr) eq 0))
;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
plots,energyarr(x(0,*)),pitcharr(x(1,*)),psym=4,symsize=2,thick=ztt,color=113
endif
loadct,5



wk=get_kbrd(1)
;sig=tangarr
;shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig),xtitle=xt,ytitle=yt,charsize=cz

;sig=pangarr
;shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig),xtitle=xt,ytitle=yt,charsize=cz

ntor=0
mpol=1
modef=21. ;15.; 21. ;15.0
freqs=reverse(modef/(1.+findgen(3))) ;findgen(10)*15.

pp=2
;omeg=-ntor*abs(tangarr)+(mpol+pp)*abs(pangarr)
;omeg=-ntor*abs(tangarr)+(pp)*abs(pangarr)
;resonance = omega=p*omegabounce
;so need to make countours at modef/p
trin=where(orbcarr eq 1)
temp=array_indices(orbcarr,trin)

;ft325=15. ft425=21

omeg=abs(pangarr)/2./!pi
sigo=omeg*1.E-3
sigo(eq2)=-10.0

plotomeg=0
if plotomeg then begin
shade_surf,sigo,energyarr,pitcharr,ax=90,az=0,$
shades=bytscl(sigo,min=min(sigo(ne2)),max=max(sigo(ne2))),xtitle=xt,ytitle=yt,charsize=cz

contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=ztt,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czs
endif

szz=.25
ztt=.5
if wl1(0) gt -1 then begin
loadct,4
plots,energyarr(x(0,*)),pitcharr(x(1,*)),psym=4,symsize=szz,thick=ztt,color=113
endif

plotdist=1
if plotdist gt 0 then begin
;*******getting bills trans dumped dist. funcs.
;
;a1=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M18/142111M18_fi_1.cdf')
a1=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M18/142111M18_fi_2.cdf')


;trrunM21 w/ dt=.003
;OUTTIM= 0.316500 , 0.321500 , 0.326500 , 0.331500 , 0.336500 , 0.341500 , 0.346
;500 , 0.356500 , 0.366500
;a1=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M21/142111M21_fi_3.cdf')
;a2=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M21/142111M21_fi_7.cdf')

;a3=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M21/142111M21_fi_4.cdf')
;a4=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M21/142111M21_fi_5.cdf')

;t=325 beam on and 340 off w/ same egam amp

f1=a1.f_d_nbi


enedf=1.e-3*a1.e_d_nbi
pitchdf=a1.a_d_nbi

nf=findloc(enedf,5)
nenn=findloc(enedf,100)

temp=min((ci.r0-a1.r2d/100.)^2.+(ci.z0-a1.z2d/100.)^2,nin2)

loadct,5
cz=1.5
sig=f1(nf:nenn-1,*,nin2)
stemp=3E7
shade_surf,sig,enedf(nf:nenn-1),pitchdf,ax=90,az=0,shades=bytscl(sig,$
min=min(sig),max=stemp),$
xtitle='Energy (keV)',zstyle=4,ytitle='Pitch',charsize=cz,xstyle=1,$
xrange=[5,100]

contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=ztt,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czs

if wl1(0) gt -1 then begin
loadct,4
plots,energyarr(x(0,*)),pitcharr(x(1,*)),psym=4,symsize=szz,thick=ztt,color=113
endif

plot2=0
if plot2 then begin
f2=a2.f_d_nbi

loadct,5
cz=1.5
sig=f2(nf:nenn-1,*,nin2)

shade_surf,sig,enedf(nf:nenn-1),pitchdf,ax=90,az=0,shades=bytscl(sig,$
min=min(sig),max=stemp),$
xtitle='Energy (keV)',zstyle=4,ytitle='Pitch',charsize=cz,xstyle=1,$
xrange=[5,100]

contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=ztt,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czs

if wl1(0) gt -1 then begin
loadct,4
plots,energyarr(x(0,*)),pitcharr(x(1,*)),psym=4,symsize=szz,thick=ztt,color=113
endif

endif
endif


loadct,5

sigp=fildpitcharr
shhsp=bytscl(sigp,min=min(sigp(wl1)),max=max(sigp(wl1)),top=254)
shhsp(wn1)=255
;shade_surf,sigp,energyarr,pitcharr,ax=90,az=0,shades=shhsp,xtitle=xt,ytitle=yt,charsize=cz,$
;max_value=254


wl12i=where(energyarr(x(0,*)) lt 75.)
patf=fildpitcharr(wl1(wl12i))
etotf=energyarr(x(0,(wl12i)))
omegf=pangarr(wl1(wl12i))/2./!pi*1.E-3
dfo=1


eperpf=etotf*(1.-patf^2) ;*1000.*ee
bfild=1.5228 ;T
;rlif=(mm*mi)^.5/z/ee/bfild*100.*(eperpf*1.E3*ee*2)^.5
rlif=(mm*mi)^.5/z/ee/bfild*100.*(etotf*1.E3*ee*2)^.5

patfdg=abs(acos(abs(patf))*180./!pi)
plot,rlif,patfdg,psym=4,xtitle='Gyroradius @ FILD (cm)',$
ytitle='Pitch @ FILD (Deg.)',yrange=[min(patfdg),max(patfdg)],charsize=cz
loadct,5
for i=1,3 do begin
inc=where(abs(omegf-modef/i) lt dfo,ncc)  ;finds points w/ dfo of reson
if ncc gt 0 then begin
plots,rlif(inc),patfdg(inc),psym=4,color=i*45
endif
endfor

end




