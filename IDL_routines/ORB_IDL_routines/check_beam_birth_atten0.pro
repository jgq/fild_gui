xbeams=findgen(501)-250.
us_nb=fltarr(8)-150.
vs_nb=fltarr(8)
vs_nb_s=fltarr(8)
us_nb_s=-us_nb

   u = (findgen(101)-50)*3
for i=0,7 do begin
nn=fltarr(8)
nn(i)=1
beam_geometry_d3d,nn,i,nb,doplot=1
vs_nb(i)= nb.vs + (us_nb(i)-nb.us) * tan(nb.alpha)
vs_nb_s(i)= nb.vs + (us_nb_s(i)-nb.us) * tan(nb.alpha)

oplot, u, nb.vs + (u-nb.us) * tan(nb.alpha),thick=2,psym=4,color=100
 plots,us_nb_s(i),vs_nb_s(i),psym=4,color=45
 plots,us_nb(i),vs_nb(i),psym=4,color=45
 
wk=get_kbrd(1)
endfor
   
   
beamslopes=us_nb*0.
beamyints=us_nb*0.
for i=0,7 do begin
beamslopes(i)=(vs_nb(i)-vs_nb_s(i))/(us_nb(i)-us_nb_s(i))
beamyints(i)=vs_nb(i)-beamslopes(i)*us_nb(i)
endfor


thets=findgen(201)/200.*2.*!pi
plot,250*cos(thets),250*sin(thets)

for i=0,7 do begin

oplot,xbeams,xbeams*beamslopes(i)+beamyints(i)
endfor

end
