g=readg('/u/vanzee/TRANSP/142111/TRY1/HRSEFITS/g142111.00525_x257')

widthbeam=(8.8*!pi^.5)
heightbeam=(21.1*!pi^.5) ;
beamarea=widthbeam*heightbeam ; ;in cm^2



topp=1
if topp gt 0 then begin
;************************************************************************
;************
xbeams=findgen(501)-250.
us_nb=fltarr(8)-150.
vs_nb=fltarr(8)
vs_nb_s=fltarr(8)
us_nb_s=-us_nb

   u = (findgen(101)-50)*3
for i=0,7 do begin
nn=fltarr(8)
nn(i)=1
beam_geometry_d3d,nn,i,nb
vs_nb(i)= nb.vs + (us_nb(i)-nb.us) * tan(nb.alpha)
vs_nb_s(i)= nb.vs + (us_nb_s(i)-nb.us) * tan(nb.alpha)

endfor
      
beamslopes=us_nb*0.
beamyints=us_nb*0.
for i=0,7 do begin
beamslopes(i)=(vs_nb(i)-vs_nb_s(i))/(us_nb(i)-us_nb_s(i))
beamyints(i)=vs_nb(i)-beamslopes(i)*us_nb(i)
endfor
;************
revi=[1,1,1,1,-1,-1,-1,-1]
tths=findgen(100)/99*2.*!pi
ndrays=100.
beam_rays=fltarr(8,4,ndrays)

plot,250*cos(tths),250*sin(tths)
oplot,100*cos(tths),100*sin(tths)
for ii=0,7 do begin
beamin=ii

xf00=xbeams(500)
yf00=xf00*beamslopes(beamin)+beamyints(beamin)
xl0=xbeams(0)
yl0=xl0*beamslopes(beamin)+beamyints(beamin)
zf00=0.
zl0=0.

totlen=((xf00-xl0)^2.+(yf00-yl0)^2.+(zf00-zl0)^2.)^.5 

dpath=1.
ntt=long(totlen/dpath)
tt=findgen(ntt)/(ntt-1.)*totlen  ;/totlen

;equations for lines in x,y,z are
xvals=fltarr(ntt)
yvals=xvals
zvals=xvals


xvals=xf00+tt*(xl0-xf00)/totlen
yvals=yf00+tt*(yl0-yf00)/totlen
zvals=zf00+tt*(zl0-zf00)/totlen

if revi(ii) lt 0 then begin
xvals=reverse(xvals)
yvals=reverse(yvals)
zvals=reverse(zvals)
endif

rvals=(xvals^2+yvals^2)^.5
wltr=where(rvals lt 2.35E2)
xvals=xvals(wltr)
yvals=yvals(wltr)
zvals=zvals(wltr)
rvals=rvals(wltr)
ntt=n_elements(rvals)
tt=findgen(ntt)*dpath

oplot,xvals,yvals,psym=5

rminrv=min(rvals)
inrv=!C

indi=lindgen(ntt)
wltr=where(rvals lt 100.*max(g.bdry(0,*)))
wgt1=where(rvals gt 100.0)
wlt1=where(rvals lt 100.0,nlt1)
goodi=wltr
if nlt1 gt 0 then begin
temp=indi(0:min(wlt1)-1)
wtemp=where(rvals(temp) lt 100.*max(g.bdry(0,*)))
goodi=temp(wtemp)
endif

oplot,xvals(goodi),yvals(goodi),color=100,psym=4
plots,xvals(goodi(0)),yvals(goodi(0)),psym=6,symsize=2,color=45,thick=2

beam_rays(ii,0,*)=congrid(xvals(goodi),ndrays)

beam_rays(ii,1,*)=congrid(yvals(goodi),ndrays)

beam_rays(ii,2,*)=congrid(zvals(goodi),ndrays)

beam_rays(ii,3,*)=congrid(rvals(goodi),ndrays)
print,ii,' ', min(abs(beam_rays(ii,3,*)))
beam_rays(ii,*,*)=beam_rays(ii,*,*)/100.
beam_unit=fltarr(3,8)  ;x,y,z
beam_unit(0,*)=1.
beam_unit(1,*)=beamslopes*revi
beam_unit_rzphi=fltarr(3,8,ndrays) ;r,z,phi
for jj=0,7 do begin
beam_unit(0:2,jj)=beam_unit(0:2,jj)/total(beam_unit(0:2,jj)^2)^.5

beam_unit_rzphi(0,jj,*)=beam_unit(0,jj)*beam_rays(jj,0,*)/beam_rays(jj,3,*)+$
beam_unit(1,jj)*beam_rays(jj,1,*)/beam_rays(jj,3,*)
beam_unit_rzphi(1,jj,*)=beam_unit(2,jj)

beam_unit_rzphi(2,jj,*)=-beam_unit(0,jj)*beam_rays(jj,1,*)/beam_rays(jj,3,*)+$
beam_unit(1,jj)*beam_rays(jj,0,*)/beam_rays(jj,3,*)
endfor


endfor

beam_pitch=reform(beam_rays(*,0,*))*0.
temp=reform(beam_pitch(0,*)*0.)+1.
for i=0,7 do begin
rrrays=reform(beam_rays(i,3,*))
zzrays=reform(beam_rays(i,2,*))
beam_pitch(i,*)=calculate_pitch_vdotb(g,rrrays,zzrays,$
reform(beam_unit_rzphi(0,i,*)),reform(beam_unit_rzphi(2,i,*)),reform(beam_unit_rzphi(1,i,*)))
endfor

stop
;************************************************************************
;************************************************************************

energyarr=[1.,1./2.,1./3.]*77. ;findgen(ner)/(ner-1.)*70.+10.

ner=n_elements(energyarr)
npit=30
phi0=0.0
z0=0.0

withinbeam=beamarea^.5/100.  ;will be considered in beam if in this distance


fild_rtz=[2.25,225.*!pi/180.,-.66]  ;r,phi (in radians),z of fild
;fild_rtz=[2.27,0.,-.15]  ;r,phi (in radians),z of fild

energyarr=findgen(ner)/(ner-1.)*70.+10.
pitcharr=(findgen(npit)/(npit-1.)*.9+.05)

prompthit=intarr(8,ner,npit)  ;will be 1 if prompt can hit this energy and pitch of fild
hitbelowfild=intarr(ner,npit)
t0=systime(1)

for ii=0,ner-1 do begin
for jj=0,npit-1 do begin

orb_gc_mv,g,fild_rtz,pitcharr(jj),energyarr(ii),plot=1,$
gci=gci,outs=outs,printinfo=1,orbinteg=1,nmin=100,skip_lostint=0,reverse_time=1,nsteps=5E3

if outs.error eq 1 then goto,jumpchk
wgz=where(abs(outs.yout(2,*)) lt heightbeam/2.,nwgz)

if nwgz eq 0 then goto,jumpchk

plot,2.50*cos(tths),2.50*sin(tths)
oplot,1.00*cos(tths),1.00*sin(tths)
oplot,beam_rays(*,0,*),beam_rays(*,1,*),psym=6
plots,outs.yout(0,wgz)*cos(outs.yout(1,wgz)),$
outs.yout(0,wgz)*sin(outs.yout(1,wgz)),psym=4,color=100

for kk=0,7 do begin ;checking beams
for ki=0,nwgz-1 do begin
ry=outs.yout(0,wgz(ki))*sin(outs.yout(1,wgz(ki)))
rx=outs.yout(0,wgz(ki))*cos(outs.yout(1,wgz(ki)))
zz=outs.yout(2,wgz(ki))
py=outs.pitchyout(wgz(ki))
temp=where(( ( (rx-beam_rays(kk,0,*))^2.+(ry-beam_rays(kk,1,*))^2.+(zz-beam_rays(kk,2,*))^2.)^.5 lt withinbeam ) and (abs(py-beam_pitch(kk,*)) lt 0.05 ), npro)
if npro gt 0 then begin
plots,rx,ry,psym=5,symsize=2,color=45
prompthit(kk,ii,jj)= prompthit(kk,ii,jj)+1

endif

endfor
endfor

inout=check_lim_inoutorb(outs.rcont0,outs.zcont0,g=g,usefinewall=1) ;IF PASSES FIRST TEST THEN TEST ALL POINTS ON CONTOUR
temp=where(inout lt 1,nino)
if nino gt 0 then begin
temp2=where(outs.zcont0(temp) lt fild_rtz(2),nbel)
if nbel gt 0 then hitbelowfild(ii,jj)=1
endif
jumpchk:


;wk=get_kbrd(1)

endfor
endfor


endif

t1=systime(1)
print,'IT TOOK: ',t1-t0,' s'

ee=double(1.60E-19)
mi=double(1.67E-27)
mm=2.
z=1.
rlif=(mm*mi)^.5/z/ee/outs.bfild*100.*(energyarr*1.E3*ee*2)^.5

patfdg=abs(acos(abs(pitcharr))*180./!pi)

prompthit01=prompthit*0
wgt0=where(prompthit gt 0, ng00)
if ng00 gt 0 then prompthit01(wgt0)=1

!p.multi=[0,3,3]
for i=0,7 do begin
shade_surf,reform(prompthit01(i,*,*))*0,energyarr,pitcharr,ax=90,az=0,xtitle='Energy',ytitle='Pitch',shades=bytscl(reform(prompthit01(i,*,*)))
wb=where(hitbelowfild gt 0)
xwb=array_indices(hitbelowfild,wb)
plots,energyarr(xwb(0,*)),pitcharr(xwb(1,*)),psym=5,color=100
endfor
shade_surf,total(prompthit01,1)*0,energyarr,pitcharr,xtitle='Energy',ax=90,az=0,ytitle='Pitch',shades=bytscl(total(prompthit01,1))

!p.multi=[0,3,3]
for i=0,7 do begin
shade_surf,reform(prompthit01(i,*,*))*0,rlif,patfdg,ax=90,az=0,xtitle='Rli',ytitle='Pitch',shades=bytscl(reform(prompthit01(i,*,*))),charsize=2

w1=where(reform(prompthit01(i,*,*)) gt 0,nw1)
if nw1 gt 0 then begin
xw1=array_indices(reform(prompthit01(i,*,*)),w1)
plots,rlif(xw1(0,*)),patfdg(xw1(1,*)),psym=4,color=45,symsize=2,thick=2
endif
wb=where(hitbelowfild gt 0,nwb)
if nwb gt 0 then begin
xwb=array_indices(hitbelowfild,wb)
plots,rlif(xwb(0,*)),patfdg(xwb(1,*)),psym=5,color=100,symsize=2
endif

endfor
shade_surf,total(prompthit01,1)*0,rlif,patfdg,xtitle='Rli',ax=90,az=0,ytitle='Pitch',shades=bytscl(total(prompthit01,1)),charsize=2

end
