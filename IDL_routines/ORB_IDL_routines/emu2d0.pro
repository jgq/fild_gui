g=readg(142111,525,runid='EFIT04')
f=fluxfun(g)

sps=sort(f.psi)
psis=f.psi(sps)
fpsi=f.f(sps)
fg=interpol(fpsi,psis,g.psirz)
;****will have a problem if fg is every double valued in psi

psig=g.psirz

calculate_bfield,bp,br,bt,bz,g
btot=(bp^2.+bt^2.)^.5

;example lost part = 80.,0.62,2.23,-.695 to fild
energy=60. ;kev
pitch0=0.5 ;  ;v_||/v   0.7,0.65,.6,.55,.5,.4,.3,.2
mm=2.  ;proton masses
z=1.  ;charge in proton chg
R0=2.23
Z0=-0.10

energy=80.
pitch0=.62
r0=2.23
z0=-.695

ee=1.60E-19
mi=1.67E-27
ejou=energy*ee*1.E3
psi0=interpolatexy(g.r,g.z,psig,r0,z0)
f0=interpolatexy(g.r,g.z,fg,r0,z0)
b0=interpolatexy(g.r,g.z,btot,r0,z0)
v0=(ejou/mi/mm*2.)^.5
wci0=9.58E3*z/mm*b0*1.E4

rli0=v0*(1.-pitch0^2.)/wci0
nsmrli=fix(rli0*2./avg(deriv(g.r))+.5)

b0mu=interpolatexy(g.r,g.z,smooth(btot,nsmrli),r0,z0)
;uses b0mu to approximate it at guiding center
;could replace w/ instantaneous b0
b0mu=b0 & print,'Using instantaneous b0 for mu etc.'

mu0=ejou/b0mu*(1.-pitch0^2.)  ;original magnetic moment

signfud=-1. ;dont yet know for sure about this -1.
;this is the initial toroidal canonical angular momentum
;in straight field line coordinates
pphi0=psi0-signfud*f0*pitch0/z/ee/b0mu*(2.*mm*mi*ejou)^.5 ;


pitchrz=-(pphi0-psig)/(f0/z/ee/btot*(2.*mm*mi*ejou)^.5)
;this is the pitch at all r,z that have same canonical ang. momentum
;and energy

murz=ejou/btot*(1.-pitchrz^2.) ;magnetic moments from pitchrz
;not all possible though because murz must = mu0

vel2u=calculate_pitchv(g,r0,z0,pitch0)

orbm_adams,g,[r0,0,z0],vel2u,rv0,e0=energy,nsteps=10.E3,/time_reverse




ng1=where(abs(pitchrz) gt 1.0)
pitchrzgt1=pitchrz
pitchrzgt1(ng1)=999.
!p.multi=0
shhs=bytscl(pitchrzgt1,min=-1.0,max=1.0,top=254)
shhs(ng1)=255
loadct,5
tvlct,rc,gc,bc,/get
rc(255)=0
gc(255)=0
bc(255)=0
tvlct,rc,gc,bc

cz=2
yr=[-1.3,1.3]
xr=[.8,2.5]

shade_surf,shhs*0,g.r,g.z,ax=90,az=0,shades=shhs,$
max_value=254,color=254,xtitle='R (m)',ytitle='z (m)',$
yrange=yr,xrange=xr,xstyle=1,ystyle=1,charsize=cz


nds=0
if nds gt 0 then begin
oplot,dsamp(rv0(3,*),nds),dsamp(rv0(5,*),nds),color=100,thick=.5
endif else begin
loadct,4
oplot,rv0(3,*),rv0(5,*),color=100,thick=.5

endelse

loadct,5
contour,pitchrz,g.r,g.z,levels=0.,thick=2,color=0,/overplot,$
yrange=yr,xrange=xr,xstyle=1,ystyle=1
oplot,g.bdry(0,*),g.bdry(1,*),thick=2,color=100  ;this defines the surface
;where it would reflect if trapped, i.e. v|| = 0
plots,r0,z0,psym=4,thick=2,color=100
contour,psig,g.r,g.z,levels=psi0,color=0,c_linestyle=2,$
thick=2,/overplot,$
yrange=yr,xrange=xr,xstyle=1,ystyle=1 ;this is the 
;original flux surface
contour,murz,g.r,g.z,levels=mu0,color=0,c_linestyle=3,thick=2,/overplot,$
yrange=yr,xrange=xr,xstyle=1,ystyle=1 

;trapped or passing can be found by totaling contour 
;atan(ri-rax,z-zax). If gt 0 it doesn't go around 2pi 
;and is trapped
xyouts,2.38,.65,'PITCH',charsize=2
;of mu=mu0 in murz array. I
dxc=-.05
cbar_m2,-1,1,.95+dxc,.25,.99+dxc,.75,ylg=0,yax=1

end
