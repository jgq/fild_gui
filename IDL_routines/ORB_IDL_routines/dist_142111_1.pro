
;*******getting bills trans dumped dist. funcs.
;a1=transp_cdf_restore('/u/pacedc/MP2009-52-02/VanZeeland/138387/138387P01_fi_1.cdf')
a1=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M18/142111M18_fi_1.cdf')


f1=a1.f_d_nbi

;ft1=fltarr(75,50) & ft2=ft1 & ft3=ft1  ;totalling over all positions
;for i=0,74 do for j=0,49 do begin
;  ft1(i,j)=total(f1(i,j,*))
 
;end

enedf=1.e-3*a1.e_d_nbi
pitchdf=a1.a_d_nbi

nf=findloc(enedf,5)
nenn=findloc(enedf,100)

temp=min((2.0-a1.r2d)^2.+(-.05-a1.z2d)^2,nin2)

;ngt0=where(ebthru gt 0)


!p.multi=[0,1,0]
cz=1.5
sig=f1(nf:nenn-1,*,nin2)
shade_surf,sig,enedf(nf:nenn-1),pitchdf,ax=90,az=0,shades=bytscl(sig,$
min=min(sig),max=stemp),$
xtitle='Energy (keV)',zstyle=4,ytitle='Pitch',charsize=cz,xstyle=1,$
xrange=[5,100]



;surf0=reform(total(total(reform(f1(*,0:24,*)),1),1))  ;defining a surface of just counter particles 
;;surf0 is a vector of counter going particle densities for the weird spiral coordinates

;dd=tdf_rzmap(a1,surf0)  ;remaps surf0 to R,z


;shade_surf,dd.surfrz,dd.rtr,dd.ztr,ax=90,az=0,shades=bytscl(dd.surfrz),xtitle='R (cm)',ytitle='z (cm)'


end


