
;example lost part = 80.,0.62,2.23,-.695 to fild
energy=60. ;kev
pitch0=0.61955 ;  ;v_||/v   0.7,0.65,.6,.55,.5,.4,.3,.2
mm=2.  ;proton masses
z=1.  ;charge in proton chg ;can be negative
R0i=2.23 ;23
Z0i=-0.10

orbinteg=0  ;to integrate orbit or not

topp=1
if topp gt 0 then begin
;g=readg(142111,525,runid='EFIT04')
g=readg('/u/vanzee/idl/lib/LINETRACE/g142111.00525_e257') ;129,257,65

;to get f from g struc only
ngf=n_elements(g.fpol)
psigf=findgen(ngf)/(ngf-1.)*(g.ssibry-g.ssimag)+g.ssimag
fg=interpol(g.fpol,psigf,g.psirz)
psig=g.psirz
calculate_bfield,bp,br,bt,bz,g
btot=(bp^2.+bt^2.)^.5
endif

;****fg is double valued sometimes so that can make more than one possible mu=mu0 surface

ee=1.60E-19
mi=1.67E-27
ejou=energy*ee*1.E3
v0=(ejou/mi/mm*2.)^.5

;to find gyrocenter and larmor radius
vel2ui=calculate_pitchv(g,r0i,z0i,pitch0,vxb=vxbi,bvec=bveci)

b0i=total(bveci^2)^.5

wci0i=9.58E3*z/mm*b0i*1.E4
rli0i=v0*(1.-pitch0^2.)/wci0i

rlvec=vxbi*rli0i*z/abs(z)   ;larmor radius vector from particle position to guiding center

;make the new r0,z0 at the gyrocenter to calculate mu and momentum
r0=rlvec(0)+r0i
z0=rlvec(2)+z0i


psi0=interpolatexy(g.r,g.z,psig,r0,z0)  ;now evaluate psi0, f0, and b0 at GC
f0=interpolatexy(g.r,g.z,fg,r0,z0)
b0=interpolatexy(g.r,g.z,btot,r0,z0)

wci0=9.58E3*z/mm*b0*1.E4
rli0=v0*(1.-pitch0^2.)/wci0

print,'R_li0=',rli0

nsmrli=fix(rli0*2./avg(deriv(g.r))+.5)

mu0=ejou/b0*(1.-pitch0^2.)  ;original magnetic moment

signfud=-1. ;dont yet know for sure about this -1.
;this is the initial toroidal canonical angular momentum
;in straight field line coordinates
pphi0=psi0-signfud*f0*pitch0/z/ee/b0*(2.*mm*mi*ejou)^.5 ;


pitchrz=-(pphi0-psig)/(f0/z/ee/btot*(2.*mm*mi*ejou)^.5)
;this is the pitch at all r,z that have same canonical ang. momentum
;and energy

murz=ejou/btot*(1.-pitchrz^2.)/mu0 ;magnetic moments from pitchrz
;not all possible though because murz must = mu0

ng1=where(abs(pitchrz) gt 1.0)
pitchrzgt1=pitchrz
pitchrzgt1(ng1)=999.   ;set all unphysical pitches to 999.

;Plotting below here
;***********************************************************************************

!p.multi=0

cz=1.5
yr=[-1.3,1.3]
xr=[.8,2.37]
cc=254
;shade_surf,shhs*0,g.r,g.z,ax=90,az=0,shades=shhs,$
;max_value=254,color=254,xtitle='R (m)',ytitle='z (m)',$
;yrange=yr,xrange=xr,xstyle=1,ystyle=1,charsize=cz

plot,g.lim(0,*),g.lim(1,*),/iso,xrange=xr,yrange=yr,xstyle=1,ystyle=1,xtitle='R (m)',ytitle='z (m)',charsize=cz

if orbinteg then begin
;vel2u=calculate_pitchv(g,r0,z0,pitch0)
vel2u=vel2ui ;not sure which yet
orbm_adams,g,[r0i,0,z0i],vel2u,rv0,e0=energy,nsteps=10.E3,/time_reverse,epsabs=1.E-8,epsrel=1.E-8

nds=0
if nds gt 0 then begin
oplot,dsamp(rv0(3,*),nds),dsamp(rv0(5,*),nds),color=100,thick=.5
endif else begin
loadct,4
oplot,rv0(3,*),rv0(5,*),color=100,thick=.5
endelse
endif


loadct,5

oplot,g.bdry(0,*),g.bdry(1,*),thick=2,color=100  ;this defines the surface
;where it would reflect if trapped, i.e. v|| = 0
plots,r0,z0,psym=4,thick=2,color=100
plots,g.rmaxis,g.zmaxis,psym=4,thick=3,symsize=1,color=45
contour,psig,g.r,g.z,levels=psi0,color=cc,c_linestyle=2,$
thick=2,/overplot,$
yrange=yr,xrange=xr,xstyle=1,ystyle=1 ;this is the 
;original flux surface
contour,murz,g.r,g.z,levels=1.0,color=cc,c_linestyle=3,thick=2,/overplot,$  ;scaled murz to mu0
yrange=yr,xrange=xr,xstyle=1,ystyle=1 ,path_xy=paths,path_data_coords=1,$
path_info=pathinf  ,closed=0



;DETERMINING ORBIT INFO FROM CONTOUR OUTPUT
;*****************************************************************************************************
nct=n_elements(paths(0,*))
rcont=reform(paths(0,0:nct-1)) ;could have be double valued in mu0..
zcont=reform(paths(1,0:nct-1))
ncont=n_elements(pathinf.level) ;number of contours drawn
ind0=0
pioff=pathinf.offset
pin=pathinf.n
ptyp=pathinf.type

if ncont gt 1 then begin
temp=min((rcont-r0)^2.+(zcont-z0)^2,mind)  ;finding contour that goes through initial gc location
indemp=mind-pioff
ind0=max(where(indemp ge 0))
endif

ii0=pioff(ind0)
ii1=ii0+pin(ind0)-1

;oplot,rcont,zcont,psym=5,color=45
rcont0=rcont(ii0:ii1)
zcont0=zcont(ii0:ii1)
oplot,rcont0,zcont0,psym=-4
plots,r0,z0,psym=4,thick=2,color=100

;thet=atan(rcont0-g.rmaxis,zcont0-g.zmaxis)
;thet=phunwrap(thet)
;dthet=thet-thet(1:*)
;encir=total(thet(1:*)*dthet)
;print,encir


if ptyp(ind0) then begin   ;checking whether its closed or not using pathinf.type
print,'CLOSED ORBIT'
endif else begin
print,'OPEN ORBIT HITS WALL'
endelse
;Before finding transit times etc. need to determine lost, trapped, and if contour is more than one enclosed region
;*****************************************************************************************************

end
