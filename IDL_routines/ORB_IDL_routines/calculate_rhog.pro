;MVZ modified and removed afile as input - unnecessary 11/17/06
;+ 
; NAME: 
;
;     CALCULATE_RHOG
;
; PURPOSE: 
;
;     Calulate square root of normalized toroidal flux, rho, at a given (R,Z)
;     location or for a given value of normalized poloidal flux, psin.  The
;     variable rho is often referred to as the normalized radius.
;
; CALLING SEQUENCE: 
;
;     data = CALCULATE_RHOg(rin, zin, g [,psin=psingiven] [,grid=grid])
;
; INPUT PARAMETERS: 
;
;	rin	 input major radius (m), scalar or vector
;	zin	 input vertical position (m), scalar or vector
;       NOTE:    rin and zin are ignored if PSIN values (via keyword) are
;                given.
;       a        structure containing A0 parameters - removed
;       g        structure containing G0 parameters
;
; OPTIONAL INPUT PARAMETERS: 
;      NONE
;
; KEYWORDS: 
;
;       psin     optional input of psi normalized values.  If this keyword is
;                supplied, then rho is calculated from these normalized
;                psi values, and any R and Z inputs are ignored.  psin may
;                be a scalar or a vector.
;       GRID     /GRID uses r and z for grid (like in calculate_psi) so that
;                rho is returned as if r and z represented a grid of
;                r's and z's rather than just ordered pairs.
;
; OUTPUTS: 
;
;	rho	 output value of normalized toroidal flux
;
; COMMON BLOCKS: 
;
;     NONE
;
; SIDE EFFECTS: 
;
;     NONE
;
; RESTRICTIONS:
;
;     Prior to this using this function, you must have first read in
;     the EFIT data to fill the a and g structures. 
;
;     Algorithm used to get rho works only inside the separatrix
;
; PROCEDURE: 
;
; CODE TYPE: modeling, analysis
;
; CODE SUBJECT:  operation, handling, edge, rf, transport, equilibrium, other
;
; EASE OF USE: can be used with existing documentation
;
; OPERATING SYSTEMS:  UNIX of all flavors
;
; EXTERNAL CALLS:  CALCULATE_PSI
;
; RESPONSIBLE PERSON: Ray Jong
;	
; DATE OF LAST MODIFICATION:  02/17/98
;
; MODIFICATION HISTORY:
;     Created 1993.01.06 by Gary D. Porter
;     1993.01.26:     Improved by Michael D. Brown
;     1995.06.01:     Improved by Michael D. Brown
;     1996.05.29:     Improved by Michael D. Brown
;     1998.02.17:     Modified to use a,g structures by Gary Porter
;-	

function calculate_rhog,rin,zin,g,psin=psingiven,grid=grid

pi=4*atan(1)

; get value of normalized psi
if n_elements(psingiven) eq 0 $
;then psin=[calculate_psi(rin,zin,a,g,/NORM,grid=grid)] $
then psin=[calculate_psig(rin,zin,g,/NORM,grid=grid)] $

else psin=[psingiven]

; check to see if point is outside separatrix
w = where(psin ge 1.0,count)
if count gt 0 then psin(w)=1.0

; calculate toroidal flux and its integral
dpsi=abs((g.ssimag-g.ssibry)/(g.mw-1))

; integrate toroidal flux.
tflx=0.
tflux=0.
rhox=fltarr(g.mw)
dpsi=abs((g.ssimag-g.ssibry)/(g.mw-1))
for j=1,g.mw-1 do begin
  tflx=2.0 *pi *   (g.qpsi(j-1)+g.qpsi(j))*dpsi/2.0
  tflux=tflux+tflx
  rhox(j)=sqrt(tflux/(pi*abs(g.bcentr)))
endfor

; normalize rho
rhomax=max(rhox)
rhox=rhox/rhomax

; now use spline fit to determine rho at input position

; xvector, between 0 and 1
xvect=findgen(g.mw)/(g.mw-1)
; get sort indexes (spline requires increasing values).
ix=sort(psin)
; do spline interpolation.
rhtmp=spline(xvect,rhox,psin(ix))
; allocate result and put rho's in original psin order.
rho=psin*0.0  ; fltarr(n_elements(psin))
rho(ix)=rhtmp
; return resultant rho.
return,rho
end
