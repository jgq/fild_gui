FUNCTION magfld,position
; Returns (Br,Bphi,Bz) given (R,phi,z) in meters
; b is the field in units of Tesla/(rad/sec)
common bcom,b0,r0,br,bphi,bz,gr0,gz0,dr,dz

; simple poloidal field model
;rr=sqrt(y(5)^2 + (y(3)-r0)^2)
;const=0.33*b0/position(0)

rgrid=(position(0) - gr0)/dr
zgrid=(position(2) - gz0)/dz
bbr=interpolate(br,[rgrid],[zgrid])
bbphi=interpolate(bphi,[rgrid],[zgrid]) ;*(1.+.2*sin(2.*!pi/4.*position(1)))
bbz=interpolate(bz,[rgrid],[zgrid])
return,[bbr,bbphi,bbz]/b0
;return,[const*position(2),b0*r0/position(0),const*(r0-position(0))]
;br=const*y(5)
;bp=b0*r0/y(3)
;bz=const*(r0-y(3))
end
