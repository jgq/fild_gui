FUNCTION find_psi,g,r,z
; Returns psi given (R,z) in meters

gr0=g.r(0) & gz0=g.z(0) 
dr=g.r(1)-g.r(0) & dz=g.z(1)-g.z(0)
rgrid=(r - gr0)/dr
zgrid=(z - gz0)/dz
psi=interpolate(g.psirz,rgrid,zgrid)
return,psi
end