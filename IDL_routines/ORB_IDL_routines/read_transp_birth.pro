function read_transp_birth, fil,plott=plott,n2use=n2use,nins=nins
;Will read in transp birth profiles
;fil='/u/vanzee/TRANSP/OUTPUTFILES/142111M18/142111M18_birth_4.out'

;plott=1
;n2use=200000L
if n_elements(n2use) lt 1 then n2use=0
if n_elements(plott) lt 1 then plott=0
if n_elements(nins) lt 1 then nins=5   ;beam birth has 5 alphas has 4
a='a'
openr,1,fil

for i=0,1000 do begin
readf,1,a
print,a
sn=strmatch(a,'*N= *')
if sn eq 1 then begin
a2=strsplit(a,/extract)
npart=long(a2(1))
print, 'Npart=',npart
endif

sa=strmatch(a,'*start-of-data*')
if sa eq 1 then goto,jump1
endfor
jump1:
; '  <start-of-data>'


if n2use eq 0 then n2use=npart
partinfo=fltarr(nins,n2use)


for i=0L,n2use-1 do begin
temp=fltarr(nins)
readf,1,temp
partinfo(*,i)=temp
endfor


close,1

if plott gt 0 then begin

!p.multi=[0,2,1]
sz=.03
plot,partinfo(0,*),partinfo(1,*),psym=4,xtitle='R (cm)',ytitle='z (cm)',$
charsize=2,/iso,symsize=sz
theta=findgen(201)/200.*2.*!pi
plot,2.3*100.*cos(theta),2.3*100.*sin(theta),$
xtitle='x (cm)',ytitle='y (cm)',charsize=2,/iso
oplot,100.*cos(theta),100.*sin(theta)
oplot,partinfo(0,*)*cos(partinfo(4,*)*!pi/180.),partinfo(0,*)*sin(partinfo(4,*)*!pi/180.),$
psym=4,symsize=sz
endif

;nxx=100
;nyy=nxx
;xx=findgenm(nxx,range=[-max(partinfo(0,*)),max(partinfo(0,*))])
;yy=xx
;r0=reform(partinfo(0,*))
;t0=reform(partinfo(4,*)*!pi/180.)

return,partinfo


end
