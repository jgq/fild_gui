g=readg(142111,525,runid='EFIT04')
gr0=double(g.r(0)) & gz0=double(g.z(0)) 
dr=double(g.r(1)-g.r(0)) & dz=double(g.z(1)-g.z(0))
calculate_bfield,bp,br,bphi,bz,g



position=[1.5,0.,0.3]

ntrials=20000
sys0=systime(1)
for i=0,ntrials-1 do begin
rgrid=(position(0) - gr0)/dr
zgrid=(position(2) - gz0)/dz
bbr0=interpolate(br,[rgrid],[zgrid])
endfor
print,systime(1)-sys0

sys0=systime(1)
for i=0,ntrials-1 do begin
rgrid=(position(0) - gr0)/dr
zgrid=(position(2) - gz0)/dz
bbr=interpolate(br(rgrid-.5:rgrid+.5,zgrid-.5:zgrid+.5),rgrid-fix(rgrid-.5),zgrid-fix(zgrid-.5)) 
endfor
print,systime(1)-sys0

print, bbr0,' ',bbr


end
