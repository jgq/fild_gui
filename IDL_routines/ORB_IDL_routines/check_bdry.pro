FUNCTION check_bdry,g,rwall,zwall,r,z
; Check if orbit is inside or outside of vessel
; INPUT:	g	eqdsk 
;		(rwall,zwall) arrays of fine coordinates of wall
;		(r,z)	coordinates of point (m)
; OUTPUT:		1 if inside, 0 if outside

r0=g.rmaxis & z0=g.zmaxis
dum=min((rwall-r)^2+(zwall-z)^2,imin)
if (rwall(imin)-r0)^2 + (zwall(imin)-z0)^2 gt (r-r0)^2+(z-z0)^2 then $
  inside=1 else inside=0
return,inside
end
