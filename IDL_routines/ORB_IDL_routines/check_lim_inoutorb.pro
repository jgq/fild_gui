PRO finewallorb,g,rwall,zwall,ds=ds
; fill in the vessel wall with a finer grid
; INPUT:	g	eqdsk
; OUTPUT:	(rwall,zwall)	coordinates (m) of wall
if n_elements(ds) lt 1 then ds=.005
ii=0
tmp=fltarr(2,10000)
x=g.lim(0,*) & y=g.lim(1,*)
for i=1,n_elements(x)-1 do begin
  ii=ii+1
  x0=x(i-1) & x1=x(i) & y0=y(i-1) & y1=y(i)
  tmp(0,ii-1)=x0 & tmp(1,ii-1)=y0
  dist=sqrt((x1-x0)^2 + (y1-y0)^2)
  
   ;avoid math errors when DIST is zero, does not affect 
    ;WHILE statement that follows
    if dist eq 0.0 then dist = 1e-20
    dx = (x1-x0) * ds / dist 
    dy = (y1-y0) * ds / dist
    

  j=1
  while j*ds lt dist do begin
    j=j+1 & ii=ii+1
    tmp(0,ii-1)=j*dx + x0
    tmp(1,ii-1)=j*dy + y0
  end
end
;ii=ii+1
tmp(0,ii)=x(i-1) & tmp(1,ii)=y(i-1)
rwall=fltarr(ii+1) & zwall=fltarr(ii+1)
rwall(*)=tmp(0,0:ii) & zwall(*)=tmp(1,0:ii)
end
    
;**************************************************************************************



function check_lim_inoutorb,r,z,g=g,indin=indin,usefinewall=usefinewall,dwall=dwall
;checks if a vector of R,z points are inside the limiter (1=yes,0=no)
;g=str. from readg

;r=indgen(2)
;z=fltarr(2)
nr=n_elements(r)
sg=size(g)
if sg(n_elements(sg)-2) ne 8 then g=readg(135851,400)
if n_elements(usefinewall) lt 1 then usefinewall=0
if n_elements(dwall) lt 1 then dwall=.05

;r0=avg(g.lim(0,*))
r0=avg(g.bdry(0,*))

z0=0.

if usefinewall lt 1 then begin
nw=n_elements(g.lim(0,*))
rw=reform(g.lim(0,*))
zw=reform(g.lim(1,*))
rwall=rebin(rw,nw,nr)
zwall=rebin(zw,nw,nr)
endif else begin

finewallorb,g,rw,zw,ds=dwall
nw=n_elements(rw)

rwall=rebin(rw,nw,nr)
zwall=rebin(zw,nw,nr)

endelse

r2=rebin(reform(r,1,nr),nw,nr)
z2=rebin(reform(z,1,nr),nw,nr)

dum=min((rwall-r2)^2+(zwall-z2)^2,iminv,dimension=1)


win=r*0
wint=where((rwall(iminv)-r0)^2+(zwall(iminv)-z0)^2 gt (r-r0)^2+(z-z0)^2,cnt)

if cnt gt 0 then win(wint)=1
indin=wint

return,win

end
