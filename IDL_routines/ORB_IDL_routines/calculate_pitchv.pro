function calculate_pitchv,g,rr0,zz0,pitch,vxb=vxb,bvec=bvec
;Given a g file and r,z location it calcualtes the 
;R,tor,z unit vector components for a given pitch
;vxb=another unit vector that is vxb useful for getting gyrocenter
;bvec=vector magnetic field with magnitude

;g=readg(122117,365.,runid='EFIT04')
;rr0=2.
;zz0=0.

calculate_bfield,bp,br,bt,bz,g

brp0=interp2d(br,g.r,g.z,rr0,zz0)
btp0=interp2d(bt,g.r,g.z,rr0,zz0)
bzp0=interp2d(bz,g.r,g.z,rr0,zz0)


bunit0=[brp0,btp0,bzp0]/(brp0^2.+btp0^2.+bzp0^2.)^.5
bunitperp=[bzp0-btp0,brp0-bzp0,btp0-brp0]
bunitperp=bunitperp/(total(bunitperp^2.))^.5

;pitch=.75 ;defined as v_par/vtot, where we'll say
sipitch=1.
if g.bcentr*g.cpasma lt 0 then sipitch=-1.

vtot0=1.
vpar=(vtot0*pitch*sipitch)
vperp=(vtot0^2.-vpar^2.)^.5

vel=vpar*bunit0+vperp*bunitperp
print,'pitch=',pitch
print,'vr=',vel(0),' vtor=',vel(1),' vz',vel(2)
print, 'Ang w/ B=',acos(total(vel/abs(vtot0)*bunit0))*180./!pi,' Degrees'
print,'V0 dot B is:',total(vel/abs(vtot0)*bunit0)

vxb=crossp(vel/abs(vtot0),bunit0)
bvec=[brp0,btp0,bzp0]

print, 'vunit cross b is:',vxb
return,vel
end
