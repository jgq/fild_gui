
function get_beam_pitch,g,ndrays=ndrays,nzv=nzv,widthbeam=widthbeam,heightbeam=heightbeam,$
plott=plott

;g=readg('/u/vanzee/TRANSP/142111/TRY1/HRSEFITS/g142111.00525_x257')


;g=readg('/u/vanzee/efits/146096/KEFIT/BYHAND/HIGHRES/g146096.00365')

;widthbeam=(8.8*!pi^.5)
;heightbeam=(21.1*!pi^.5) ;

if n_elements(heightbeam) lt 1 then heightbeam=(21.1*!pi^.5) 
if n_elements(widthbeam) lt 1 then widthbeam=(8.8*!pi^.5)  ;does nothing right now
if n_elements(maxmajr) lt 1 then maxmajr=240.
if n_elements(ndrays) lt 1 then ndrays=100.
if n_elements(nzv) lt 1 then nzv=11
if n_elements(plott) lt 1 then plott=1
zvalsv=[0.]
if nzv gt 1 then begin
zvalsv=(findgen(nzv)/(nzv-1.)*2.-1.)*heightbeam /2.
endif
beamarea=widthbeam*heightbeam ; ;in cm^2

;************************************************************************
;************
xbeams=findgen(501)-250.
us_nb=fltarr(8)-150.
vs_nb=fltarr(8)
vs_nb_s=fltarr(8)
us_nb_s=-us_nb

   u = (findgen(101)-50)*3
for i=0,7 do begin
nn=fltarr(8)
nn(i)=1
beam_geometry_d3d,nn,i,nb
vs_nb(i)= nb.vs + (us_nb(i)-nb.us) * tan(nb.alpha)
vs_nb_s(i)= nb.vs + (us_nb_s(i)-nb.us) * tan(nb.alpha)

endfor
      
beamslopes=us_nb*0.
beamyints=us_nb*0.
for i=0,7 do begin
beamslopes(i)=(vs_nb(i)-vs_nb_s(i))/(us_nb(i)-us_nb_s(i))
beamyints(i)=vs_nb(i)-beamslopes(i)*us_nb(i)
endfor
;************
revi=[1,1,1,1,-1,-1,-1,-1]
tths=findgen(100)/99*2.*!pi

beam_rays=fltarr(8,4,ndrays)
if plott gt 0 then begin
plot,250*cos(tths),250*sin(tths)
oplot,100*cos(tths),100*sin(tths)
endif

for ii=0,7 do begin
beamin=ii

xf00=xbeams(500)
yf00=xf00*beamslopes(beamin)+beamyints(beamin)
xl0=xbeams(0)
yl0=xl0*beamslopes(beamin)+beamyints(beamin)
zf00=0.
zl0=0.

totlen=((xf00-xl0)^2.+(yf00-yl0)^2.+(zf00-zl0)^2.)^.5 

dpath=1.
ntt=long(totlen/dpath)
tt=findgen(ntt)/(ntt-1.)*totlen  ;/totlen

;equations for lines in x,y,z are
xvals=fltarr(ntt)
yvals=xvals
zvals=xvals


xvals=xf00+tt*(xl0-xf00)/totlen
yvals=yf00+tt*(yl0-yf00)/totlen
zvals=zf00+tt*(zl0-zf00)/totlen

if revi(ii) lt 0 then begin
xvals=reverse(xvals)
yvals=reverse(yvals)
zvals=reverse(zvals)
endif

rvals=(xvals^2+yvals^2)^.5
wltr=where(rvals lt 2.35E2)
xvals=xvals(wltr)
yvals=yvals(wltr)
zvals=zvals(wltr)
rvals=rvals(wltr)
ntt=n_elements(rvals)
tt=findgen(ntt)*dpath

if plott gt 0 then oplot,xvals,yvals,psym=5

rminrv=min(rvals)
inrv=!C

indi=lindgen(ntt)
;wltr=where(rvals lt 100.*max(g.bdry(0,*)))
wltr=where(rvals lt maxmajr)


wgt1=where(rvals gt 100.0)
wlt1=where(rvals lt 100.0,nlt1)
goodi=wltr
if nlt1 gt 0 then begin
temp=indi(0:min(wlt1)-1)
;wtemp=where(rvals(temp) lt 100.*max(g.bdry(0,*)))
wtemp=where(rvals(temp) lt maxmajr)

goodi=temp(wtemp)
endif
if plott gt 0 then begin
oplot,xvals(goodi),yvals(goodi),color=100,psym=4
plots,xvals(goodi(0)),yvals(goodi(0)),psym=6,symsize=2,color=45,thick=2
endif

beam_rays(ii,0,*)=congrid(xvals(goodi),ndrays)

beam_rays(ii,1,*)=congrid(yvals(goodi),ndrays)

beam_rays(ii,2,*)=congrid(zvals(goodi),ndrays)

beam_rays(ii,3,*)=congrid(rvals(goodi),ndrays)
print,ii,' ', min(abs(beam_rays(ii,3,*)))
beam_rays(ii,*,*)=beam_rays(ii,*,*)/100.
beam_unit=fltarr(3,8)  ;x,y,z

;beam_unit(0,*)=1.              ;swapped on 3/9/12  was a bug
;beam_unit(1,*)=beamslopes*revi

beam_unit(0,*)=beam_rays(*,0,0)-beam_rays(*,0,ndrays-1)
beam_unit(1,*)=beam_rays(*,1,0)-beam_rays(*,1,ndrays-1)


beam_unit_rzphi=fltarr(3,8,ndrays) ;r,z,phi
for jj=0,7 do begin
beam_unit(0:2,jj)=beam_unit(0:2,jj)/total(beam_unit(0:2,jj)^2)^.5

beam_unit_rzphi(0,jj,*)=beam_unit(0,jj)*beam_rays(jj,0,*)/beam_rays(jj,3,*)+$
beam_unit(1,jj)*beam_rays(jj,1,*)/beam_rays(jj,3,*)
beam_unit_rzphi(1,jj,*)=beam_unit(2,jj)

beam_unit_rzphi(2,jj,*)=-beam_unit(0,jj)*beam_rays(jj,1,*)/beam_rays(jj,3,*)+$
beam_unit(1,jj)*beam_rays(jj,0,*)/beam_rays(jj,3,*)
endfor


endfor

beam_pitch=reform(beam_rays(*,0,*))*0.
beam_pitch_dz=fltarr(8,n_elements(beam_pitch(0,*)),nzv)
beam_rays_dz=fltarr(8,4,n_elements(beam_pitch(0,*)),nzv)
temp=reform(beam_pitch(0,*)*0.)+1.
for i=0,7 do begin
rrrays=reform(beam_rays(i,3,*))
zzrays=reform(beam_rays(i,2,*))
beam_pitch(i,*)=calculate_pitch_vdotb(g,rrrays,zzrays,$
reform(beam_unit_rzphi(0,i,*)),reform(beam_unit_rzphi(2,i,*)),reform(beam_unit_rzphi(1,i,*)))

if nzv gt 1 then begin
for kk=0,nzv-1 do begin
beam_pitch_dz(i,*,kk)=calculate_pitch_vdotb(g,rrrays,zzrays+zvalsv(kk)/100.,$
reform(beam_unit_rzphi(0,i,*)),reform(beam_unit_rzphi(2,i,*)),reform(beam_unit_rzphi(1,i,*)))

beam_rays_dz(i,*,*,kk)=beam_rays(i,*,*)
beam_rays_dz(i,2,*,kk)=beam_rays(i,2,*)+zvalsv(kk)/100.
endfor
endif else begin
beam_pitch_dz(i,*,0)=beam_pitch(i,*)
endelse

endfor
zvalsm=zvalsv
;beam_rays(i,j,k)  j=0,1,2,3 x,y,z,r of ith beam, kth position
;beam_pitch(i,k)= pitch
;beam_pitch_dz(i,k,j)  jth zvalsm = offset from centerline in z

outs={beam_rays:beam_rays,beam_pitch:beam_pitch,beam_pitch_dz:beam_pitch_dz,zvalsm:zvalsm,$
ndrays:ndrays,nzv:nzv,beam_unit:beam_unit,beam_unit_rzphi:beam_unit_rzphi,$
widthbeam:widthbeam,heightbeam:heightbeam,beam_rays_dz:beam_rays_dz,beamslopes:beamslopes}

return,outs



end
