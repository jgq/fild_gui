;+ 
; NAME: g
;     CALCULATE_PSIg
;
; PURPOSE: 
;
;     Given r and z (or array of r and array of z) calculate and return
;     a corresponding scalar or array of psi OR normalize psi (NORM=1),
;     where psi is the poloidal flux.
;
; CALLING SEQUENCE: 
;
;     data = calculate_psig(r, z,  g [,grid=grid] [,norm=norm])
;
; INPUT PARAMETERS: 
;
;     r:      - scalar or array of radius values, in meters.
;     z:      - scalar or array of vertical z values, in meters.
;     a:      - structure containing A0 parameters
;     g:      - structure containing G0 parameters
;
; OPTIONAL INPUT PARAMETERS: 
;
;     NONE
;
; KEYWORDS: 
;
;     GRID   - Specify /GRID or GRID=1 to specify that the calculated
;                   psi normalized be returned as if r and z represented
;                   a grid of r's and z's rather than just ordered pairs.
;     NORM   - Specify /NORM or NORM=1 to return NORMALIZED psi values.
;
; OUTPUTS: 
;
;     temppsi: Scalar, vector, or matrix of psi values, depending
;              on input types and keywords specified.
;
; COMMON BLOCKS: 
;
;     NONE
;
; SIDE EFFECTS: 
;
;     NONE
;
; RESTRICTIONS:
;
;     Prior to this using this function, you must have first read in
;     the EFIT data to fill the a and g structures. 
;
; PROCEDURE: 
;
; CODE TYPE: modeling, analysis
;
; CODE SUBJECT:  edge, equilibrium
;
; EASE OF USE: can be used with existing documentation
;
; OPERATING SYSTEMS:  UNIX of all flavors
;
; EXTERNAL CALLS:  BICUBICSPLINE
;
; RESPONSIBLE PERSON: Ray Jong
;	
; DATE OF LAST MODIFICATION:  10/14/98
;
; MODIFICATION HISTORY:
;     Created 1993.01.25 by Michael D. Brown, LLNL
;     1998.02.17:    Gary D. Porter
;                    Modified to use new EFIT routines and structures.  No
;                    longer uses efitcommon.
;
;-	


function calculate_psig,r,z,g,grid=grid,norm=norm

; check that efit data has been read
if  n_elements(g.shot) eq 0 then begin
  message,/cont,'You must first read in the efit data with READ_EFIT,shot,time'
  return,[0.0]
endif

; check input parameters.
if n_elements(grid) eq 0 then grid=0
if n_elements(norm) eq 0 then norm=0
if (n_elements(r) ne n_elements(z) and grid eq 0) or  $
    n_elements(r) eq 0 or n_elements(z) eq 0 then return,[0.0]

; calculate psi at given r's and z's.
temppsi = bicubicspline(g.psirz(0:g.mw-1,0:g.mh-1),g.r(0:g.mw-1), $
                        g.z(0:g.mh-1),r,z,grid=grid,ierr=ierr)
if ierr ne 0 then temppsi=[0.0]

; convert to scalar or leave as vector, as required by inputs
rinfo=size(r) & zinfo=size(z)
if rinfo(0) eq 0 and zinfo(0) eq 0 then temppsi=temppsi(0)

; normalize psi values if required.
if norm ne 0 then temppsi=(temppsi-g.ssimag)/(g.ssibry-g.ssimag)
return,temppsi
END
