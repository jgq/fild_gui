;************
g=readg('/u/vanzee/TRANSP/142111/TRY1/HRSEFITS/g142111.00525_x257')

xbeams=findgen(501)-250.
us_nb=fltarr(8)-150.
vs_nb=fltarr(8)
vs_nb_s=fltarr(8)
us_nb_s=-us_nb

   u = (findgen(101)-50)*3
for i=0,7 do begin
nn=fltarr(8)
nn(i)=1
beam_geometry_d3d,nn,i,nb
vs_nb(i)= nb.vs + (us_nb(i)-nb.us) * tan(nb.alpha)
vs_nb_s(i)= nb.vs + (us_nb_s(i)-nb.us) * tan(nb.alpha)

endfor
      
beamslopes=us_nb*0.
beamyints=us_nb*0.
for i=0,7 do begin
beamslopes(i)=(vs_nb(i)-vs_nb_s(i))/(us_nb(i)-us_nb_s(i))
beamyints(i)=vs_nb(i)-beamslopes(i)*us_nb(i)
endfor
;************
revi=[1,1,1,1,-1,-1,-1,-1]
tths=findgen(100)/99*2.*!pi
ndrays=100.
beam_rays=fltarr(8,4,ndrays)

plot,250*cos(tths),250*sin(tths)
oplot,100*cos(tths),100*sin(tths)
for ii=0,7 do begin
beamin=ii

xf00=xbeams(500)
yf00=xf00*beamslopes(beamin)+beamyints(beamin)
xl0=xbeams(0)
yl0=xl0*beamslopes(beamin)+beamyints(beamin)
zf00=0.
zl0=0.

totlen=((xf00-xl0)^2.+(yf00-yl0)^2.+(zf00-zl0)^2.)^.5 

dpath=1.
ntt=long(totlen/dpath)
tt=findgen(ntt)/(ntt-1.)*totlen  ;/totlen

;equations for lines in x,y,z are
xvals=fltarr(ntt)
yvals=xvals
zvals=xvals


xvals=xf00+tt*(xl0-xf00)/totlen
yvals=yf00+tt*(yl0-yf00)/totlen
zvals=zf00+tt*(zl0-zf00)/totlen

if revi(ii) lt 0 then begin
xvals=reverse(xvals)
yvals=reverse(yvals)
zvals=reverse(zvals)
endif

rvals=(xvals^2+yvals^2)^.5
wltr=where(rvals lt 2.35E2)
xvals=xvals(wltr)
yvals=yvals(wltr)
zvals=zvals(wltr)
rvals=rvals(wltr)
ntt=n_elements(rvals)
tt=findgen(ntt)*dpath

oplot,xvals,yvals,psym=5

rminrv=min(rvals)
inrv=!C

indi=lindgen(ntt)
wltr=where(rvals lt 100.*max(g.bdry(0,*)))
wgt1=where(rvals gt 100.0)
wlt1=where(rvals lt 100.0,nlt1)
goodi=wltr
if nlt1 gt 0 then begin
temp=indi(0:min(wlt1)-1)
wtemp=where(rvals(temp) lt 100.*max(g.bdry(0,*)))
goodi=temp(wtemp)
endif

print, 'Beam: ' ,ii
print,'beamx0= ',xvals(goodi(0))/100.
print,'beamy0= ',yvals(goodi(0))/100.
print,'beamx1=',xvals(max(goodi))/100.
print,'beamy1= ',yvals(max(goodi))/100.


oplot,xvals(goodi),yvals(goodi),color=100,psym=4
plots,xvals(goodi(0)),yvals(goodi(0)),psym=6,symsize=2,color=45,thick=2

beam_rays(ii,0,*)=congrid(xvals(goodi),ndrays)

beam_rays(ii,1,*)=congrid(yvals(goodi),ndrays)

beam_rays(ii,2,*)=congrid(zvals(goodi),ndrays)

beam_rays(ii,3,*)=congrid(rvals(goodi),ndrays)
print,ii,' ', min(abs(beam_rays(ii,3,*)))
beam_rays(ii,*,*)=beam_rays(ii,*,*)/100.
beam_unit=fltarr(3,8)  ;x,y,z
beam_unit(0,*)=1.
beam_unit(1,*)=beamslopes*revi
beam_unit_rzphi=fltarr(3,8,ndrays) ;r,z,phi
for jj=0,7 do begin
beam_unit(0:2,jj)=beam_unit(0:2,jj)/total(beam_unit(0:2,jj)^2)^.5

beam_unit_rzphi(0,jj,*)=beam_unit(0,jj)*beam_rays(jj,0,*)/beam_rays(jj,3,*)+$
beam_unit(1,jj)*beam_rays(jj,1,*)/beam_rays(jj,3,*)
beam_unit_rzphi(1,jj,*)=beam_unit(2,jj)

beam_unit_rzphi(2,jj,*)=-beam_unit(0,jj)*beam_rays(jj,1,*)/beam_rays(jj,3,*)+$
beam_unit(1,jj)*beam_rays(jj,0,*)/beam_rays(jj,3,*)
endfor


endfor

beam_pitch=reform(beam_rays(*,0,*))*0.
temp=reform(beam_pitch(0,*)*0.)+1.
for i=0,7 do begin
rrrays=reform(beam_rays(i,3,*))
zzrays=reform(beam_rays(i,2,*))
beam_pitch(i,*)=calculate_pitch_vdotb(g,rrrays,zzrays,$
reform(beam_unit_rzphi(0,i,*)),reform(beam_unit_rzphi(2,i,*)),reform(beam_unit_rzphi(1,i,*)))
endfor


;************************************************************************
;************************************************************************

end
