
function derivs,x,y
drag=0
return,[drag,-10.-drag,y(0),y(1)]
end

nfeval=0L
tempnf=0

nsteps=100
h=.1
y=fltarr(4)
ang=45.*!pi/180.

for j=10,60,10 do begin
i=0L
v0=70-j*1. ;m/s
y(0)=v0*cos(ang)  ;vx
y(1)=v0*sin(ang) ;vy
y(2)=0. ;x
y(3)=0. ;y
yout=fltarr(4,nsteps)

while i lt nsteps-1 do begin

  i=long(i*1. + 1L)

  ddeabm,'derivs',0.,y,h
  
  yout(*,i)=y(*)
 endwhile
 print,j
 if j eq 10 then  begin
 plot,yout(2,*),yout(3,*)
 endif else begin
 oplot,yout(2,*),yout(3,*) ,color=j*3
 endelse
 
 
 endfor
 
 
end
