;gfil='/u/vanzee/efits/146121/KEFIT/AUTOKNOT/UPLOAD/HIGHRES/g146121.00555'
gfil='/u/vanzee/efits/146096/KEFIT/BYHAND/HIGH_RES_EFITS/g146096.00545'
phifildintransp=90.-(165.+360.)
fild_rpz=[2.372,phifildintransp*!pi/180.,-0.134]
energ=74.5 ;45 ;74.5
pit=0.57


nmin=100 ;default is 20

dpitch=0.08  ;must be within this pitch
drzmin=0.15  ;



g=readg(gfil)
b=get_beam_pitch(g,plott=0,ndrays=300,nzv=10)



;need to consider correct toroidal angles
;orb_gc_mv,g,fild_rpz,pit,energ,plot=1,orbinteg=0,outs=o, gci=gci; 149
orb_gc_mv,g,fild_rpz,pit,energ,plot=1,orbinteg=1,outs=o, gci=gci,reverse_time=1,nmin=nmin,nsteps=nmin/20.*1.E3 ; 149


;if o.error gt 0 then goto,jump1

rgc=reform(o.yout(0,*)) ;rcont0
zgc=reform(o.yout(2,*)) ;zcont0
phgc=reform(o.yout(1,*))
xgc=rgc*cos(o.yout(1,*))
ygc=rgc*sin(o.yout(1,*))
prz=o.pitchyout*o.pitchsignflip ;o.pitchcont*o.pitchsignflip

dzbeam=1.5*b.heightbeam/2.*1.E-2  ;within this of beam positions in R,z
match=0
wmatch=0
wrz=where(abs(zgc) lt dzbeam,ncz)
if ncz gt 0 then begin
drz=fltarr(ncz,n_elements(b.beam_pitch_dz(*,0,0)),n_elements(b.beam_pitch_dz(0,*,0)),n_elements(b.beam_pitch_dz(0,0,*)))
dprz=drz
dxy=drz
for i=0,ncz-1 do begin  ;replace with vector operation eventually
dprz(i,*,*,*)=abs(prz(wrz(i))-b.beam_pitch_dz)
;drz(i,*,*,*)=((rgc(wrz(i))-reform(b.beam_rays_dz(*,3,*,*)))^2.+(zgc(wrz(i))-reform(b.beam_rays_dz(*,2,*,*)))^2.)^.5

drz(i,*,*,*)=((xgc(wrz(i))-reform(b.beam_rays_dz(*,0,*,*)))^2.+$
(ygc(wrz(i))-reform(b.beam_rays_dz(*,1,*,*)))^2.+$
(zgc(wrz(i))-reform(b.beam_rays_dz(*,2,*,*)))^2.)^.5

;dxy(i,*,*,*)=((xgc(wrz(i))-reform(b.beam_rays_dz(*,0,*,*)))^2.+$   ;just checking in xy plane since already constrained z
;(ygc(wrz(i))-reform(b.beam_rays_dz(*,1,*,*)))^2.)^.5


endfor


wmatch=where(abs(dprz lt dpitch) and abs(drz lt drzmin),match)
;wmatch=where(abs(dprz lt dpitch) and abs(dxy lt dxymin),match)

if match gt 0 then begin
wrmatch=array_indices(dprz,wmatch)

rmatch=rgc(wrz(wrmatch(0,*)))
zmatch=zgc(wrz(wrmatch(0,*)))
xmatch=xgc(wrz(wrmatch(0,*)))
ymatch=ygc(wrz(wrmatch(0,*)))

pmatch=prz(wrz(wrmatch(0,*)))
beammatch=reform(wrmatch(1,*))

!p.multi=[0,2,1]
plot,g.lim(0,*),g.lim(1,*),/iso
oplot,g.bdry(0,*),g.bdry(1,*)
oplot,rgc,zgc,color=200
oplot,rmatch,zmatch,psym=4,color=100,symsize=1.

ths=indgen(100)/99.*2.*!pi
plot,2.5*sin(ths),2.5*cos(ths),/iso
oplot,xgc,ygc,color=200

oplot,b.beam_rays(*,0,*),b.beam_rays(*,1,*),psym=5,symsize=0.5
oplot,xmatch,ymatch,psym=4,color=100,symsize=1.1,thick=2


endif else begin
print,'NO MATCHING BEAM FOUND'
endelse
endif else begin
print,'NO MATCHING BEAM FOUND'
endelse

jump1:



end
