;****************************************************************************************
function derivsgc,t,y
;****************************************************************************************

;gets drift + parallel motion velocities for gc integrator
;
common vars1,vrdrift,vzdrift,vtdrift,g

vrd=interpolatexy(g.r,g.z,vrdrift,y(0,*),y(2,*))
vtd=interpolatexy(g.r,g.z,vtdrift,y(0,*),y(2,*))
vzd=interpolatexy(g.r,g.z,vzdrift,y(0,*),y(2,*))

;print, vrd,' ',vtd,' ',vzd
;return,[[vrd],[vtd/y(0)],[vzd]]
return,[vrd,vtd/y(0),vzd]

end

;****************************************************************************************
function getvels,r,z
;****************************************************************************************
;gets drift + parallel motion velocities given vector of r,z
;same thing as derivsgc except can take sep. vectors of r,z
common vars1,vrdrift,vzdrift,vtdrift,g

vrd=interpolatexy(g.r,g.z,vrdrift,r,z)
vtd=interpolatexy(g.r,g.z,vtdrift,r,z)
vzd=interpolatexy(g.r,g.z,vzdrift,r,z)

;print, vrd,' ',vtd,' ',vzd
;return,[[vrd],[vtd/y(0)],[vzd]]
return,[[vrd],[vtd/r],[vzd]]

end

;****************************************************************************************
function getdrift,r,z,vpar,vperp,gci
;****************************************************************************************

;returns [ur,ut,uz] drift*(z*ee/mm/mi)
;interpolates drifts to r,z for a given vpar vperp

ucurvr=interpolatexy(gci.g.r,gci.g.z,gci.ucurvr*vpar^2,r,z)
ucurvt=interpolatexy(gci.g.r,gci.g.z,gci.ucurvt*vpar^2,r,z)
ucurvz=interpolatexy(gci.g.r,gci.g.z,gci.ucurvz*vpar^2,r,z)

ugradr=interpolatexy(gci.g.r,gci.g.z,gci.ugradr*vperp^2/2.,r,z)
ugradt=interpolatexy(gci.g.r,gci.g.z,gci.ugradt*vperp^2/2.,r,z)
ugradz=interpolatexy(gci.g.r,gci.g.z,gci.ugradz*vperp^2/2.,r,z)

return,[ucurvr+ugradr,ucurvt+ugradt,ucurvz+ugradz]
end



;****************************************************************************************
;MAIN
;****************************************************************************************

common vars1,vrdrift,vzdrift,vtdrift,g
;****************************************************************************************
fild_rtz=[2.25,0.,-.66]  ;r,phi (in radians),z of fild

;example lost part = 80.,0.62,2.23,-.695 to fild
energy=double(50.) ;kev
pitch0i=double(.3) ;61955 ;  ;v_||/v   0.7,0.65,.6,.55,.5,.4,.3,.2
mm=2.  ;proton masses
z=1.  ;charge in proton chg ;can be negative
R0i=2.2 ;25 ;2.23 ;23   ;fild is 2.2-2.3, -.61--.72
Z0i=-.05 ;-0.10
phi0=0.
inputsatgc=1  ;if input is at guiding center then =1, if instant. then =0
orbinteg=1  ;to integrate orbit or not
reverse_time=0 ;to reverse time from initial position = 1
calcdrifts=1   ;will calculate drifts at all positions on integrated orbit
plott=1  ;if gt 0 then will plott orbit
integrator2use='ddeabm'  ;'ddeabm'  the integrator for actually following gc.  Only
    	    	    ;used if orbinteg gt 0

;****************************************************************************************


error=0
topp=1

if topp gt 0 then begin
;g=readg(142111,325,runid='EFIT04')
g=readg('/u/vanzee/TRANSP/142111/TRY1/HRSEFITS/g142111.00525_x257')
;g=readg('/u/vanzee/idl/lib/LINETRACE/g142111.00525_e257') ;129,257,65
gci=get_gcinputs(g,driftterms=1)  ;made separate routine from 
;com inputs because equil. may not change for many calcs but 
;interesting pitch energy etc. may more often
endif

ee=double(1.60E-19)
mi=double(1.67E-27)
ejou=energy*ee*1.E3
v0=(ejou/mi/mm*2.)^.5

eqovm=ee*z/mm/mi

;taking position, energ. and pitch and getting inputs for coms at
;gc. trivial if already starting at gc, i.e. just an interpolation of f0,psi0,b0,etc.
ci=get_com_ins(eqovm,ejou,v0,pitch0i,r0i,z0i,gci,inputsatgc=inputsatgc)

z0=ci.z0
r0=ci.r0

;****************************************************************************************
;Getting original COM
;****************************************************************************************
mu0=ejou/ci.b0*(1.-ci.pitch0^2.)  ;magnetic moment using pitch rel to guiding center??
;mu0=mm*mi*(vperp0)^2/2/b0  ;original magnetic moment at gc  ;should this include the perp dfrift??

signfud=-1. ;dont yet know for sure about this -1., may cause
;an error for difft current directions or something

;this is the initial toroidal canonical angular momentum
pphi0=ci.psi0-signfud*ci.f0*ci.pitch0/z/ee/ci.b0*(2.*mm*mi*ejou)^.5

pitchrz=-(pphi0-gci.psirz)/(gci.frz/z/ee/gci.btot*(2.*mm*mi*ejou)^.5 )
;this is the pitch at all r,z that have same canonical ang. momentum
;and energy

murz=ejou/gci.btot*(1.-pitchrz^2.)/mu0 ;magnetic moments from pitchrz scaled to mu0
;not all possible though because murz must = mu0

;***********************************************************************************

;Plotting below here
;***********************************************************************************
dev0=!D
if plott eq 0 then begin
set_plot,'null' 
endif ;cant just skip plotting commands even if you dont want a plot because contour needs to run

rebinrz=0 ;factor to upsample the efit dimensions for calculating contour - might be useful for 65x65 efits
;to do anything it has to be >1 and an integer
;allowed pitches and contour
conmurz=murz
conr=g.r
conz=g.z
if rebinrz gt 1 then begin
rebinrz=fix(rebinrz)
nrg=n_elements(g.r)
nzg=n_elements(g.z)
conmurz=rebin(conmurz,nrg*rebinrz,nzg*rebinrz)
conr=rebin(conr,nrg*rebinrz)
conz=rebin(conz,nzg*rebinrz)
endif

contour,conmurz,conr,conz,levels=1.0,path_xy=paths,path_data_coords=1,$
path_info=pathinf,closed=0  

if plott eq 0 then begin
set_plot,dev0.name  ;setting back original !D
endif


;DETERMINING ORBIT INFO FROM CONTOUR OUTPUT
;*****************************************************************************************************
GROSSCLASS=0
if n_elements(paths) eq 0 then begin
;had a problem with contour. could iterate here using rebin above and might help
error=1
goto,jumpath
endif

nct=n_elements(paths(0,*))
rcont=reform(paths(0,0:nct-1)) ;could have be double valued in mu0..so will look for closest to original position
zcont=reform(paths(1,0:nct-1))
ncont=n_elements(pathinf.level) ;number of contours drawn
ind0=0
pioff=pathinf.offset
pin=pathinf.n
ptyp=pathinf.type

if ncont gt 1 then begin
temp=min((rcont-r0)^2.+(zcont-z0)^2,mind)  ;finding contour that goes through initial gc location
indemp=mind-pioff
ind0=max(where(indemp ge 0))
endif

ii0=pioff(ind0)
ii1=ii0+pin(ind0)-1

rcont0=rcont(ii0:ii1)
zcont0=zcont(ii0:ii1)

temp=min((rcont0-r0)^2.+(zcont0-z0)^2,mind0) ;contour doesnt always start at r0,z0
rcont0=shift(rcont0,-mind0)
zcont0=shift(zcont0,-mind0)
nc0=n_elements(rcont0)
tempr0=rcont0
tempz0=zcont0
for i=0,nc0-1 do begin
rcont0(i)=tempr0(nc0-1-i)
zcont0(i)=tempz0(nc0-1-i)
endfor

nsrc=0
rcont0=smooth(rcont0,nsrc,/edge_truncate)   ;These are the r,z coordinates of the contour evaluated gc orbit
zcont0=smooth(zcont0,nsrc,/edge_truncate)

;*****************************************************************************************************
;Classifying particle 
;;ORBIT CLASSIFICATION
;0=SOME ERROR IN CONTOURING  ;1=PASSING, 2=TRAPPED, 3=CLOSED ORBIT ON EFIT GRID BUT HITS WALL, 4 = OPEN ORBIT ON EFIT GRID
;*****************************************************************************************************
pitchcont=interpolatexy(g.r,g.z,pitchrz,rcont0,zcont0)
temp=where(pitchcont gt 0.0,ncountg)  ;seeing if sign of pitch flipped
temp=where(pitchcont lt 0.0,ncountl)  ;seeing if sign of pitch flipped
;anywhere on contour, if it did, then went through v||=0 and is trapped
ncount=0
if ncountg ge 1 and ncountl ge 1 then  ncount=1

if ptyp(ind0) gt 0 then begin   ;checking whether its closed or not using pathinf.type
GROSSCLASS=1 
if ncount gt 0 then begin
GROSSCLASS=2  ;BOUNCED SO ITS TRAPPED
print,'TRAPPED ORBIT'
endif else begin
print,'PASSING ORBIT'
endelse
if max(rcont0) lt max(g.lim(0,*)) then begin  ;FIRST SIMPLE TEST IF ANY POINTS ARE OUTSIDE LIMITER
;test for hitting wall
inout=check_lim_inout(rcont0,zcont0,g=g,usefinewall=1) ;IF PASSES FIRST TEST THEN TEST ALL POINTS ON CONTOUR
temp=where(inout lt 1,nino)

if nino gt 0 then begin
GROSSCLASS=3 
 print,'HITS WALL'
endif

endif else begin
GROSSCLASS=3
print,'HITS WALL'
endelse

endif else begin
print,'OPEN ORBIT'
GROSSCLASS=4
endelse
;***********DONE CLASSIFYING ORBIT
;****************now know whether passing trapped or lost
circumrcont=total( ((rcont0(1:*)-rcont0(0:*))^2+(zcont0(1:*)-zcont0(0:*))^2)^.5 )
;rough circumference of r,z contour

;***********************************************************************************
;NOW FINDING CLOSEST POINT OF APPROACH TO FILD  only in R,z using contour
dfild=min(((fild_rtz(0)-rcont0)^2+(fild_rtz(2)-zcont0)^2)^.5,fin0)  ;closest in R,z it gets to fild
dfild_rz=[rcont0(fin0),zcont0(fin0)] ;R,z at that position
pitch_dfild_rz=interpolatexy(g.r,g.z,pitchrz,rcont0(fin0),zcont0(fin0))  ;pitch at position of closest approach
bfild=interpolatexy(g.r,g.z,gci.btot,fild_rtz(0),fild_rtz(2)) ;magnetic field at fild



;drifts
vparrz=-1 & vperprz=-1 & vrdrift=-1 & vzdrift=-1 & vpdrift=-1 & vtdrift=-1
tstep=-1 & thets=-1 & timearr=-1 & yout=-1 & phtot=-1 & timeorb=-1 & timearr=-1 & dphitor=-1 & 
dpoltime=-1 & omegatoroidal=-1 & omegapoloidal=-1 & pitchyout=-1  
vd_tot=-1 & vd_o=-1 & dfild_i=-1 & dfild_rz_i=-1 & $
pitch_dfild_rz_i=-1 & dfild_it=-1 & dfilt_rz_it=-1 & pitch_dfild_rz_it=-1


;NOW WILL ITNEGRATE GC ORBIT AND GET TRANSIT TIMES IF REQUESTED
if orbinteg gt 0 then begin
;***********************************************************************************
;finding a point inside of enclosed orbit at average z location
;this defines the position that is used to evaluate if orbit has gone
;around 2pi in integrator
z_orb=avg(zcont0)  ;finding approx. center of midplane banana
zlt1=where(abs(zcont0) lt 0.015*max(abs(zcont0)),nlt1)  ;could do something else looking at avg. z heigh
if nlt1 lt 2 then zlt1=where(abs(zcont0) lt 0.1*max(abs(zcont0)),nlt1) ;pretty stupid way to find the avg. r of banana midplane
if nlt1 lt 2 then zlt1=where(abs(zcont0) lt 0.5*max(abs(zcont0)),nlt1)
if nlt1 lt 2 then zlt1=where(abs(zcont0) lt 0.75*max(abs(zcont0)),nlt1)

r_orb=avg(rcont0(zlt1))

;going to follow orbit for a full poloidal transit
;doing orbit gc integration
;***********************************************************************************

;***********************************************************************************

;NOW CALCULATING ALL VELOCITY COMPONENTS - ONLY NECESSARY IF REQUESTING 
;TRANSIT TIMES / ORBITAL FREQUENCIES
;;***********************************************************************************

omegatoroidal=0.0 ;will be toroidal passing+precession if passing or just prece. if trapped
omegapoloidal=0.0 

vparrz=(ejou/mi/mm*2.)^.5*pitchrz  
vperprz=(ejou/mi/mm*2.-vparrz^2.)^.5

vpartrz=vparrz*gci.bt/gci.btot
vparrrz=vparrz*gci.br/gci.btot
vparzrz=vparrz*gci.bz/gci.btot

ucurvr=gci.ucurvr*mm*mi*vparrz^2/z/ee
ucurvt=gci.ucurvt*mm*mi*vparrz^2/z/ee
ucurvz=gci.ucurvz*mm*mi*vparrz^2/z/ee

ugradr=gci.ugradr*mm*mi*vperprz^2/2/z/ee
ugradt=gci.ugradt*mm*mi*vperprz^2/2/z/ee
ugradz=gci.ugradz*mm*mi*vperprz^2/2/z/ee

vrdrift=(ucurvr+ugradr)+vparrrz
vzdrift=(ucurvz+ugradz)+vparzrz
vpdrift=(vrdrift^2+vzdrift^2)^.5
vtdrift=(ucurvt+ugradt)+vpartrz

;estimating transit times based on contour only ...could be an option if other one takes too long
;boils down to what is in that thesis
;vcont=getvels(rcont0,zcont0)
;vcontp=(vcont(*,0)^2+vcont(*,2)^2.)^.5
;dtpolcont= circumrcont/avg(vcontp)  ;
;omegapolcont=2.*!pi/dtpolcont


y=dblarr(3) ;
y=[r0,phi0,z0]

t0s=systime(1)
; Main loop
i=0L
nsteps=1E3  ;maximum number of times
yout=y ;dblarr(3,nsteps)

der0=derivsgc(0,y)
nmin=25.
if grossclass eq 0 then begin  ;trying to get timestep appropriate to transit velocity
dtmin=2.*!pi*r0/abs(der0(2))/nmin ;20./ci.q0
endif else begin
dtmin=circumrcont/(der0(0)^2+der0(1)^2)^.5/nmin
endelse
h=min([dtmin,70./ci.wci0])
if reverse_time gt 0 then h=-h
;h=40./ci.wci0 ;timestep   

nfeval=0.
thets=atan(y(0)-r_orb,y(2)-z_orb)
timearr=0D
tstep=h

while i lt nsteps-1 do begin
dydx=derivsgc(0,y)
  i=long(i*1. + 1L)
if integrator2use eq 'rk4' then begin
  y=rk4(y,dydx,0.,h,'derivsgc',/double)
  tempnf=1.
endif else begin
ddeabm,'derivsgc',0.,y,h,nfev=tempnf,epsabs=epsabs,epsrel=epsrel; ,tgrid=tgrid,ygrid=ygrid,/intermediate
endelse

 nfeval=nfeval+tempnf
 
  ;*****COULD IMPROVE ALL THIS BY KEEPING ALL EVALUATIONS FROM DDEABM
  thets=[thets,atan(y(0)-r_orb,y(2)-z_orb)]
  yout=[[yout],[y]]
 
 timearr=[timearr,i]
  if i gt 2 then begin
 circsofar= total(((yout(0,1:*)-yout(0,*))^2+(yout(2,1:*)-yout(2,*))^2)^.5)
 
 ;checks to make sure you went around at least one
if (circsofar gt 0.8*circumrcont) then begin
phtot=phunwrap(thets)  ;thets can have 2pi flips so this unwraps it
 npht=n_elements(phtot)
 dphtot=phtot(npht-1)-phtot(0)
 
if (abs(dphtot) gt 2.*!pi) then goto,jumpi
if circsofar gt 2.*circumrcont then begin
print,'LIKELY ERROR'
error=1
goto,jumpi
endif

endif

endif

endwhile
jumpi:

t1s=systime(1)
print,'t1s-t0s',t1s-t0s
;***********************************************************************************

;DONE WITH ORBIT INTEGRATION NOW GETTING TRANSIT FREQUENCIES
timeorb=i*h ;seconds
timearr=timearr*h ;seconds
print,'saved steps:',n_elements(timearr)
print,'actual steps:',nfeval

phtot0=phunwrap(thets)
phtot0=abs(phtot0-phtot0(0))
dphitor=interpol(reform(yout(1,*)-yout(1,0)),phtot0,2.*!pi) ;interpolating phi vs theta data and finding exact change in toroidal
;angle after 1 poloidal orbit transit
dpoltime=interpol(timearr,phtot0,2.*!pi)  ;poloidal dt, interpolating to 2pi using poloidal angle vs. time to 
;get exact time at 1 poloidal transit

omegatoroidal = dphitor/dpoltime  ;angular toroidal velocity avg. over pol. transit
omegapoloidal = 2.*!pi/dpoltime   ;angular poloidal transit  veloc.

;to find out exactly how far toroidally it went in 1 poloidal transit

;orbm_adams,g,[r0i,0,z0i],vel2ui,rv0,e0=energy,nsteps=30.E3,time_reverse=0,$
;epsabs=1.E-8,epsrel=1.E-8

;endif
pitchyout=interpolatexy(g.r,g.z,pitchrz,yout(0,*),yout(2,*))
vd_tot=0.
vd_o=0.
if calcdrifts gt 0 then begin
vd_tot=derivsgc(0,yout)
vd_o=getdrift(yout(0,*),yout(2,*),vparrz,vperprz,gci)*mm*mi/z/ee   
endif


print, 'TOROIDAL ANGULAR ROTAT. FREQ IS:',OMEGATOROIDAL/2./!PI*1.E-3,' kHz'
print, 'POLOIDAL ANGULAR ROTAT. FREQ IS:',OMEGAPOLOIDAL/2./!PI*1.E-3,' kHz'


;NOW FINDING CLOSEST POINT OF APPROACH TO FILD using RZ alone from integrated orbit
dfild_i=min(((fild_rtz(0)-yout(0,*))^2+(fild_rtz(2)-yout(2,*))^2)^.5,fin0)  ;closest in R,z it gets to fild
dfild_rz_i=[yout(0,fin0),yout(2,fin0)] ;R,z at that position
pitch_dfild_rz_i=interpolatexy(g.r,g.z,pitchrz,yout(0,fin0),yout(2,fin0))  ;pitch at position of closest approach

;NOW FINDING CLOSEST POINT OF APPROACH TO FILD using RZ,phi from integrated orbit
dfild_it=min(((fild_rtz(0)*cos(fild_rtz(1))-yout(0,*)*cos(yout(1,*)))^2+$
(fild_rtz(0)*sin(fild_rtz(1))-yout(0,*)*sin(yout(1,*)))^2+$
(fild_rtz(2)-yout(2,*))^2)^.5,fin0)  ;closest in R,phi,z it gets to fild
dfild_rz_it=[yout(0,fin0),yout(1,fin0),yout(2,fin0)] ;R,phi,z at that position
pitch_dfild_rz_it=interpolatexy(g.r,g.z,pitchrz,yout(0,fin0),yout(2,fin0))  ;pitch at position of closest approach

ENDIF ;END OF ORBIT INTEGRATION AND ORBITAL TIME CALC
;***********************************************************************************

;***********************************************************************************
;JUST PLOTTING OUTPUT BELOW HERE IF REQUESTED
;***********************************************************************************

if plott gt 0 then begin
!p.multi=0

if (orbinteg gt 0) and (plott gt 0) then !p.multi=[0,2,1]

cz=1.5
yr=[-1.3,1.3]
xr=[min(g.r),max(g.r)] ;0 ;[.8,2.4]
cc=254

plot,g.lim(0,*),g.lim(1,*),/iso,xrange=xr,yrange=yr,xstyle=1,ystyle=1,xtitle='R (m)',ytitle='z (m)',charsize=cz

if plott gt 0 then begin
oplot,g.bdry(0,*),g.bdry(1,*),thick=2,color=100  ;this defines the surface
;where it would reflect if trapped, i.e. v|| = 0
plots,r0,z0,psym=4,thick=2,color=100
plots,g.rmaxis,g.zmaxis,psym=4,thick=3,symsize=1,color=45
endif

dzf=0.05
drf=dzf
oplot,fild_rtz(0)+[-drf,drf,drf,-drf,-drf],fild_rtz(2)+[-dzf,-dzf,dzf,dzf,-dzf],color=100,thick=2

oplot,rcont0,zcont0,psym=4,symsize=.5
plots,r0,z0,psym=4,thick=2,color=100
oplot,yout(0,*),yout(2,*),color=45
plots,r_orb,z_orb,psym=5,thick=2,color=150
plot,yout(0,*)*cos(yout(1,*)),yout(0,*)*sin(yout(1,*)),thick=.5,psym=0,xtitle='x(m)',ytitle='y(m)',charsize=cz,/iso,$
xrange=[-2.5,2.5],yrange=[-2.5,2.5],ystyle=1,xstyle=1,symsize=.25

oplot,yout(0,*)*cos(yout(1,*)),yout(0,*)*sin(yout(1,*)),thick=.5,psym=-4
plots,r0*cos(phi0),r0*sin(phi0),color=100,psym=4,thick=2
endif
;***********************************************************************************

jumpath:


outs = {error:error, r0:r0, z0:z0, pitch0:ci.pitch0, phi0:phi0, $
gci:gci, ci:ci, rcont0:rcont0, zcont0:zcont0, mu0:mu0, pphi0:pphi0, velvec0:ci.velvec0, pitchrz:pitchrz, murz:murz, pitchcont:pitchcont, $
grossclass:grossclass, dfild:dfild, dfild_rz:dfild_rz, pitch_dfild_rz:pitch_dfild_rz, bfild:bfild, circumrcont:circumrcont, $
inputs:{energy:energy, pitch0i:pitch0i, R0i:r0i, Z0i:z0i, phi0i:phi0, mm:mm, z:z, ejou:ejou, $
v0:v0, inputsatgc:inputsatgc, reverse_time:reverse_time, $
orbinteg:orbinteg, calcdrifts:calcdrifts, integrator2use:integrator2use}, $
;below here will be all -1 if no orbit integration
drifts:{vparrz:vparrz, vperprz:vperprz, vrdrift:vrdrift, vzdrift:vzdrift, vpdrift:vpdrift, vtdrift:vtdrift}, $
;these are from orbit integration
tstep:tstep, thets:thets, timearr:timearr, yout:yout, phtot:phtot, timeorb:timeorb, dphitor:dphitor, dpoltime:dpoltime, $
omegatoroidal:omegatoroidal, omegapoloidal:omegapoloidal, pitchyout:pitchyout, vd_tot:vd_tot, vd_o:vd_o, $
dfild_i:dfild_i, dfild_rz_i:dfild_rz_i, pitch_dfild_rz_i:pitch_dfild_rz_i, dfild_it:dfild_it, dfild_rz_it:dfild_rz_it, pitch_dfild_rz_it:pitch_dfild_rz_it}

;
end
