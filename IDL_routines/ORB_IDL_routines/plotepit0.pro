!p.multi=[0,2,2]
loadct,5
xt='Energy (kev)'
yt='Pitch (V||/V)'
cz=1.5
sig=orbcarr
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig),xtitle=xt,ytitle=yt,charsize=cz
contour,sig,energyarr,pitcharr,levels=[0,1,2,3],/overplot,c_thick=2,c_colors=100,$
c_labels=[1,1,1,1]
wl1=where(filddistarr lt 0.1)
x=array_indices(filddistarr,wl1)
badi=array_indices(tangarr,where(finite(tangarr) eq 0))
plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
;plots,energyarr(x(0,*)),pitcharr(x(1,*)),psym=4,symsize=2,thick=2,color=113
endif
loadct,5

sig=tangarr
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig),xtitle=xt,ytitle=yt,charsize=cz

sig=pangarr
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig),xtitle=xt,ytitle=yt,charsize=cz

ntor=0
mpol=1
pp=1
omeg=-ntor*abs(tangarr)+(mpol+pp)*abs(pangarr)
omeg=omeg/2./!pi
sig=omeg*1.E-3
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig),xtitle=xt,ytitle=yt,charsize=cz
freqs=findgen(10)*15.
contour,sig,energyarr,pitcharr,levels=freqs,/overplot,c_thick=2,c_colors=255,$
c_labels=freqs*0+1


if wl1(0) gt -1 then begin
loadct,4
plots,energyarr(x(0,*)),pitcharr(x(1,*)),psym=4,symsize=2,thick=2,color=113
endif
end
