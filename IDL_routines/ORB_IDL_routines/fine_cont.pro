function fine_cont,rins,zins,ds=ds
; fill in a closed contour with finer points

if n_elements(ds) lt 1 then ds=.00005
ii=0
tmp=fltarr(2,10000)
x=rins & y=zins

for i=1,n_elements(x)-1 do begin
  ii=ii+1
  x0=x(i-1) & x1=x(i) & y0=y(i-1) & y1=y(i)
  tmp(0,ii-1)=x0 & tmp(1,ii-1)=y0
  dist=sqrt((x1-x0)^2 + (y1-y0)^2)
  dx=(x1-x0)*ds/dist & dy=(y1-y0)*ds/dist
  j=1
  while j*ds lt dist do begin
    j=j+1 & ii=ii+1
    tmp(0,ii-1)=j*dx + x0
    tmp(1,ii-1)=j*dy + y0
  end
end
;ii=ii+1
tmp(0,ii)=x(i-1) & tmp(1,ii)=y(i-1)
routs=fltarr(ii+1) & zouts=fltarr(ii+1)
routs(*)=tmp(0,0:ii) & zouts(*)=tmp(1,0:ii)

rzouts=fltarr(2,n_elements(routs))
rzouts(0,*)=routs
rzouts(1,*)=zouts
return,rzouts

end
    
