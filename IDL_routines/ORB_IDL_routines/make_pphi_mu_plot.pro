
;restore,'epr_scan_t725_142111.sav'
;restore,'epr_scan_t325_142111_40x40x40.sav'
restore,'epr_scan_t525_142111_20x50x50.sav'

dct=13

e2d=fltarr(nnrad,ner,npit)
p2d=e2d
erangepl=[73,78] ;[37,40.]
ee=double(1.60E-19)
for j=0,nnrad-1 do begin
for i=0,ner-1 do begin
e2d(j,i,*)=energyarr(i)
p2d(j,i,*)=pitcharr
endfor
endfor
e2dj=e2d*ee*1.E3
xold=phi0arr/g.ssibry
yold=mu0arr*abs(g.bcentr)/e2dj
plot,xold,yold,psym=4

xtit='p_phi0/psi_lcfs'
ytit='mu0*Bo / Eo'
orbcarrsign=orbcarr
wpas=where((orbcarr eq 1) or (orbcarr eq 2) and (p2d ne 0.0))
orbcarrsign(wpas)=orbcarrsign(wpas)*p2d(wpas)/abs(p2d(wpas))  ;will
;make counter passing negative 1 or 2
nrr=100
nzz=100
phix=findgenm(nrr,range=[min(xold),max(xold)])
emuy=findgenm(nzz,range=[min(yold),max(yold)])
in2plxy=where(e2d gt min(erangepl) and e2d lt max(erangepl),ncti)


modef=80.  ;mode freq in khz
moden=4.   ;mode n
modem=12.  ;mode m
dp=.3  ;find particles with p=+/- dp
p1=-3.   ;resonance 

;pmode=-reform(modef*1.E3*2.*!pi-moden*abs(tangarr(in2plxy)))/reform(abs(pangarr(in2plxy)))-modem  ;according to bills pop for l
;but gives what seems like negative values for pmode

;pmode=reform(modef*1.E3*2.*!pi+moden*abs(tangarr(in2plxy)))/reform(abs(pangarr(in2plxy)))-modem  ;according to Raffis pop for 
;but doesn't produce -p similar to our papers

pmode=reform(modef*1.E3*2.*!pi+moden*(tangarr(in2plxy)))/reform(abs(pangarr(in2plxy)))  ;according to raffi except grouping m+p

;omeg2=reform(modefa(i)*1.E3*2.*!pi+modena(i)*abs(tangarr(*,e2pr,*)))/reform(abs(pangarr(*,e2pr,*)))-modema(i)

peq1=0
for p1=-30,30 do begin
temp=where(pmode gt p1-dp and pmode lt p1+dp,nctt)
if nctt gt 0 then begin

peq1=[peq1,temp]
endif
endfor
peq1=peq1(1:*)
np1=n_elements(peq1)

classxy=tri_surf(orbcarr(in2plxy),xold(in2plxy),yold(in2plxy),bounds=[min(phix),min(emuy),$
max(phix),max(emuy)],nx=nrr,ny=nzz,missing=0,/linear)
classxysign=tri_surf(orbcarrsign(in2plxy),xold(in2plxy),yold(in2plxy),bounds=[min(phix),min(emuy),$
max(phix),max(emuy)],nx=nrr,ny=nzz,missing=0,/linear)

pmodexy=tri_surf(pmode,xold(in2plxy),yold(in2plxy),bounds=[min(phix),min(emuy),$
max(phix),max(emuy)],nx=nrr,ny=nzz,missing=0,/linear)



fild_rpz=[2.25,0.,-.66]
e2pp=avg(erangepl)*ee*1.E3
orb_gc_mv,g,fild_rpz,0.6,avg(erangepl),plot=0,orbinteg=0,outs=o ;149
orb_gc_mv,g,fild_rpz,0.7,avg(erangepl),plot=0,orbinteg=0,outs=o1 ;149
orb_gc_mv,g,fild_rpz,0.75,avg(erangepl),plot=0,orbinteg=0,outs=o2 ;149




!p.multi=[0,2,2]
cz=2
clevs=[-2,-1,0,1,2,3,4,5,6]
loadct,dct
;shade_surf,classxy,phix,emuy,ax=90,az=0,shades=bytscl(classxy,min=-3,max=6),$
;xtitle=xtit,ytitle=ytit,charsize=cz
;loadct,4
;plot,xold,yold,psym=4,symsize=.2,color=118,$
;xtitle=xtit,ytitle=ytit,charsize=cz
;loadct,dct


shade_surf,classxysign,phix,emuy,ax=90,az=0,shades=bytscl(classxysign,min=-3,max=6),$
xtitle=xtit,ytitle=ytit,charsize=cz
;loadct,4
;plot,xold,yold,psym=4,symsize=.2,color=118

rlabs=['COUNT PASS-NAE','COUNT PASS-AE',' ','PASS-AE','PASS-NAE',' ','TRAPPED','CLOSED-HITS WALL','OPEN ORBIT']
for j=0,8 do begin
i=j-2
xyouts,max(phix)*1.05,.4+i*.1,rlabs(j),color=bytscl(i,min=-3,max=6),$
chars=2,charthick=2

endfor
ccols=bytscl(clevs,min=-3,max=6)
contour,classxysign,phix,emuy,levels=clevs,c_lab=clevs*0+1,c_charsize=2,c_col=ccols,$
c_chart=2,xtitle=xtit,ytitle=ytit,charsize=cz,xstyle=4,ystyle=4

shade_surf,classxysign,phix,emuy,ax=90,az=0,shades=bytscl(classxysign,min=-3,max=6),$
xtitle=xtit,ytitle=ytit,charsize=cz
loadct,4
oplot,xold(in2plxy),yold(in2plxy),psym=4,symsize=.25,color=118


!p.multi=0
loadct,dct
plot,phix,emuy,xtitle=xtit,ytitle=ytit,charsize=cz,/nodata
in2pl=where(orbcarr eq i and e2d gt min(erangepl) and e2d lt max(erangepl),ncti)

for j=0,8 do begin  ;orbcarrsign,xold,yold
i=j-2
ind2pl=where(orbcarrsign eq i and e2d gt min(erangepl) and e2d lt max(erangepl),ncti)

if ncti gt 0 then begin
oplot,xold(ind2pl),yold(ind2pl),color=ccols(j),psym=4,symsize=1,thick=.5
endif
endfor
loadct,5
;if np1 gt 1 then oplot,xold(in2plxy(peq1)),yold(in2plxy(peq1)),psym=5,color=255,thick=3,symsize=.5
;contour,pmodexy,phix,emuy,levels=reverse(-indgen(33)),/overplot,color=100,thick=2
;contour,pmodexy,phix,emuy,levels=indgen(40)-5,/overplot,color=255,thick=2
;contour,pmodexy,phix,emuy,levels=indgen(20)+15,/overplot,color=255,thick=2

plots,o.pphi0/g.ssibry,o.mu0*abs(g.bcentr)/e2pp,psym=5,symsize=2,color=180,thick=5 
plots,o1.pphi0/g.ssibry,o1.mu0*abs(g.bcentr)/e2pp,psym=5,symsize=2,color=180,thick=5 
plots,o2.pphi0/g.ssibry,o2.mu0*abs(g.bcentr)/e2pp,psym=5,symsize=2,color=180,thick=5 
;plots,o.pphi0/g.ssibry,o.mu0,psym=5,symsize=2,color=180,thick=5 
;plots,o1.pphi0/g.ssibry,o1.mu0,psym=5,symsize=2,color=180,thick=5 
;plots,o2.pphi0/g.ssibry,o2.mu0,psym=5,symsize=2,color=180,thick=5 


end
