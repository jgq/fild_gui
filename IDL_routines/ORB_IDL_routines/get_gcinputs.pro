;Just a copy of calculate_bfield but with added f, btot,gradients, etc.

function get_gcinputs,g,driftterms=driftterms

if n_elements(driftterms) lt 1 then driftterms=0  ;will return
;all curvature and gradient drift terms

mw=g.mw & mh=g.mh
bp=fltarr(mw,mh) & bt=fltarr(mw,mh) & br=fltarr(mw,mh) & bz=fltarr(mw,mh)
dpsidx = fltarr(mw,mh)
dpsidy = fltarr(mw,mh)

; calculate vertical derivative of psi
for i = 0,mw-1 do begin
 dpsidy(i,*) = Deriv(g.z(0:mh-1),g.psirz(i,0:mh-1))
endfor

; calculate horizontal derivative of psi
for j = 0,mh-1 do begin
  dpsidx(*,j) = Deriv(g.r(0:mw-1),g.psirz(0:mw-1,j))
endfor

; calculate array of Br, Bz, and Bp
for j = 0,mh-1 do begin
   br(*,j) = dpsidy(0:mw-1,j)/g.r(0:mw-1)
   bz(*,j) = -dpsidx(0:mw-1,j)/g.r(0:mw-1)
endfor
bp = sqrt(br*br+bz*bz)

; WWH get right sign
if g.cpasma lt 0. then begin
  br=-br & bz=-bz
end

; Calculate toroidal field
; Original coding was from gfield.for by Peter Politzer,
;   translated to IDL by Gary Porter (see BFIELD.PRO).
; The code below has be optimized for IDL by Michael D. Brown, 2/16/94

dpsi = (g.ssibry-g.ssimag)/float(mw-1)
; first order Bt value.
for j=0,mh-1 do bt(0:mw-1,j)=g.bcentr*g.rzero/g.r(0:mw-1)  
k = long((g.psirz - g.ssimag)/dpsi) 
iw=where(k ge 0 and k lt mw-1,n)  ; 1-d indexes where k is a valid index.
if n gt 0 then begin
  iwr = iw mod mw  ; map matrix 1-d selected indexes to an refit row index.
  bt(iw) = ( g.fpol(k(iw))+(g.fpol(k(iw)+1)-g.fpol(k(iw)))* $
             (g.psirz(iw)-(k(iw)*dpsi+g.ssimag))/dpsi ) / g.r(iwr)
endif


;below here is the only part that is dfft from calculate_bfield used in the orb code

btot=(bp^2+bt^2)^.5
ngf=n_elements(g.fpol)
psigf=findgen(ngf)/(ngf-1.)*(g.ssibry-g.ssimag)+g.ssimag
frz=interpol(g.fpol,psigf,g.psirz)
;psig=g.psirz



ucurvr=-1
ucurvt=-1
ucurvz=-1
ugradr=-1
ugradt=-1
ugradz=-1

rarr=bp*0
zarr=rarr
for i=0,mw-1 do begin
rarr(i,*)=g.r(i)
zarr(i,*)=g.z
endfor

if driftterms gt 0 then begin
;calculating R,z derivatives of BR,Bt,Bz

drbr=fltarr(mw,mh)
drbt=drbr
drbz=drbr
dzbr=drbr
dzbt=drbr
dzbz=drbr
dzbtot=drbr
drbtot=drbr

for i=0,mw-1 do begin
dzbr(i,*)=deriv(g.z,br(i,*))
dzbt(i,*)=deriv(g.z,bt(i,*))
dzbz(i,*)=deriv(g.z,bz(i,*))
dzbtot(i,*)=deriv(g.z,btot(i,*))
endfor
for i=0,mh-1 do begin
drbr(*,i)=deriv(g.r,br(*,i))
drbt(*,i)=deriv(g.r,bt(*,i))
drbz(*,i)=deriv(g.r,bz(*,i))
drbtot(*,i)=deriv(g.r,btot(*,i))
endfor


;these form the curva. drift velocity when 
;multiplied by mv||^2/q

ucurvr=( bt*(br*drbz+bz*dzbz)-bz*(br*drbt+bz*dzbt+bt*br/rarr))/btot^4
ucurvt=( bz*(br*drbr+bz*dzbr-bt^2/rarr)-br*(br*drbz+bz*dzbz))/btot^4
ucurvz=( br*(br*drbt+bz*dzbt+bt*br/rarr)-bt*(br*drbr+bz*dzbr-bt^2/rarr))/btot^4

;these form the gradient drift when multiplied by mv_perp^2/(2q)
ugradr=( bt*dzbtot )/btot^3
ugradt=( bz*drbtot - br*dzbtot)/btot^3
ugradz=( -bt*drbtot)/btot^3
endif


outs={bp:bp,br:br,bt:bt,bz:bz,g:g,btot:btot,frz:frz,psirz:g.psirz,$
ucurvr:ucurvr,ucurvt:ucurvt,ucurvz:ucurvz,ugradr:ugradr,$
ugradt:ugradt,ugradz:ugradz,rarr:rarr,zarr:zarr}

return,outs
end
