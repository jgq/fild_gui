pro wall

shot=28061;                  ; Numero de la Descarga
shot=long(shot)          
timer=3200;                  ; Instante de tiempo del equilibrio (en ms)
g = readg(shot,timer)        ; Leer el equilibrio de la descarga shot en el instante timer
pos=[2.18,1.32,0.33]         ; Es el vector que indica la posición [R(m),fi(rad),Z(m)] desde la cual se inicia la simulación del ion
pit=.70                       ; Pitch Angle. Se define como el cociente entre la veolcidad paralela y la velocidad total del ion
energy=90                    ; Energía inicial del Ion (keV).
checkfull=1                  ; Dependiendo del valor que tomemos podríamos tener full orbit (1) o guiding center orbit (0); 
printinfo=1                  ; Dependiendo del valor que tomemos podríamos imprimir la informacion en el terminal (1) o no (0);
reverse_time=0               ; reverse_time=0(simulación normal) y reverse_time=1 (simulación inversa);
;
;A continuación se pone la línea necesaria para hacer la simulación
;
orb_gc_aug,g,[pos[0],pos[1],pos[2]],pit,energy, INPUTSATGC=0, mm=1,checkfull=checkfull,reverse_time=reverse_time,orbinteg=0,plott=1,printinfo=prinnfo,outs=o
xx=o.fout(3,*);               ; Coordenadas R
yy=o.fout(4,*)                ; Coordenadas fi
zz=o.fout(5,*)                ; Coordenadas Z

stop
return
END
