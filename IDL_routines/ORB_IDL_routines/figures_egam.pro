
;;ORBIT CLASSIFICATION
;0=SOME ERROR IN CONTOURING  ;1=PASSING axis encircling, 2 = PASSING NOT-axis encircling
; 3=TRAPPED axis encircling (rare), 4=TRAPPED NOT-axis encircling, 
;5=CLOSED ORBIT ON EFIT GRID BUT HITS WALL, 6 = OPEN ORBIT ON EFIT GRID

restore,'epr_scan_t325_142111.sav'
loadct,5
!p.multi=[0,2,3]
loadct,5
xt='Energy (kev)'
yt='Pitch (V||/V)'
cz=2.5
czs=1.5 ;cz
sz=.25
dfld=.05
xrr=[10,80]
yr=[-1.,1.]
ee=double(1.60E-19)
mi=double(1.67E-27)
mm=2.
z=1.
bfss=10


;**********************************************************************************************************
set_plot,'ps'
device,filename='figure_epr_142111_t325.eps',/color,xsize=21,$
ysize=24,xoffset=.5,yoffset=1$
,font_size=bfss,bits_per_pixel=6,bold=0,scale=1
!x.thick=5
!y.thick=5
tt=4



for i=0,4 do begin
loadct,5
sig=reform(orbcarr(i,*,*))
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

xyouts,40,.75,'R='+stringc(radarr(i)),charsize=czs,color=255,font=0
wl1=where(abs(filddistarr(i,*,*)) le dfld,nwl1)
wn1=where(abs(filddistarr(i,*,*)) gt dfld)
if nwl1 gt 0 then x=array_indices(reform(filddistarr(0,*,*)),wl1)
temp=where(errorarr(i,*,*) gt 0,nce)
if nce gt 0 then ai=array_indices(reform(errorarr(i,*,*)),temp)
;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=ztt,color=113
loadct,5
if nce gt 0 then plots,energyarr(ai(0,*)),pitcharr(ai(1,*)),psym=5,symsize=sz,thick=ztt,color=255
endif
endfor

device,/close_file

set_plot,'x'


;**********************************************************************************************************

;wk=get_kbrd(1)

set_plot,'ps'
device,filename='figure_1r_res_142111_t325.eps',/color,xsize=21,$
ysize=22,xoffset=.5,yoffset=1$
,font_size=bfss,bits_per_pixel=6,bold=0,scale=1
!x.thick=5
!y.thick=5
tt=4


loadct,5

sz=.7

;With resonances overlayed
clabs=reverse(['p=1','p=2','p=3'])

modef=15. ;15.; 21. ;15.0
freqs=reverse(modef/(1.+findgen(3))) ;findgen(10)*15.

pp=2
;omeg=-ntor*abs(tangarr)+(mpol+pp)*abs(pangarr)
;omeg=-ntor*abs(tangarr)+(pp)*abs(pangarr)
;resonance = omega=p*omegabounce
;so need to make countours at modef/p

;ft325=15. ft425=21
i=3
omeg=reform(abs(pangarr(i,*,*))/2./!pi)
sigo=omeg*1.E-3
wlost=where(orbcarr(i,*,*) eq 6)
;sigo(wlost)=-999
plotomeg=1
!p.multi=0 ;[0,1,1]
if plotomeg then begin




wl1=where(abs(filddistarr(i,*,*)) le dfld,nwl1)
wn1=where(abs(filddistarr(i,*,*)) gt dfld)
if nwl1 gt 0 then x=array_indices(reform(filddistarr(0,*,*)),wl1)
temp=where(errorarr(i,*,*) gt 0,nce)
if nce gt 0 then ai=array_indices(reform(errorarr(i,*,*)),temp)

sig=reform(orbcarr(i,*,*))
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

xyouts,40,.995,'R='+stringc(radarr(i)),charsize=czs*1.5,color=0,font=0

;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5
if nce gt 0 then plots,energyarr(ai(0,*)),pitcharr(ai(1,*)),psym=5,symsize=sz,thick=1,color=255

endif

contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=7,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=1,c_chars=czs*1.5,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1

device,/close
set_plot,'x'


eq56=where(orbcarr(i,*,*) eq 5 or orbcarr(i,*,*) eq 6)
xeq56=array_indices(reform(orbcarr(i,*,*)),eq56)

;**********************************************************************************************************


;NOW TRANSP FIGURE
;*****************************

!p.multi=[0,1,2]
bfss=8
set_plot,'ps'
device,filename='figure_transp_res_142111_t325.eps',/color,xsize=21,$
ysize=25,xoffset=.5,yoffset=2$
,font_size=bfss,bits_per_pixel=6,bold=0,scale=1
!x.thick=5
!y.thick=5
tt=4
czsc=2
ymm=[2,2]
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1,ymargin=ymm
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

xyouts,40,.975,'R='+stringc(radarr(i)),charsize=czs,color=255

;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5
endif

contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=7,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1
endif

;a1=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M18/142111M18_fi_1.cdf')

f1=a1.f_d_nbi


enedf=1.e-3*a1.e_d_nbi
pitchdf=a1.a_d_nbi

nf=findloc(enedf,xrr(0))
nenn=findloc(enedf,xrr(1))

temp=min((radarr(i)-a1.r2d/100.)^2.+(z0-a1.z2d/100.)^2,nin2)

loadct,5

sig=(f1(nf:nenn-1,*,nin2))
stemp=.5E7
shade_surf,sig,enedf(nf:nenn-1),pitchdf,ax=90,az=0,shades=bytscl((sig),$
min=min(sig),max=stemp),$
xtitle='Energy (keV)',zstyle=4,ytitle='Pitch',charsize=cz,xstyle=1,$
font=0,xrange=xrr,yrange=yr,ystyle=1,ymargin=ymm
contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=7,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1

loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5


device,/close_file
set_plot,'x'

;**********************************************************************************************************
;Now expected pitch at fild

fildpitcharri=fildpitcharr(i,*,*)
pangarri=pangarr(i,*,*)
patf=fildpitcharri(wl1)
etotf=reform(energyarr(x(0,*)))
omegf=pangarri(wl1)/2./!pi*1.E-3
dfo=.5

!p.multi=0

eperpf=etotf*(1.-patf^2) ;*1000.*ee
bfild=outs.bfild ;T
;rlif_perp=(mm*outsmi)^.5/z/ee/bfild*100.*(eperpf*1.E3*ee*2)^.5
rlif=(mm*mi)^.5/z/ee/bfild*100.*(etotf*1.E3*ee*2)^.5

patfdg=abs(acos(abs(patf))*180./!pi)


patf56=fildpitcharri(eq56)
etotf56=reform(energyarr(xeq56(0,*)))

rlif56=(mm*mi)^.5/z/ee/bfild*100.*(etotf56*1.E3*ee*2)^.5
patfdg56=abs(acos(abs(patf56))*180./!pi)


set_plot,'ps'
device,filename='figure_fild_142111_t325.eps',/color,xsize=20,$
ysize=20,xoffset=.5,yoffset=2$
,font_size=bfss,bits_per_pixel=6,bold=0,scale=1

!x.thick=5
!y.thick=5

;plot,rlif56,patfdg56,psym=4,xtitle='Gyroradius @ FILD (cm)',$
;ytitle='Pitch @ FILD (Deg.)',yrange=[min(patfdg),max(patfdg)],charsize=cz,font=0,thick=tt
;loadct,4
;oplot,rlif,patfdg,psym=4,thick=tt,color=113
plot,rlif,patfdg,psym=4,xtitle='Gyroradius @ FILD (cm)',$
ytitle='Pitch @ FILD (Deg.)',yrange=[min(patfdg),max(patfdg)],charsize=cz,font=0,thick=tt
loadct,4


loadct,5
coli=[180,100,45]
for j=1,3 do begin
inc=where(abs(omegf-modef/j) lt dfo,ncc)  ;finds points w/ dfo of reson
if ncc gt 0 then begin
plots,rlif(inc),patfdg(inc),psym=4,color=coli(j-1),thick=tt
endif
endfor



device,/close_file
set_plot,'x'








;**********************************************************************************************************
;This figure has 3 panels with loss regions etc. and pitch/e at fild
set_plot,'ps'
device,filename='figure_epr_pefild_142111_t325.eps',/color,xsize=21,$
ysize=24,xoffset=.5,yoffset=1$
,font_size=bfss,bits_per_pixel=6,bold=0,scale=1
!x.thick=5
!y.thick=5
tt=4


!p.multi=[0,2,3]
for i=1,3 do begin
loadct,5
sig=reform(orbcarr(i,*,*))
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

xyouts,40,.975,'R='+stringc(radarr(i)),charsize=czs*1.2,color=0,font=0
wl1=where(abs(filddistarr(i,*,*)) le dfld,nwl1)
wn1=where(abs(filddistarr(i,*,*)) gt dfld)
if nwl1 gt 0 then x=array_indices(reform(filddistarr(0,*,*)),wl1)
temp=where(errorarr(i,*,*) gt 0,nce)
if nce gt 0 then ai=array_indices(reform(errorarr(i,*,*)),temp)
;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=ztt,color=113
loadct,5
;if nce gt 0 then plots,energyarr(ai(0,*)),pitcharr(ai(1,*)),psym=5,symsize=sz,thick=ztt,color=255
endif

omeg=reform(abs(pangarr(i,*,*))/2./!pi)
sigo=omeg*1.E-3
contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=7,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1


fildpitcharri=fildpitcharr(i,*,*)
pangarri=pangarr(i,*,*)
patf=fildpitcharri(wl1)
etotf=reform(energyarr(x(0,*)))
omegf=pangarri(wl1)/2./!pi*1.E-3
dfo=.5

eperpf=etotf*(1.-patf^2) ;*1000.*ee
bfild=outs.bfild ;T
;rlif_perp=(mm*outsmi)^.5/z/ee/bfild*100.*(eperpf*1.E3*ee*2)^.5
rlif=(mm*mi)^.5/z/ee/bfild*100.*(etotf*1.E3*ee*2)^.5

patfdg=abs(acos(abs(patf))*180./!pi)

modef=15.; 21. ;15.0
freqs=reverse(modef/(1.+findgen(3))) ;findgen(10)*15.

plot,rlif,patfdg,psym=4,xtitle='Gyroradius @ FILD (cm)',$
ytitle='Pitch @ FILD (Deg.)',yrange=[min(patfdg),max(patfdg)],charsize=cz*1.25,font=0,thick=tt
loadct,4

loadct,5
coli=[180,100,45]
for j=1,3 do begin
inc=where(abs(omegf-modef/j) lt dfo,ncc)  ;finds points w/ dfo of reson
if ncc gt 0 then begin
plots,rlif(inc),patfdg(inc),psym=4,color=coli(j-1),thick=tt
endif
endfor


endfor

device,/close_file

set_plot,'x'


;**********************************************************************************************************








;NOW 2nd TRANSP FIGURE  with two times
;*****************************

!p.multi=[0,1,2]
bfss=8
modei=2
set_plot,'ps'
device,filename='figure_transp_142111_t325_fineres.eps',/color,xsize=20,$
ysize=25,xoffset=.5,yoffset=2$
,font_size=bfss,bits_per_pixel=6,bold=0,scale=1
!x.thick=5
!y.thick=5
tt=4
czsc=2
ymm=[2,2]
wl1=where(abs(filddistarr(modei,*,*)) le dfld,nwl1)
wn1=where(abs(filddistarr(modei,*,*)) gt dfld)
if nwl1 gt 0 then x=array_indices(reform(filddistarr(0,*,*)),wl1)
sig=reform(orbcarr(modei,*,*))
;shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
;xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1,ymargin=ymm
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

;xyouts,40,.975,'R='+stringc(radarr(i)),charsize=czs,color=255

;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
;if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5
endif

;contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=7,c_colors=255,$
;c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1


;a1=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M18/142111M18_fi_1.cdf')
;OUTTIM= 0.316500 , 0.321500 , 0.326500 , 0.331500 , 0.336500 , 0.341500 , 0.346
;500 , 0.356500 , 0.366500
;a11=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M21/142111M21_fi_3.cdf')
a2=transp_cdf_restore('/u/vanzee/TRANSP/OUTPUTFILES/142111M21/142111M21_fi_6.cdf')

f1=a11.f_d_nbi


enedf=1.e-3*a11.e_d_nbi
pitchdf=a11.a_d_nbi

nf=findloc(enedf,xrr(0))
nenn=findloc(enedf,xrr(1))

temp=min((radarr(modei)-a11.r2d/100.)^2.+(z0-a11.z2d/100.)^2,nin2)

loadct,5

sig=(f1(nf:nenn-1,*,nin2))
stemp=1E7
shade_surf,sig,enedf(nf:nenn-1),pitchdf,ax=90,az=0,shades=bytscl((sig),$
min=min(sig),max=stemp),$
xtitle='Energy (keV)',zstyle=4,ytitle='Pitch',charsize=cz,xstyle=1,$
font=0,xrange=xrr,yrange=yr,ystyle=1,ymargin=ymm
contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=7,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1

loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5


f1=a2.f_d_nbi

nf=findloc(enedf,xrr(0))
nenn=findloc(enedf,xrr(1))

loadct,5

sig=(f1(nf:nenn-1,*,nin2))

shade_surf,sig,enedf(nf:nenn-1),pitchdf,ax=90,az=0,shades=bytscl((sig),$
min=min(sig),max=stemp),$
xtitle='Energy (keV)',zstyle=4,ytitle='Pitch',charsize=cz,xstyle=1,$
font=0,xrange=xrr,yrange=yr,ystyle=1,ymargin=ymm
contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=7,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1

loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=2,color=113
loadct,5





device,/close_file
set_plot,'x'

;**********************************************************************************************************








;**********************************************************************************************************

;LATER TIME FIGURE

restore,'epr_scan_t425_142111.sav'
loadct,5
!p.multi=[0,2,3]
loadct,5
xt='Energy (kev)'
yt='Pitch (V||/V)'
cz=2.5
czs=1.5 ;cz
sz=.25
dfld=.05
xrr=[10,80]
yr=[-1.,1.]
ee=double(1.60E-19)
mi=double(1.67E-27)
mm=2.
z=1.
bfss=10


;**********************************************************************************************************

set_plot,'ps'
device,filename='figure_epr_142111_t425.eps',/color,xsize=21,$
ysize=24,xoffset=.5,yoffset=1$
,font_size=bfss,bits_per_pixel=6,bold=0,scale=1
!x.thick=5
!y.thick=5
tt=4


restore,'epr_scan_t425_142111.sav'

for i=0,4 do begin
loadct,5
sig=reform(orbcarr(i,*,*))
shade_surf,sig,energyarr,pitcharr,ax=90,az=0,shades=bytscl(sig,min=0,max=6),$
xtitle=xt,ytitle=yt,charsize=cz,font=0,zstyle=4,xrange=xrr,xstyle=1,yrange=yr,ystyle=1
;contour,sig,energyarr,pitcharr,levels=[0,1,2],/overplot,c_thick=ztt,c_colors=100,$
;c_labels=[1,1,1]

xyouts,40,.75,'R='+stringc(radarr(i)),charsize=czs,color=255,font=0
wl1=where(abs(filddistarr(i,*,*)) le dfld,nwl1)
wn1=where(abs(filddistarr(i,*,*)) gt dfld)
if nwl1 gt 0 then x=array_indices(reform(filddistarr(0,*,*)),wl1)
temp=where(errorarr(i,*,*) gt 0,nce)
if nce gt 0 then ai=array_indices(reform(errorarr(i,*,*)),temp)
;plots,energyarr(badi(0,*)),pitcharr(badi(1,*)),psym=4
if wl1(0) gt -1 then begin
loadct,4
if nwl1 gt 0 then oplot,energyarr(x(0,*)),pitcharr(x(1,*)),psym=5,symsize=sz,thick=ztt,color=113
loadct,5
if nce gt 0 then plots,energyarr(ai(0,*)),pitcharr(ai(1,*)),psym=5,symsize=sz,thick=ztt,color=255
endif
modef=21.; 21. ;15.0
freqs=reverse(modef/(1.+findgen(3))) ;findgen(10)*15.

omeg=reform(abs(pangarr(i,*,*))/2./!pi)
sigo=omeg*1.E-3
contour,sigo,energyarr,pitcharr,levels=freqs,/overplot,c_thick=7,c_colors=255,$
c_labels=freqs*0+1,c_ann=clabs,closed=0,c_chars=czsc,c_charthick=2,xrange=xrr,xstyle=1,font=0,thick=2,yrange=yr,ystyle=1




endfor

device,/close_file

set_plot,'x'









end




