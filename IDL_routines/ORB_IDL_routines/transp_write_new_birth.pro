outfil='142111M30_birth_4_nogt_rho_1.out'
outfilnw='142111M30_birth_4_nowall.out'
restore,'birth_info_t525_142111M30.sav'
openw,1,outfil

newi=where(maxrho lt 1.0)
newn=n_elements(newi)

printf,1,'N= ',long(newn)
printf,1,' R(cm)         Z(cm)         vpll/v        E(eV)         zeta(deg)'
printf,1,' <start-of-data>'
 
for i=0L,newn-1 do begin
;for i=0,5 do begin
printf,1,(pts(*,newi(i))),format='(5(1x, E13.6))'

endfor

close,1

openw,1,outfilnw

newiw=where(orbcarr lt 5)
newnw=n_elements(newiw)

printf,1,'N= ',long(newnw)
printf,1,' R(cm)         Z(cm)         vpll/v        E(eV)         zeta(deg)'
printf,1,' <start-of-data>'
 
for i=0L,newnw-1 do begin

printf,1,(pts(*,newiw(i))),format='(5(1x, E13.6))'

endfor

close,1





end
