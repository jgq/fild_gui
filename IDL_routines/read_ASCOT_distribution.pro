pro read_ASCOT,filename,output,PITCH_SIGN=PITCH_SIGN

;=====================================================================================================
;                   J. Galdon               23/12/2015
;=====================================================================================================
; Routine to read ASCOT input. It reads ASCOT filename given as 9 columns R,Phi,Z,Energy,Pitch,A,Z,Weight,Time
; R,Z in meters
; Phi in deg
; Pitch in v||/v
; E in eV
; A in units of proton mass
; WEIGHT in Number of Ions per MC marker
;=====================================================================================================

filename=filename

ON_IOERROR,err

openr,lun,filename,/get_lun

R=double(0)
Z=double(0)
Phi=double(0)
Energy=double(0)
Pitch=double(0)
A=double(0)
Q=double(0)
WEIGHT=double(0)
TIME=double(0)

WHILE NOT EOF(lun) DO BEGIN
	readf,lun,Ro,Phio,Zo,Energyo,Pitcho,ao,qo,weighto,timeo             ;format='(2x,F10.6,5x,F9.6,5x,F9.6,5x,F9.6,4x,F9.6,4x,F9.6,4x,F9.6)'

	R=[R,Ro]
	Z=[Z,Zo]
	Phi=[Phi,Phio]
	Energy=[Energy,Energyo]
	Pitch=[Pitch,Pitcho]
	A=[A,ao]
	Q=[Q,qo]
	Weight=[Weight,weighto]
	time=[time,timeo]


ENDWHILE

close,lun
free_lun,lun

ON_IOERROR,NULL

R=R(1:*)		;/100.
Z=Z(1:*)		;/100.
Phi=Phi(1:*)
Energy=Energy(1:*)
Pitch=Pitch(1:*)
A=A(1:*)
Q=Q(1:*)
Weight=Weight(1:*)
Time=Time(1:*)



; Pitch is given in v||/v. Transform to deg

raw_pitch=Pitch

IF keyword_set(pitch_sign) THEN BEGIN

	Pitch*=-1.		; In ASCOT pitch<0 is cogoing ions with Ip, counter with Bt

ENDIF ELSE BEGIN

	Pitch*=1.

ENDELSE

Pitch=acos(pitch)*180./!pi

FOR kk=0,n_elements(pitch)-1 DO BEGIN

	IF pitch(kk) GT 90 THEN pitch(kk)=90-pitch(kk)

ENDFOR

print,'Total number of markers: ',n_elements(Weight)
print,'Total rate of ions lost (ions/s): ',total(weight)
print,'Total power loss (W): ',total(weight*energy)*1.60218e-19
print,'Total heat load (W/m2) assuming FILD area of 0.0042 m2: ',total(weight*energy)*1.60218e-19/0.0042

output=create_struct('R',R,'Z',Z,'Phi',Phi,'Energy',Energy,'Pitch',Pitch,'raw_pitch',raw_pitch,'A',A,'Q',Q,'Weight',weight,'Time',time)

return

err: BEGIN

	print,'Error reading ASCOT filename: '+filename
	print,!ERR_STRING
	output='ERROR'

	return

END


end
