function FILD_get_PMT_signals,shot,FILD_ID=FILD_ID


;=====================================================================================================
;                   J. Galdon               17/06/2021
;=====================================================================================================
;
; Routine to read FILD PMT signals
;
;=====================================================================================================

CASE FILD_ID OF

    1: BEGIN

        diagname='FHC'
        signalname='FILD3_'
        nchannels=20

    END

    2: BEGIN

        diagname='FHA'
        signalname='FIPM_'
        nchannels=20

    END


    4: BEGIN

        diagname='FHD'
        signalname='Chan-'
        nchannels=32

    END


    5: BEGIN

        diagname='FHE'
        signalname='Chan-'
        nchannels=64

    END

    ELSE: BEGIN

        print,'Define FILD_ID: 1,2,4 or 5'
        return,-1

    END

ENDCASE

; Define signal names

signals=strarr(nchannels)

FOR kk=1,nchannels DO BEGIN

    pmtnum=string(kk,format='(I2)')
    pmtnum=strcompress(pmtnum,/remove_all)

    num0='00'
    l=strlen(pmtnum)
    strput,num0,pmtnum,2-l

    signals(kk-1)=signalname+num0

END

; Read_signals

cc=0

FOR kk=0,nchannels-1 DO BEGIN

    read_signal,ier,shot,diagname,signals(kk),tt,dd,phys,edition=0

    IF ier NE 0 THEN GOTO,skip

    IF cc EQ 0 THEN BEGIN

        time=tt
        fild=dblarr(nchannels,n_elements(time))

        cc+=1

    ENDIF

    fild(kk,*)=dd

    skip:

ENDFOR

IF cc EQ 0 THEN BEGIN

    print,'No signal could be retrieved. Returning ...'
    return,-1

ENDIF

output={shot:shot,fild_id:fild_id,diagname:diagname,signals:signals,nchannels:nchannels,$
    time:time,fild:fild}

return,output
end
