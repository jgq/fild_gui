PRO fild_gui_EVENT, event

;*** Determine which button sent the event:***
WIDGET_CONTROL,event.top,GET_UVALUE=uvalue


CASE event.id OF

	UVALUE.EXIT_BUTTON: BEGIN

		widget_control,event.top,/destroy

	END

;***************************************************************************************************************************************************

	UVALUE.OPEN_MOVIE_BUTTON: BEGIN

		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON SETTINGS,settings
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase


		thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

		CASE thistab of

			 0: BEGIN


				ccd_ccd_file_path = DIALOG_PICKFILE(/READ,PATH=settings.fild_data_path,/DIRECTORY)

				images=file_search(strtrim(ccd_ccd_file_path,1)+'*.png')

				IF n_elements(images) EQ 1 THEN BEGIN

					print,'No PNG files found in this directory: ',ccd_ccd_file_path
					return

				ENDIF

				dumm=read_png(images(0))
				ss=size(dumm,/dimensions)

				ccd_frames=fltarr(ss(0),ss(1),n_elements(images))

				print,'Loading frames from PNG files in: ',ccd_ccd_file_path
				widget_control,/HOURGLASS


				FOR kk=0,n_elements(images)-1 DO BEGIN

					ccd_frames(*,*,kk)=read_png(images(kk))

				ENDFOR


				txt_filename=file_search(strtrim(ccd_ccd_file_path,1)+'*.txt')
				txt_data=FILD_read_PCO_info(txt_filename)
				IF n_tags(txt_data) LT 2 THEN return
				ccd_ccd_txt_frames=txt_data.frame
				ccd_ccd_txt_times=txt_data.exp_start
				ccd_ccd_txt_times=abs(ccd_ccd_txt_times(1)-ccd_ccd_txt_times(0))*ccd_ccd_txt_frames

				ccd_times=ccd_ccd_txt_times

				ccd_ccd_frame_num=ccd_ccd_txt_frames(0)

				widget_control,uvalue.ccd_frame_slider,SENSITIVE=1,set_value=ccd_ccd_frame_num,set_slider_min=ccd_ccd_txt_frames(0),$
					set_slider_max=ccd_ccd_txt_frames(n_elements(ccd_ccd_txt_frames)-1)

				widget_control,uvalue.af_frame_slider,SENSITIVE=1,set_value=ccd_ccd_frame_num,set_slider_min=ccd_ccd_txt_frames(0),$
					set_slider_max=ccd_ccd_txt_frames(n_elements(ccd_ccd_txt_frames)-1)

				widget_control,uvalue.ccd_saturation_slider,SENSITIVE=1,set_value=256,$
				set_slider_min=0,set_slider_max=256

				widget_control,uvalue.ccd_frame_draw,get_value=draw_id
				wset,draw_id

				ccd_current_image=ccd_frames(*,*,ccd_ccd_frame_num-1)
				tvscl,congrid(ccd_current_image,ccd_draw_size_x,ccd_draw_size_y)

				widget_control,uvalue.ccd_frame_info_frame,set_value='Frame='+str(ccd_ccd_frame_num)
				widget_control,uvalue.ccd_frame_info_time,set_value='Time='+$
						str(ccd_ccd_txt_times(ccd_ccd_frame_num))+' s'

				settings.ccd_data_loaded=1


			END

			1: BEGIN

				widget_control,/HOURGLASS

				widget_control,uvalue.checkbox_settings,get_value=values
				pixel_depth_phantom=values(4)


				readpulse,pulse=settings.shot,time=phantom_initial_time,$
						dtime=abs(phantom_initial_time-phantom_final_time),$
						frames=frames,cintime=cintime,PIXEL_DEPTH_8=pixel_depth_phantom,error


				IF error NE 0 THEN BEGIN

					print,'************ WARNING !!!! ****************'
					print,'When trying to read the .cin file, sometimes you will get an error like -->'
					print,'--> conflicting data structures: timebase, cinheaderinfo'
					print,' At the moment the only solution I find is to restart the whole idl session'
					print,'******************************************'

					; This errors appears because the .cin information is stored under a structure.
					; One of the variables of this structure is timebase. If you then try to open
					; another video which happened to had different frame_rate, the size of the timebase array
					; will be different and therefore you will get the error message over the structure

					return

				ENDIF

				phantom_frame_num=0
				phantom_frames=frames
				phantom_times=cintime
				phantom_current_frame=phantom_frames(*,*,phantom_frame_num)

				s=size(phantom_frames,/dimensions)

				help,phantom_frames

				widget_control,uvalue.phantom_slider,SENSITIVE=1,set_value=phantom_frame_num,set_slider_min=0,set_slider_max=s(2)-1
				widget_control,uvalue.af_frame_slider,SENSITIVE=1,set_value=phantom_frame_num+1,set_slider_min=0,set_slider_max=s(2)-1

				widget_control,uvalue.phantom_draw,get_value=draw_id,draw_xsize=s(0),draw_ysize=s(1)
				wset,draw_id

				tvscl,phantom_current_frame

				widget_control,uvalue.phantom_info_frame,set_value='Frame='+str(phantom_frame_num)
				widget_control,uvalue.phantom_info_time,set_value='Time='+$
						str(phantom_times(phantom_frame_num))+' s



				settings.phantom_data_loaded=1
			END

			ELSE: return

		ENDCASE

	END


;***************************************************************************************************************************************************

	UVALUE.CCD_SATURATION_SLIDER: BEGIN

	COMMON SETTINGS,settings
	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times

	widget_control,uvalue.ccd_saturation_slider,get_value=sat_level

	widget_control,uvalue.ccd_frame_draw,get_value=draw_id
	wset,draw_id

	ccd_current_image=ccd_frames(*,*,ccd_ccd_frame_num)

	ccd_current_image<=max(ccd_current_image)*sat_level/256

	IF settings.filter GT 0 THEN BEGIN
		tvscl,congrid(median(ccd_current_image,settings.filter),ccd_draw_size_x,ccd_draw_size_y)
	ENDIF ELSE BEGIN
		tvscl,congrid(ccd_current_image,ccd_draw_size_x,ccd_draw_size_y)
	ENDELSE

	END


;***************************************************************************************************************************************************

	UVALUE.OPEN_FRAME_BUTTON:BEGIN

		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times

		thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

		IF thistab EQ 0 THEN BEGIN

			movie_file = DIALOG_PICKFILE(/READ,PATH=settings.fild_data_path)


			widget_control,uvalue.ccd_frame_draw,get_value=draw_id
			wset,draw_id
			ccd_current_image=read_png(movie_file)

			dumm=ccd_current_image
			ss=size(dumm,/dimensions)

			IF n_elements(ss) EQ 3 THEN BEGIN
				dumm=total(dumm,1)
				print,'Original image has 3 color channels'
			ENDIF

			ss=size(dumm,/dimensions)

			ccd_current_image=dumm

			IF ss(0) NE ccd_draw_size_x OR ss(1) NE ccd_draw_size_y THEN BEGIN

				ccd_current_image=congrid(ccd_current_image,ccd_draw_size_x,ccd_draw_size_y)
				print,'Resizing original frame to fit it in window'

			ENDIF


			tvscl,ccd_current_image

			widget_control,uvalue.ccd_frame_slider,SENSITIVE=0

		ENDIF

		IF thistab EQ 1 THEN BEGIN

			COMMON SETTINGS,settings
			COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

			phantom_frame_loc = DIALOG_PICKFILE(/READ,PATH='~/')
			phantom_current_frame=read_image(phantom_frame_loc)

			s=size(phantom_current_frame,/dimensions)

			dumm=phantom_current_frame


			IF n_elements(s) EQ 3 THEN BEGIN
				dumm=total(dumm,1)
				print,'Original image has 3 color channels'
			ENDIF

			s=size(dumm,/dimensions)

			phantom_current_frame=dumm

			widget_control,uvalue.phantom_draw,get_value=draw_id,draw_xsize=s(0),draw_ysize=s(1)
			wset,draw_id

			tvscl,phantom_current_frame

		ENDIF

	END

;***************************************************************************************************************************************************

	UVALUE.CREATE_VIDEO_CCD_BUTTON: BEGIN

	COMMON SETTINGS,settings
	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
	COMMON GRID_PARAMS,grid_params
	COMMON FILD_GRID_INFO,fild_grid_info

	xshift=grid_params.xshift_pix
	yshift=grid_params.yshift_pix
	xscale=grid_params.xscale_pix
	yscale=grid_params.yscale_pix
	alfa=grid_params.deg_pix

	widget_control,/HOURGLASS

	FILD_video_ccd,FILE_PATH=ccd_ccd_file_path,PULSE=settings.SHOT,TIME=ccd_ccd_txt_times,$
		FRAMES=ccd_ccd_txt_frames,RES_PATH=settings.results_path,$
		xshift,yshift,xscale,yscale,alfa,settings.grid_image,STRIKE_MAP=fild_grid_info.strike_map_filename,$
		OVERLAY_GRID=settings.overlay_grid


	END

;***************************************************************************************************************************************************

	UVALUE.CREATE_VIDEO_PHANTOM_BUTTON: BEGIN

	COMMON SETTINGS,settings
	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
	COMMON GRID_PARAMS,grid_params

	xshift=grid_params.xshift_im
	yshift=grid_params.yshift_im
	xscale=grid_params.xscale_im
	yscale=grid_params.yscale_im
	alfa=grid_params.deg_im

	widget_control,/HOURGLASS

	FILD_video_phantom,PULSE=settings.SHOT,TIME=phantom_times,DATA=phantom_frames,RES_PATH=settings.results_path,$
	xshift,yshift,xscale,yscale,alfa,settings.grid_image


	END

;***************************************************************************************************************************************************

	UVALUE.SELECT_GRID_IMAGE: BEGIN

	COMMON SETTINGS,settings

	grid_image= DIALOG_PICKFILE(/READ, FILTER = '*.bmp',PATH=settings.GUI_routines_path)
	settings.grid_image=grid_image

	END

;***************************************************************************************************************************************************

	UVALUE.SELECT_SCINTILLATOR_PLATE: BEGIN

	COMMON SETTINGS,settings

	scint_plate= DIALOG_PICKFILE(/READ, FILTER = '*.pl',PATH=settings.GUI_routines_path)
	settings.default_scintillator_plate=scint_plate

	END

;***************************************************************************************************************************************************

	UVALUE.SAVE_IMAGE_BUTTON: BEGIN

		thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

		CASE thistab of
			0: BEGIN
				widget_control,uvalue.ccd_frame_draw,get_value=draw_id
				wset,draw_id
				save_image=tvrd(/true)
				save=dialog_write_image(save_image,FILENAME='ccd_frame.png',TYPE='.PNG')

				widget_control,uvalue.ccd_remap_draw,get_value=draw_id
				wset,draw_id
				save_image=tvrd(/true)
				save=dialog_write_image(save_image,FILENAME='ccd_remap.png',TYPE='.PNG')

			END

			1: BEGIN
				widget_control,uvalue.phantom_draw,get_value=draw_id
				wset,draw_id
				save_image=tvrd(/true)
				save=dialog_write_image(save_image,FILENAME='phantom_frame.png',TYPE='.PNG')

				widget_control,uvalue.phantom_remap_draw,get_value=draw_id
				wset,draw_id
				save_image=tvrd(/true)
				save=dialog_write_image(save_image,FILENAME='phantom_remap.png',TYPE='.PNG')


			END

			3: BEGIN
				widget_control,uvalue.fildsim_output_draw,get_value=draw_id
				wset,draw_id
				save_image=tvrd(/true)
				save=dialog_write_image(save_image,FILENAME='test_synthetic_output.png',TYPE='.PNG')

				widget_control,uvalue.fildsim_input_draw,get_value=draw_id
				wset,draw_id
				save_image=tvrd(/true)
				save=dialog_write_image(save_image,FILENAME='Test_synthetic_input.png',TYPE='.PNG')


			END

			4: BEGIN
				widget_control,uvalue.sm_draw_resolutions,get_value=draw_id
				wset,draw_id
				save_image=tvrd(/true)
				save=dialog_write_image(save_image,FILENAME='Resolutions.png',TYPE='.PNG')


				widget_control,uvalue.sm_draw_strike_map,get_value=draw_id
				wset,draw_id

				save_image=tvrd(/true)
				save=dialog_write_image(save_image,FILENAME='Strike_map.png',TYPE='.PNG')


			END


			ELSE: BEGIN
				print,'Returning...'
				RETURN
			END


		ENDCASE




	END

;***************************************************************************************************************************************************

	UVALUE.SAVE_FILDSIM_OUTPUT_BUTTON: BEGIN

		COMMON FILDSIM_results,fildsim_gyr,fildsim_pitch,fildsim_output_dist,fildsim_input_dist
		COMMON SETTINGS,settings
		COMMON FILDSIM_settings,fildsim_settings

		print,'Saving variables'

		fn=strtrim(settings.results_path,1)+strtrim(systime(/julian),1)+'_test_fildsim_save.sav'

		strike_map_filename=fildsim_settings.strike_map_filename
		strike_points_filename=fildsim_settings.strike_points_filename

		save,fildsim_gyr,fildsim_pitch,fildsim_output_dist,fildsim_input_dist,$
			strike_map_filename,strike_points_filename,filename=fn

		print,'variables saved as '+strtrim(fn,1)
	END

;***************************************************************************************************************************************************

	UVALUE.CCD_FRAME_SLIDER: BEGIN

		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON SETTINGS,settings

		widget_control,uvalue.ccd_frame_slider,get_value=slider_num

		widget_control,uvalue.ccd_saturation_slider,get_value=sat_level


		ccd_ccd_frame_num=slider_num-1
		ccd_current_image=ccd_frames(*,*,slider_num-1)

		ccd_current_image<=max(ccd_current_image)*sat_level/256.

		widget_control,uvalue.ccd_frame_draw,get_value=draw_id
		wset,draw_id

		IF settings.filter GT 0 THEN BEGIN
			tvscl,congrid(median(ccd_current_image,settings.filter),ccd_draw_size_x,ccd_draw_size_y)
		ENDIF ELSE BEGIN
			tvscl,congrid(ccd_current_image,ccd_draw_size_x,ccd_draw_size_y)
		ENDELSE

		widget_control,uvalue.ccd_frame_info_frame,set_value='Frame='+str(ccd_ccd_txt_frames(ccd_ccd_frame_num))
				widget_control,uvalue.ccd_frame_info_time,set_value='Time='+$
						str(ccd_ccd_txt_times(ccd_ccd_frame_num))+' s'

		IF settings.overlay_grid EQ 1 THEN BEGIN

			widget_control,uvalue.plot_grid_button,send_event={ID:uvalue.plot_grid_button,TOP:uvalue.ccd_frame_base,HANDLER:uvalue.ccd_frame_slider}

		ENDIF

		IF settings.profiles EQ 1 THEN BEGIN

			widget_control,uvalue.ccd_gyroradius_profile_plot_button,$
					send_event={ID:uvalue.ccd_gyroradius_profile_plot_button,TOP:uvalue.ccd_gyroradius_profile_base,HANDLER:uvalue.ccd_frame_slider}

			widget_control,uvalue.ccd_pitch_profile_plot_button,$
					send_event={ID:uvalue.ccd_pitch_profile_plot_button,TOP:uvalue.ccd_pitch_profile_base,HANDLER:uvalue.ccd_frame_slider}

		ENDIF
	END

;***************************************************************************************************************************************************


	UVALUE.PHANTOM_SLIDER: BEGIN

		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
		COMMON SETTINGS,settings

		widget_control,uvalue.phantom_slider,get_value=slider_num


		phantom_frame_num=slider_num
		phantom_current_frame=phantom_frames(*,*,slider_num)


		widget_control,uvalue.phantom_draw,get_value=draw_id
		wset,draw_id

		IF settings.filter GT 0 THEN BEGIN
			tvscl,median(phantom_current_frame,settings.filter)
		ENDIF ELSE BEGIN
			tvscl,phantom_current_frame
		ENDELSE

		widget_control,uvalue.phantom_info_frame,set_value='Frame='+str(phantom_frame_num)
				widget_control,uvalue.phantom_info_time,set_value='Time='+$
						str(phantom_times(phantom_frame_num))+' s

		IF settings.overlay_grid EQ 1 THEN BEGIN

			widget_control,uvalue.phantom_plot_grid_button,send_event={ID:uvalue.phantom_plot_grid_button,TOP:uvalue.phantom_base,HANDLER:uvalue.phantom_slider}


		ENDIF



		;IF settings.profiles EQ 1 THEN BEGIN

		;	widget_control,uvalue.phantom_gyroradius_profile_plot_button,$
		;			send_event={ID:uvalue.phantom_gyroradius_profile_plot_button,TOP:uvalue.phantom_gyroradius_profile_base,HANDLER:uvalue.phantom_slider}

		;	widget_control,uvalue.phantom_pitch_profile_plot_button,$
		;			send_event={ID:uvalue.phantom_pitch_profile_plot_button,TOP:uvalue.phantom_pitch_profile_base,HANDLER:uvalue.phantom_slider}

		;ENDIF

	END

;***************************************************************************************************************************************************


	UVALUE.CHECKBOX_SETTINGS_2: BEGIN

		COMMON SETTINGS,settings
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.checkbox_settings_2,get_value=value

		IF value EQ 0 THEN BEGIN

			settings.cal_type=0
			widget_control,uvalue.xscale_field,set_value=grid_params.xscale_im
			widget_control,uvalue.yscale_field,set_value=grid_params.yscale_im
			widget_control,uvalue.xshift_field,set_value=grid_params.xshift_im
			widget_control,uvalue.yshift_field,set_value=grid_params.yshift_im
			widget_control,uvalue.deg_field,set_value=grid_params.deg_im

		ENDIF ELSE BEGIN

			settings.cal_type=1
			widget_control,uvalue.xscale_field,set_value=grid_params.xscale_pix
			widget_control,uvalue.yscale_field,set_value=grid_params.yscale_pix
			widget_control,uvalue.xshift_field,set_value=grid_params.xshift_pix
			widget_control,uvalue.yshift_field,set_value=grid_params.yshift_pix
			widget_control,uvalue.deg_field,set_value=grid_params.deg_pix

		ENDELSE

		IF settings.overlay_grid EQ 1 THEN BEGIN

			widget_control,uvalue.plot_grid_button,send_event={ID:uvalue.plot_grid_button,TOP:uvalue.ccd_frame_base,HANDLER:uvalue.ccd_frame_slider}

		ENDIF

	END

;***************************************************************************************************************************************************


	UVALUE.SAVE_CALIBRATION_PARAMETERS: BEGIN

		COMMON SETTINGS,settings
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.xscale_field,get_value=xscale
		widget_control,uvalue.yscale_field,get_value=yscale
		widget_control,uvalue.xshift_field,get_value=xshift
		widget_control,uvalue.yshift_field,get_value=yshift
		widget_control,uvalue.deg_field,get_value=deg

		IF settings.cal_type EQ 0 THEN BEGIN

			grid_params.xscale_im=xscale
			grid_params.yscale_im=yscale
			grid_params.xshift_im=xshift
			grid_params.yshift_im=yshift
			grid_params.deg_im=deg

		ENDIF ELSE BEGIN

			grid_params.xscale_pix=xscale
			grid_params.yscale_pix=yscale
			grid_params.xshift_pix=xshift
			grid_params.yshift_pix=yshift
			grid_params.deg_pix=deg

		ENDELSE

	END

;***************************************************************************************************************************************************


	UVALUE.GET_CALIBRATION_BUTTON:BEGIN

		COMMON GRID_PARAMS,grid_params
		COMMON SETTINGS,settings

		thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

		data=FILD_get_calibration(settings.shot,CAMERA=thistab,CALIBRATION=settings.cal_type,$
			FILD_ID=settings.fild,CALIB_FILE=settings.calibration_file)

		IF n_tags(DATA) LT 2 THEN return

		IF settings.cal_type EQ 0 THEN BEGIN

			grid_params.xscale_im=data.xscale
			grid_params.yscale_im=data.yscale
			grid_params.xshift_im=data.xshift
			grid_params.yshift_im=data.yshift
			grid_params.deg_im=data.deg

			widget_control,uvalue.xscale_field,set_value=grid_params.xscale_im
			widget_control,uvalue.yscale_field,set_value=grid_params.yscale_im
			widget_control,uvalue.xshift_field,set_value=grid_params.xshift_im
			widget_control,uvalue.yshift_field,set_value=grid_params.yshift_im
			widget_control,uvalue.deg_field,set_value=grid_params.deg_im

		ENDIF ELSE BEGIN

			grid_params.xscale_pix=data.xscale
			grid_params.yscale_pix=data.yscale
			grid_params.xshift_pix=data.xshift
			grid_params.yshift_pix=data.yshift
			grid_params.deg_pix=data.deg

			widget_control,uvalue.xscale_field,set_value=grid_params.xscale_pix
			widget_control,uvalue.yscale_field,set_value=grid_params.yscale_pix
			widget_control,uvalue.xshift_field,set_value=grid_params.xshift_pix
			widget_control,uvalue.yshift_field,set_value=grid_params.yshift_pix
			widget_control,uvalue.deg_field,set_value=grid_params.deg_pix

		ENDELSE


	END


;***************************************************************************************************************************************************


	UVALUE.PLOT_GRID_BUTTON: BEGIN

		COMMON SETTINGS,settings
		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.xscale_field,get_value=xscale
		widget_control,uvalue.yscale_field,get_value=yscale
		widget_control,uvalue.xshift_field,get_value=xshift
		widget_control,uvalue.yshift_field,get_value=yshift
		widget_control,uvalue.deg_field,get_value=deg

		IF settings.cal_type EQ 0 THEN BEGIN

			widget_control,uvalue.ccd_frame_draw,get_value=draw_id
			wset,draw_id

			FILD_overlay_image,ccd_current_image,image_grid,xscale,yscale,xshift,yshift,deg,settings.grid_image,filter=settings.filter

			TVSCL,congrid(image_grid,ccd_draw_size_x,ccd_draw_size_y)


		ENDIF ELSE BEGIN

			widget_control,uvalue.ccd_frame_draw,get_value=draw_id
			wset,draw_id

			FILD_overlay_pix,ccd_current_image,output_image,xscale,yscale,xshift,yshift,deg,$
				filter=settings.filter,filename=fild_grid_info.strike_map_filename,$
				scintillator_plate=settings.default_scintillator_plate

			TVSCL,congrid(output_image,ccd_draw_size_x,ccd_draw_size_y)

		ENDELSE


	END


;**************************************************************************************************************************************************

	UVALUE.PHANTOM_PLOT_GRID_BUTTON: BEGIN

		COMMON SETTINGS,settings
		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.xscale_field,get_value=xscale
		widget_control,uvalue.yscale_field,get_value=yscale
		widget_control,uvalue.xshift_field,get_value=xshift
		widget_control,uvalue.yshift_field,get_value=yshift
		widget_control,uvalue.deg_field,get_value=deg

		IF settings.cal_type EQ 0 THEN BEGIN

			widget_control,uvalue.phantom_draw,get_value=draw_id
			wset,draw_id

			FILD_overlay_image,phantom_current_frame,image_grid,xscale,yscale,xshift,yshift,deg,settings.grid_image,filter=settings.filter

			TVSCL,image_grid

		ENDIF ELSE BEGIN

			widget_control,uvalue.phantom_draw,get_value=draw_id
			wset,draw_id

			FILD_overlay_pix,phantom_current_frame,output_image,xscale,yscale,xshift,yshift,deg,$
				filter=settings.filter,filename=fild_grid_info.strike_map_filename,$
				scintillator_plate=settings.default_scintillator_plate

			tvscl,output_image

		ENDELSE


	END

;**************************************************************************************************************************************************

	UVALUE.SAVE_STRIKE_MAP_PIXELS: BEGIN

		COMMON SETTINGS,settings
		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
		COMMON GRID_PARAMS,grid_params
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times

		widget_control,uvalue.xscale_field,get_value=xscale
		widget_control,uvalue.yscale_field,get_value=yscale
		widget_control,uvalue.xshift_field,get_value=xshift
		widget_control,uvalue.yshift_field,get_value=yshift
		widget_control,uvalue.deg_field,get_value=deg

		thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

		CASE thistab of
			0: BEGIN

				export_filename=settings.results_path+'strike_map_pixels_ccd_'+strtrim(settings.shot,1)+'.sav'

				FILD_overlay_pix,ccd_current_image,output_image,xscale,yscale,xshift,yshift,deg,$
					filter=settings.filter,filename=fild_grid_info.strike_map_filename,$
					scintillator_plate=settings.default_scintillator_plate,EXPORT_VARS=export_filename


			END

			1: BEGIN

				export_filename=settings.results_path+'strike_map_pixels_phantom_'+strtrim(settings.shot,1)+'.sav'

				FILD_overlay_pix,phantom_current_frame,output_image,xscale,yscale,xshift,yshift,deg,$
					filter=settings.filter,filename=fild_grid_info.strike_map_filename,$
					scintillator_plate=settings.default_scintillator_plate,EXPORT_VARS=export_filename
			END

			ELSE: BEGIN

				print,'No variables saved... Tab must be CCD analysis or Phantom analysis'
				return

			END
		ENDCASE


	END

;**************************************************************************************************************************************************

	UVALUE.SAVE_REMAP_VARS: BEGIN

		COMMON SETTINGS,settings
		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON GRID_PARAMS,grid_params
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
		COMMON PHANTOM_remap,phantom_remap_frame,phantom_remap_gyroradius,phantom_remap_pitch
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON CCD_remap,ccd_remap_frame,ccd_remap_gyroradius,ccd_remap_pitch


		thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

		CASE thistab of
			0: BEGIN

				export_filename=settings.results_path+'remap_variables_ccd_'+strtrim(settings.shot,1)+'_'+$
					strtrim(ccd_ccd_txt_times(ccd_ccd_frame_num),1)+'.sav'
				ccd_remap_info={data:ccd_remap_frame,gyroradius:ccd_remap_gyroradius,pitch:ccd_remap_pitch}
				save,filename=export_filename,ccd_remap_info
				print,'Variables saved in: ',export_filename

			END

			1: BEGIN

				export_filename=settings.results_path+'remap_variables_phantom_'+strtrim(settings.shot,1)+'_'+$
					strtrim(phantom_times(phantom_frame_num),1)+'.sav'
				phantom_remap_info={data:phantom_remap_frame,gyroradius:phantom_remap_gyroradius,pitch:phantom_remap_pitch}
				save,filename=export_filename,phantom_remap_info
				print,'Variables saved in: ',export_filename

			END

			ELSE: BEGIN

				print,'No variables saved... Tab must be CCD analysis or Phantom analysis'
				return

			END
		ENDCASE


	END



;***************************************************************************************************************************************************

	UVALUE.FILD_FIELD: BEGIN

		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON SETTINGS,settings

		widget_control,uvalue.fild_field,get_value=fild_id

		settings.fild=fild_id

		;FILD_info=FILDSIM_read_std_set(FILD=settings.fild)
		IF n_tags(FILD_info) EQ 0 THEN return

		fild_grid_info.fild_name=fild_info.fild_name
		fild_grid_info.strike_map_filename=fild_info.strike_map_filename
		fild_grid_info.strike_points_filename=fild_info.strike_points_filename

	END

;***************************************************************************************************************************************************


	UVALUE.SHOT_FIELD: BEGIN
		COMMON SETTINGS,settings

		widget_control,uvalue.shot_field,get_value=shot
		settings.shot=shot

	END

;***************************************************************************************************************************************************


	UVALUE.PHANTOM_INITIAL_TIME_BUTTON: BEGIN
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

		widget_control,uvalue.phantom_initial_time_button,get_value=init
		phantom_initial_time=init

	END

;***************************************************************************************************************************************************


	UVALUE.PHANTOM_FINAL_TIME_BUTTON: BEGIN
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

		widget_control,uvalue.phantom_final_time_button,get_value=fin
		phantom_final_time=fin

	END

;***************************************************************************************************************************************************

UVALUE.PHANTOM_CALCULATE_TIMETRACE_BUTTON: BEGIN


	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
	COMMON TIMETRACE,ccd_timetrace,ccd_timetrace_roi,phantom_timetrace,phantom_timetrace_roi


	widget_control,uvalue.phantom_draw,get_value=draw_id
	wset,draw_id
	roi=CW_DEFROI(uvalue.phantom_draw,/restore)

	ss=size(phantom_frames,/dimensions)

	roi_timetrace=fltarr(ss(2))


	FOR kk=0,ss(2)-1 DO BEGIN
		dumm=reform(phantom_frames(*,*,kk))
		roi_timetrace(kk)=total(dumm(roi))
	ENDFOR

	widget_control,uvalue.phantom_remap_draw,get_value=draw_id
	wset,draw_id

	plot,phantom_times,roi_timetrace,color=0,background=255,xtitle='Time (s)',ytitle='FILD ROI intensity (counts)',psym=-4,charsize=2.

	print,' calculation finished'

	phantom_timetrace=phantom_times
	phantom_timetrace_roi=roi_timetrace

END

;*******************************************************************************************

UVALUE.CCD_TIMETRACE_BUTTON: BEGIN


		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON SETTINGS,settings
		COMMON TIMETRACE,ccd_timetrace,ccd_timetrace_roi,phantom_timetrace,phantom_timetrace_roi

		widget_control,uvalue.ccd_frame_draw,get_value=draw_id
		wset,draw_id
		roi=CW_DEFROI(uvalue.ccd_frame_draw,/restore)

		roi_timetrace=fltarr(n_elements(ccd_ccd_txt_times))
		roi_time=ccd_ccd_txt_times

		widget_control,/HOURGLASS

		FOR kk=0,n_elements(ccd_ccd_txt_times)-1 DO BEGIN

			dumm=ccd_frames(*,*,kk)-ccd_background_image
			dumm>=0

			IF settings.filter GT 0 THEN dumm=median(dumm,settings.filter)

			roi_timetrace(kk)=total(dumm(roi))

		ENDFOR

		widget_control,uvalue.ccd_remap_draw,get_value=draw_id
		wset,draw_id

		roi_time=ccd_ccd_txt_times
		plot,roi_time,roi_timetrace,color=0,background=255,xtitle='Time (s)',ytitle='FILD ROI intensity (counts)',psym=-4,charsize=2.,xrange=[0,max(ccd_ccd_txt_times)],xstyle=1

		print,'Timetrace calculation finished'

		ccd_timetrace=roi_time
		ccd_timetrace_roi=roi_timetrace
END


;***************************************************************************************************************************************************



	UVALUE.checkbox_settings: BEGIN

		COMMON SETTINGS,settings
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

		widget_control,uvalue.checkbox_settings,get_value=values


		IF values(0) EQ 1 THEN BEGIN
			settings.filter=3
		ENDIF ELSE settings.filter=0

		settings.remap=values(1)
		settings.overlay_grid=values(2)
		settings.profiles=values(3)

		IF settings.remap EQ 1 THEN BEGIN

			widget_control,uvalue.ccd_remap_button,SENSITIVE=1
			widget_control,uvalue.phantom_remap_button,SENSITIVE=1

		ENDIF ELSE BEGIN

			widget_control,uvalue.ccd_remap_button,SENSITIVE=0
			widget_control,uvalue.phantom_remap_button,SENSITIVE=0

		ENDELSE

		IF settings.overlay_grid EQ 1 THEN BEGIN

			widget_control,uvalue.plot_grid_button,SENSITIVE=1
			widget_control,uvalue.phantom_plot_grid_button,SENSITIVE=1

		ENDIF ELSE BEGIN

			widget_control,uvalue.plot_grid_button,SENSITIVE=0
			widget_control,uvalue.phantom_plot_grid_button,SENSITIVE=0

			widget_control,uvalue.ccd_frame_draw,get_value=draw_id
			wset,draw_id
			tvscl,ccd_current_image

			widget_control,uvalue.phantom_draw,get_value=draw_id
			wset,draw_id
			tvscl,phantom_current_frame

		ENDELSE

		IF settings.profiles EQ 1 THEN BEGIN

			widget_control,uvalue.ccd_gyroradius_profile_plot_button,SENSITIVE=1
			widget_control,uvalue.ccd_pitch_profile_plot_button,SENSITIVE=1

			widget_control,uvalue.phantom_gyroradius_profile_plot_button,SENSITIVE=1
			widget_control,uvalue.phantom_pitch_profile_plot_button,SENSITIVE=1

		ENDIF ELSE BEGIN

			widget_control,uvalue.ccd_gyroradius_profile_plot_button,SENSITIVE=0
			widget_control,uvalue.ccd_pitch_profile_plot_button,SENSITIVE=0

			widget_control,uvalue.phantom_gyroradius_profile_plot_button,SENSITIVE=1
			widget_control,uvalue.phantom_pitch_profile_plot_button,SENSITIVE=1

		ENDELSE

	END

;***************************************************************************************************************************************************


	UVALUE.CCD_GYRORADIUS_PROFILE_PLOT_BUTTON: BEGIN

		COMMON SETTINGS,settings
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON REMAP_SETTINGS,remap_settings
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.gyroradius_profile_minpitch,get_value=minpitch
		widget_control,uvalue.gyroradius_profile_maxpitch,get_value=maxpitch
		widget_control,uvalue.settings_dpitch,get_value=dpitch

		widget_control,uvalue.gyroradius_profile_mingyr,get_value=mingyr
		widget_control,uvalue.gyroradius_profile_maxgyr,get_value=maxgyr
		widget_control,uvalue.settings_dgyr,get_value=dgyr

		widget_control,uvalue.gyroradius_profile_maxint,get_value=maxint

		remap_settings.profile_gyro_minpitch=minpitch
		remap_settings.profile_gyro_maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.profile_gyro_mingyr=mingyr
		remap_settings.profile_gyro_maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.profile_gyro_maxint=maxint

		FILD_gyroradius_profile,o,REF_FRAME=ccd_current_image,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
					GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

		IF n_tags(o) LT 2 THEN return

		widget_control,uvalue.ccd_gyroradius_profile_draw,get_value=draw_id
		wset,draw_id

		plot,o.gyro,o.gyro_intensity,psym=-4,color=0,background=255,charsize=1.0,$
			xrange=[remap_settings.profile_gyro_mingyr,remap_settings.profile_gyro_maxgyr],xstyle=1,$
			xtitle='Gyroradius (cm)',ytitle='Intensity (a.u.)',$
			yrange=[0,maxint],ystyle=1

			xyouts,0.2,0.9,'Gyroradius Profile. Pitch Integrated=['+str(minpitch)+','+str(maxpitch)+']',/normal,charsize=1.0

	END

;***************************************************************************************************************************************************


	UVALUE.CCD_PITCH_PROFILE_PLOT_BUTTON: BEGIN

		COMMON SETTINGS,settings
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON REMAP_SETTINGS,remap_settings
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.pitch_profile_minpitch,get_value=minpitch
		widget_control,uvalue.pitch_profile_maxpitch,get_value=maxpitch
		widget_control,uvalue.settings_dpitch,get_value=dpitch

		widget_control,uvalue.pitch_profile_mingyr,get_value=mingyr
		widget_control,uvalue.pitch_profile_maxgyr,get_value=maxgyr
		widget_control,uvalue.settings_dgyr,get_value=dgyr

		widget_control,uvalue.pitch_profile_maxint,get_value=maxint

		remap_settings.profile_pitch_minpitch=minpitch
		remap_settings.profile_pitch_maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.profile_pitch_mingyr=mingyr
		remap_settings.profile_pitch_maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.profile_pitch_maxint=maxint

		FILD_pitch_profile,o,REF_FRAME=ccd_current_image,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
			GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

		IF n_tags(o) LT 2 THEN return

		widget_control,uvalue.ccd_pitch_profile_draw,get_value=draw_id
		wset,draw_id

		plot,o.pitch,o.pitch_intensity,psym=-4,color=0,background=255,charsize=1.0,$
			xrange=[remap_settings.profile_pitch_minpitch,remap_settings.profile_pitch_maxpitch],xstyle=1,$
			xtitle='Pitch Angle (deg)',ytitle='Intensity (a.u.)',$
			yrange=[0,maxint],ystyle=1

			xyouts,0.2,0.9,'Pitch Angle Profile. Gyroradius Integrated=['+str(mingyr)+','+str(maxgyr)+']',/normal,charsize=1.0




	END

;***************************************************************************************************************************************************


	UVALUE.PHANTOM_GYRORADIUS_PROFILE_PLOT_BUTTON: BEGIN

		COMMON SETTINGS,settings
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
		COMMON REMAP_SETTINGS,remap_settings
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.phantom_gyroradius_profile_minpitch,get_value=minpitch
		widget_control,uvalue.phantom_gyroradius_profile_maxpitch,get_value=maxpitch
		widget_control,uvalue.phantom_settings_dpitch,get_value=dpitch

		widget_control,uvalue.phantom_gyroradius_profile_mingyr,get_value=mingyr
		widget_control,uvalue.phantom_gyroradius_profile_maxgyr,get_value=maxgyr
		widget_control,uvalue.phantom_settings_dgyr,get_value=dgyr

		widget_control,uvalue.phantom_gyroradius_profile_maxint,get_value=maxint

		remap_settings.profile_gyro_minpitch=minpitch
		remap_settings.profile_gyro_maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.profile_gyro_mingyr=mingyr
		remap_settings.profile_gyro_maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.profile_gyro_maxint=maxint

		FILD_gyroradius_profile,o,REF_FRAME=phantom_current_frame,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

		IF n_tags(o) LT 2 THEN return

		widget_control,uvalue.phantom_gyroradius_profile_draw,get_value=draw_id
		wset,draw_id

		plot,o.gyro,o.gyro_intensity,psym=-4,color=0,background=255,charsize=1.0,$
			xrange=[remap_settings.profile_gyro_mingyr,remap_settings.profile_gyro_maxgyr],xstyle=1,$
			xtitle='Gyroradius (cm)',ytitle='Intensity (a.u.)',$
			yrange=[0,maxint],ystyle=1

			xyouts,0.2,0.9,'Gyroradius Profile. Pitch Integrated=['+str(minpitch)+','+str(maxpitch)+']',/normal,charsize=1.0

	END

;***************************************************************************************************************************************************


	UVALUE.PHANTOM_GYRORADIUS_PROFILE_VIDEO: BEGIN

		COMMON SETTINGS,settings
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
		COMMON REMAP_SETTINGS,remap_settings
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.phantom_gyroradius_profile_minpitch,get_value=minpitch
		widget_control,uvalue.phantom_gyroradius_profile_maxpitch,get_value=maxpitch
		widget_control,uvalue.phantom_settings_dpitch,get_value=dpitch

		widget_control,uvalue.phantom_gyroradius_profile_mingyr,get_value=mingyr
		widget_control,uvalue.phantom_gyroradius_profile_maxgyr,get_value=maxgyr
		widget_control,uvalue.phantom_settings_dgyr,get_value=dgyr

		widget_control,uvalue.phantom_gyroradius_profile_maxint,get_value=maxint

		remap_settings.profile_gyro_minpitch=minpitch
		remap_settings.profile_gyro_maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.profile_gyro_mingyr=mingyr
		remap_settings.profile_gyro_maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.profile_gyro_maxint=maxint


		;;;;;;;;;;CREATE MPEG;;;;;;;;;;;;;;;;;;;

		vid_xsize=640
		vid_ysize=480

		s=size(phantom_frames,/dimensions)
		frames=s(2)
		time=phantom_times

		Filename_save=settings.RESULTS_PATH+strtrim(settings.shot,2)+'_'+strtrim(time(0),2)+'_'+strtrim(time(n_elements(time)-1),2)+'_phantom_gyroradius_profile.mpeg'
		mpegID = MPEG_Open([vid_xsize, vid_ysize], Filename=filename_save,quality=100)

		set_plot,'Z'

		widget_control,/HOURGLASS
		device,set_pixel_depth=8,decomposed=0,set_resolution=[vid_xsize,vid_ysize],set_colors=256

		FOR j=0,frames-1 DO BEGIN
      			print,j,' out of ',frames,'  frames loaded.'

			fr=reform(LONG(phantom_frames(*,*,j)))-LONG(phantom_background_frame)
			fr>=0

			FILD_gyroradius_profile,o,REF_FRAME=fr,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

			plot,o.gyro,o.gyro_intensity,psym=-4,color=0,background=255,charsize=1.0,$
			xrange=[remap_settings.profile_gyro_mingyr,remap_settings.profile_gyro_maxgyr],xstyle=1,$
			xtitle='Gyroradius (cm)',ytitle='Intensity (a.u.)',$
			yrange=[0,maxint],ystyle=1,$
			title='Gyroradius Profile. Pitch Integrated=['+strtrim(string(minpitch,format='(f6.3)'))+','+strtrim(string(maxpitch,format='(f6.3)'))+'];t='+$
				strtrim(string(time[j],format='(f6.3)'))

			data_2=TVRD()

			MPEG_Put, mpegID, image=data_2, Frame=j,/ORDER

		ENDFOR


		print,'Saving Mpeg video into '+settings.RESULTS_PATH+'      ... can take a while'
		MPEG_Save, mpegID
                                ; Close the MPEG sequence and file.
    		MPEG_Close, mpegID

		set_plot,'X'


	END


;***************************************************************************************************************************************************


	UVALUE.PHANTOM_PITCH_PROFILE_VIDEO: BEGIN

		COMMON SETTINGS,settings
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
		COMMON REMAP_SETTINGS,remap_settings
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.phantom_pitch_profile_minpitch,get_value=minpitch
		widget_control,uvalue.phantom_pitch_profile_maxpitch,get_value=maxpitch
		widget_control,uvalue.phantom_settings_dpitch,get_value=dpitch

		widget_control,uvalue.phantom_pitch_profile_mingyr,get_value=mingyr
		widget_control,uvalue.phantom_pitch_profile_maxgyr,get_value=maxgyr
		widget_control,uvalue.phantom_settings_dgyr,get_value=dgyr

		widget_control,uvalue.phantom_pitch_profile_maxint,get_value=maxint

		remap_settings.profile_pitch_minpitch=minpitch
		remap_settings.profile_pitch_maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.profile_pitch_mingyr=mingyr
		remap_settings.profile_pitch_maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.profile_pitch_maxint=maxint


		;;;;;;;;;;CREATE MPEG;;;;;;;;;;;;;;;;;;;

		vid_xsize=640
		vid_ysize=480

		s=size(phantom_frames,/dimensions)
		frames=s(2)
		time=phantom_times

		Filename_save=settings.RESULTS_PATH+strtrim(settings.shot,2)+'_'+strtrim(time(0),2)+'_'+strtrim(time(n_elements(time)-1),2)+'_phantom_pitch_profile.mpeg'
		mpegID = MPEG_Open([vid_xsize, vid_ysize], Filename=filename_save,quality=100)

		set_plot,'Z'

		widget_control,/HOURGLASS
		device,set_pixel_depth=8,decomposed=0,set_resolution=[vid_xsize,vid_ysize],set_colors=256

		FOR j=0,frames-1 DO BEGIN
      			print,j,' out of ',frames,'  frames loaded.'

			fr=reform(LONG(phantom_frames(*,*,j)))-LONG(phantom_background_frame)
			fr>=0

			FILD_pitch_profile,o,REF_FRAME=fr,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

			plot,o.pitch,o.pitch_intensity,psym=-4,color=0,background=255,charsize=1.0,$
			xrange=[remap_settings.profile_gyro_minpitch,remap_settings.profile_gyro_maxpitch],xstyle=1,$
			xtitle='Pitch Angle (deg)',ytitle='Intensity (a.u.)',$
			yrange=[0,maxint],ystyle=1,$
			title='Pitch Profile. Gyro Integrated=['+strtrim(string(mingyr,format='(f6.3)'))+','+strtrim(string(maxgyr,format='(f6.3)'))+'];t='+$
				strtrim(string(time[j],format='(f6.3)'))

			data_2=TVRD()

			MPEG_Put, mpegID, image=data_2, Frame=j,/ORDER

		ENDFOR


		print,'Saving Mpeg video into '+settings.RESULTS_PATH+'      ... can take a while'
		MPEG_Save, mpegID
                                ; Close the MPEG sequence and file.
    		MPEG_Close, mpegID

		set_plot,'X'


	END

;***************************************************************************************************************************************************


	UVALUE.PHANTOM_PITCH_PROFILE_PLOT_BUTTON: BEGIN

		COMMON SETTINGS,settings
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
		COMMON REMAP_SETTINGS,remap_settings
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.phantom_pitch_profile_minpitch,get_value=minpitch
		widget_control,uvalue.phantom_pitch_profile_maxpitch,get_value=maxpitch
		widget_control,uvalue.phantom_settings_dpitch,get_value=dpitch

		widget_control,uvalue.phantom_pitch_profile_mingyr,get_value=mingyr
		widget_control,uvalue.phantom_pitch_profile_maxgyr,get_value=maxgyr
		widget_control,uvalue.phantom_settings_dgyr,get_value=dgyr

		widget_control,uvalue.phantom_pitch_profile_maxint,get_value=maxint

		remap_settings.profile_pitch_minpitch=minpitch
		remap_settings.profile_pitch_maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.profile_pitch_mingyr=mingyr
		remap_settings.profile_pitch_maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.profile_pitch_maxint=maxint

		FILD_pitch_profile,o,REF_FRAME=phantom_current_frame,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

		IF n_tags(o) LT 2 THEN return

		widget_control,uvalue.phantom_pitch_profile_draw,get_value=draw_id
		wset,draw_id

		plot,o.pitch,o.pitch_intensity,psym=-4,color=0,background=255,charsize=1.0,$
			xrange=[remap_settings.profile_pitch_minpitch,remap_settings.profile_pitch_maxpitch],xstyle=1,$
			xtitle='Pitch Angle (deg)',ytitle='Intensity (a.u.)',$
			yrange=[0,maxint],ystyle=1

			xyouts,0.2,0.9,'Pitch Angle Profile. Gyroradius Integrated=['+str(mingyr)+','+str(maxgyr)+']',/normal,charsize=1.0




	END


;***************************************************************************************************************************************************

	UVALUE.SET_BACKGROUND_BUTTON: BEGIN

	COMMON SETTINGS,settings
	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times

	ccd_background_image=ccd_current_image

	END

	UVALUE.PHANTOM_SET_BACKGROUND_BUTTON: BEGIN

	COMMON SETTINGS,settings
	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

	phantom_background_frame=phantom_current_frame

	END


;***************************************************************************************************************************************************

	UVALUE.SUBSTRACT_BACKGROUND_BUTTON: BEGIN

	COMMON SETTINGS,settings
	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times

	widget_control,uvalue.ccd_saturation_slider,get_value=sat_level


		IF settings.overlay_grid EQ 1 THEN BEGIN

			ccd_current_image=ccd_current_image-ccd_background_image
			ccd_current_image>=0

			ccd_current_image<=max(current_image)*sat_level/256

			widget_control,uvalue.plot_grid_button,send_event={ID:uvalue.plot_grid_button,TOP:uvalue.ccd_frame_base,HANDLER:uvalue.ccd_frame_slider}

			IF settings.profiles EQ 1 THEN BEGIN

				widget_control,uvalue.ccd_gyroradius_profile_plot_button,$
					send_event={ID:uvalue.ccd_gyroradius_profile_plot_button,TOP:uvalue.ccd_gyroradius_profile_base,HANDLER:uvalue.ccd_frame_slider}
				widget_control,uvalue.ccd_pitch_profile_plot_button,$
					send_event={ID:uvalue.ccd_pitch_profile_plot_button,TOP:uvalue.ccd_pitch_profile_base,HANDLER:uvalue.ccd_frame_slider}

			ENDIF


		ENDIF ELSE BEGIN

		widget_control,uvalue.ccd_frame_draw,get_value=draw_id
		wset,draw_id

			IF settings.filter GT 0 THEN BEGIN
				imm=ccd_current_image-ccd_background_image
				imm=median(imm,settings.filter)
			ENDIF ELSE BEGIN
				imm=ccd_current_image-ccd_background_image
			ENDELSE

		imm>=0

		imm<=max(imm)*sat_level/256

		TVSCL,imm

		ccd_current_image=imm

		ENDELSE


	END

;***************************************************************************************************************************************************


	UVALUE.PHANTOM_SUBSTRACT_BACKGROUND_BUTTON: BEGIN

	COMMON SETTINGS,settings
	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

		IF settings.overlay_grid EQ 1 THEN BEGIN

			phantom_current_frame=LONG(phantom_current_frame)-LONG(phantom_background_frame)
			phantom_current_frame>=0
			widget_control,uvalue.phantom_plot_grid_button,$
				send_event={ID:uvalue.phantom_plot_grid_button,TOP:uvalue.phantom_base,HANDLER:uvalue.phantom_slider}


		ENDIF ELSE BEGIN

		widget_control,uvalue.phantom_draw,get_value=draw_id
		wset,draw_id


			IF settings.filter GT 0 THEN BEGIN
				phantom_current_frame=LONG(phantom_current_frame)-LONG(phantom_background_frame)
				phantom_current_frame=median(phantom_current_frame,settings.filter)
			ENDIF ELSE BEGIN
				phantom_current_frame=LONG(phantom_current_frame)-LONG(phantom_background_frame)
			ENDELSE


		phantom_current_frame>=0

		TVSCL,phantom_current_frame

		ENDELSE


	END


;***************************************************************************************************************************************************


	UVALUE.CCD_FRAME_DRAW: BEGIN

	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times

	widget_control,uvalue.ccd_frame_draw,get_value=draw_id


	IF !D.Name EQ 'X' THEN BEGIN


		wset,draw_id

		cursor,x,y,/device,/change

		intens=ccd_current_image(x,y)
		widget_control,uvalue.ccd_frame_xpixel,set_value=strcompress(x),/dynamic_resize
		widget_control,uvalue.ccd_frame_ypixel,set_value=strcompress(y),/dynamic_resize
		widget_control,uvalue.ccd_frame_intensity,set_value='Intensity= '+strcompress(intens)

	ENDIF

	END

;***************************************************************************************************************************************************



	UVALUE.PHANTOM_DRAW: BEGIN

	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

	widget_control,uvalue.phantom_draw,get_value=draw_id

	IF !D.Name EQ 'X' THEN BEGIN

		wset,draw_id

		cursor,x,y,/device,/change

		intens=phantom_current_frame(x,y)
		widget_control,uvalue.phantom_info_xpixel,set_value=strcompress(x),/dynamic_resize
		widget_control,uvalue.phantom_info_ypixel,set_value=strcompress(y),/dynamic_resize
		widget_control,uvalue.phantom_info_intensity,set_value='Intensity= '+strcompress(intens)

	ENDIF

	END


;***************************************************************************************************************************************************


	UVALUE.CCD_REMAP_BUTTON: BEGIN

		COMMON SETTINGS,settings
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON GRID_PARAMS,grid_params
		COMMON REMAP_SETTINGS,remap_settings
		COMMON CCD_remap,ccd_remap_frame,ccd_remap_gyroradius,ccd_remap_pitch



		widget_control,uvalue.settings_minpitch,get_value=minpitch
		widget_control,uvalue.settings_maxpitch,get_value=maxpitch
		widget_control,uvalue.settings_dpitch,get_value=dpitch

		widget_control,uvalue.settings_mingyr,get_value=mingyr
		widget_control,uvalue.settings_maxgyr,get_value=maxgyr
		widget_control,uvalue.settings_dgyr,get_value=dgyr

		widget_control,uvalue.settings_remap_levels,get_value=nlevels

		remap_settings.minpitch=minpitch
		remap_settings.maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.mingyr=mingyr
		remap_settings.maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.nlev=nlevels

		print,'REMAPING FRAME... THIS MIGHT TAKE A WHILE...'
		print,'REMEMBER TO CLICK ON SAVE PARAMETERS TO PERFORM CALCULATION WITH CURRENT PARAMETERS IN SCREEN'
		widget_control,/HOURGLASS

		FILD_remap_grid,o,REF_FRAME=ccd_current_image,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS,OLD_ALGORITHM=0

		ccd_interpolated_gyro=o.ginterp
		ccd_interpolated_pitch=o.pinterp
		ccd_interpolated_fcol=o.cinterp
		ccd_interpolated_angle=o.anginterp
		ccd_interpolated_gyrophase=o.gyrophaseinterp


		widget_control,uvalue.ccd_remap_draw,get_value=draw_id
		wset,draw_id


		lev=findgen(remap_settings.nlev)/(remap_settings.nlev)*(max(o.h)-min(o.h))+min(o.h)
		col=findgen(remap_settings.nlev)/(remap_settings.nlev)*255

		ccd_remap_frame=o.h
		ccd_remap_gyroradius=o.gir
		ccd_remap_pitch=o.pitch

		contour,o.h,o.pitch,o.gir,levels=lev,c_colors=col,/fill,xtitle='Pitch Angle (deg)',ytitle='Gyroradius (cm)',$
			xrange=[remap_settings.minpitch,remap_settings.maxpitch],yrange=[remap_settings.mingyr,remap_settings.maxgyr],$
			xstyle=1,ystyle=1,charsize=2.0,POSITION=[0.2,0.2,0.8,0.8]

		colorbar,max=max(o.h),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e11.3)',/vert

		print,'REMAP FINISHED'

	END

;***************************************************************************************************************************************************

	UVALUE.EXPORT_EPS_CCD_REMAP: BEGIN

		COMMON SETTINGS,settings
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON GRID_PARAMS,grid_params
		COMMON REMAP_SETTINGS,remap_settings

		set_plot,'ps'

		dumm_filename=strtrim(settings.results_path,1)+'remap_'+strtrim(ccd_ccd_shot_num,1)+'_fild'+strtrim(settings.fild,1)+'_'+$
			strtrim(ccd_ccd_txt_times(ccd_ccd_frame_num),1)+'.ps'
		device,filename=dumm_filename,/color

		widget_control,uvalue.settings_minpitch,get_value=minpitch
		widget_control,uvalue.settings_maxpitch,get_value=maxpitch
		widget_control,uvalue.settings_dpitch,get_value=dpitch

		widget_control,uvalue.settings_mingyr,get_value=mingyr
		widget_control,uvalue.settings_maxgyr,get_value=maxgyr
		widget_control,uvalue.settings_dgyr,get_value=dgyr

		widget_control,uvalue.settings_remap_levels,get_value=nlevels

		remap_settings.minpitch=minpitch
		remap_settings.maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.mingyr=mingyr
		remap_settings.maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.nlev=nlevels

		print,'EXPORTING REMAP TO .EPS --> '+strtrim(dumm_filename,1)

		widget_control,/HOURGLASS

		FILD_remap_grid,o,REF_FRAME=ccd_current_image,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

		widget_control,uvalue.ccd_remap_draw,get_value=draw_id


		lev=findgen(remap_settings.nlev)/(remap_settings.nlev)*(max(o.h)-min(o.h))+min(o.h)
		col=findgen(remap_settings.nlev)/(remap_settings.nlev)*255

		contour,o.h,o.pitch,o.gir,levels=lev,c_colors=col,/fill,xtitle='Pitch Angle (deg)',ytitle='Gyroradius (cm)',$
			xrange=[remap_settings.minpitch,remap_settings.maxpitch],yrange=[remap_settings.mingyr,remap_settings.maxgyr],$
			xstyle=1,ystyle=1,charsize=2.0,POSITION=[0.2,0.2,0.8,0.8]

		colorbar,max=max(o.h),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e11.3)',/vert


		device,/close
		set_plot,'x'

	END

;***************************************************************************************************************************************************


	UVALUE.PHANTOM_REMAP_BUTTON: BEGIN

		COMMON SETTINGS,settings
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON GRID_PARAMS,grid_params
		COMMON REMAP_SETTINGS,remap_settings
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase
		COMMON PHANTOM_remap,phantom_remap_frame,phantom_remap_gyroradius,phantom_remap_pitch



		widget_control,uvalue.phantom_settings_minpitch,get_value=minpitch
		widget_control,uvalue.phantom_settings_maxpitch,get_value=maxpitch
		widget_control,uvalue.phantom_settings_dpitch,get_value=dpitch

		widget_control,uvalue.phantom_settings_mingyr,get_value=mingyr
		widget_control,uvalue.phantom_settings_maxgyr,get_value=maxgyr
		widget_control,uvalue.phantom_settings_dgyr,get_value=dgyr

		widget_control,uvalue.phantom_settings_remap_levels,get_value=nlevels

		remap_settings.minpitch=minpitch
		remap_settings.maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.mingyr=mingyr
		remap_settings.maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.nlev=nlevels

		print,'REMAPING FRAME... THIS MIGHT TAKE A WHILE...'

		widget_control,/HOURGLASS

		FILD_remap_grid,o,REF_FRAME=phantom_current_frame,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
					GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS,OLD_ALGORITHM=0

		phantom_interpolated_gyro=o.ginterp
		phantom_interpolated_pitch=o.pinterp
		phantom_interpolated_fcol=o.cinterp
		phantom_interpolated_angle=o.anginterp
		phantom_interpolated_gyrophase=o.gyrophaseinterp


		widget_control,uvalue.phantom_remap_draw,get_value=draw_id
		wset,draw_id


		lev=findgen(remap_settings.nlev)/(remap_settings.nlev)*(max(o.h)-min(o.h))+min(o.h)
		col=findgen(remap_settings.nlev)/(remap_settings.nlev)*255

		phantom_remap_frame=o.h
		phantom_remap_gyroradius=o.gir
		phantom_remap_pitch=o.pitch

		contour,o.h,o.pitch,o.gir,levels=lev,c_colors=col,/fill,xtitle='Pitch Angle (deg)',ytitle='Gyroradius (cm)',$
			xrange=[remap_settings.minpitch,remap_settings.maxpitch],yrange=[remap_settings.mingyr,remap_settings.maxgyr],$
			xstyle=1,ystyle=1,charsize=2.0,POSITION=[0.2,0.2,0.8,0.8]

		colorbar,max=max(o.h),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e11.3)',/vert


		print,'REMAP FINISHED'
	END

;***************************************************************************************************************************************************

	UVALUE.EXPORT_EPS_PHANTOM_REMAP: BEGIN

		COMMON SETTINGS,settings
		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON GRID_PARAMS,grid_params
		COMMON REMAP_SETTINGS,remap_settings
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase


		set_plot,'ps'

		dumm_filename=strtrim(settings.results_path,1)+'remap_'+strtrim(settings.shot,1)+'_'+strtrim(phantom_times(phantom_frame_num),1)+$
			'_fild'+strtrim(settings.fild,1)+'.ps'
		device,filename=dumm_filename,/color


		widget_control,uvalue.phantom_settings_minpitch,get_value=minpitch
		widget_control,uvalue.phantom_settings_maxpitch,get_value=maxpitch
		widget_control,uvalue.phantom_settings_dpitch,get_value=dpitch

		widget_control,uvalue.phantom_settings_mingyr,get_value=mingyr
		widget_control,uvalue.phantom_settings_maxgyr,get_value=maxgyr
		widget_control,uvalue.phantom_settings_dgyr,get_value=dgyr

		widget_control,uvalue.phantom_settings_remap_levels,get_value=nlevels

		remap_settings.minpitch=minpitch
		remap_settings.maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.mingyr=mingyr
		remap_settings.maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.nlev=nlevels

		print,'EXPORTING REMAP TO .EPS --> '+strtrim(dumm_filename,1)

		widget_control,/HOURGLASS

		FILD_remap_grid,o,REF_FRAME=phantom_current_frame,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
					GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

		widget_control,uvalue.phantom_remap_draw,get_value=draw_id

		lev=findgen(remap_settings.nlev)/(remap_settings.nlev)*(max(o.h)-min(o.h))+min(o.h)
		col=findgen(remap_settings.nlev)/(remap_settings.nlev)*255

		contour,o.h,o.pitch,o.gir,levels=lev,c_colors=col,/fill,xtitle='Pitch Angle (deg)',ytitle='Gyroradius (cm)',$
			xrange=[remap_settings.minpitch,remap_settings.maxpitch],yrange=[remap_settings.mingyr,remap_settings.maxgyr],$
			xstyle=1,ystyle=1,charsize=2.0,POSITION=[0.2,0.2,0.8,0.8]

		colorbar,max=max(o.h),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e11.3)',/vert

		device,/close
		set_plot,'x'


	END

;***************************************************************************************************************************************************


UVALUE.EXPORT_EPS_PHANTOM_STRIKE_MAP: BEGIN

		COMMON SETTINGS,settings
		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON GRID_PARAMS,grid_params
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase


		widget_control,uvalue.xscale_field,get_value=xscale
		widget_control,uvalue.yscale_field,get_value=yscale
		widget_control,uvalue.xshift_field,get_value=xshift
		widget_control,uvalue.yshift_field,get_value=yshift
		widget_control,uvalue.deg_field,get_value=deg


		widget_control,uvalue.phantom_slider,get_value=slider_num

		phantom_frame_num=slider_num
		phantom_current_frame=phantom_frames(*,*,slider_num)

		phantom_current_frame=LONG(phantom_current_frame)-LONG(phantom_background_frame)
		phantom_current_frame>=0


		IF settings.filter GT 0 THEN BEGIN
			phantom_current_frame=median(phantom_current_frame,settings.filter)
		ENDIF



		IF settings.overlay_grid EQ 1 THEN BEGIN

			FILD_overlay_pix,phantom_current_frame,output_image,xscale,yscale,xshift,yshift,deg,$
				filter=settings.filter,filename=fild_grid_info.strike_map_filename,$
				scintillator_plate=settings.default_scintillator_plate

		ENDIF ELSE BEGIN

			output_image=phantom_current_frame

		ENDELSE



		set_plot,'ps'

		dumm_filename=strtrim(settings.results_path,1)+'frame_'+strtrim(settings.shot,1)+'_'+strtrim(phantom_times(phantom_frame_num),1)+$
			'_fild'+strtrim(settings.fild,1)+'.ps'
		print,'EXPORTING FRAME TO .EPS --> '+strtrim(dumm_filename,1)
		widget_control,/HOURGLASS

		device,filename=dumm_filename,/color

			tvscl,output_image


		device,/close
		set_plot,'x'


	END

;***************************************************************************************************************************************************


	UVALUE.PLOT_GINTERP: BEGIN

	COMMON SETTINGS,settings
	COMMON FILD_GRID_INFO,fild_grid_info
	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
	COMMON GRID_PARAMS,grid_params
	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

	thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

	CASE thistab OF

	0: BEGIN


		widget_control,uvalue.ccd_frame_draw,get_value=draw_id
		wset,draw_id

		tvscl,ccd_interpolated_gyro

		IF max(ccd_interpolated_gyro) LT 1 THEN BEGIN

			print,'Interpolated matrix is calculated ONLY after a remap operation...'

		ENDIF

		END

	1: BEGIN


		widget_control,uvalue.phantom_draw,get_value=draw_id
		wset,draw_id

		tvscl,phantom_interpolated_gyro

		IF max(phantom_interpolated_gyro) LT 1 THEN BEGIN

			print,'Interpolated matrix is calculated ONLY after a remap operation...'

		ENDIF



	END

	ENDCASE

	END


;***************************************************************************************************************************************************

	UVALUE.PLOT_PINTERP: BEGIN

	COMMON SETTINGS,settings
	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
	COMMON GRID_PARAMS,grid_params
	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

	thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

	CASE thistab OF

	0: BEGIN


		widget_control,uvalue.ccd_frame_draw,get_value=draw_id
		wset,draw_id

		tvscl,ccd_interpolated_pitch

		IF max(ccd_interpolated_pitch) LT 1 THEN BEGIN

			print,'Interpolated matrix is calculated ONLY after a remap operation...'

		ENDIF


	END

	1: BEGIN

		widget_control,uvalue.phantom_draw,get_value=draw_id
		wset,draw_id

		tvscl,phantom_interpolated_pitch

		IF max(phantom_interpolated_pitch) LT 1 THEN BEGIN

			print,'Interpolated matrix is calculated ONLY after a remap operation...'

		ENDIF


	END

	ENDCASE

	END


;***************************************************************************************************************************************************

	UVALUE.PLOT_CINTERP: BEGIN

	COMMON SETTINGS,settings
	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
	COMMON GRID_PARAMS,grid_params
	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

	thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

	CASE thistab OF

	0: BEGIN


		widget_control,uvalue.ccd_frame_draw,get_value=draw_id
		wset,draw_id

		tvscl,ccd_interpolated_fcol

		IF max(ccd_interpolated_fcol) LT 1 THEN BEGIN

			print,'Interpolated matrix is calculated ONLY after a remap operation...'

		ENDIF


	END

	1: BEGIN

		widget_control,uvalue.phantom_draw,get_value=draw_id
		wset,draw_id

		tvscl,phantom_interpolated_fcol

		IF max(phantom_interpolated_fcol) LT 1 THEN BEGIN

			print,'Interpolated matrix is calculated ONLY after a remap operation...'

		ENDIF

	END

	ENDCASE

	END


;***************************************************************************************************************************************************

	UVALUE.SAVE_INTERPOLATED_DATA:BEGIN

	COMMON SETTINGS,settings
	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
	COMMON FILD_GRID_INFO,fild_grid_info
	COMMON GRID_PARAMS,grid_params
	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

	thistab=WIDGET_INFO(uvalue.tab_1,/TAB_CURRENT)

	CASE thistab OF

	0: BEGIN

		fn=strtrim(settings.results_path,1)+'interpolated_data_FILD_CCD_'+strtrim(settings.shot,1)+'.sav'

		interpolated_gyro=ccd_interpolated_gyro
		interpolated_pitch=ccd_interpolated_pitch
		interpolated_fcol=ccd_interpolated_fcol
		interpolated_angle=ccd_interpolated_angle
		interpolated_gyrophase=ccd_interpolated_gyrophase

		interpolated_data={interpolated_gyro:interpolated_gyro,interpolated_pitch:interpolated_pitch,interpolated_fcol:interpolated_fcol,$
				interpolated_angle:interpolated_angle,interpolated_gyrophase:interpolated_gyrophase}

		save,filename=fn,interpolated_data

		print,'Variables saved into: ',fn


	END

	1: BEGIN

		fn=strtrim(settings.results_path,1)+'interpolated_data_FILD_phantom_'+strtrim(settings.shot,1)+'.sav'

		interpolated_gyro=phantom_interpolated_gyro
		interpolated_pitch=phantom_interpolated_pitch
		interpolated_fcol=phantom_interpolated_fcol
		interpolated_angle=phantom_interpolated_angle
		interpolated_gyrophase=phantom_interpolated_gyrophase

		interpolated_data={interpolated_gyro:interpolated_gyro,interpolated_pitch:interpolated_pitch,interpolated_fcol:interpolated_fcol,$
				interpolated_angle:interpolated_angle,interpolated_gyrophase:interpolated_gyrophase}

		save,filename=fn,interpolated_data

		print,'Variables saved into: ',fn


	END

	ENDCASE


	END



;***************************************************************************************************************************************************

	UVALUE.FILDSIM_GET_INPUT_FILE:BEGIN

	COMMON FILDSIM_SETTINGS,fildsim_settings
	COMMON SETTINGS,settings

	fildsim_settings.input_filename= DIALOG_PICKFILE(/READ,PATH=settings.HOME_path)

	widget_control,uvalue.fildsim_current_input,set_value='CURRENT INPUT FILENAME: '+fildsim_settings.input_filename

	END

;***************************************************************************************************************************************************


	UVALUE.FILDSIM_GET_AUG_BFIELD: BEGIN

	COMMON FILDSIM_SETTINGS,fildsim_settings

	widget_control,uvalue.fildsim_shot,get_value=shot
	widget_control,uvalue.fildsim_time,get_value=time
	widget_control,uvalue.fildsim_r_fild,get_value=r_fild
	widget_control,uvalue.fildsim_z_fild,get_value=z_fild

	fildsim_settings.r_fild=r_fild
	fildsim_settings.z_fild=z_fild
	fildsim_settings.shot=shot
	fildsim_settings.time=time

	get_FILD_bfields,fildsim_settings.r_fild,fildsim_settings.z_fild,fildsim_settings.shot,fildsim_settings.time,$
		br,bz,bt,b_total

	fildsim_settings.b_field=b_total
	widget_control,uvalue.fildsim_magnetic,set_value=b_total

	END


;***************************************************************************************************************************************************


	UVALUE.fildsim_load_fildsim_run: BEGIN

	COMMON FILDSIM_SETTINGS,fildsim_settings
	COMMON FILDSIM_RESULTS,fildsim_gyr,fildsim_pitch,fildsim_output_dist,fildsim_input_dist

	fildsim_run= DIALOG_PICKFILE(/READ,PATH='~/',FILTER='*.sav')

	print,'Restoring FILDSIM results from file: ',fildsim_run
	restore,fildsim_run,/verb

	widget_control,uvalue.fildsim_current_input,set_value='CURRENT INPUT FILENAME: '+fildsim_run


	widget_control,uvalue.fildsim_grid_mingyr,get_value=mingyr
	widget_control,uvalue.fildsim_grid_maxgyr,get_value=maxgyr
	widget_control,uvalue.fildsim_grid_dg,get_value=dg
	widget_control,uvalue.fildsim_grid_minpitch,get_value=minpitch
	widget_control,uvalue.fildsim_grid_maxpitch,get_value=maxpitch
	widget_control,uvalue.fildsim_grid_dp,get_value=dp
	widget_control,uvalue.fildsim_grid_nlev,get_value=nlev


	widget_control,uvalue.fildsim_input_draw,get_value=draw_id
	wset,draw_id


	lev=findgen(nlev)/(nlev-1)*(max(fildsim_input_dist)-min(fildsim_input_dist))+min(fildsim_input_dist)
	col=findgen(nlev)/(nlev-1)*255

	contour,fildsim_input_dist,fildsim_pitch,fildsim_gyr,levels=lev,c_colors=col,/fill,charsize=2.0,$
		yrange=[mingyr,maxgyr],ystyle=1,ytitle='Gyroradius (cm)',$
		xrange=[minpitch,maxpitch],xstyle=1,xtitle='Pitch Angle (deg)',$
		POSITION=[0.2,0.2,0.8,0.8],title='Weight/(cm*deg)'

		colorbar,max=max(fildsim_input_dist),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert

	widget_control,uvalue.fildsim_output_draw,get_value=draw_id
	wset,draw_id

	lev=findgen(nlev)/(nlev)*(max(fildsim_output_dist)-min(fildsim_output_dist))+min(fildsim_output_dist)
	col=findgen(nlev)/(nlev)*255

	contour,fildsim_output_dist,fildsim_pitch,fildsim_gyr,levels=lev,c_colors=col,/fill,charsize=2.0,$
		yrange=[mingyr,maxgyr],ystyle=1,ytitle='Gyroradius (cm)',$
		xrange=[minpitch,maxpitch],xstyle=1,xtitle='Pitch Angle (deg)',$
		POSITION=[0.2,0.2,0.8,0.8],title='Weight/(cm*deg)'

	colorbar,max=max(fildsim_output_dist),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert

	widget_control,uvalue.fildsim_slider,SENSITIVE=1,set_value=n_elements(lev),$
	set_slider_min=0,set_slider_max=n_elements(lev)


	END

;***************************************************************************************************************************************************

	UVALUE.FILDSIM_RUN_BUTTON: BEGIN

	COMMON SETTINGS,settings
	COMMON FILDSIM_SETTINGS,fildsim_settings
	COMMON PHYSIC_CONSTANTS,physic_constants
	COMMON FILDSIM_results,fildsim_gyr,fildsim_pitch,fildsim_output_dist,fildsim_input_dist
	COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
		af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
		af_input_path,af_efficiency_curve_file,af_calibration_file


	widget_control,uvalue.fild_field,get_value=fild_id


	widget_control,uvalue.fildsim_shot,get_value=shot
	widget_control,uvalue.fildsim_time,get_value=time
	widget_control,uvalue.fildsim_magnetic,get_value=b_field
	widget_control,uvalue.fildsim_r_fild,get_value=r_fild
	widget_control,uvalue.fildsim_z_fild,get_value=z_fild
	widget_control,uvalue.fildsim_smooth_input_fild,get_value=sm


	widget_control,uvalue.fildsim_grid_mingyr,get_value=mingyr
	widget_control,uvalue.fildsim_grid_maxgyr,get_value=maxgyr
	widget_control,uvalue.fildsim_grid_dg,get_value=dg
	widget_control,uvalue.fildsim_grid_minpitch,get_value=minpitch
	widget_control,uvalue.fildsim_grid_maxpitch,get_value=maxpitch
	widget_control,uvalue.fildsim_grid_dp,get_value=dp
	widget_control,uvalue.fildsim_grid_nlev,get_value=nlev

	widget_control,uvalue.checkbox_codes_settings,get_value=input_code
	widget_control,uvalue.fildsim_checkbox_settings,get_value=ascot_pitch

	IF ascot_pitch(0) EQ 1 THEN change_pitch_sign=1 ELSE change_pitch_sign=0
	IF ascot_pitch(1) EQ 1 THEN smooth_option=1 ELSE smooth_option=0

	IF ascot_pitch(2) EQ 1 THEN BEGIN
		efficiency={A:2,Z:1,B:b_field,efficiency_file:strtrim(af_input_path,1)+strtrim(af_efficiency_curve_file,1)}
		print,'Including scintillator efficiency in weight function (photons/ion)'
		print,'Setting A=2 and Z=1 by default for the scintillator efficiency energy calculation'
		print,'Will take efficiency from: ',efficiency.efficiency_file
	ENDIF ELSE BEGIN
		efficiency=0
		print,'Not using scintillator efficiency'
	ENDELSE

	IF ascot_pitch(4) EQ 1 THEN save_output_vars=1 ELSE save_output_vars=0

	IF smooth_option EQ 1 THEN smooth_input=sm ELSE smooth_input=0


	fildsim_settings.shot=shot
	fildsim_settings.time=time
	fildsim_settings.b_field=b_field
	fildsim_settings.r_fild=r_fild
	fildsim_settings.z_fild=z_fild
	fildsim_settings.input_code=input_code

	fildsim_settings.main_grid.mingyr=mingyr
	fildsim_settings.main_grid.maxgyr=maxgyr
	fildsim_settings.main_grid.dg=dg
	fildsim_settings.main_grid.minpitch=minpitch
	fildsim_settings.main_grid.maxpitch=maxpitch
	fildsim_settings.main_grid.dp=dp
	fildsim_settings.main_grid.nlev=nlev

	print,'***************************'
	print,'Starting FILDSIM simulation'
	print,'Check that A and Z are set correctly'
	print,'***************************'

	widget_control,/HOURGLASS

; Apply old Method
;	FILD_synthetic,output,DEFAULT_PATH=settings.fildsimf90_path,$
;		B_FILD=fildsim_settings.b_field,SIM_CODE=fildsim_settings.input_code,GRID_PARAM=fildsim_settings.main_grid,$
;		PHYSIC_CONSTANTS=PHYSIC_CONSTANTS,INPUT_FILENAME=fildsim_settings.input_filename,ASCOT_pitch=change_pitch_sign

	FILD_synthetic_matrix_method,output,DEFAULT_PATH=settings.fildsimf90_path,B_FILD=fildsim_settings.b_field,$
	SIM_CODE=fildsim_settings.input_code,$
	GRID_PARAM=fildsim_settings.main_grid,PHYSIC_CONSTANTS=PHYSIC_CONSTANTS,INPUT_FILENAME=fildsim_settings.input_filename,$
	ASCOT_pitch=change_pitch_sign,GAUSSIAN_MODEL=ascot_pitch(3),SMOOTH_INPUT=smooth_input,$
	SCINT_EFFICIENCY=efficiency,SAVE_OUTPUT=save_output_vars


	IF n_tags(output) LT 2 THEN BEGIN

		IF output EQ 'ERROR' THEN BEGIN

			print,'An error has ocurred during the execution of FILD_synthetic...Simulation aborted'
			return

		ENDIF

		return

	ENDIF

	fildsim_settings.strike_map_filename=output.strike_map_file
	fildsim_settings.strike_points_filename=output.strike_points_file

	fildsim_output_dist=output.output_dist
	fildsim_input_dist=output.input_dist
	fildsim_gyr=output.gyr
	fildsim_pitch=output.pitch

	widget_control,uvalue.fildsim_input_draw,get_value=draw_id
	wset,draw_id


	lev=findgen(nlev)/(nlev-1)*(max(output.input_dist,/NAN)-min(output.input_dist,/NAN))+min(output.input_dist,/NAN)
	col=findgen(nlev)/(nlev-1)*255

	contour,output.input_dist,output.pitch,output.gyr,levels=lev,c_colors=col,/fill,charsize=2.0,$
		yrange=[fildsim_settings.main_grid.mingyr,fildsim_settings.main_grid.maxgyr],ystyle=1,ytitle='Gyroradius (cm)',$
		xrange=[fildsim_settings.main_grid.minpitch,fildsim_settings.main_grid.maxpitch],xstyle=1,xtitle='Pitch Angle (deg)',$
		POSITION=[0.2,0.2,0.8,0.8],title='Weight/(cm*deg)'

		colorbar,max=max(lev),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert

	widget_control,uvalue.fildsim_output_draw,get_value=draw_id
	wset,draw_id

	lev=findgen(nlev)/(nlev)*(max(output.output_dist,/NAN)-min(output.output_dist,/NAN))+min(output.output_dist,/NAN)
	col=findgen(nlev)/(nlev)*255

	contour,output.output_dist,output.pitch,output.gyr,levels=lev,c_colors=col,/fill,charsize=2.0,$
		yrange=[fildsim_settings.main_grid.mingyr,fildsim_settings.main_grid.maxgyr],ystyle=1,ytitle='Gyroradius (cm)',$
		xrange=[fildsim_settings.main_grid.minpitch,fildsim_settings.main_grid.maxpitch],xstyle=1,xtitle='Pitch Angle (deg)',$
		POSITION=[0.2,0.2,0.8,0.8],title='Weight/(cm*deg)'


	colorbar,max=max(lev),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert


	print,'***************************'
	print,'FILDSIM simulation finished'
	print,'***************************'

	widget_control,uvalue.fildsim_slider,SENSITIVE=1,set_value=n_elements(lev),$
	set_slider_min=0,set_slider_max=n_elements(lev)


	END


;*********************************************************************************************

	UVALUE.SAVE_FILDSIM_EPS_BUTTON: BEGIN

	COMMON SETTINGS,settings
	COMMON FILDSIM_SETTINGS,fildsim_settings
	COMMON PHYSIC_CONSTANTS,physic_constants
	COMMON FILDSIM_results,fildsim_gyr,fildsim_pitch,fildsim_output_dist,fildsim_input_dist

	widget_control,uvalue.fildsim_grid_mingyr,get_value=mingyr
	widget_control,uvalue.fildsim_grid_maxgyr,get_value=maxgyr
	widget_control,uvalue.fildsim_grid_dg,get_value=dg
	widget_control,uvalue.fildsim_grid_minpitch,get_value=minpitch
	widget_control,uvalue.fildsim_grid_maxpitch,get_value=maxpitch
	widget_control,uvalue.fildsim_grid_dp,get_value=dp
	widget_control,uvalue.fildsim_grid_nlev,get_value=nlev

	set_plot,'ps'

	input_plot_filename=strtrim(settings.RESULTS_path,1)+strtrim(systime(/julian),1)+'_fildsim_input_distribution.ps'
	output_plot_filename=strtrim(settings.RESULTS_path,1)+strtrim(systime(/julian),1)+'_fildsim_output_distribution.ps'


	device,filename=input_plot_filename,/color


	lev=findgen(nlev)/(nlev-1)*(max(fildsim_input_dist)-min(fildsim_input_dist))+min(fildsim_input_dist)
	col=findgen(nlev)/(nlev-1)*255


	contour,fildsim_input_dist,fildsim_pitch,fildsim_gyr,levels=lev,c_colors=col,/fill,charsize=2.0,$
		yrange=[mingyr,maxgyr],ystyle=1,ytitle='Gyroradius (cm)',$
		xrange=[minpitch,maxpitch],xstyle=1,xtitle='Pitch Angle (deg)',$
		POSITION=[0.2,0.2,0.8,0.8],title='Weight/(cm*deg)'


	colorbar,max=max(fildsim_input_dist),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert


	device,/close



	device,filename=output_plot_filename,/color

	widget_control,uvalue.fildsim_slider,get_value=sat_level

	lev=findgen(nlev)/(nlev)*(max(fildsim_output_dist)-min(fildsim_output_dist))+min(fildsim_output_dist)
	col=findgen(nlev)/(nlev)*255

		col2=findgen(sat_level)/(sat_level-1)*255
		col=fltarr(nlev)+255
		col(0:sat_level-1)=col2

	contour,fildsim_output_dist,fildsim_pitch,fildsim_gyr,levels=lev,c_colors=col,/fill,charsize=2.0,$
		yrange=[mingyr,maxgyr],ystyle=1,ytitle='Gyroradius (cm)',$
		xrange=[minpitch,maxpitch],xstyle=1,xtitle='Pitch Angle (deg)',$
		POSITION=[0.2,0.2,0.8,0.8],title='Weight/(cm*deg)'


	colorbar,max=lev(sat_level-1),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert

	device,/close


	print,'FILDSIM plots saved as: ',strtrim(input_plot_filename,1),' ; ',strtrim(output_plot_filename,1)

	set_plot,'X'

	END


;***************************************************************************************************************************************************

	UVALUE.FILDSIM_SLIDER: BEGIN

	COMMON SETTINGS,settings
	COMMON FILDSIM_SETTINGS,fildsim_settings
	COMMON PHYSIC_CONSTANTS,physic_constants
	COMMON FILDSIM_results,fildsim_gyr,fildsim_pitch,fildsim_output_dist,fildsim_input_dist

	widget_control,uvalue.fildsim_slider,get_value=sat_level

	widget_control,uvalue.fildsim_output_draw,get_value=draw_id
	wset,draw_id

	nlev=fildsim_settings.main_grid.nlev
	lev=findgen(nlev)/(nlev)*(max(fildsim_output_dist)-min(fildsim_output_dist))+min(fildsim_output_dist)
	col=findgen(nlev)/(nlev)*255

		col2=findgen(sat_level)/(sat_level-1)*255
		col=fltarr(nlev)+255
		col(0:sat_level-1)=col2

	contour,fildsim_output_dist,fildsim_pitch,fildsim_gyr,levels=lev,c_colors=col,/fill,charsize=2.0,$
		yrange=[fildsim_settings.main_grid.mingyr,fildsim_settings.main_grid.maxgyr],ystyle=1,ytitle='Gyroradius (cm)',$
		xrange=[fildsim_settings.main_grid.minpitch,fildsim_settings.main_grid.maxpitch],xstyle=1,xtitle='Pitch Angle (deg)',$
		POSITION=[0.2,0.2,0.8,0.8]

	colorbar,max=lev(sat_level),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert



	END
;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_CALC_B: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files

	widget_control,uvalue.sm_inputs_shot,get_value=shot
	widget_control,uvalue.sm_inputs_time,get_value=time
	widget_control,uvalue.sm_inputs_r,get_value=r_fild
	widget_control,uvalue.sm_inputs_z,get_value=z_fild

	sm_settings.nm_r=r_fild
	sm_settings.nm_z=z_fild
	sm_settings.nm_shot=shot
	sm_settings.nm_time=time

	get_FILD_bfields,sm_settings.nm_r,sm_settings.nm_z,sm_settings.nm_shot,sm_settings.nm_time,$
		br,bz,bt,b_total

	sm_settings.nm_bt=bt
	sm_settings.nm_br=br
	sm_settings.nm_bz=bz

	widget_control,uvalue.sm_inputs_bt,set_value=bt
	widget_control,uvalue.sm_inputs_br,set_value=br
	widget_control,uvalue.sm_inputs_bz,set_value=bz


	END

;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_SELECT_SCINT_FILES: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON SETTINGS,settings

	filen = DIALOG_PICKFILE(/READ, FILTER = '*.pl',PATH=settings.FILDSIMf90_path,/MULTIPLE_FILES)

	ss=size(filen,/dimensions)
	ss=ss(0)

	sm_settings.nm_N_scintillator=ss
	nm_scintillator_files=filen

	widget_control,uvalue.sm_namelist_scintillator_files,set_value='Scintillator files: '+nm_scintillator_files

	END

;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_SELECT_SLIT_FILES: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON SETTINGS,settings

	filen = DIALOG_PICKFILE(/READ, FILTER = '*.pl',PATH=settings.FILDSIMf90_path,/MULTIPLE_FILES)
	ss=size(filen,/dimensions)
	ss=ss(0)

	sm_settings.nm_N_slits=ss
	nm_slit_files=filen

	widget_control,uvalue.sm_namelist_slit_files,set_value='Slit files: '+nm_slit_files

	END


;***************************************************************************************************************************************************

	UVALUE.SM_CALCULATOR_SELECT_GEOM_DIR: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON SETTINGS,settings

	dirn = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,/DIRECTORY)

	sm_settings.nm_geometry_dir=dirn

	widget_control,uvalue.sm_namelist_geom_dir,set_value='Geometry Directory: '+sm_settings.nm_geometry_dir

	END


;***************************************************************************************************************************************************

	UVALUE.SM_CALCULATOR_SELECT_RESULT_DIR: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	dirn = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,/DIRECTORY)

	sm_settings.nm_result_dir=dirn

	widget_control,uvalue.sm_namelist_result_dir,set_value='Result Directory: '+sm_settings.nm_result_dir

	END

;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_SAVE_STD: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON SETTINGS,settings

	;=============================================================
	; GET CURRENT VALUES TO SAVE STANDARD SET
	;=============================================================
	widget_control,uvalue.sm_namelist_runID,get_value=runID
	widget_control,uvalue.sm_namelist_backtrace,get_value=backtrace
	widget_control,uvalue.sm_namelist_save_orbits,get_value=save_orbits
	widget_control,uvalue.sm_namelist_N_ions,get_value=N_ions
	widget_control,uvalue.sm_namelist_step,get_value=step
	widget_control,uvalue.sm_namelist_length,get_value=length
	widget_control,uvalue.sm_namelist_gyroradius,get_value=gyroradius
	widget_control,uvalue.sm_namelist_pitch,get_value=pitch
	widget_control,uvalue.sm_namelist_gyrophase,get_value=gyrophase
	widget_control,uvalue.sm_namelist_start_x,get_value=start_x
	widget_control,uvalue.sm_namelist_start_y,get_value=start_y
	widget_control,uvalue.sm_namelist_start_z,get_value=start_z
	widget_control,uvalue.sm_namelist_alpha,get_value=alpha
	widget_control,uvalue.sm_namelist_beta,get_value=beta

	dumm=strsplit(gyroradius,',',/EXTRACT)
	gyroradius=float(dumm)

	dumm=strsplit(pitch,',',/EXTRACT)
	pitch=float(dumm)

	dumm=strsplit(gyrophase,',',/EXTRACT)
	gyrophase=float(dumm)

	dumm=strsplit(start_x,',',/EXTRACT)
	start_x=float(dumm)

	dumm=strsplit(start_y,',',/EXTRACT)
	start_y=float(dumm)

	dumm=strsplit(start_z,',',/EXTRACT)
	start_z=float(dumm)

	sm_settings.nm_runID=runID
	sm_settings.nm_backtrace=backtrace
	sm_settings.nm_save_orbits=save_orbits
	sm_settings.nm_N_ions=N_ions
	sm_settings.nm_step=step
	sm_settings.nm_length=length
	nm_gyroradius=gyroradius
	nm_pitch=pitch
	sm_settings.nm_gyrophase=gyrophase
	sm_settings.nm_start_x=start_x
	sm_settings.nm_start_y=start_y
	sm_settings.nm_start_z=start_z

	sm_settings.nm_alpha=alpha
	sm_settings.nm_beta=beta


	s=size(nm_slit_files,/dimensions)
	sm_settings.nm_N_slits=s(0)

	s=size(nm_scintillator_files,/dimensions)
	sm_settings.nm_N_scintillator=s(0)

	s=size(nm_gyroradius,/dimensions)
	sm_settings.nm_N_gyroradius=s(0)

	s=size(nm_pitch,/dimensions)
	sm_settings.nm_N_pitch=s(0)

	;====================================================

	filen = DIALOG_PICKFILE(/WRITE,PATH=settings.FILDSIMf90_path,FILE='fildsim_std.idl')

	FILDSIM_save_standard_namelist,filen,sm_settings.nm_result_dir,sm_settings.nm_geometry_dir,$
		sm_settings.nm_backtrace,sm_settings.nm_save_orbits,$
		sm_settings.nm_n_gyroradius,sm_settings.nm_n_pitch,sm_settings.nm_n_ions,$
		sm_settings.nm_step,sm_settings.nm_length,sm_settings.nm_gyrophase,$
		sm_settings.nm_start_x,sm_settings.nm_start_y,sm_settings.nm_start_z,$
		sm_settings.nm_alpha,sm_settings.nm_beta,$
		nm_gyroradius,nm_pitch,$
		sm_settings.nm_n_scintillator,sm_settings.nm_n_slits,$
		nm_scintillator_files,nm_slit_files

	END

;***************************************************************************************************************************************************

	UVALUE.SM_CALCULATOR_LOAD_STD: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON SETTINGS,settings

	filen = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*.idl')
	restore,filen

	sm_settings.nm_result_dir=setup.result_dir
	sm_settings.nm_geometry_dir=setup.geometry_dir
	sm_settings.nm_N_gyroradius=setup.N_gyroradius
	sm_settings.nm_N_pitch=setup.N_pitch
	sm_settings.nm_N_ions=setup.N_ions
	sm_settings.nm_step=setup.step
	sm_settings.nm_length=setup.length
	sm_settings.nm_gyrophase=setup.gyrophase
	sm_settings.nm_start_x=setup.start_x
	sm_settings.nm_start_y=setup.start_y
	sm_settings.nm_start_z=setup.start_z
	sm_settings.nm_alpha=setup.alpha
	sm_settings.nm_beta=setup.beta
	sm_settings.nm_N_scintillator=setup.N_scintillator
	sm_settings.nm_N_slits=setup.N_slits
	nm_gyroradius=setup.gyroradius
	nm_pitch=setup.pitch
	nm_scintillator_files=setup.scintillator_files
	nm_slit_files=setup.slit_files

	s=size(nm_gyroradius,/dimensions)
	gyroradius_str=''
	FOR kk=0,s(0)-1 DO BEGIN
		gyroradius_str=gyroradius_str+string(nm_gyroradius(kk))+', '
	ENDFOR

	s=size(nm_pitch,/dimensions)
	pitch_str=''
	FOR kk=0,s(0)-1 DO BEGIN
		pitch_str=pitch_str+string(nm_pitch(kk))+', '
	ENDFOR

	s=size(sm_settings.nm_gyrophase,/dimensions)
	gyrophase_str=''
	FOR kk=0,s(0)-1 DO BEGIN
		gyrophase_str=gyrophase_str+string(sm_settings.nm_gyrophase(kk))+', '
	ENDFOR

	s=size(sm_settings.nm_start_x,/dimensions)
	start_x_str=''
	FOR kk=0,s(0)-1 DO BEGIN
		start_x_str=start_x_str+string(sm_settings.nm_start_x(kk))+', '
	ENDFOR


	s=size(sm_settings.nm_start_y,/dimensions)
	start_y_str=''
	FOR kk=0,s(0)-1 DO BEGIN
		start_y_str=start_y_str+string(sm_settings.nm_start_y(kk))+', '
	ENDFOR

	s=size(sm_settings.nm_start_z,/dimensions)
	start_z_str=''
	FOR kk=0,s(0)-1 DO BEGIN
		start_z_str=start_z_str+string(sm_settings.nm_start_z(kk))+', '
	ENDFOR


	widget_control,uvalue.sm_namelist_runID,set_value=sm_settings.nm_runID
	widget_control,uvalue.sm_namelist_N_ions,set_value=sm_settings.nm_N_ions
	widget_control,uvalue.sm_namelist_step,set_value=sm_settings.nm_step
	widget_control,uvalue.sm_namelist_length,set_value=sm_settings.nm_length
	widget_control,uvalue.sm_namelist_gyroradius,set_value=gyroradius_str
	widget_control,uvalue.sm_namelist_pitch,set_value=pitch_str
	widget_control,uvalue.sm_namelist_gyrophase,set_value=gyrophase_str
	widget_control,uvalue.sm_namelist_start_x,set_value=start_x_str
	widget_control,uvalue.sm_namelist_start_y,set_value=start_y_str
	widget_control,uvalue.sm_namelist_start_z,set_value=start_z_str
	widget_control,uvalue.sm_namelist_alpha,set_value=sm_settings.nm_alpha
	widget_control,uvalue.sm_namelist_beta,set_value=sm_settings.nm_beta

	widget_control,uvalue.sm_namelist_scintillator_files,set_value='Scintillator files: '+nm_scintillator_files
	widget_control,uvalue.sm_namelist_slit_files,set_value='Slit files: '+nm_slit_files
	widget_control,uvalue.sm_namelist_geom_dir,set_value='Geometry Directory: '+sm_settings.nm_geometry_dir
	widget_control,uvalue.sm_namelist_result_dir,set_value='Result Directory: '+sm_settings.nm_result_dir


	END

;***************************************************************************************************************************************************

	UVALUE.SM_CALCULATOR_WRITE_NAMELIST: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON SETTINGS,settings

	; READ THE VALUES FOR THE NAMELIST


	widget_control,uvalue.sm_namelist_runID,get_value=runID
	widget_control,uvalue.sm_namelist_backtrace,get_value=backtrace
	widget_control,uvalue.sm_namelist_save_orbits,get_value=save_orbits
	widget_control,uvalue.sm_namelist_N_ions,get_value=N_ions
	widget_control,uvalue.sm_namelist_step,get_value=step
	widget_control,uvalue.sm_namelist_length,get_value=length
	widget_control,uvalue.sm_namelist_gyroradius,get_value=gyroradius
	widget_control,uvalue.sm_namelist_pitch,get_value=pitch
	widget_control,uvalue.sm_namelist_gyrophase,get_value=gyrophase
	widget_control,uvalue.sm_namelist_start_x,get_value=start_x
	widget_control,uvalue.sm_namelist_start_y,get_value=start_y
	widget_control,uvalue.sm_namelist_start_z,get_value=start_z
	widget_control,uvalue.sm_namelist_alpha,get_value=alpha
	widget_control,uvalue.sm_namelist_beta,get_value=beta


	dumm=strsplit(gyroradius,',',/EXTRACT)
	gyroradius=float(dumm)

	dumm=strsplit(pitch,',',/EXTRACT)
	pitch=float(dumm)

	dumm=strsplit(gyrophase,',',/EXTRACT)
	gyrophase=float(dumm)

	dumm=strsplit(start_x,',',/EXTRACT)
	start_x=float(dumm)

	dumm=strsplit(start_y,',',/EXTRACT)
	start_y=float(dumm)

	dumm=strsplit(start_z,',',/EXTRACT)
	start_z=float(dumm)



	sm_settings.nm_runID=runID
	sm_settings.nm_backtrace=backtrace
	sm_settings.nm_save_orbits=save_orbits
	sm_settings.nm_N_ions=N_ions
	sm_settings.nm_step=step
	sm_settings.nm_length=length
	nm_gyroradius=gyroradius
	nm_pitch=pitch
	sm_settings.nm_gyrophase=gyrophase
	sm_settings.nm_start_x=start_x
	sm_settings.nm_start_y=start_y
	sm_settings.nm_start_z=start_z

	sm_settings.nm_alpha=alpha
	sm_settings.nm_beta=beta


	s=size(nm_slit_files,/dimensions)
	sm_settings.nm_N_slits=s(0)

	s=size(nm_scintillator_files,/dimensions)
	sm_settings.nm_N_scintillator=s(0)

	s=size(nm_gyroradius,/dimensions)
	sm_settings.nm_N_gyroradius=s(0)

	s=size(nm_pitch,/dimensions)
	sm_settings.nm_N_pitch=s(0)

	nm_scintillator_files_tmp=strarr(n_elements(nm_scintillator_files))
	nm_slit_files_tmp=strarr(n_elements(nm_slit_files))

	FOR kk=0,sm_settings.nm_N_scintillator-1 DO BEGIN
		dumm=strsplit(nm_scintillator_files(kk),sm_settings.nm_geometry_dir,/extract,/regex)
		nm_scintillator_files_tmp(kk)=dumm
	ENDFOR

	print,nm_scintillator_files_tmp,nm_scintillator_files

	FOR kk=0,sm_settings.nm_N_slits-1 DO BEGIN
		dumm=strsplit(nm_slit_files(kk),sm_settings.nm_geometry_dir,/extract,/regex)
		nm_slit_files_tmp(kk)=dumm
	ENDFOR


	; ALPHA AND BETA ARE THE ORIENTATION FOR FILD. WE NEED THETA PHI FOR THE NAMELIST.
	; COMPUTE THEM WITH calculate_fild_orientation.pro

	widget_control,uvalue.sm_inputs_bt,get_value=bt
	widget_control,uvalue.sm_inputs_br,get_value=br
	widget_control,uvalue.sm_inputs_bz,get_value=bz


	FILDSIM_calculate_fild_orientation,bt,br,bz,alpha,beta,theta,phi

	sm_settings.nm_theta=theta
	sm_settings.nm_phi=phi

	; SET THE NAME OF THE NAMELIST FOR FILDSIM.f90

	filen = DIALOG_PICKFILE(/WRITE,PATH=settings.FILDSIMf90_path,FILE=string(runID)+'.cfg')

	sm_settings.nm_namelist_filename=filen

	; WRITE THE NAMELIST FILE
	; Print all the elements that will be written in the NAMELIST

	FILDSIM_write_namelist,sm_settings.nm_runID,sm_settings.nm_result_dir,sm_settings.nm_backtrace,sm_settings.nm_N_gyroradius,sm_settings.nm_N_pitch,$
		sm_settings.nm_save_orbits,sm_settings.nm_N_ions,sm_settings.nm_step,sm_settings.nm_length,nm_gyroradius,nm_pitch,sm_settings.nm_gyrophase,$
		sm_settings.nm_start_x,sm_settings.nm_start_y,sm_settings.nm_start_z,sm_settings.nm_theta,sm_settings.nm_phi,$
		sm_settings.nm_geometry_dir,sm_settings.nm_N_scintillator,sm_settings.nm_N_slits,$
		nm_scintillator_files_tmp,nm_slit_files_tmp,FILENAME=sm_settings.nm_namelist_filename

	END


;***************************************************************************************************************************************************

	UVALUE.SM_RUN_SIMULATION: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON SETTINGS,settings

	filen = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*.cfg')

	sm_settings.nm_namelist_filename=filen

	print,'*******************************************'
	print,'Starting fildsim.f90 simulation locally' 
	print,'*******************************************'

	widget_control,/HOURGLASS
	;print,'ssh '+strtrim(sm_settings.machine,1)+' '+strtrim(sm_settings.fildsimf90_executable,1)+' '+sm_settings.nm_namelist_filename
	;spawn,'ssh '+strtrim(sm_settings.machine,1)+' '+strtrim(sm_settings.fildsimf90_executable,1)+' '+sm_settings.nm_namelist_filename

    print,strtrim(sm_settings.fildsimf90_executable,1)+' '+sm_settings.nm_namelist_filename
    spawn,strtrim(sm_settings.fildsimf90_executable,1)+' '+sm_settings.nm_namelist_filename

	print,'Simulation performed'
	print,'*******************************************'

	;*******************
	; PLOT STRIKE MAP
	;*******************

	strike_map_filename =sm_settings.nm_result_dir+sm_settings.nm_runID+'_strike_map.dat'

	; READ SCINTILLATOR PLATE
	FILDSIM_read_plate,plate,filename=nm_scintillator_files

	widget_control,/HOURGLASS

	; READ STRIKE MAP
	FILDSIM_read_strike_map,strike_map,filename=strike_map_filename

	widget_control,uvalue.sm_draw_strike_map,get_value=draw_id
	wset,draw_id

	plot,plate.y,plate.z,psym=-3,color=0,background=255,/iso,xtitle='Y (cm)',ytitle='Z (cm)',charsize=2.0,$
	xrange=[min(strike_map.yy)-1,max(strike_map.yy)+1],yrange=[min(strike_map.zz)-1,max(strike_map.zz)+1]
	oplot,strike_map.yy,strike_map.zz,psym=1,color=0

	; DRAW GYRORADIUS LINES

	u_pitch=strike_map.pitch_angle[UNIQ(strike_map.pitch_angle, SORT(strike_map.pitch_angle))]

	FOR kk=0,n_elements(u_pitch)-1 DO BEGIN

		w=where(strike_map.pitch_angle EQ u_pitch(kk),count)
		oplot,strike_map.yy(w),strike_map.zz(w),psym=-3,color=0

	ENDFOR

	; DRAW PITCH LINES

	u_gyr=strike_map.gyroradius[UNIQ(strike_map.gyroradius, SORT(strike_map.gyroradius))]

	FOR kk=0,n_elements(u_gyr)-1 DO BEGIN

		w=where(strike_map.gyroradius EQ u_gyr(kk),count)
		oplot,strike_map.yy(w),strike_map.zz(w),psym=-3,color=0

	ENDFOR


	END


;***************************************************************************************************************************************************



	UVALUE.SM_CALCULATOR_PLOT_STRIKE_MAP: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings



	; READ SCINTILLATOR PLATE
	FILDSIM_read_plate,plate,filename=nm_scintillator_files

	; READ STRIKE MAP

	strike_map_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_map.dat')

	widget_control,/HOURGLASS

	IF (strike_map_filename NE sm_settings.nm_strike_map_filename) THEN BEGIN

		FILDSIM_read_strike_map,strike_map,filename=strike_map_filename
		sm_settings.nm_strike_map_filename=strike_map_filename
		nm_strike_map=strike_map

	ENDIF


	widget_control,uvalue.sm_draw_strike_map,get_value=draw_id
	wset,draw_id

	plot,plate.y,plate.z,psym=-3,color=0,background=255,/iso,xtitle='Y (cm)',ytitle='Z (cm)',charsize=2.0,$
	xrange=[min(nm_strike_map.yy,/NAN)-1,max(nm_strike_map.yy,/NAN)+1],yrange=[min(nm_strike_map.zz,/NAN)-1,max(nm_strike_map.zz,/NAN)+1]

	oplot,nm_strike_map.yy,nm_strike_map.zz,psym=3,color=0

	; DRAW GYRORADIUS LINES

	u_pitch=nm_strike_map.pitch_angle[UNIQ(nm_strike_map.pitch_angle, SORT(nm_strike_map.pitch_angle))]

	FOR kk=0,n_elements(u_pitch)-1 DO BEGIN

		w=where(nm_strike_map.pitch_angle EQ u_pitch(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=0

	ENDFOR

	; DRAW PITCH LINES

	u_gyr=nm_strike_map.gyroradius[UNIQ(nm_strike_map.gyroradius, SORT(nm_strike_map.gyroradius))]

	FOR kk=0,n_elements(u_gyr)-1 DO BEGIN

		w=where(nm_strike_map.gyroradius EQ u_gyr(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=0

	ENDFOR


	print,'Strike Map plotted'

	END


;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_PLOT_GYRORADIUS_RESOLUTION: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings


	strike_points_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_points.dat')

	widget_control,uvalue.sm_pitch_resolution,get_value=pitch_res
	widget_control,uvalue.sm_gyro_resolution,get_value=gir_res


	; READ STRIKE POINTS
	widget_control,/HOURGLASS

	IF (strike_points_filename NE sm_settings.nm_strike_points_filename) THEN BEGIN

		FILDSIM_read_strike_points,strike_points,filename=strike_points_filename
		sm_settings.nm_strike_points_filename=strike_points_filename
		nm_strike_points=strike_points
		print,'Strike Points Filename read: ',strike_points_filename

	ENDIF

	widget_control,uvalue.sm_binsize_resolution,get_value=bin_size
	widget_control,uvalue.sm_draw_resolutions,get_value=draw_id
	wset,draw_id

	; FOR EACH PAIR PITCH-GYRO CALCULATE THE RESOLUTION

	w=where(nm_strike_points.pitch_angle EQ pitch_res,count)

	IF count EQ 0 THEN BEGIN
		print,'There are no strike points for this pitch angle... Returning'
		return
	ENDIF


	w=where(nm_strike_points.pitch_angle EQ pitch_res AND nm_strike_points.gyroradius EQ gir_res,count)
	print,'Gyr res / Pitch_res',gir_res,pitch_res
	print,'Gyr count',count
	remap_gyro=nm_strike_points.remap_gyro(w)


	;calculate_gyroradius_resolution,remap_gyro,o,BINSIZE=bin_size
	FILDSIM_calculate_gyroradius_resolution_v2,remap_gyro,o,BINSIZE=bin_size,PLOT=1


	;IF size(o,/TYPE) EQ 8 THEN BEGIN

	;	xx=findgen(1000)/999*20

	;	plot,o.x_hist,o.h,color=0,psym=-1,background=255,xtitle='Gyroradius (cm)',ytitle='N strikes',charsize=2.0,$
	;	xrange=[gir_res-3,gir_res+3]


	;	oplot,xx,o.coeff(0)*exp(-((xx-o.coeff(1))/o.coeff(2))^2/2),color=50,psym=-3

	;ENDIF

	END

;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_PLOT_GYRORADIUS_ALL: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings


	strike_points_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_points.dat')

	widget_control,uvalue.sm_pitch_resolution,get_value=pitch_res
	widget_control,uvalue.sm_gyro_resolution,get_value=gir_res


	; READ STRIKE POINTS
	widget_control,/HOURGLASS

	IF (strike_points_filename NE sm_settings.nm_strike_points_filename) THEN BEGIN

		FILDSIM_read_strike_points,strike_points,filename=strike_points_filename
		sm_settings.nm_strike_points_filename=strike_points_filename
		nm_strike_points=strike_points
		print,'Strike Points Filename read: ',strike_points_filename

	ENDIF

	widget_control,uvalue.sm_binsize_resolution,get_value=bin_size
	widget_control,uvalue.sm_draw_resolutions,get_value=draw_id
	wset,draw_id

	; FOR EACH PAIR PITCH-GYRO CALCULATE THE RESOLUTION

	w=where(nm_strike_points.pitch_angle EQ pitch_res,count)

	IF count EQ 0 THEN BEGIN
		print,'There are no strike points for this pitch angle... Returning'
		return
	ENDIF

	remap_gyro=nm_strike_points.remap_gyro(w)
	gg=nm_strike_points.gyroradius(w)
	unique_gg=gg[UNIQ(gg, SORT(gg))]
	ss=size(unique_gg,/dimensions)
	nn=n_elements(gg)

	; FIRST LOOP TO GET MAXIMUM VALUE OF HISTROGRAM

	maxh=0

	FOR kk=0,ss(0)-1 DO BEGIN

		ww=where(gg EQ unique_gg(kk),count)

		FILDSIM_calculate_gyroradius_resolution,remap_gyro(ww),o,BINSIZE=bin_size

		IF (max(o.h) GT maxh) THEN maxh=max(o.h)

	ENDFOR


	plot_counter=0

	save_gyr_resol=fltarr(ss(0),2,1000)		; variable to save output for mauri

	FOR kk=0,ss(0)-1 DO BEGIN


		ww=where(gg EQ unique_gg(kk),count)

		FILDSIM_calculate_gyroradius_resolution,remap_gyro(ww),o,BINSIZE=bin_size

		IF size(o,/TYPE) EQ 8 THEN BEGIN
			IF plot_counter EQ 0 THEN BEGIN
				xx=findgen(1000)/999*20

				plot,o.x_hist,o.h,color=0,psym=-1,background=255,xtitle='Gyroradius (cm)',ytitle='N strikes',charsize=2.0,$
				xrange=[1.,20.],yrange=[0,o.coeff(0)*2.5],/nodata,$
				title='Pitch Angle: '+string(pitch_res)


				oplot,xx,o.coeff(0)*exp(-((xx-o.coeff(1))/o.coeff(2))^2/2),color=50,psym=-3

				plot_counter=1
			ENDIF ELSE BEGIN
				;oplot,o.x_hist,o.h,color=250./ss(0)*kk,psym=-1
				xx=findgen(1000)/999*20
				oplot,xx,o.coeff(0)*exp(-((xx-o.coeff(1))/o.coeff(2))^2/2),color=250./ss(0)*kk,psym=-3
			ENDELSE

			save_gyr_resol(kk,0,*)=xx
			save_gyr_resol(kk,1,*)=o.coeff(0)*exp(-((xx-o.coeff(1))/o.coeff(2))^2/2)


		ENDIF

	ENDFOR

;	save,filename='Test_Mauri_save_gyr_res.sav',save_gyr_resol


	END

;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_PLOT_PITCH_RESOLUTION: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings


	strike_points_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_points.dat')

	widget_control,uvalue.sm_pitch_resolution,get_value=pitch_res
	widget_control,uvalue.sm_gyro_resolution,get_value=gir_res


	; READ STRIKE POINTS
	widget_control,/HOURGLASS

	IF (strike_points_filename NE sm_settings.nm_strike_points_filename) THEN BEGIN

		FILDSIM_read_strike_points,strike_points,filename=strike_points_filename
		sm_settings.nm_strike_points_filename=strike_points_filename
		nm_strike_points=strike_points
		print,'Strike Points Filename read: ',strike_points_filename

	ENDIF

	widget_control,uvalue.sm_binsize_resolution,get_value=bin_size
	widget_control,uvalue.sm_draw_resolutions,get_value=draw_id
	wset,draw_id

	; FOR EACH PAIR PITCH-GYRO CALCULATE THE RESOLUTION

	w=where(nm_strike_points.gyroradius EQ gir_res,count)

	IF count EQ 0 THEN BEGIN
		print,'There are no strike points for this pitch angle... Returning'
		return
	ENDIF


	w=where(nm_strike_points.pitch_angle EQ pitch_res AND nm_strike_points.gyroradius EQ gir_res,count)
	print,'Gir res / Pitch_res',gir_res,pitch_res
	print,'Pitch count',count
	remap_pitch=nm_strike_points.remap_pitch(w)
	FILDSIM_calculate_pitch_resolution_v2,remap_pitch,o,BINSIZE=bin_size,PLOT=1

	END

;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_PLOT_PITCH_ALL: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings


	strike_points_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_points.dat')

	widget_control,uvalue.sm_pitch_resolution,get_value=pitch_res
	widget_control,uvalue.sm_gyro_resolution,get_value=gir_res


	; READ STRIKE POINTS
	widget_control,/HOURGLASS

	IF (strike_points_filename NE sm_settings.nm_strike_points_filename) THEN BEGIN

		FILDSIM_read_strike_points,strike_points,filename=strike_points_filename
		sm_settings.nm_strike_points_filename=strike_points_filename
		nm_strike_points=strike_points
		print,'Strike Points Filename read: ',strike_points_filename

	ENDIF

	widget_control,uvalue.sm_binsize_resolution,get_value=bin_size
	widget_control,uvalue.sm_draw_resolutions,get_value=draw_id
	wset,draw_id

	; FOR EACH PAIR PITCH-GYRO CALCULATE THE RESOLUTION

	w=where(nm_strike_points.gyroradius EQ gir_res,count)

	IF count EQ 0 THEN BEGIN
		print,'There are no strike points for this pitch angle-gyroradius... Returning'
		return
	ENDIF

	remap_pitch=nm_strike_points.remap_pitch(w)
	gg=nm_strike_points.pitch_angle(w)
	unique_gg=gg[UNIQ(gg, SORT(gg))]
	ss=size(unique_gg,/dimensions)
	nn=n_elements(gg)

	plot_counter=0

	; FIRST LOOP TO GET MAXIMUM VALUE OF HISTROGRAM

	maxh=0

	FOR kk=0,ss(0)-1 DO BEGIN

		ww=where(gg EQ unique_gg(kk),count)

		FILDSIM_calculate_pitch_resolution,remap_pitch(ww),o,BINSIZE=bin_size

		IF (max(o.h) GT maxh) THEN maxh=max(o.h)

	ENDFOR


	FOR kk=0,ss(0)-1 DO BEGIN

		ww=where(gg EQ unique_gg(kk),count)

		FILDSIM_calculate_pitch_resolution,remap_pitch(ww),o,BINSIZE=bin_size

		IF size(o,/TYPE) EQ 8 THEN BEGIN
			IF plot_counter EQ 0 THEN BEGIN
				xx=findgen(1000)/999*100

				plot,o.x_hist,o.h,color=0,psym=-1,background=255,xtitle='Pitch Angle (deg)',ytitle='N strikes',charsize=2.0,$
				xrange=[0.,100.],yrange=[0,o.coeff(0)],/nodata,$
				title='Gyroradius: '+string(gir_res)

				oplot,xx,o.coeff(0)*exp(-((xx-o.coeff(1))/o.coeff(2))^2/2),color=50,psym=-3

				plot_counter=1
			ENDIF ELSE BEGIN
				;oplot,o.x_hist,o.h,color=250./ss(0)*kk,psym=-1
				xx=findgen(1000)/999*100
				oplot,xx,o.coeff(0)*exp(-((xx-o.coeff(1))/o.coeff(2))^2/2),color=250./ss(0)*kk,psym=-3
			ENDELSE

			save_pitch_resol(kk,0,*)=xx
			save_pitch_resol(kk,1,*)=o.coeff(0)*exp(-((xx-o.coeff(1))/o.coeff(2))^2/2)

		ENDIF

	ENDFOR


	END


;***************************************************************************************************************************************************


	UVALUE.SELECT_FILDSIM_GRID: BEGIN

	COMMON FILD_GRID_INFO,fild_grid_info
	COMMON SETTINGS,settings

		filename= DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_map.dat')
		fild_grid_info.strike_map_filename=filename

	END


;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_PLOT_STRIKE_POINTS: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings


	strike_points_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_points.dat')

	widget_control,uvalue.sm_pitch_resolution,get_value=pitch_res
	widget_control,uvalue.sm_gyro_resolution,get_value=gir_res

	; READ SCINTILLATOR PLATE
	FILDSIM_read_plate,plate,filename=nm_scintillator_files

	; READ STRIKE MAP
	strike_map=nm_strike_map

	; READ STRIKE POINTS
	widget_control,/HOURGLASS

	IF (strike_points_filename NE sm_settings.nm_strike_points_filename) THEN BEGIN

		FILDSIM_read_strike_points,strike_points,filename=strike_points_filename
		sm_settings.nm_strike_points_filename=strike_points_filename
		nm_strike_points=strike_points
		print,'Strike Points Filename read: ',strike_points_filename

	ENDIF

	widget_control,uvalue.sm_draw_strike_map,get_value=draw_id
	wset,draw_id

	w=where(nm_strike_points.pitch_angle EQ pitch_res AND nm_strike_points.gyroradius EQ gir_res,count)

	IF count EQ 0 THEN BEGIN
		print,'There are no strike points for this pitch angle-gyroradius... Returning'
		return
	ENDIF

	yy=nm_strike_points.yy(w)
	zz=nm_strike_points.zz(w)



	; PLOT STRIKE MAP

	plot,plate.y,plate.z,psym=-3,color=0,background=255,xtitle='Y (cm)',ytitle='Z (cm)',charsize=2.0,$
	xrange=[min(yy,/NAN),max(yy,/NAN)],yrange=[min(zz,/NAN),max(zz,/NAN)]

	oplot,nm_strike_map.yy,nm_strike_map.zz,psym=3,color=0

	; DRAW GYRORADIUS LINES

	u_pitch=nm_strike_map.pitch_angle[UNIQ(nm_strike_map.pitch_angle, SORT(nm_strike_map.pitch_angle))]

	FOR kk=0,n_elements(u_pitch)-1 DO BEGIN

		w=where(nm_strike_map.pitch_angle EQ u_pitch(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=0

	ENDFOR

	; DRAW PITCH LINES

	u_gyr=nm_strike_map.gyroradius[UNIQ(nm_strike_map.gyroradius, SORT(nm_strike_map.gyroradius))]

	FOR kk=0,n_elements(u_gyr)-1 DO BEGIN

		w=where(nm_strike_map.gyroradius EQ u_gyr(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=0

	ENDFOR

	oplot,yy,zz,psym=3,color=50

	END

;***************************************************************************************************************************************************


	UVALUE.SM_CALCULATOR_CONTOUR_COLFAC: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings


	strike_map_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_map.dat')

	; READ STRIKE POINTS
	widget_control,/HOURGLASS

	IF (strike_map_filename NE sm_settings.nm_strike_map_filename) THEN BEGIN

		FILDSIM_read_strike_map,strike_map,filename=strike_map_filename
		sm_settings.nm_strike_map_filename=strike_map_filename
		nm_strike_map=strike_map

	ENDIF

	FILDSIM_read_plate,plate,filename=nm_scintillator_files

	widget_control,uvalue.sm_draw_strike_map,get_value=draw_id
	wset,draw_id

	yy=nm_strike_map.yy
	zz=nm_strike_map.zz
	colfac=nm_strike_map.collimator_factor

	; SELECT points where colfac NE 0

	w=where(colfac GT 0.)

	yy=yy(w)
	zz=zz(w)
	colfac=colfac(w)



	ysize=100
	zsize=100

	TRIANGULATE, yy, zz, tr,b

	yy_int=findgen(ysize)/(ysize-1)*abs(max(plate.y)-min(plate.y))+min(plate.y)
	zz_int=findgen(zsize)/(zsize-1)*abs(max(plate.z)-min(plate.z))+min(plate.z)


	colfac_int=trigrid(yy,zz,colfac,tr,[1,1],$
		[min(plate.y),min(plate.z),max(plate.y),max(plate.z)],nx=ysize,ny=zsize,/quintic,xout=yy_int,yout=zz_int)

	help,colfac_int
	print,'MAX COLFAC:',max(colfac_int)

	plot,plate.y,plate.z,psym=-3,color=0,background=255,/iso,xtitle='Y (cm)',ytitle='Z (cm)',charsize=2.0,$
	xrange=[min(nm_strike_map.yy,/NAN)-1,max(nm_strike_map.yy,/NAN)+1],yrange=[min(nm_strike_map.zz,/NAN)-1,max(nm_strike_map.zz,/NAN)+1],$
	position=[0.2, 0.20, 0.80, 0.9],title='Collimator Factor',/nodata

	nlev=250
	lev=findgen(nlev)/(nlev-1)*(max(colfac_int)-min(colfac_int))+min(colfac_int)


	contour,colfac_int,yy_int,zz_int,levels=lev,/fill,background=255,/overplot



	; PLOT STRIKE MAP


	; DRAW GYRORADIUS LINES

	u_pitch=nm_strike_map.pitch_angle[UNIQ(nm_strike_map.pitch_angle, SORT(nm_strike_map.pitch_angle))]

	FOR kk=0,n_elements(u_pitch)-1 DO BEGIN

		w=where(nm_strike_map.pitch_angle EQ u_pitch(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=255

	ENDFOR

	; DRAW PITCH LINES

	u_gyr=nm_strike_map.gyroradius[UNIQ(nm_strike_map.gyroradius, SORT(nm_strike_map.gyroradius))]

	FOR kk=0,n_elements(u_gyr)-1 DO BEGIN

		w=where(nm_strike_map.gyroradius EQ u_gyr(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=255

	ENDFOR

	oplot,plate.y,plate.z,psym=-3,color=100

	colorbar,max=max(colfac_int),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(f8.3)',/vert,color=0

	END



;***************************************************************************************************************************************************

	UVALUE.SM_CALCULATOR_CALC_ALL_RES: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings

	widget_control,uvalue.sm_binsize_resolution,get_value=bin_size

	strike_points_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_points.dat')

	IF (strike_points_filename NE sm_settings.nm_strike_points_filename) THEN BEGIN

		sm_settings.nm_strike_points_filename=strike_points_filename
		nm_strike_points=strike_points
	ENDIF

	widget_control,/HOURGLASS

	nm_resolutions=FILDSIM_calculate_all_resolutions(strike_points_filename,BINSIZE=bin_size)


	print,'Gyro / Pitch / sigma gyr / sigma pitch / Centroid gyr / Centroid pitch / Amplitude gyr / Amplitude pitch'
	print,nm_resolutions

	END

;***************************************************************************************************************************************************


UVALUE.SAVE_IDL_STRIKE_MAP_RESOLUTIONS: BEGIN


	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions

	ss=size(nm_resolutions,/dimensions)

	IF ss(0) EQ 8 THEN BEGIN

		fn=strtrim(sm_settings.nm_strike_points_filename,1)+'_resolutions.sav'

		resolution_info={Gyroradius:reform(nm_resolutions(0,*)),Pitch_angle:reform(nm_resolutions(1,*)),$
			Sigma_gyroradius:reform(nm_resolutions(2,*)),Sigma_pitch_angle:reform(nm_resolutions(3,*)),$
			Centroid_gyroradius:reform(nm_resolutions(4,*)),Centroid_pitch_angle:reform(nm_resolutions(5,*)),$
			Amplitude_gyroradius:reform(nm_resolutions(6,*)),Amplitude_pitch_angle:reform(nm_resolutions(7,*)),$
			filename:sm_settings.nm_strike_points_filename}

		save,resolution_info,filename=fn

		print,'Resolution information saved in: '+strtrim(fn,1)

	ENDIF ELSE BEGIN

		print,'No resolutions have been calculated yet... Use CALC ALL RES button ...'

	ENDELSE


END

;***************************************************************************************************************************************************

UVALUE.SM_CALCULATOR_CONTOUR_SIGMA_GYR: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings


	IF size(nm_resolutions,/N_DIMENSIONS) NE 2 THEN BEGIN
		help,nm_resolutions
		print,'Resolutions are not yet calculated... Returning'
		return

	ENDIF


	strike_map_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_map.dat')

	; READ STRIKE POINTS
	widget_control,/HOURGLASS

	IF (strike_map_filename NE sm_settings.nm_strike_map_filename) THEN BEGIN

		FILDSIM_read_strike_map,strike_map,filename=strike_map_filename
		sm_settings.nm_strike_map_filename=strike_map_filename
		nm_strike_map=strike_map

	ENDIF

	FILDSIM_read_plate,plate,filename=nm_scintillator_files

	widget_control,uvalue.sm_draw_strike_map,get_value=draw_id
	wset,draw_id


	u_pitch=nm_strike_map.pitch_angle[UNIQ(nm_strike_map.pitch_angle, SORT(nm_strike_map.pitch_angle))]
	u_gyr=nm_strike_map.gyroradius[UNIQ(nm_strike_map.gyroradius, SORT(nm_strike_map.gyroradius))]

	yy=fltarr(n_elements(u_pitch)*n_elements(u_gyr))
	zz=fltarr(n_elements(u_pitch)*n_elements(u_gyr))
	sigma_gyr=fltarr(n_elements(u_pitch)*n_elements(u_gyr))
	sigma_pitch=fltarr(n_elements(u_pitch)*n_elements(u_gyr))

	counter=0

	FOR kk=0,n_elements(u_gyr)-1 DO BEGIN
		FOR jj=0,n_elements(u_pitch)-1 DO BEGIN

			w=where(nm_strike_map.gyroradius EQ u_gyr(kk) AND nm_strike_map.pitch_angle EQ u_pitch(jj))
			yy(counter)=nm_strike_map.yy(w)
			zz(counter)=nm_strike_map.zz(w)

			ww=where(nm_resolutions(0,*) EQ u_gyr(kk) AND nm_resolutions(1,*) EQ u_pitch(jj),count)

			IF (count GT 0) THEN BEGIN
				sigma_gyr(counter)=nm_resolutions(2,ww)
				sigma_pitch(counter)=nm_resolutions(3,ww)
			ENDIF

			counter=counter+1

		ENDFOR
	ENDFOR

	; REMOVE NAN values

	w=where(finite(yy) EQ 1)
	yy=yy(w)
	zz=zz(w)
	sigma_gyr=sigma_gyr(w)
	sigma_pitch=sigma_pitch(w)

	ysize=100
	zsize=100

	TRIANGULATE, yy, zz, tr,b

	yy_int=findgen(ysize)/(ysize-1)*abs(max(plate.y)-min(plate.y))+min(plate.y)
	zz_int=findgen(zsize)/(zsize-1)*abs(max(plate.z)-min(plate.z))+min(plate.z)


	sigma_gyr_int=trigrid(yy,zz,sigma_gyr,tr,[1,1],$
		[min(plate.y),min(plate.z),max(plate.y),max(plate.z)],nx=ysize,ny=zsize,/quintic,xout=yy_int,yout=zz_int)


	plot,plate.y,plate.z,psym=-3,color=0,background=255,/iso,xtitle='Y (cm)',ytitle='Z (cm)',charsize=2.0,$
	xrange=[min(nm_strike_map.yy,/NAN)-1,max(nm_strike_map.yy,/NAN)+1],yrange=[min(nm_strike_map.zz,/NAN)-1,max(nm_strike_map.zz,/NAN)+1],$
	position=[0.2, 0.20, 0.80, 0.9],title='FHWM Gyroradius',/nodata

	nlev=250
	lev=findgen(nlev)/(nlev-1)*(max(sigma_gyr_int)-min(sigma_gyr_int))+min(sigma_gyr_int)



	contour,2.35*sigma_gyr_int,yy_int,zz_int,levels=lev,/fill,background=255,/overplot



	; PLOT STRIKE MAP


	; DRAW GYRORADIUS LINES

	u_pitch=nm_strike_map.pitch_angle[UNIQ(nm_strike_map.pitch_angle, SORT(nm_strike_map.pitch_angle))]

	FOR kk=0,n_elements(u_pitch)-1 DO BEGIN

		w=where(nm_strike_map.pitch_angle EQ u_pitch(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=0

	ENDFOR

	; DRAW PITCH LINES

	u_gyr=nm_strike_map.gyroradius[UNIQ(nm_strike_map.gyroradius, SORT(nm_strike_map.gyroradius))]

	FOR kk=0,n_elements(u_gyr)-1 DO BEGIN

		w=where(nm_strike_map.gyroradius EQ u_gyr(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=0

	ENDFOR

	oplot,plate.y,plate.z,psym=-3,color=100

	colorbar,max=max(2.35*sigma_gyr_int),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(f8.3)',/vert,color=0


	END

;***************************************************************************************************************************************************


UVALUE.SM_CALCULATOR_CONTOUR_SIGMA_PITCH: BEGIN

	COMMON STRIKE_MAP_CALCULATOR,sm_settings
	COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files
	COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions
	COMMON SETTINGS,settings


	IF size(nm_resolutions,/N_DIMENSIONS) NE 2 THEN BEGIN
		help,nm_resolutions
		print,'Resolutions are not yet calculated... Returning'
		return

	ENDIF



	strike_map_filename = DIALOG_PICKFILE(/READ,PATH=settings.FILDSIMf90_path,FILTER='*strike_map.dat')

	; READ STRIKE POINTS
	widget_control,/HOURGLASS

	IF (strike_map_filename NE sm_settings.nm_strike_map_filename) THEN BEGIN

		FILDSIM_read_strike_map,strike_map,filename=strike_map_filename
		sm_settings.nm_strike_map_filename=strike_map_filename
		nm_strike_map=strike_map

	ENDIF

	FILDSIM_read_plate,plate,filename=nm_scintillator_files

	widget_control,uvalue.sm_draw_strike_map,get_value=draw_id
	wset,draw_id


	u_pitch=nm_strike_map.pitch_angle[UNIQ(nm_strike_map.pitch_angle, SORT(nm_strike_map.pitch_angle))]
	u_gyr=nm_strike_map.gyroradius[UNIQ(nm_strike_map.gyroradius, SORT(nm_strike_map.gyroradius))]

	yy=fltarr(n_elements(u_pitch)*n_elements(u_gyr))
	zz=fltarr(n_elements(u_pitch)*n_elements(u_gyr))
	sigma_gyr=fltarr(n_elements(u_pitch)*n_elements(u_gyr))
	sigma_pitch=fltarr(n_elements(u_pitch)*n_elements(u_gyr))

	counter=0

	FOR kk=0,n_elements(u_gyr)-1 DO BEGIN
		FOR jj=0,n_elements(u_pitch)-1 DO BEGIN

			w=where(nm_strike_map.gyroradius EQ u_gyr(kk) AND nm_strike_map.pitch_angle EQ u_pitch(jj))
			yy(counter)=nm_strike_map.yy(w)
			zz(counter)=nm_strike_map.zz(w)

			ww=where(nm_resolutions(0,*) EQ u_gyr(kk) AND nm_resolutions(1,*) EQ u_pitch(jj),count)


			IF (count GT 0) THEN BEGIN

				sigma_gyr(counter)=nm_resolutions(2,ww)
				sigma_pitch(counter)=nm_resolutions(3,ww)

			ENDIF

			counter=counter+1

		ENDFOR
	ENDFOR

	; REMOVE NAN values

	w=where(finite(yy) EQ 1)
	yy=yy(w)
	zz=zz(w)
	sigma_gyr=sigma_gyr(w)
	sigma_pitch=sigma_pitch(w)


	ysize=100
	zsize=100

	TRIANGULATE, yy, zz, tr,b

	yy_int=findgen(ysize)/(ysize-1)*abs(max(plate.y)-min(plate.y))+min(plate.y)
	zz_int=findgen(zsize)/(zsize-1)*abs(max(plate.z)-min(plate.z))+min(plate.z)


	sigma_pitch_int=trigrid(yy,zz,sigma_pitch,tr,[1,1],$
		[min(plate.y),min(plate.z),max(plate.y),max(plate.z)],nx=ysize,ny=zsize,/quintic,xout=yy_int,yout=zz_int)


	plot,plate.y,plate.z,psym=-3,color=0,background=255,/iso,xtitle='Y (cm)',ytitle='Z (cm)',charsize=2.0,$
	xrange=[min(nm_strike_map.yy,/NAN)-1,max(nm_strike_map.yy,/NAN)+1],yrange=[min(nm_strike_map.zz,/NAN)-1,max(nm_strike_map.zz,/NAN)+1],$
	position=[0.2, 0.20, 0.80, 0.9],title='FWHM Pitch Angle'

	nlev=250
	lev=findgen(nlev)/(nlev-1)*(max(sigma_pitch_int)-min(sigma_pitch_int))+min(sigma_pitch_int)



	contour,2.35*sigma_pitch_int,yy_int,zz_int,levels=lev,/fill,background=255,/overplot


	; PLOT STRIKE MAP


	; DRAW GYRORADIUS LINES

	u_pitch=nm_strike_map.pitch_angle[UNIQ(nm_strike_map.pitch_angle, SORT(nm_strike_map.pitch_angle))]

	FOR kk=0,n_elements(u_pitch)-1 DO BEGIN

		w=where(nm_strike_map.pitch_angle EQ u_pitch(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=0

	ENDFOR

	; DRAW PITCH LINES

	u_gyr=nm_strike_map.gyroradius[UNIQ(nm_strike_map.gyroradius, SORT(nm_strike_map.gyroradius))]

	FOR kk=0,n_elements(u_gyr)-1 DO BEGIN

		w=where(nm_strike_map.gyroradius EQ u_gyr(kk),count)
		oplot,nm_strike_map.yy(w),nm_strike_map.zz(w),psym=-3,color=0

	ENDFOR

	oplot,plate.y,plate.z,psym=-3,color=100

	colorbar,max=max(2.35*sigma_pitch_int),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(f8.3)',/vert,color=0


	END


;*******************************************************************************************************************************


UVALUE.PHANTOM_REMAP_SHOT: BEGIN



	COMMON SETTINGS,settings
	COMMON FILD_GRID_INFO,fild_grid_info
	COMMON GRID_PARAMS,grid_params
	COMMON REMAP_SETTINGS,remap_settings
	COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase



		widget_control,uvalue.phantom_settings_minpitch,get_value=minpitch
		widget_control,uvalue.phantom_settings_maxpitch,get_value=maxpitch
		widget_control,uvalue.phantom_settings_dpitch,get_value=dpitch

		widget_control,uvalue.phantom_settings_mingyr,get_value=mingyr
		widget_control,uvalue.phantom_settings_maxgyr,get_value=maxgyr
		widget_control,uvalue.phantom_settings_dgyr,get_value=dgyr

		widget_control,uvalue.phantom_settings_remap_levels,get_value=nlevels

		remap_settings.minpitch=minpitch
		remap_settings.maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.mingyr=mingyr
		remap_settings.maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.nlev=nlevels


		print,'REMAPING WHOLE SHOT... THIS MIGHT TAKE A WHILE...'

		widget_control,/HOURGLASS

		dumm_size=size(phantom_frames)
		nframes=dumm_size(3)

		FOR kk=0,nframes-1 DO BEGIN

			print,'Frame '+strtrim(kk+1,1)+' out of '+strtrim(nframes,1)

			dumm_current_frame=LONG(phantom_frames(*,*,kk))-LONG(phantom_background_frame)
			dumm_current_frame>=0

			FILD_remap_grid,o,REF_FRAME=dumm_current_frame,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

			IF kk EQ 0 THEN BEGIN

				shot_remap_array=fltarr(nframes,n_elements(o.pitch),n_elements(o.gir))
				shot_remap_pitch=o.pitch
				shot_remap_gyroradius=o.gir
				shot_remap_time=fltarr(nframes)

				dumm=size(phantom_frames,/dimensions)
				raw_frames=fltarr(dumm(0),dumm(1),nframes)

			ENDIF

			shot_remap_array(kk,*,*)=o.h
			shot_remap_time(kk)=phantom_times(kk)
			raw_frames(*,*,kk)=dumm_current_frame

		ENDFOR

		print,'REMAP FINISHED'

		shot_remap={data:shot_remap_array,time:shot_remap_time,pitch:shot_remap_pitch,gyroradius:shot_remap_gyroradius}		;,raw_frames:raw_frames}

		remap_filename=strtrim(settings.results_path,1)+'shot_remap_'+strtrim(settings.shot,1)+'_'+strtrim(phantom_initial_time,1)+'-'+$
			strtrim(phantom_final_time,1)+'.sav'

		print,'Saving data into '+strtrim(remap_filename)
		save,filename=remap_filename,shot_remap



	END


;*******************************************************************************************************************************


UVALUE.CCD_REMAP_SHOT: BEGIN



	COMMON SETTINGS,settings
	COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
	COMMON FILD_GRID_INFO,fild_grid_info
	COMMON GRID_PARAMS,grid_params
	COMMON REMAP_SETTINGS,remap_settings



		widget_control,uvalue.settings_minpitch,get_value=minpitch
		widget_control,uvalue.settings_maxpitch,get_value=maxpitch
		widget_control,uvalue.settings_dpitch,get_value=dpitch

		widget_control,uvalue.settings_mingyr,get_value=mingyr
		widget_control,uvalue.settings_maxgyr,get_value=maxgyr
		widget_control,uvalue.settings_dgyr,get_value=dgyr

		widget_control,uvalue.settings_remap_levels,get_value=nlevels

		remap_settings.minpitch=minpitch
		remap_settings.maxpitch=maxpitch
		remap_settings.dpitch=dpitch

		remap_settings.mingyr=mingyr
		remap_settings.maxgyr=maxgyr
		remap_settings.dgyr=dgyr

		remap_settings.nlev=nlevels


		print,'REMAPING WHOLE SHOT... THIS MIGHT TAKE A WHILE...'

		widget_control,/HOURGLASS

		nframes=n_elements(ccd_ccd_txt_times)

		FOR kk=0,nframes-1 DO BEGIN

			print,'Frame '+strtrim(kk+1,1)+' out of '+strtrim(nframes,1)

			ccd_current_image=ccd_frames(*,*,kk)
			dumm_current_frame=LONG(ccd_current_image)-LONG(ccd_background_image)
			dumm_current_frame>=0

			FILD_remap_grid,o,REF_FRAME=dumm_current_frame,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS

			IF kk EQ 0 THEN BEGIN

				shot_remap_array=fltarr(nframes,n_elements(o.pitch),n_elements(o.gir))
				shot_remap_pitch=o.pitch
				shot_remap_gyroradius=o.gir
				shot_remap_time=fltarr(nframes)
				dumm=size(dumm_current_frame,/dimensions)
				raw_frames=fltarr(dumm(0),dumm(1),nframes)

			ENDIF

			shot_remap_array(kk,*,*)=o.h
			shot_remap_time(kk)=ccd_ccd_txt_times(kk)
			raw_frames(*,*,kk)=dumm_current_frame

		ENDFOR


		print,'REMAP FINISHED'

		shot_remap={data:shot_remap_array,time:shot_remap_time,pitch:shot_remap_pitch,gyroradius:shot_remap_gyroradius}	;,raw_frames:raw_frames}

		remap_filename=strtrim(settings.results_path,1)+'CCD_shot_remap_'+strtrim(settings.shot,1)+'.sav'

		print,'Saving data into '+strtrim(remap_filename)
		save,filename=remap_filename,shot_remap



	END

;*******************************************************************************************************************************


UVALUE.AF_FRAME_SLIDER: BEGIN

		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON SETTINGS,settings
		COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
			af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
			af_input_path,af_efficiency_curve_file,af_calibration_file,af_timetrace,af_mean_calibration_frame
		COMMON AF_ROI_VARS,af_roi,af_flux_frame,af_heat_load_frame,af_flux,af_heat_load,af_photon_flux_frame,af_photon_flux
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

		widget_control,uvalue.af_frame_slider,get_value=slider_num
		widget_control,uvalue.af_input_data_checkbox,get_value=af_input_data
		widget_control,uvalue.af_input_data_checkbox_2,get_value=af_input_data_2
		widget_control,uvalue.af_input_data_checkbox_3,get_value=spot_detection_method
		widget_control,uvalue.af_magnetic_field,get_value=magnetic_field
		widget_control,uvalue.af_exp_time_widget,get_value=exp_time
		widget_control,uvalue.af_pinhole_area_widget,get_value=pinhole_area
		widget_control,uvalue.af_A_widget,get_value=AA
		widget_control,uvalue.af_Z_widget,get_value=ZZ
		widget_control,uvalue.af_bandpass_filter_widget,get_value=bandpass_filter

		widget_control,uvalue.af_integrated_photon_flux_widget,get_value=int_photon_flux
		widget_control,uvalue.af_calibration_exp_time_widget,get_value=calib_exp_time
		widget_control,uvalue.af_calibration_area_pixel_widget,get_value=calib_area_pixel


		widget_control,uvalue.af_calib_frame,set_value='CURRENT CALIB FRAME: '+strtrim(af_calibration_file,1),/dynamic_resize
		widget_control,uvalue.af_efficiency,set_value='CURRENT EFFICIENCY FILE: '+strtrim(af_efficiency_curve_file,1),/dynamic_resize


		af_filter=af_input_data_2(0)
		af_background_substraction=af_input_data_2(1)
		af_overlay_grid=af_input_data_2(2)
		af_ignore_fcol=af_input_data_2(3)
		mean_calibration_frame_option=af_input_data_2(5)
		af_bfield=magnetic_field
		af_exp_time=exp_time
		af_pinhole_area=pinhole_area
		af_A=AA
		af_Z=ZZ
		af_bandpass_filter=bandpass_filter

		af_integrated_photon_flux=int_photon_flux
		af_calibration_exp_time=calib_exp_time
		af_calibration_area_pixel=calib_area_pixel


		spot_detection_method+=1

		;********************************
		; READ CALIBRATION FRAME
		;********************************

		af_calibration_frame=read_png(strtrim(af_input_path,1)+strtrim(af_calibration_file,1))


		IF mean_calibration_frame_option EQ 1 THEN calibration=af_mean_calibration_frame ELSE calibration=af_calibration_frame

		;********************************
		; LOAD EFFICIENCY CURVE
		;********************************

		efficiency=FILD_read_scintillator_efficiency(strtrim(af_input_path,1)+strtrim(af_efficiency_curve_file,1))

		;********************************

		IF af_input_data EQ 0 THEN BEGIN

			IF settings.ccd_data_loaded EQ 0 THEN BEGIN

				widget_control,uvalue.af_frame_slider;SENSITIVE=0
				print,'No CCD data loaded'
				return

			ENDIF


			widget_control,uvalue.af_frame_slider,get_value=slider_num

			ccd_ccd_frame_num=slider_num-1
			ccd_current_image=ccd_frames(*,*,slider_num-1)

			af_frame_num=ccd_ccd_frame_num
			af_current_image=ccd_current_image

			sat_level=65520.
			wsat=where(af_current_image EQ sat_level,npixels_sat)
			ss=size(af_current_image)
			npixels=ss(4)
			npixels=FLOAT(npixels)
			npixels_sat=FLOAT(npixels_sat)

			IF af_filter EQ 1 THEN af_current_image=median(af_current_image,3)
			IF af_background_substraction EQ 1 THEN af_current_image=af_current_image-ccd_background_image

			af_current_image>=0

			FILD_calculate_photon_flux,photon_flux,af_current_image,calibration,af_roi,$
				PINHOLE_AREA=af_pinhole_area,EXPOSURE_TIME=af_exp_time,$
				CALIB_EXPOSURE_TIME=af_calibration_exp_time,PIXEL_AREA_COVERED=af_calibration_area_pixel,$
				INT_PHOTON_FLUX=af_integrated_photon_flux,ROI=spot_detection_method

			af_photon_flux_frame=photon_flux.photon_flux_frame
			af_photon_flux=photon_flux.photon_flux


			IF af_input_data_2(4) EQ 1 THEN BEGIN

				FILD_calculate_ion_flux,ion_flux,photon_flux.photon_flux_frame,$
					efficiency.energy,efficiency.yield,af_bfield,af_bandpass_filter,$
					ccd_interpolated_gyro,ccd_interpolated_fcol,af_roi,$
					IGNORE_FCOL=af_ignore_fcol,A=af_A,Z=af_Z,METHOD=spot_detection_method


				af_flux_frame=ion_flux.absolute_flux_frame
				af_heat_load_frame=ion_flux.absolute_heat_load_frame
				af_flux=ion_flux.absolute_flux
				af_heat_load=ion_flux.absolute_heat_load

				s=size(af_current_image,/dimensions)
				widget_control,uvalue.af_frame_draw,get_value=draw_id,draw_xsize=s(0),draw_ysize=s(1)
				wset,draw_id
				tvscl,ion_flux.absolute_flux_frame
				xyouts,0.1,0.15,'Absolute heat load (kW/m^2): '+strtrim(ion_flux.absolute_heat_load/1.e3,1),/norm
				xyouts,0.1,0.1,'Absolute flux (ions/s/m^2): '+strtrim(ion_flux.absolute_flux,1),/norm


				IF spot_detection_method EQ 2 THEN BEGIN

					plots,ion_flux.spot_track(0,*),ion_flux.spot_track(1,*),psym=1,thick=3.,color=100,/device

				ENDIF

				;print,'Npixels sat / npixels',npixels_sat,npixels,npixels_sat/npixels*100.
				xyouts,0.1,0.2,'Pixels saturated (%): '+strtrim(npixels_sat/npixels*100.,1),/norm

				colorbar,max=max(ion_flux.absolute_flux_frame,/NAN),position=[0.2,0.9,0.8,0.95],format='(E12.5)',title='Absolute Flux (ions/s/m2)'

			ENDIF ELSE BEGIN


				af_flux_frame=0.
				af_heat_load_frame=0.
				af_flux=0.
				af_heat_load=0.

				s=size(af_current_image,/dimensions)
				widget_control,uvalue.af_frame_draw,get_value=draw_id,draw_xsize=s(0),draw_ysize=s(1)
				wset,draw_id
				tvscl,photon_flux.photon_flux_frame
				xyouts,0.1,0.1,'Photon flux (photons/s/m^2): '+strtrim(photon_flux.photon_flux,1),/norm


				;print,'Npixels sat / npixels',npixels_sat,npixels,npixels_sat/npixels*100.
				xyouts,0.1,0.2,'Pixels saturated (%): '+strtrim(npixels_sat/npixels*100.,1),/norm

				colorbar,max=max(photon_flux.photon_flux_frame,/NAN),position=[0.2,0.9,0.8,0.95],format='(E12.5)',title='Photon Flux (photons/s/m2)'



			ENDELSE


			widget_control,uvalue.af_frame_info_frame,set_value='Frame='+str(af_frame_num)
			widget_control,uvalue.af_frame_info_time,set_value='Time='+$
				str(ccd_ccd_txt_times(af_frame_num))+' s'




			IF af_overlay_grid EQ 1 THEN BEGIN

			;	widget_control,uvalue.plot_grid_button,send_event={ID:uvalue.plot_grid_button,TOP:uvalue.ccd_frame_base,HANDLER:uvalue.ccd_frame_slider}

			ENDIF


			s=size(af_current_image,/dimensions)
			widget_control,uvalue.af_yield_curve_draw,get_value=draw_id,draw_xsize=s(0),draw_ysize=s(1)
			wset,draw_id

			plot,efficiency.energy,efficiency.yield,color=0,background=255,charsize=2.,psym=4,$
				xtitle='Energy (eV)',ytitle='Yield (Photons/ion)',xrange=[0.,max(efficiency.energy)*1.5],xstyle=1

			int_energy=findgen(1000)/(999.)*(max(efficiency.energy)*2.)
			int_yield=interpol(efficiency.yield,efficiency.energy,int_energy)
			int_yield>=1.

			oplot,int_energy,int_yield,psym=-3,color=100





		ENDIF ELSE BEGIN

			IF settings.phantom_data_loaded EQ 0 THEN BEGIN

				print,'No phantom camera data loaded'
				return

			ENDIF


			widget_control,uvalue.af_frame_slider,SENSITIVE=1,set_value=slider_num,$
				set_slider_min=0,set_slider_max=n_elements(phantom_frames(0,0,*))-1
			af_current_image=phantom_frames(*,*,slider_num)

			sat_level=4064.	;2.^12
			wsat=where(af_current_image GE sat_level,npixels_sat)
			ss=size(af_current_image)
			npixels=ss(4)

			IF af_filter EQ 1 THEN af_current_image=median(af_current_image,3)
			IF af_background_substraction EQ 1 THEN af_current_image=af_current_image-phantom_background_frame
			af_current_image>=0



			FILD_calculate_photon_flux,photon_flux,af_current_image,calibration,af_roi,$
				PINHOLE_AREA=af_pinhole_area,EXPOSURE_TIME=af_exp_time,$
				CALIB_EXPOSURE_TIME=af_calibration_exp_time,PIXEL_AREA_COVERED=af_calibration_area_pixel,$
				INT_PHOTON_FLUX=af_integrated_photon_flux,ROI=spot_detection_method

			af_photon_flux_frame=photon_flux.photon_flux_frame
			af_photon_flux=photon_flux.photon_flux


			IF af_input_data_2(4) EQ 1 THEN BEGIN

				FILD_calculate_ion_flux,ion_flux,photon_flux.photon_flux_frame,$
					efficiency.energy,efficiency.yield,af_bfield,af_bandpass_filter,$
					phantom_interpolated_gyro,phantom_interpolated_fcol,af_roi,$
					IGNORE_FCOL=af_ignore_fcol,A=af_A,Z=af_Z,METHOD=spot_detection_method


				af_flux_frame=ion_flux.absolute_flux_frame
				af_heat_load_frame=ion_flux.absolute_heat_load_frame
				af_flux=ion_flux.absolute_flux
				af_heat_load=ion_flux.absolute_heat_load

				s=size(af_current_image,/dimensions)
				widget_control,uvalue.af_frame_draw,get_value=draw_id,draw_xsize=s(0),draw_ysize=s(1)
				wset,draw_id
				tvscl,ion_flux.absolute_flux_frame
				xyouts,0.1,0.15,'Absolute heat load (kW/m^2): '+strtrim(ion_flux.absolute_heat_load/1.e3,1),/norm
				xyouts,0.1,0.1,'Absolute flux (ions/s/m^2): '+strtrim(ion_flux.absolute_flux,1),/norm


				IF spot_detection_method EQ 2 THEN BEGIN

					plots,ion_flux.spot_track(0,*),ion_flux.spot_track(1,*),psym=1,thick=3.,color=100,/device

				ENDIF

				;print,'Npixels sat / npixels',npixels_sat,npixels,npixels_sat/npixels*100.
				xyouts,0.1,0.2,'Pixels saturated (%): '+strtrim(npixels_sat/npixels*100.,1),/norm
				xyouts,0.1,0.3,'# Pixels saturated: '+strtrim(npixels_sat,1),/norm

				colorbar,max=max(ion_flux.absolute_flux_frame,/NAN),position=[0.2,0.9,0.8,0.95],format='(E12.5)',title='Absolute Flux (ions/s/m2)'

			ENDIF ELSE BEGIN


				af_flux_frame=0.
				af_heat_load_frame=0.
				af_flux=0.
				af_heat_load=0.

				s=size(af_current_image,/dimensions)
				widget_control,uvalue.af_frame_draw,get_value=draw_id,draw_xsize=s(0),draw_ysize=s(1)
				wset,draw_id
				tvscl,photon_flux.photon_flux_frame
				xyouts,0.1,0.1,'Photon flux (photons/s/m^2): '+strtrim(photon_flux.photon_flux,1),/norm


				;print,'Npixels sat / npixels',npixels_sat,npixels,npixels_sat/npixels*100.
				xyouts,0.1,0.2,'Pixels saturated (%): '+strtrim(npixels_sat/npixels*100.,1),/norm

				colorbar,max=max(photon_flux.photon_flux_frame,/NAN),position=[0.2,0.9,0.8,0.95],format='(E12.5)',title='Photon Flux (photons/s/m2)'



			ENDELSE


			widget_control,uvalue.af_frame_info_frame,set_value='Frame='+str(slider_num)
			widget_control,uvalue.af_frame_info_time,set_value='Time='+$
					str(phantom_times(slider_num))+' s

			IF af_overlay_grid EQ 1 THEN BEGIN

			;	widget_control,uvalue.plot_grid_button,send_event={ID:uvalue.plot_grid_button,TOP:uvalue.ccd_frame_base,HANDLER:uvalue.ccd_frame_slider}

			ENDIF


			s=size(af_current_image,/dimensions)
			widget_control,uvalue.af_yield_curve_draw,get_value=draw_id,draw_xsize=s(0),draw_ysize=s(1)
			wset,draw_id

			plot,efficiency.energy,efficiency.yield,color=0,background=255,charsize=2.,psym=4,$
				xtitle='Energy (eV)',ytitle='Yield (Photons/ion)',xrange=[0.,max(efficiency.energy)*1.5],xstyle=1

			int_energy=findgen(1000)/(999.)*(max(efficiency.energy)*2.)
			int_yield=interpol(efficiency.yield,efficiency.energy,int_energy)
			int_yield>=1.

			oplot,int_energy,int_yield,psym=-3,color=100



		ENDELSE

	END

;*******************************************************************************************************************************


UVALUE.AF_SET_MEAN_CALIBRATION_FRAME: BEGIN

	COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
		af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
		af_input_path,af_efficiency_curve_file,af_calibration_file,af_timetrace,af_mean_calibration_frame


	;********************************
	; READ CALIBRATION FRAME
	;********************************

	af_calibration_frame=read_png(strtrim(af_input_path,1)+strtrim(af_calibration_file,1))


	s=size(af_calibration_frame,/dimensions)
	widget_control,uvalue.af_yield_curve_draw,get_value=draw_id,draw_xsize=s(0),draw_ysize=s(1)
	wset,draw_id

	tvscl,af_calibration_frame

	widget_control,uvalue.af_frame_draw,get_value=draw_id
	wset,draw_id
	roi_calibration_frame=CW_DEFROI(uvalue.af_yield_curve_draw,/restore)
	help,roi_calibration

	data=af_calibration_frame(roi_calibration_frame)

	h2=histogram(data,locations=xloc)
	plot,xloc,h2,color=0,background=255,psym=10,xtitle='Pixel counts',ytitle='Number of Events'

	af_mean_calibration_frame=mean(data)

	plots,[af_mean_calibration_frame,af_mean_calibration_frame],[0.,max(h2)],color=100,psym=-4

END


;*******************************************************************************************************************************


UVALUE.AF_PRINT_INFO_BUTTON: BEGIN


	COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
		af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
		af_input_path,af_efficiency_curve_file,af_calibration_file,af_timetrace,af_mean_calibration_frame


	print,'**********************************'
	print,'Absolute Flux Estimation Settings '
	print,'**********************************'
	print,'Calibration Frame: '+strtrim(af_calibration_file,1)
	print,'Calibration frame mean (counts): '+strtrim(af_mean_calibration_frame,1)
	print,'Exposure Time of Calibration Frame (s): '+strtrim(af_calibration_exp_time,1)
	print,'Integrated photon flux (photons/s/m^2): '+strtrim(af_integrated_photon_flux,1)
	print,'Area covered by pixels (m^2): '+strtrim(af_calibration_area_pixel,1)
	print,'Yield curve file:'+strtrim(af_efficiency_curve_file,1)
	print,'**********************************'

	print,'Before calculating absolute flux please check: '
	print,'- Calculate a remap before running it. Otherwise it will give an error.'
	print,'- Scintillator yield file is consistent with A and Z options'
	print,'- Effective pixel area is correct (use Estimate Pixel Area button if needed)'
	print,'- Use Mean Calibration Mode for the calibration frame if data is from FILD1. Use Set Mean Calibration Frame button for this.'
	print,'- Use bandpass filter variable to control noise filtering. This is done to identify regions with signal level above noise, to avoid artifacts due to small values of f_col (1./0. --> INF) '
	print,'**********************************'

END

;***************************************************************************************************************************************************



UVALUE.AF_ESTIMATE_PIXEL_AREA: BEGIN


	COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
		af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
		af_input_path,af_efficiency_curve_file,af_calibration_file,af_timetrace,af_mean_calibration_frame

	widget_control,uvalue.xscale_field,get_value=xscale
	widget_control,uvalue.yscale_field,get_value=yscale


	af_calibration_area_pixel=FILD_estimate_effective_pixel_area(xscale,yscale)

	print,'**********************************'
	print,'Absolute Flux Estimation Settings '
	print,'**********************************'
	print,'Calibration Frame: '+strtrim(af_calibration_file,1)
	print,'Calibration frame mean (counts): '+strtrim(af_mean_calibration_frame,1)
	print,'Exposure Time of Calibration Frame (s): '+strtrim(af_calibration_exp_time,1)
	print,'Integrated photon flux (photons/s/m^2): '+strtrim(af_integrated_photon_flux,1)
	print,'Area covered by pixels (m^2): '+strtrim(af_calibration_area_pixel,1)
	print,'Yield curve file:'+strtrim(af_efficiency_curve_file,1)
	print,'**********************************'

	widget_control,uvalue.af_calibration_area_pixel_widget,set_value=af_calibration_area_pixel


END

;***************************************************************************************************************************************************


UVALUE.AF_SELECT_CALIBRATION_FRAME_BUTTON: BEGIN


	COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
		af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
		af_input_path,af_efficiency_curve_file,af_calibration_file,af_timetrace,af_mean_calibration_frame

	calibration_frame = DIALOG_PICKFILE(/READ,PATH=af_input_path)
	af_calibration_file=strmid(calibration_frame,strlen(af_input_path))

	widget_control,uvalue.af_calib_frame,set_value='CURRENT CALIB FRAME: '+strtrim(af_calibration_file,1),/dynamic_resize

END

;***************************************************************************************************************************************************

UVALUE.AF_SELECT_EFFICIENCY_FILE_BUTTON: BEGIN


	COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
		af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
		af_input_path,af_efficiency_curve_file,af_calibration_file,af_timetrace,af_mean_calibration_frame


	efficiency_file = DIALOG_PICKFILE(/READ,PATH=af_input_path)
	af_efficiency_curve_file=strmid(efficiency_file,strlen(af_input_path))

	widget_control,uvalue.af_efficiency,set_value='CURRENT EFFICIENCY FILE: '+strtrim(af_efficiency_curve_file,1),/dynamic_resize


END

;***************************************************************************************************************************************************

UVALUE.AF_DEF_ROI_BUTTON: BEGIN


	COMMON AF_ROI_VARS,af_roi,af_flux_frame,af_heat_load_frame,af_flux,af_heat_load,af_photon_flux_frame,af_photon_flux


	widget_control,uvalue.af_frame_draw,get_value=draw_id
	wset,draw_id
	af_roi=CW_DEFROI(uvalue.af_frame_draw,/restore)
	help,af_roi


END

;***************************************************************************************************************************************************

UVALUE.EXPORT_EPS_ABSOLUTE_FLUX: BEGIN


	COMMON AF_ROI_VARS,af_roi,af_flux_frame,af_heat_load_frame,af_flux,af_heat_load,af_photon_flux_frame,af_photon_flux
	COMMON SETTINGS,settings

	set_plot,'ps'
	output_filename=strtrim(settings.results_path,1)+'Absolute_flux_test.eps'

	device,filename=output_filename,/color

	tvscl,af_flux_frame,/norm
	xyouts,0.1,0.15,'Absolute heat load (kW/m^2): '+strtrim(af_heat_load/1.e3,1),/norm,color=255
	xyouts,0.1,0.1,'Absolute flux (ions/s/m^2): '+strtrim(af_flux,1),/norm,color=255
	colorbar,max=max(af_flux_frame,/NAN),position=[0.2,0.95,0.8,1.0],format='(E12.5)',title='Absolute Flux (ions/s/m2)'

	device,/close
	set_plot,'X'

	print,'Figure saved as: ',output_filename

END

;***************************************************************************************************************************************************

UVALUE.SAVE_ABSOLUTE_FLUX_VARS: BEGIN


		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON SETTINGS,settings
		COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
			af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
			af_input_path,af_efficiency_curve_file,af_calibration_file,af_timetrace,af_mean_calibration_frame

		COMMON AF_ROI_VARS,af_roi,af_flux_frame,af_heat_load_frame,af_flux,af_heat_load,af_photon_flux_frame,af_photon_flux
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

		absolute_flux_vars={af_flux_frame:af_flux_frame,af_heat_load_frame:af_heat_load_frame,af_photon_flux_frame:af_photon_flux_frame,$
				af_flux:af_flux,af_heat_load:af_heat_load,af_photon_flux:af_photon_flux,$
				af_calibration_frame:af_calibration_frame,af_integrated_photon_flux:af_integrated_photon_flux,$
				af_calibration_exp_time:af_calibration_exp_time,af_calibration_area_pixel:af_calibration_area_pixel,$
				af_efficiency_curve_file:af_efficiency_curve_file}

		vars_name=strtrim(settings.results_path,1)+'Absolute_flux_variables_'+strcompress(systime(),/remove_all)+'.sav'
		save,filename=vars_name,absolute_flux_vars
		print,'Data saved as: ',vars_name


END

;***************************************************************************************************************************************************

UVALUE.AF_CALCULATE_TIMETRACE: BEGIN


		COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times
		COMMON SETTINGS,settings
		COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
			af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
			af_input_path,af_efficiency_curve_file,af_calibration_file,af_timetrace,af_mean_calibration_frame

		COMMON AF_ROI_VARS,af_roi,af_flux_frame,af_heat_load_frame,af_flux,af_heat_load,af_photon_flux_frame,af_photon_flux
		COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

		COMMON FILD_GRID_INFO,fild_grid_info
		COMMON GRID_PARAMS,grid_params
		COMMON REMAP_SETTINGS,remap_settings

		print,'WARNING: Still testing this function !!!'

		print,'Calculating absolute flux for all the loaded frames ...'

		widget_control,uvalue.af_frame_slider,get_value=slider_num
		widget_control,uvalue.af_input_data_checkbox,get_value=af_input_data
		widget_control,uvalue.af_input_data_checkbox_2,get_value=af_input_data_2
		widget_control,uvalue.af_input_data_checkbox_3,get_value=spot_detection_method
		widget_control,uvalue.af_magnetic_field,get_value=magnetic_field
		widget_control,uvalue.af_exp_time_widget,get_value=exp_time
		widget_control,uvalue.af_pinhole_area_widget,get_value=pinhole_area
		widget_control,uvalue.af_A_widget,get_value=AA
		widget_control,uvalue.af_Z_widget,get_value=ZZ
		widget_control,uvalue.af_bandpass_filter_widget,get_value=bandpass_filter

		widget_control,uvalue.af_integrated_photon_flux_widget,get_value=int_photon_flux
		widget_control,uvalue.af_calibration_exp_time_widget,get_value=calib_exp_time
		widget_control,uvalue.af_calibration_area_pixel_widget,get_value=calib_area_pixel


		widget_control,uvalue.af_calib_frame,set_value='CURRENT CALIB FRAME: '+strtrim(af_calibration_file,1),/dynamic_resize
		widget_control,uvalue.af_efficiency,set_value='CURRENT EFFICIENCY FILE: '+strtrim(af_efficiency_curve_file,1),/dynamic_resize


		af_filter=af_input_data_2(0)
		af_background_substraction=af_input_data_2(1)
		af_overlay_grid=af_input_data_2(2)
		af_ignore_fcol=af_input_data_2(3)
		mean_calibration_frame_option=af_input_data_2(5)
		remap_frame_option=af_input_data_2(6)
		af_bfield=magnetic_field
		af_exp_time=exp_time
		af_pinhole_area=pinhole_area
		af_A=AA
		af_Z=ZZ
		af_bandpass_filter=bandpass_filter

		af_integrated_photon_flux=int_photon_flux
		af_calibration_exp_time=calib_exp_time
		af_calibration_area_pixel=calib_area_pixel


		spot_detection_method+=1


		;********************************
		; READ CALIBRATION FRAME
		;********************************

		af_calibration_frame=read_png(strtrim(af_input_path,1)+strtrim(af_calibration_file,1))


		IF mean_calibration_frame_option EQ 1 THEN calibration=af_mean_calibration_frame ELSE calibration=af_calibration_frame

		;********************************
		; LOAD EFFICIENCY CURVE
		;********************************

		efficiency=FILD_read_scintillator_efficiency(strtrim(af_input_path,1)+strtrim(af_efficiency_curve_file,1))

		;********************************


		IF af_input_data EQ 0 THEN BEGIN

			IF settings.ccd_data_loaded EQ 0 THEN BEGIN

				widget_control,uvalue.af_frame_slider;SENSITIVE=0
				print,'No CCD data loaded'
				return

			ENDIF

			print,'Function not yet checked for CCD data ... Use with caution'

			;return


			frames=ccd_frames

			FILD_calculate_absolute_flux_whole_shot_phantom,output,frames,ccd_times,phantom_background_frame,calibration,af_roi,$
				efficiency.energy,efficiency.yield,af_bfield,af_bandpass_filter,$
				ccd_interpolated_gyro,ccd_interpolated_fcol,$
				PINHOLE_AREA=af_pinhole_area,EXPOSURE_TIME=af_exp_time,$
				CALIB_EXPOSURE_TIME=af_calibration_exp_time,PIXEL_AREA_COVERED=af_calibration_area_pixel,$
				INT_PHOTON_FLUX=af_integrated_photon_flux,METHOD=spot_detection_method,$
				IGNORE_FCOL=af_ignore_fcol,A=af_A,Z=af_Z,$
				FILTER=af_filter,BACKGROUND_SUBSTRACTION=af_background_substraction,$
				REMAP_FRAMES=remap_frame_option,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS


			;absolute_flux_timetrace=output
			absolute_flux_timetrace={time:output.time,photon_flux:output.photon_flux,$
				absolute_flux:output.absolute_flux,absolute_heat_load:output.absolute_heat_load,$
				inputs:output.inputs}

			fn=strtrim(settings.results_path,1)+'af_timetrace'+strcompress(systime(),/remove_all)+'.sav'

			save,absolute_flux_timetrace,filename=fn

			print,'Absolute flux timetrace saved as: ',fn


		ENDIF ELSE BEGIN


			IF settings.phantom_data_loaded EQ 0 THEN BEGIN

				print,'No phantom camera data loaded'
				return

			ENDIF

			frames=phantom_frames

			FILD_calculate_absolute_flux_whole_shot_phantom,output,frames,phantom_times,phantom_background_frame,calibration,af_roi,$
				efficiency.energy,efficiency.yield,af_bfield,af_bandpass_filter,$
				phantom_interpolated_gyro,phantom_interpolated_fcol,$
				PINHOLE_AREA=af_pinhole_area,EXPOSURE_TIME=af_exp_time,$
				CALIB_EXPOSURE_TIME=af_calibration_exp_time,PIXEL_AREA_COVERED=af_calibration_area_pixel,$
				INT_PHOTON_FLUX=af_integrated_photon_flux,METHOD=spot_detection_method,$
				IGNORE_FCOL=af_ignore_fcol,A=af_A,Z=af_Z,$
				FILTER=af_filter,BACKGROUND_SUBSTRACTION=af_background_substraction,$
				REMAP_FRAMES=remap_frame_option,STRIKE_MAP_FILENAME=fild_grid_info.strike_map_filename,$
				GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS


			;absolute_flux_timetrace=output
			absolute_flux_timetrace={time:output.time,photon_flux:output.photon_flux,$
				absolute_flux:output.absolute_flux,absolute_heat_load:output.absolute_heat_load,$
				inputs:output.inputs}


			fn=strtrim(settings.results_path,1)+'af_timetrace'+strcompress(systime(),/remove_all)+'.sav'

			save,absolute_flux_timetrace,filename=fn

			print,'Absolute flux timetrace saved as: ',fn



		ENDELSE

END


;***************************************************************************************************************************************************

UVALUE.TOMO_RUN_BUTTON: BEGIN


		COMMON SETTINGS,settings
		COMMON GRID_PARAMS,grid_params

		widget_control,uvalue.tomo_grid_scint_mingyr,get_value=scint_mingyr
		widget_control,uvalue.tomo_grid_scint_maxgyr,get_value=scint_maxgyr
		widget_control,uvalue.tomo_grid_scint_dg,get_value=scint_dgyr


		widget_control,uvalue.tomo_grid_scint_minpitch,get_value=scint_minpitch
		widget_control,uvalue.tomo_grid_scint_maxpitch,get_value=scint_maxpitch
		widget_control,uvalue.tomo_grid_scint_dp,get_value=scint_dpitch


		widget_control,uvalue.tomo_grid_pin_mingyr,get_value=pin_mingyr
		widget_control,uvalue.tomo_grid_pin_maxgyr,get_value=pin_maxgyr
		widget_control,uvalue.tomo_grid_pin_dg,get_value=pin_dgyr

		widget_control,uvalue.tomo_grid_pin_minpitch,get_value=pin_minpitch
		widget_control,uvalue.tomo_grid_pin_maxpitch,get_value=pin_maxpitch
		widget_control,uvalue.tomo_grid_pin_dp,get_value=pin_dpitch

		widget_control,uvalue.tomo_exp_frame,get_value=measurement_file
		widget_control,uvalue.tomo_strike_map,get_value=strike_map_file
		widget_control,uvalue.tomo_strike_points,get_value=strike_points_file
		widget_control,uvalue.tomo_efficiency_file,get_value=efficiency_file
		widget_control,uvalue.tomo_b_field,get_value=bfield
		widget_control,uvalue.tomo_A,get_value=A
		widget_control,uvalue.tomo_Z,get_value=Z

		widget_control,uvalue.tomo_alpha,get_value=alpha


		widget_control,uvalue.tomo_methods,get_value=checkbox_methods
		widget_control,uvalue.tomo_model,get_value=checkbox_model
		widget_control,uvalue.tomo_checkbox,get_value=checkbox_settings


		pin_grid={mingyr:pin_mingyr,maxgyr:pin_maxgyr,dgyr:pin_dgyr,minpitch:pin_minpitch,maxpitch:pin_maxpitch,dpitch:pin_dpitch}
		scint_grid={mingyr:scint_mingyr,maxgyr:scint_maxgyr,dgyr:scint_dgyr,minpitch:scint_minpitch,maxpitch:scint_maxpitch,dpitch:scint_dpitch}

		print,'************************************'
		print,'Starting FILD tomographic inversion'
		print,'************************************'

		widget_control,/HOURGLASS

		bin_filename=strtrim(settings.results_path,1)+strcompress(systime(),/remove_all)+'_tomography.sav'

		FILDSIM_tomography,tomo_output,measurement_file,pin_grid,scint_grid,strike_map_file,strike_points_file,efficiency_file,$
			LIMIT_REGION_FCOL=checkbox_settings(0),A=A,Z=Z,BFIELD=BFIELD,GRID_ALIGNMENT_PARAMETERS=grid_params,$
			INVERSION_METHOD=checkbox_methods,NON_CALIBRATED_FRAME=checkbox_settings(1),EXPORT_BINARY=checkbox_settings(2),$
			OUTPUT_FILENAME=bin_filename,GAUSS_MODEL=checkbox_model,TSVD_VALUE=alpha

		help,tomo_output,/str

		widget_control,uvalue.tomo_input_draw,get_value=draw_id
		wset,draw_id

		nlev=250
		lev=findgen(nlev)/(nlev-1)*(max(tomo_output.measurement_2D,/NAN)-min(tomo_output.measurement_2D,/NAN))+min(tomo_output.measurement_2D,/NAN)
		col=findgen(nlev)/(nlev-1)*255


		contour,transpose(tomo_output.measurement_2D/(abs(tomo_output.scint_grid.rscint(2)-tomo_output.scint_grid.rscint(1))*abs(tomo_output.scint_grid.pscint(2)-tomo_output.scint_grid.pscint(1)))),$
			tomo_output.scint_grid.pscint,tomo_output.scint_grid.rscint,$
			levels=lev,c_colors=col,/fill,charsize=2.0,$
			yrange=[min(tomo_output.scint_grid.rscint),max(tomo_output.scint_grid.rscint)],ystyle=1,ytitle='Gyroradius (cm)',$
			xrange=[min(tomo_output.scint_grid.pscint),max(tomo_output.scint_grid.pscint)],xstyle=1,xtitle='Pitch Angle (deg)',$
			POSITION=[0.2,0.2,0.8,0.8],title='Scintillator - Photons/(s*m^2*cm*deg)'

		colorbar,max=max(lev),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert

		IF checkbox_methods NE 0 THEN BEGIN

			widget_control,uvalue.tomo_output_draw,get_value=draw_id
			wset,draw_id

			nlev=250
			lev=findgen(nlev)/(nlev-1)*(max(tomo_output.measurement_inversion_2D,/NAN)-min(tomo_output.measurement_inversion_2D,/NAN))+min(tomo_output.measurement_inversion_2D,/NAN)
			col=findgen(nlev)/(nlev-1)*255


			contour,transpose(tomo_output.measurement_inversion_2D),tomo_output.pin_grid.ppin,tomo_output.pin_grid.rpin,$
				levels=lev,c_colors=col,/fill,charsize=2.0,$
				yrange=[min(tomo_output.pin_grid.rpin),max(tomo_output.pin_grid.rpin)],ystyle=1,ytitle='Gyroradius (cm)',$
				xrange=[min(tomo_output.pin_grid.ppin),max(tomo_output.pin_grid.ppin)],xstyle=1,xtitle='Pitch Angle (deg)',$
				POSITION=[0.2,0.2,0.8,0.8],title='Pinhole - Ions/(s*m^2)'

			colorbar,max=max(lev),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert

		ENDIF



END


;***************************************************************************************************************************************************

	UVALUE.TOMO_LOAD_EXP_FRAME:BEGIN
	COMMON SETTINGS,settings

	tomo_measurement_file= DIALOG_PICKFILE(/READ,PATH=settings.results_path,FILTER='*.sav')

	widget_control,uvalue.tomo_exp_frame,set_value=tomo_measurement_file

	END

;***************************************************************************************************************************************************


	UVALUE.TOMO_SET_STRIKE_MAP:BEGIN

	COMMON SETTINGS,settings

	tomo_strike_map_file= DIALOG_PICKFILE(/READ,PATH=settings.fildsimf90_path,FILTER='*strike_map.dat')

	widget_control,uvalue.tomo_strike_map,set_value=tomo_strike_map_file

	END


;***************************************************************************************************************************************************


	UVALUE.TOMO_SET_STRIKE_POINTS:BEGIN

	COMMON SETTINGS,settings

	tomo_strike_points_file= DIALOG_PICKFILE(/READ,PATH=settings.fildsimf90_path,FILTER='*strike_points.dat')

	widget_control,uvalue.tomo_strike_points,set_value=tomo_strike_points_file

	END

;***************************************************************************************************************************************************


	UVALUE.TOMO_SET_EFFICIENCY_FILE:BEGIN

	COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
			af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
			af_input_path,af_efficiency_curve_file,af_calibration_file

	tomo_efficiency_file= DIALOG_PICKFILE(/READ,PATH=af_input_path)

	widget_control,uvalue.tomo_efficiency_file,set_value=tomo_efficiency_file

	END


;***************************************************************************************************************************************************

	UVALUE.SAVE_CCD_TIMETRACE:BEGIN

	COMMON TIMETRACE,ccd_timetrace,ccd_timetrace_roi,phantom_timetrace,phantom_timetrace_roi
	COMMON SETTINGS,settings

	fn=strtrim(settings.results_path,1)+'CCD_timetrace'+strcompress(systime(),/remove_all)+'.sav'

	save,filename=fn,ccd_timetrace,ccd_timetrace_roi

	print,'Timetrace saved as: ',fn

	END

;***************************************************************************************************************************************************

	UVALUE.EXPORT_CCD_TIMETRACE_ASCII:BEGIN

	COMMON TIMETRACE,ccd_timetrace,ccd_timetrace_roi,phantom_timetrace,phantom_timetrace_roi
	COMMON SETTINGS,settings

	fn=strtrim(settings.results_path,1)+'CCD_timetrace'+strcompress(systime(),/remove_all)+'.dat'

	FILD_export_ascii_timetrace,fn,ccd_timetrace,ccd_timetrace_roi

	print,'Timetrace saved as: ',fn

	END



;***************************************************************************************************************************************************

	UVALUE.SAVE_PHANTOM_TIMETRACE:BEGIN

	COMMON TIMETRACE,ccd_timetrace,ccd_timetrace_roi,phantom_timetrace,phantom_timetrace_roi
	COMMON SETTINGS,settings

	fn=strtrim(settings.results_path,1)+'Phantom_timetrace'+strcompress(systime(),/remove_all)+'.sav'

	save,filename=fn,phantom_timetrace,phantom_timetrace_roi

	print,'Timetrace saved as: ',fn

	END

;***************************************************************************************************************************************************

	UVALUE.EXPORT_PHANTOM_TIMETRACE_ASCII:BEGIN

	COMMON TIMETRACE,ccd_timetrace,ccd_timetrace_roi,phantom_timetrace,phantom_timetrace_roi
	COMMON SETTINGS,settings

	fn=strtrim(settings.results_path,1)+'Phantom_timetrace'+strcompress(systime(),/remove_all)+'.dat'

	FILD_export_ascii_timetrace,fn,phantom_timetrace,phantom_timetrace_roi

	print,'Timetrace saved as: ',fn

	END


;***************************************************************************************************************************************************

	UVALUE.LOAD_COLOR_TABLE_BUTTON:BEGIN

	loadct

	return
	END



;***************************************************************************************************************************************************

UVALUE.fild_synthetic_frame_set_scintillator_yield:BEGIN

	COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
			af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
			af_input_path,af_efficiency_curve_file,af_calibration_file


	efficiency_file= DIALOG_PICKFILE(/READ,PATH=af_input_path)

	widget_control,uvalue.fild_synthetic_frame_scintillator_yield,set_value=efficiency_file

	END


;***************************************************************************************************************************************************

UVALUE.fild_synthetic_frame_set_scintillator_file:BEGIN

	COMMON SETTINGS,settings

	scintillator_file= DIALOG_PICKFILE(/READ,PATH=settings.fildsimf90_path,FILTER='*.pl')

	widget_control,uvalue.fild_synthetic_frame_scintillator_file,set_value=scintillator_file

	END

;***************************************************************************************************************************************************

UVALUE.fild_synthetic_frame_set_strike_map:BEGIN

	COMMON SETTINGS,settings

	strike_map_file= DIALOG_PICKFILE(/READ,PATH=settings.fildsimf90_path,FILTER='*strike_map.dat')

	widget_control,uvalue.fild_synthetic_frame_strike_map,set_value=strike_map_file

	END



;***************************************************************************************************************************************************

UVALUE.fild_synthetic_frame_load_camera_properties:BEGIN

	widget_control,uvalue.fild_synthetic_frame_camera_name,get_value=camera_name

	print,'Remember to set Camera Name to a valid number!!'

	dumm=LONG(camera_name)

	camera_properties=FILDSIM_camera_properties(dumm)

	widget_control,uvalue.fild_synthetic_frame_camera_name,set_value=camera_properties.camera_name
	widget_control,uvalue.fild_synthetic_frame_camera_nxpixels,set_value=camera_properties.nx_pixels
	widget_control,uvalue.fild_synthetic_frame_camera_nypixels,set_value=camera_properties.ny_pixels
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_xsize,set_value=camera_properties.pixel_xsize
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_ysize,set_value=camera_properties.pixel_ysize
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_qefficiency,set_value=camera_properties.quantum_efficiency
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_AD,set_value=camera_properties.f_analog_digital
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_dynamic_range,set_value=camera_properties.dynamic_range


	END

;***************************************************************************************************************************************************

UVALUE.fild_synthetic_frame_set_input:BEGIN

	COMMON SETTINGS,settings

	input= DIALOG_PICKFILE(/READ,PATH=settings.results_path,FILTER='*.sav')

	widget_control,uvalue.fild_synthetic_frame_input,set_value=input

	END


;***************************************************************************************************************************************************

UVALUE.fild_synthetic_frame_run:BEGIN

	COMMON SETTINGS,settings

	widget_control,uvalue.fild_synthetic_frame_input,get_value=input
	widget_control,uvalue.fild_synthetic_frame_strike_map,get_value=strike_map_filename
	widget_control,uvalue.fild_synthetic_frame_scintillator_file,get_value=scintillator_file
	widget_control,uvalue.fild_synthetic_frame_scintillator_yield,get_value=scintillator_yield
	widget_control,uvalue.fild_synthetic_frame_geometrical_factor,get_value=geometrical_factor
	widget_control,uvalue.fild_synthetic_frame_exp_time,get_value=exposure_time


	widget_control,uvalue.fild_synthetic_frame_camera_name,get_value=camera_name
	widget_control,uvalue.fild_synthetic_frame_camera_nxpixels,get_value=nx_pixels
	widget_control,uvalue.fild_synthetic_frame_camera_nypixels,get_value=ny_pixels
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_xsize,get_value=pixel_xsize
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_ysize,get_value=pixel_ysize
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_qefficiency,get_value=quantum_efficiency
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_AD,get_value=f_analog_digital
	widget_control,uvalue.fild_synthetic_frame_camera_pixel_dynamic_range,get_value=dynamic_range

	widget_control,uvalue.fild_synthetic_frame_optics_transmission,get_value=optics_transmission
	widget_control,uvalue.fild_synthetic_frame_optics_solid_angle,get_value=optics_solid_angle
	widget_control,uvalue.fild_synthetic_frame_optics_magnification,get_value=optics_beta

	widget_control,uvalue.fild_synthetic_frame_noise_gaussian_centroid,get_value=noise_gaussian_centroid
	widget_control,uvalue.fild_synthetic_frame_noise_gaussian_width,get_value=noise_gaussian_width

	widget_control,uvalue.fild_synthetic_frame_checkbox,get_value=options


	chip_xsize=nx_pixels*pixel_xsize
	chip_ysize=ny_pixels*pixel_ysize
	saturation_level=2L^dynamic_range

	camera_properties={camera_name:camera_name,$
			nx_pixels:nx_pixels,ny_pixels:ny_pixels,pixel_xsize:pixel_xsize,pixel_ysize:pixel_ysize,$
			chip_xsize:chip_xsize,chip_ysize:chip_ysize,$
			quantum_efficiency:quantum_efficiency,f_analog_digital:f_analog_digital,$
			dynamic_range:dynamic_range,saturation_level:saturation_level}



	print,'Calculating synthetic frame'

	widget_control,/HOURGLASS

	FILDSIM_synthetic_frame,input,output,$
	STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME,SCINTILLATOR_FILE=SCINTILLATOR_FILE,$
	SCINTILLATOR_YIELD=SCINTILLATOR_YIELD,GEOMETRICAL_FACTOR=GEOMETRICAL_FACTOR,$
	EXPOSURE_TIME=EXPOSURE_TIME,CAMERA_PROPERTIES=CAMERA_PROPERTIES,$
	OPTICS_TRANSMISSION=OPTICS_TRANSMISSION,OPTICS_SOLID_ANGLE=OPTICS_SOLID_ANGLE,$
	OPTICS_BETA=OPTICS_BETA,GAUSSIAN_NOISE_CENTROID=noise_gaussian_centroid,GAUSSIAN_NOISE_WIDTH=noise_gaussian_width

	print,'Calculation finished'

	help,output,/str

	widget_control,uvalue.fild_synthetic_frame_optics_magnification,set_value=output.optics_beta

	synthetic_frame_settings={input:input,strike_map_filename:strike_map_filename,scintillator_file:scintillator_file,$
			scintillator_yield:scintillator_yield,geometrical_factor:geometrical_factor,exposure_time:exposure_time,$
			optics_transmission:optics_transmission,optics_solid_angle:optics_solid_angle,optics_beta:optics_beta,$
			noise_gaussian_centroid:noise_gaussian_centroid,noise_gaussian_width:noise_gaussian_width}

	;*************
	; PLOT
	;*************


	widget_control,uvalue.fild_synthetic_frame_plot_draw_1,get_value=draw_id
	wset,draw_id

	nlev=250
	lev=findgen(nlev)/(nlev-1)*(max(output.synthetic_scintillator,/NAN)-min(output.synthetic_scintillator,/NAN))+min(output.synthetic_scintillator,/NAN)
	col=findgen(nlev)/(nlev-1)*255


	contour,output.synthetic_scintillator,$
		levels=lev,c_colors=col,/fill,charsize=2.0,$
		xrange=[0.,camera_properties.nx_pixels],xstyle=1,ytitle='X pixel',$
		yrange=[0.,camera_properties.ny_pixels],ystyle=1,xtitle='Y pixel',$
		POSITION=[0.2,0.2,0.8,0.8],title='Synthetic Scintillator [photons/(s*pixel)]'


	;*******************
	; PLOT STRIKE MAP
	;*******************

	pitch=output.pitch_grid(UNIQ(output.pitch_grid,SORT(output.pitch_grid)))
	gir=output.gyr_grid(UNIQ(output.gyr_grid,SORT(output.gyr_grid)))

	; DRAW GYRORADIUS LINES

	FOR kk=0,n_elements(pitch)-1 DO BEGIN
		w=where(output.pitch_grid EQ pitch(kk),count)
		IF count GT 0 THEN BEGIN
			plots,output.grid_pixel.xpixel(w),output.grid_pixel.ypixel(w),psym=-3,color=255,/data
		ENDIF
	ENDFOR

	; DRAW PITCH LINES

	FOR kk=0,n_elements(gir)-1 DO BEGIN

		w=where(output.gyr_grid EQ gir(kk),count)
		IF count GT 0 THEN BEGIN
			plots,output.grid_pixel.xpixel(w),output.grid_pixel.ypixel(w),psym=-3,color=255,/data
		ENDIF

	ENDFOR

	;********************

	plots,output.plate_pixel.xpixel,output.plate_pixel.ypixel,psym=-3,color=255,/data

	colorbar,max=max(lev),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert


	widget_control,uvalue.fild_synthetic_frame_plot_draw_2,get_value=draw_id
	wset,draw_id

	nlev=250
	;lev=findgen(nlev)/(nlev-1)*(max(output.synthetic_frame,/NAN)-min(output.synthetic_frame,/NAN))+min(output.synthetic_frame,/NAN)
	lev=findgen(nlev)/(nlev-1)*max(camera_properties.saturation_level)

	col=findgen(nlev)/(nlev-1)*255


	contour,output.synthetic_frame,$
		levels=lev,c_colors=col,/fill,charsize=2.0,$
		xrange=[0.,camera_properties.nx_pixels],xstyle=1,ytitle='X pixel',$
		yrange=[0.,camera_properties.ny_pixels],ystyle=1,xtitle='Y pixel',$
		POSITION=[0.2,0.2,0.8,0.8],title='Synthetic frame [counts/pixel]'


	;*******************
	; PLOT STRIKE MAP
	;*******************

	pitch=output.pitch_grid(UNIQ(output.pitch_grid,SORT(output.pitch_grid)))
	gir=output.gyr_grid(UNIQ(output.gyr_grid,SORT(output.gyr_grid)))

	; DRAW GYRORADIUS LINES

	FOR kk=0,n_elements(pitch)-1 DO BEGIN
		w=where(output.pitch_grid EQ pitch(kk),count)
		IF count GT 0 THEN BEGIN
			plots,output.grid_pixel.xpixel(w),output.grid_pixel.ypixel(w),psym=-3,color=255,/data
		ENDIF
	ENDFOR

	; DRAW PITCH LINES

	FOR kk=0,n_elements(gir)-1 DO BEGIN

		w=where(output.gyr_grid EQ gir(kk),count)
		IF count GT 0 THEN BEGIN
			plots,output.grid_pixel.xpixel(w),output.grid_pixel.ypixel(w),psym=-3,color=255,/data
		ENDIF

	ENDFOR

	;********************

	plots,output.plate_pixel.xpixel,output.plate_pixel.ypixel,psym=-3,color=255,/data

	xyouts,20,20,'Exp. Time (s): '+strtrim(exposure_time,1),/data,color=255

	colorbar,max=max(lev),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert


	;*******************
	; SAVE DATA
	;*******************

	IF options(0) EQ 1 THEN BEGIN

		fn=strtrim(settings.results_path,1)+'Synthetic_frame_'+strcompress(systime(),/remove_all)+'.sav

		save,output,synthetic_frame_settings,camera_properties,filename=fn

		print,'Results saved as : ',fn

	ENDIF

	;*******************
	; EXPORT EPS
	;*******************

	IF options(1) EQ 1 THEN BEGIN

	set_plot,'ps'

	fn1=strtrim(settings.results_path,1)+'Synthetic_scintillator_'+strcompress(systime(),/remove_all)+'.eps
	device,filename=fn1,/color


	nlev=250
	lev=findgen(nlev)/(nlev-1)*(max(output.synthetic_scintillator,/NAN)-min(output.synthetic_scintillator,/NAN))+min(output.synthetic_scintillator,/NAN)
	col=findgen(nlev)/(nlev-1)*255


	;contour,output.synthetic_scintillator,$
	;	levels=lev,c_colors=col,/fill,charsize=1.0,$
	;	xrange=[0.,camera_properties.nx_pixels],xstyle=1,ytitle='X pixel',$
	;	yrange=[0.,camera_properties.ny_pixels],ystyle=1,xtitle='Y pixel',$
	;	POSITION=[0.2,0.2,0.8,0.8],title='Synthetic Scintillator [photons/(s*pixel)]'

	image,output.synthetic_scintillator,xtitle='X pixel',ytitle='Y pixel',title='Synthetic scintillator [photons/s*pixel]',/iso,$
		zrange=[min(lev),max(lev)]


	;*******************
	; PLOT STRIKE MAP
	;*******************

	pitch=output.pitch_grid(UNIQ(output.pitch_grid,SORT(output.pitch_grid)))
	gir=output.gyr_grid(UNIQ(output.gyr_grid,SORT(output.gyr_grid)))

	; DRAW GYRORADIUS LINES

	FOR kk=0,n_elements(pitch)-1 DO BEGIN
		w=where(output.pitch_grid EQ pitch(kk),count)
		IF count GT 0 THEN BEGIN
			plots,output.grid_pixel.xpixel(w),output.grid_pixel.ypixel(w),psym=-3,color=255,/data
		ENDIF
	ENDFOR

	; DRAW PITCH LINES

	FOR kk=0,n_elements(gir)-1 DO BEGIN

		w=where(output.gyr_grid EQ gir(kk),count)
		IF count GT 0 THEN BEGIN
			plots,output.grid_pixel.xpixel(w),output.grid_pixel.ypixel(w),psym=-3,color=255,/data
		ENDIF

	ENDFOR

	;********************

	plots,output.plate_pixel.xpixel,output.plate_pixel.ypixel,psym=-3,color=255,/data

	;colorbar,max=max(lev),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert

	device,/close
	print,'Figure exported as: ',fn1

;*********************************

	fn2=strtrim(settings.results_path,1)+'Synthetic_frame_'+strcompress(systime(),/remove_all)+'.eps
	device,filename=fn2,/color

	nlev=250
	;lev=findgen(nlev)/(nlev-1)*(max(output.synthetic_frame,/NAN)-min(output.synthetic_frame,/NAN))+min(output.synthetic_frame,/NAN)
	lev=findgen(nlev)/(nlev-1)*camera_properties.saturation_level

	col=findgen(nlev)/(nlev-1)*255


	;contour,output.synthetic_frame,$
	;	levels=lev,c_colors=col,/fill,charsize=1.0,$
	;	xrange=[0.,camera_properties.nx_pixels],xstyle=1,ytitle='X pixel',$
	;	yrange=[0.,camera_properties.ny_pixels],ystyle=1,xtitle='Y pixel',$
	;	POSITION=[0.2,0.2,0.8,0.8],title='Synthetic frame [counts/pixel]'

	image,output.synthetic_frame,xtitle='X pixel',ytitle='Y pixel',title='Synthetic frame [counts/pixel]',/iso,$
		zrange=[0,camera_properties.saturation_level]

	;*******************
	; PLOT STRIKE MAP
	;*******************

	pitch=output.pitch_grid(UNIQ(output.pitch_grid,SORT(output.pitch_grid)))
	gir=output.gyr_grid(UNIQ(output.gyr_grid,SORT(output.gyr_grid)))

	; DRAW GYRORADIUS LINES

	FOR kk=0,n_elements(pitch)-1 DO BEGIN
		w=where(output.pitch_grid EQ pitch(kk),count)
		IF count GT 0 THEN BEGIN
			plots,output.grid_pixel.xpixel(w),output.grid_pixel.ypixel(w),psym=-3,color=255,/data
		ENDIF
	ENDFOR

	; DRAW PITCH LINES

	FOR kk=0,n_elements(gir)-1 DO BEGIN

		w=where(output.gyr_grid EQ gir(kk),count)
		IF count GT 0 THEN BEGIN
			plots,output.grid_pixel.xpixel(w),output.grid_pixel.ypixel(w),psym=-3,color=255,/data
		ENDIF

	ENDFOR

	;********************

	plots,output.plate_pixel.xpixel,output.plate_pixel.ypixel,psym=-3,color=255,/data

	xyouts,20,20,'Exp. Time (s): '+strtrim(exposure_time,1),/data,color=255

	;colorbar,max=max(lev),POSITION = [0.95, 0.20, 0.98, 0.85],  FORMAT='(e10.3)',/vert

	device,/close
	print,'Figure exported as: ',fn2

	set_plot,'X'


	ENDIF


	END


;***************************************************************************************************************************************************

UVALUE.ORBIT_CALC_RUN_BUTTON: BEGIN

COMMON ORBIT,oc_shot,oc_time,oc_rini,oc_zini,oc_phiini,oc_energy,oc_pitch,oc_exp,oc_diag,oc_ed,$
    oc_r,oc_z,oc_phi,oc_mass,oc_charge,oc_nsteps,oc_fild_gyro

COMMON SETTINGS,settings

widget_control,uvalue.orb_shot,get_value=shot
widget_control,uvalue.orb_time,get_value=time
widget_control,uvalue.orb_exp,get_value=exp
widget_control,uvalue.orb_diag,get_value=diag
widget_control,uvalue.orb_ed,get_value=ed

widget_control,uvalue.orb_rini,get_value=rini
widget_control,uvalue.orb_zini,get_value=zini
widget_control,uvalue.orb_phiini,get_value=phiini
widget_control,uvalue.orb_energy,get_value=energy
widget_control,uvalue.orb_pitch,get_value=pitch
widget_control,uvalue.orb_mass,get_value=mass
widget_control,uvalue.orb_charge,get_value=charge
widget_control,uvalue.orb_nsteps,get_value=nsteps


widget_control,uvalue.orb_checkbox_settings,get_value=orb_calc_settings

oc_shot=shot
oc_time=time
oc_rini=rini
oc_zini=zini
oc_phiini=phiini
oc_energy=energy
oc_pitch=pitch
oc_exp=exp
oc_diag=diag
oc_ed=ed
oc_mass=mass
oc_charge=charge
oc_nsteps=nsteps


print,'***************************'
print,'Starting Orbit simulation'
print,'Check that A and Z are set correctly'
print,'***************************'

widget_control,/HOURGLASS

IF orb_calc_settings(0) EQ 1 THEN forw_orb=0 ELSE forw_orb=1
IF orb_calc_settings(6) EQ 1 THEN wof_dir=settings.results_path ELSE wof_dir=0

dumm_pitch_sign=oc_pitch/abs(oc_pitch)

get_FILD_back_orbit,oc_energy,cos(oc_pitch*!pi/180.)*dumm_pitch_sign,orb_output,Anum=oc_mass,Znum=oc_charge,$
	SHOT=oc_shot,TIME=oc_time*1.e3,DIAG=oc_diag,EXPERIMENT=oc_exp,ED=oc_ed,NSTEPS=oc_nsteps,RFILD=oc_rini,$
    ZFILD=oc_zini,PHIFILD=oc_phiini,$
	EXPORT_PS=0,WRITE_ORBIT_FILES=wof_dir,GET_RHOPOL=0,NO_PLOT=1,$
    FORWARD_ORBIT=forw_orb

oc_r=orb_output.orbit.fout(3,*)
oc_z=orb_output.orbit.fout(5,*)
oc_phi=orb_output.orbit.fout(4,*)

print,'Orbit Calculation Finished'

;********************
; Plotting from here
;********************

; POLOIDAL


widget_control,uvalue.poloidal_plot_draw,get_value=draw_id
wset,draw_id

plot,oc_r,oc_z,color=0,background=255,psym=-3,/iso,$
    xrange=[1.0,2.5],yrange=[-1.5,1.5],xstyle=1,ystyle=1,charsize=1.5,$
    xtitle='R (m)',ytitle='Z (m)'

plots,oc_r(0),oc_z(0),psym=4,color=50
plots,oc_r(n_elements(oc_r)-1),oc_z(n_elements(oc_z)-1),psym=1,color=50


IF orb_calc_settings(1) EQ 1 THEN BEGIN

    oplot,orb_output.orbit.rcont0,orb_output.orbit.zcont0,color=150,psym=-3,thick=2.

ENDIF

IF orb_calc_settings(2) EQ 1 THEN BEGIN

    plot_aug_walls,orb_output.gfile,col=0

ENDIF

IF orb_calc_settings(3) EQ 1 THEN BEGIN

    oplot,orb_output.gfile.bdry(0,*),orb_output.gfile.bdry(1,*),psym=-3,color=100

ENDIF

IF orb_calc_settings(5) EQ 1 THEN BEGIN

    set_plot,'ps'
    fn=strtrim(settings.results_path,1)+'orbit_poloidal_'+strtrim(oc_shot,1)+'_'+strtrim(oc_time,1)+strtrim(systime(),1)+'.eps'
    device,filename=fn,/color

    plot,oc_r,oc_z,color=0,background=255,psym=-3,/iso,$
        xrange=[1.0,2.5],yrange=[-1.5,1.5],xstyle=1,ystyle=1,charsize=1.5,$
        xtitle='R (m)',ytitle='Z (m)'

    plots,oc_r(0),oc_z(0),psym=4,color=50
    plots,oc_r(n_elements(oc_r)-1),oc_z(n_elements(oc_z)-1),psym=1,color=50


    IF orb_calc_settings(1) EQ 1 THEN BEGIN

        oplot,orb_output.orbit.rcont0,orb_output.orbit.zcont0,color=150,psym=-3,thick=2.

    ENDIF


    IF orb_calc_settings(2) EQ 1 THEN BEGIN

        plot_aug_walls,orb_output.gfile,col=0

    ENDIF

    IF orb_calc_settings(3) EQ 1 THEN BEGIN

        oplot,orb_output.gfile.bdry(0,*),orb_output.gfile.bdry(1,*),psym=-3,color=100

    ENDIF

    device,/close
    print,'Plot exported as: ',fn
    set_plot,'X'

ENDIF



; TOROIDAL

widget_control,uvalue.toroidal_plot_draw,get_value=draw_id
wset,draw_id

plot,oc_phi MOD (2.*!pi),oc_r,color=0,background=255,psym=-3,$
    xrange=[0.0,2.*!pi],xstyle=1,yrange=[1.,2.5],ystyle=1,charsize=2.,$
    xtitle='Phi (rad)',ytitle='R (m)'

plots,oc_phi(0) MOD (2.*!pi),oc_r(0),psym=4,color=50
plots,oc_phi(n_elements(oc_r)-1) MOD (2.*!pi),oc_r(n_elements(oc_z)-1),psym=1,color=50

 IF orb_calc_settings(5) EQ 1 THEN BEGIN

     set_plot,'ps'
     fn=strtrim(settings.results_path,1)+'orbit_toroidal_'+strtrim(oc_shot,1)+'_'+strtrim(oc_time,1)+strtrim(systime(),1)+'.eps'
     device,filename=fn,/color

        plot,oc_phi MOD (2.*!pi),oc_r,color=0,background=255,psym=-3,$
        xrange=[0.0,2.*!pi],xstyle=1,yrange=[1.,2.5],ystyle=1,charsize=2.,$
        xtitle='Phi (rad)',ytitle='R (m)'

        plots,oc_phi(0) MOD (2.*!pi),oc_r(0),psym=4,color=50
        plots,oc_phi(n_elements(oc_r)-1) MOD (2.*!pi),oc_r(n_elements(oc_z)-1),psym=1,color=50

     print,'Plot exported as: ',fn
     set_plot,'X'

 ENDIF


END

;***************************************************************************************************************************************************

UVALUE.ORBIT_CALC_FILD_ENERGY: BEGIN

COMMON ORBIT,oc_shot,oc_time,oc_rini,oc_zini,oc_phiini,oc_energy,oc_pitch,oc_exp,oc_diag,oc_ed,$
    oc_r,oc_z,oc_phi,oc_mass,oc_charge,oc_nsteps,oc_fild_gyro

COMMON SETTINGS,settings


widget_control,uvalue.orb_shot,get_value=shot
widget_control,uvalue.orb_time,get_value=time
widget_control,uvalue.orb_exp,get_value=exp
widget_control,uvalue.orb_diag,get_value=diag
widget_control,uvalue.orb_ed,get_value=ed

widget_control,uvalue.orb_rini,get_value=rini
widget_control,uvalue.orb_zini,get_value=zini
widget_control,uvalue.orb_phiini,get_value=phiini
widget_control,uvalue.orb_energy,get_value=energy
widget_control,uvalue.orb_pitch,get_value=pitch
widget_control,uvalue.orb_mass,get_value=mass
widget_control,uvalue.orb_charge,get_value=charge
widget_control,uvalue.orb_nsteps,get_value=nsteps
widget_control,uvalue.orb_fild_gyro,get_value=fild_gyro


widget_control,uvalue.orb_checkbox_settings,get_value=orb_calc_settings

oc_shot=shot
oc_time=time
oc_rini=rini
oc_zini=zini
oc_phiini=phiini
oc_energy=energy
oc_pitch=pitch
oc_exp=exp
oc_diag=diag
oc_ed=ed
oc_mass=mass
oc_charge=charge
oc_nsteps=nsteps
oc_fild_gyro=fild_gyro


get_FILD_bfields,oc_rini,oc_zini,oc_shot,oc_time,br,bz,bt,b_total,interp=0
fild_energy=get_energy_FILD(fild_gyro,b_total,A=oc_mass,Z=oc_charge)

; convert to keV
fild_energy/=1.e3

widget_control,uvalue.orb_energy,set_value=fild_energy


END

;***************************************************************************************************************************************************


ELSE:RETURN

ENDCASE

END
