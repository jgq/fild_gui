@windowedfft4.pro

pro calculate_spectro,shot,diagname,signalname,tmin,tmax,nff,output,$
		overlap=overlap,fsmoth=fsmoth,$
		freqmax=freqmax,freqmin=freqmin,diagname2=diagname2,signalname2=signalname2,$
		hann=hann,detrend=detrend,coherence=coherence,power=power,crosspower=crosspower,$
		conflev=conflev,phase=phase,complexcp=complexcp,fsmotafter=fsmotafter,crosscoh=crosscoh,$
		zlog=zlog,zsqrt=zsqrt,color_table=color_table,save_eps=save_eps,$
        cont_plot=cont_plot

;=====================================================================================================
;                   J. Galdon               13.07.2021
;=====================================================================================================
;
; Routine to calculate spectrograms.
; Inherited from who-knows-where ...
; Modified by J.Galdon-Quiroga
;
;=====================================================================================================
;SHOT: number of the pulse
;DIAGNAME: name of diagnostic
;SIGNALLAME: name of signal
;TMIN & TMAX: time interval to be considered
;NFF: number of fast fourier transforms
;OVERLAP: % of overlapping between windows (i.e. 50 / 60 / 70)
;FSMOT: frequency range to smooth over
;OUTOUT: structure containing output information on spectrograms
;FREQMIN & FREQMAX: frequency range to be considered
;DIAGNAME2 & SIGNALNAME2: Add another signal to the analysis
;HANN: if 1, use hanning windows in FFT calculation
;DETREND: if 1, use linear detrending on signal
;COHERENCE: if 1, calculates the coherence of 2 signals
;POWER:
;CROSSPOWER: if 1, calculates the cross-power between 2 signals
;CONFLEV
;PHASE:
;COMPLEXCP:
;FSMOTAFTER: if 1, force smoothing of the signal after analysis.
;CROSSCOH: if 1, calculates the crosscoherence of signal 1 vs signal 2.
;ZLOG: if 1, set the amplitude of the signal to log scale.
;ZSQRT: if 1, set the amplitude of the signal to sqrt scale.
;COLOR_TABLE: number of the color table to be loaded. Default is 5.
;SAVE_EPS: save the figure to an eps file. (NOT IMPLEMENTED YET)
;=====================================================================================================

; Set default keywords

shot=shot
diagname=diagname & signalname=signalname
tmin=tmin & tmax=tmax
nff=nff

IF KEYWORD_SET(overlap) THEN overlap=overlap ELSE overlap=50.
IF KEYWORD_SET(fsmoth) THEN fsmot=fsmoth ELSE fsmot=1.e3
IF KEYWORD_SET(freqmax) THEN freqmax=freqmax ELSE freqmax=200.e3
IF KEYWORD_SET(freqmin) THEN freqmin=freqmin ELSE freqmin=1.e3

IF KEYWORD_SET(HANN) THEN hann=hann ELSE hann=1
IF KEYWORD_SET(DETREND) THEN detrend=detrend ELSE detrend=0

power=power
diagname2=diagname2 & signalname2=signalname2
coherence=coherence
crosspower=crosspower
conflev=conflev
phase=phase
complexcp=complexcp
fsmotafter=fsmotafter
crosscoh=crosscoh

save_eps=save_eps

ampsp=0. & ampsp_s1=0. & ampsp_s2=0.

IF keyword_set(color_table) THEN BEGIN

    loadct,color_table

ENDIF ELSE BEGIN

    loadct,5

ENDELSE
;*************************
;*****READ SIGNAL ********
;*************************

read_signal,ier,shot,diagname,signalname,t,dat,phys_unit,edition=0

IF ier NE 0 THEN return

IF keyword_set(diagname2) AND keyword_set(signalname2) THEN BEGIN

	read_signal,ier,shot,diagname2,signalname2,t2,dat2,phys_unit,edition=0

ENDIF


;*************************************************
;*****PERFORM FFT AND CALCULATE SPECTROGRAM*******
;*************************************************

windowedfft4,dat,t,nff,tmin,tmax,overlap,fsmot,ampsp,times,freq,output,plott=0,sigb=dat2,tb=t2,$
	    freqmin=freqmin,freqmax=freqmax,hann=hann,detrend=detrend,coherence=coherence,$
	    power=power,crosspower=crosspower,conflev=conflev,phase=phase,complexcp=complexcp,$
	    fsmotafter=fsmotafter,crosscoh=crosscoh

;*************************************************
;******************** PLOT ***********************
;*************************************************

print,'Output structure is: '
help,output,/str


IF keyword_set(zlog) THEN BEGIN
	output.ampsp=alog(output.ampsp)
	output.ampsp_s1=alog(output.ampsp_S1)
	output.ampsp_s2=alog(output.ampsp_S2)
ENDIF

IF keyword_set(zsqrt) THEN BEGIN
	output.ampsp=sqrt(output.ampsp)
	output.ampsp_s1=sqrt(output.ampsp_S1)
	output.ampsp_s2=sqrt(output.ampsp_S2)
ENDIF

s=coherence+crosspower+crosscoh

IF keyword_set(diagname2) AND keyword_set(signalname2) THEN BEGIN

	CASE 1 OF
	(s GT 0):BEGIN
		window,/free,xsize=1000,ysize=700
		!P.Multi=[0,2,2]

		contour,output.ampsp_s1,output.times,output.freq,nlevels=50,/fill,xstyle=1,ystyle=1,charsize=1.5,$
			xtitle='Time (s)',ytitle='Frequencies (Hz)',title=str(diagname)+'_'+str(signalname)+'; Pulse: '+str(shot)
		contour,output.ampsp_s2,output.times,output.freq,nlevels=50,/fill,xstyle=1,ystyle=1,charsize=1.5,$
			xtitle='Time (s)',ytitle='Frequencies (Hz)',title=str(diagname2)+'_'+str(signalname2)+'; Pulse: '+str(shot)
		contour,output.ampsp,output.times,output.freq,nlevels=50,/fill,xstyle=1,ystyle=1,charsize=1.5,$
			xtitle='Time (s)',ytitle='Frequencies (Hz)',title='2 signal Spectrogram'
		!P.Multi=0
		END
	ELSE: BEGIN
		window,/free,xsize=1000,ysize=500
		!P.Multi=[0,2]
		contour,output.ampsp_s1,output.times,output.freq,nlevels=50,/fill,xstyle=1,ystyle=1,charsize=1.5,$
			xtitle='Time (s)',ytitle='Frequencies (Hz)',title=str(diagname)+'_'+str(signalname)+'; Pulse: '+str(shot)
		contour,output.ampsp_s2,output.times,output.freq,nlevels=50,/fill,xstyle=1,ystyle=1,charsize=1.5,$
			xtitle='Time (s)',ytitle='Frequencies (Hz)',title=str(diagname2)+'_'+str(signalname2)+'; Pulse: '+str(shot)
		!P.Multi=0
	      END
	ENDCASE

ENDIF ELSE BEGIN

	IF keyword_set(save_eps) THEN BEGIN

		filnm=str(shot)+'_'+str(diagname)+'_'+str(signalname)+'.eps'
		bfss=8
		set_plot,'ps'
		device,filename=filnm,/color,$
		bits_per_pixel=8,xsize=20.,ysize=9.
		loadct,5

		IF keyword_set(color_table) THEN BEGIN
			loadct,color_table
		ENDIF


		nnlev=50
		lev=findgen(nnlev)/(nnlev-1)*(max(output.ampsp)-min(output.ampsp))+min(output.ampsp)
		col=findgen(nnlev)/(nnlev-1)*255
		; contour,output.ampsp,output.times,output.freq,levels=lev,c_colors=col,/fill,xstyle=1,ystyle=1,charsize=2.0,$
		; xtitle='Time (s)',ytitle='Frequencies (Hz)',title=str(diagname)+'_'+str(signalname)+'; Pulse: '+str(shot),color=0,background=255

        image,output.ampsp,output.times,output.freq,xstyle=1,ystyle=1,charsize=2.,$
            xtitle='Time (s)',ytitle='Frequencies (Hz)',title=str(diagname)+'_'+str(signalname)+'; Pulse: '+str(shot)



	ENDIF ELSE BEGIN

    IF KEYWORD_SET(CONT_PLOT) THEN BEGIN

         contour,output.ampsp,output.times,output.freq,nlevels=50,/fill,xstyle=1,ystyle=1,charsize=2.,$
         	xtitle='Time (s)',ytitle='Frequencies (Hz)',title=str(diagname)+'_'+str(signalname)+'; Pulse: '+str(shot)

    ENDIF ELSE BEGIN

        image,output.ampsp,output.times,output.freq,xstyle=1,ystyle=1,charsize=2.,$
            xtitle='Time (s)',ytitle='Frequencies (Hz)',title=str(diagname)+'_'+str(signalname)+'; Pulse: '+str(shot)

    ENDELSE


	ENDELSE
ENDELSE

IF keyword_set(save_eps) THEN BEGIN
	device,/close_file
	set_plot,'X'
ENDIF

return
end
