@readCIN.pro
@readPulse.pro
@bpass.pro
@feature.pro

@FILDSIM_read_strike_map.pro
@FILDSIM_read_strike_points.pro
@FILDSIM_read_plate.pro
@FILDSIM_read_std_set.pro
@FILDSIM_plot_strike_map.pro
@FILDSIM_write_namelist.pro
@FILDSIM_calculate_fild_orientation.pro
@FILDSIM_calculate_pitch_resolution.pro
@FILDSIM_calculate_pitch_resolution_v2.pro
@FILDSIM_calculate_gyroradius_resolution.pro
@FILDSIM_calculate_gyroradius_resolution_v2.pro
@FILDSIM_calculate_all_resolutions.pro
@FILDSIM_save_standard_namelist.pro

@FILD_read_calibration_database.pro
@FILD_read_PCO_info.pro
@FILD_get_pixel_coord.pro
@FILD_get_calibration.pro
@FILD_interp_grid.pro
@FILD_pitch_profile.pro
@FILD_gyroradius_profile.pro
@FILD_remap_grid.pro
@FILD_overlay_image.pro
@FILD_overlay_pix.pro
@FILD_video_ccd.pro
@FILD_video_phantom.pro

@FILDSIM_build_weight_matrix.pro
@FILD_synthetic.pro
@FILD_synthetic_matrix_method.pro

@FILD_read_scintillator_efficiency
@FILD_calculate_absolute_flux
@FILD_calculate_photon_flux
@FILD_calculate_ion_flux
@FILD_calculate_absolute_calibration_frame
@FILD_estimate_effective_pixel_area
@FILD_calculate_absolute_flux_whole_shot_phantom

@FILDSIM_camera_properties.pro
@FILDSIM_synthetic_frame.pro

@FILDSIM_tomography.pro


@read_ASCOT_distribution.pro
@read_artifitial_distribution.pro
@get_FILD_bfields.pro
@rewrite_ASCOT_input.pro
@FILD_export_timetrace_ascii.pro

@get_FILD_back_orbit.pro
@int.pro



;@fild_gui_event.pro

; IF you encounter the error: "Attempt to extend common block", reset session using: > .full_reset_session
; Make sure that the first compiled definition of the common block is the one in fild_gui, and not in fild_gui_event


PRO fild_gui

print,'**********************************************************'
print,'STARTING FILD GRAPHICAL USER INTERFACE'
print,'Contact: J.Galdon // jgaldon@us.es // joaquin.galdon@ipp.mpg.de // Last update 29.11.2018'
print,'**********************************************************'

loadct,5

DEVICE, GET_SCREEN_SIZE = s, decomposed=0
xscr_size=s(0)*0.8 & yscr_size=s(1)*0.8
frame_x_size=640
frame_y_size=480


;***CREATE BASE***
main_base = WIDGET_BASE(TITLE='FILD GRAPHICAL USER INTERFACE v2', /FRAME,MBAR=menubar,tlb_frame_attr=1,x_scroll_size=xscr_size,y_scroll_size=yscr_size)

;***** MENU BAR **********

file_menu=WIDGET_BUTTON(menubar, menu=1,value="FILE")
	exit_button=WIDGET_BUTTON(file_menu,value='EXIT',/SEPARATOR)
	load_color_table_button=WIDGET_BUTTON(file_menu,value='LOADCT',/SEPARATOR)
	open_movie_button=WIDGET_BUTTON(file_menu,value='OPEN_MOVIE',/SEPARATOR)
	open_frame_button=WIDGET_BUTTON(file_menu,value='OPEN_FRAME',/SEPARATOR)
	create_video_phantom_button=WIDGET_BUTTON(file_menu,value='CREATE_VIDEO_PHANTOM',/SEPARATOR)
	create_video_ccd_button=WIDGET_BUTTON(file_menu,value='CREATE_VIDEO_CCD',/SEPARATOR)
	phantom_gyroradius_profile_video=WIDGET_BUTTON(file_menu,value='CREATE_VIDEO_PHANTOM_GYR_PROFILE',/SEPARATOR)
	phantom_pitch_profile_video=WIDGET_BUTTON(file_menu,value='CREATE_VIDEO_PHANTOM_PITCH_PROFILE',/SEPARATOR)
	phantom_remap_shot=WIDGET_BUTTON(file_menu,value='PHANTOM REMAP WHOLE SHOT',/SEPARATOR)
	ccd_remap_shot=WIDGET_BUTTON(file_menu,value='CCD REMAP WHOLE SHOT',/SEPARATOR)


grid_cal=WIDGET_BUTTON(menubar,menu=1,value='GRID CALIBRATION',/SEPARATOR)

	get_calibration_button=WIDGET_BUTTON(grid_cal,value='GET CALIBRATION',/SEPARATOR)
	save_calibration_parameters=WIDGET_BUTTON(grid_cal,value='SAVE CALIBRATION PARAMETERS',/SEPARATOR)
	plot_ginterp=WIDGET_BUTTON(grid_cal,value='PLOT INTERPOLATED GYRO',/SEPARATOR)
	plot_pinterp=WIDGET_BUTTON(grid_cal,value='PLOT INTERPOLATED PITCH',/SEPARATOR)
	plot_cinterp=WIDGET_BUTTON(grid_cal,value='PLOT INTERPOLATED COLIMATING FACTOR',/SEPARATOR)
	select_fildsim_grid=WIDGET_BUTTON(grid_cal,value='SELECT FILDSIM GRID',/SEPARATOR)
	select_grid_image=WIDGET_BUTTON(grid_cal,value='SELECT GRID IMAGE',/SEPARATOR)
    select_scintillator_plate=WIDGET_BUTTON(grid_cal,value='SELECT SCINTILLATOR PLATE',/SEPARATOR)

save_idl=WIDGET_BUTTON(menubar,menu=1,value='SAVE IDL VARS',/SEPARATOR)

	save_fildsim_output_button=WIDGET_BUTTON(save_idl,value='SAVE FILDSIM OUTPUT',/SEPARATOR)
	save_idl_strike_map_resolutions=WIDGET_BUTTON(save_idl,value='SAVE STRIKE MAP RESOLUTIONS',/SEPARATOR)
	save_interpolated_data=WIDGET_BUTTON(save_idl,value='SAVE INTERPOLATED DATA',/SEPARATOR)
	save_strike_map_pixels=WIDGET_BUTTON(save_idl,value='SAVE STRIKE MAP PIXELS',/SEPARATOR)
	save_remap_vars=WIDGET_BUTTON(save_idl,value='SAVE REMAP VARS',/SEPARATOR)
	save_absolute_flux_vars=WIDGET_BUTTON(save_idl,value='SAVE ABSOLUTE FLUX VARS',/SEPARATOR)
	save_ccd_timetrace=WIDGET_BUTTON(save_idl,value='SAVE CCD TIMETRACE',/SEPARATOR)
	save_phantom_timetrace=WIDGET_BUTTON(save_idl,value='SAVE PHANTOM TIMETRACE',/SEPARATOR)



export_eps=WIDGET_BUTTON(menubar,menu=1,value='EXPORT',/SEPARATOR)
	save_image_button=WIDGET_BUTTON(export_eps,value='SAVE IMAGE CURRENT TAB PNG',/SEPARATOR)
	save_fildsim_eps_button=WIDGET_BUTTON(export_eps,value='EXPORT FILDSIM INPUT & OUTPUT EPS',/SEPARATOR)
	export_eps_ccd_remap=WIDGET_BUTTON(export_eps,value='EXPORT CCD REMAP EPS',/SEPARATOR)
	export_eps_phantom_remap=WIDGET_BUTTON(export_eps,value='EXPORT PHANTOM REMAP EPS',/SEPARATOR)
	export_eps_phantom_strike_map=WIDGET_BUTTON(export_eps,value='EXPORT PHANTOM FRAME STRIKE MAP EPS',/SEPARATOR)
	export_eps_absolute_flux=WIDGET_BUTTON(export_eps,value='EXPORT ABSOLUTE FLUX EPS',/SEPARATOR)
	export_ccd_timetrace_ascii=WIDGET_BUTTON(export_eps,value='EXPORT CCD TIMETRACE ASCII',/SEPARATOR)
	export_phantom_timetrace_ascii=WIDGET_BUTTON(export_eps,value='EXPORT PHANTOM TIMETRACE ASCII',/SEPARATOR)


absolute_flux=WIDGET_BUTTON(menubar,menu=1,value='ABSOLUTE FLUX ESTIMATION',/SEPARATOR)
	af_print_info_button=WIDGET_BUTTON(absolute_flux,value='PRINT INFO',/SEPARATOR)
	af_select_calibration_frame_button=WIDGET_BUTTON(absolute_flux,value='SELECT CALIBRATION FRAME',/SEPARATOR)
	af_select_efficiency_file_button=WIDGET_BUTTON(absolute_flux,value='SELECT EFFICIENCY FILE',/SEPARATOR)


;****** MAIN PANEL ******

main_panel=widget_base(main_base,title='main panel',COLUMN=2)

settings_base=WIDGET_BASE(main_panel,title='Settings',COLUMN=1)
	shot_field=CW_FIELD(settings_base,title='SHOT:   ',value=-1,/LONG,/RETURN_EVENTS)
	fild_field=CW_FIELD(settings_base,title='FILD:   ',value=-1,/INT,/RETURN_EVENTS)


	xscale_field=CW_FIELD(settings_base,title='XSCALE: ',value=1.,/FLOAT,/ALL_EVENTS)
	yscale_field=CW_FIELD(settings_base,title='YSCALE: ',value=1.,/FLOAT,/ALL_EVENTS)
	xshift_field=CW_FIELD(settings_base,title='XSHIFT: ',value=1.,/FLOAT,/ALL_EVENTS)
	yshift_field=CW_FIELD(settings_base,title='YSHIFT: ',value=1.,/FLOAT,/ALL_EVENTS)
	deg_field=CW_FIELD(settings_base,title='DEG:    ',value=0.,/FLOAT,/ALL_EVENTS)

	checkbox_options=['FILTER','REMAP','OVERLAY GRID','PROFILES','Phantom: PIXEL DEPTH 8']
	checkbox_settings=CW_BGROUP(settings_base,checkbox_options,set_value=[0,0,0,0,0],/COLUMN,/NONEXCLUSIVE)

	checkbox_options_2=['OVERLAY IMAGE MODE','OVERLAY PIXEL MODE']
	checkbox_settings_2=CW_BGROUP(settings_base,checkbox_options_2,set_value=1,/COLUMN,/EXCLUSIVE)

tabs_base=WIDGET_BASE(main_panel)

tab_1=WIDGET_TAB(tabs_base)

	ccd_base=WIDGET_BASE(tab_1,title='CCD ANALYSIS',ROW=2)
		ccd_fr_rem_base=WIDGET_BASE(ccd_base,COLUMN=2)

			ccd_frame_base=WIDGET_BASE(ccd_fr_rem_base,COLUMN=1)

				ccd_frame_draw=WIDGET_DRAW(ccd_frame_base,/button_events,/MOTION_EVENTS,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
				ccd_frame_slider=WIDGET_SLIDER(ccd_frame_base,SENSITIVE=0,/drag)
				ccd_saturation_slider=WIDGET_SLIDER(ccd_frame_base,SENSITIVE=0,/drag,title='Saturation Level')

				ccd_frame_info=WIDGET_BASE(ccd_frame_base,COLUMN=5)
					ccd_frame_info_frame= WIDGET_LABEL(ccd_frame_info,value='Frame=',/dynamic_resize)
					ccd_frame_info_time= WIDGET_LABEL(ccd_frame_info,value='Time=',/dynamic_resize)
					ccd_frame_xpixel=WIDGET_LABEL(ccd_frame_info,value='',/dynamic_resize)
					ccd_frame_ypixel=WIDGET_LABEL(ccd_frame_info,value='',/dynamic_resize)
					ccd_frame_intensity=WIDGET_LABEL(ccd_frame_info,value='Intensity=',/dynamic_resize)
				plot_grid_button=WIDGET_BUTTON(ccd_frame_base,value='PLOT GRID',frame=1,SENSITIVE=0)
				set_background_button=WIDGET_BUTTON(ccd_frame_base,value='SET BACKGROUND',SENSITIVE=1)
				substract_background_button=WIDGET_BUTTON(ccd_frame_base,value='SUBSTRACT BACKGROUND')
				ccd_timetrace_button=WIDGET_BUTTON(ccd_frame_base,value='CALCULATE TIMETRACE')


			ccd_remap_base=WIDGET_BASE(ccd_fr_rem_base,COLUMN=1)

				ccd_remap_draw=WIDGET_DRAW(ccd_remap_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size,/ALIGN_CENTER)
				ccd_remap_button=WIDGET_BUTTON(ccd_remap_base,value='REMAP',frame=1,SENSITIVE=0)
				ccd_remap_settings_base=WIDGET_BASE(ccd_remap_base,COLUMN=2)
					ccd_remap_settings_pitch_base=WIDGET_BASE(ccd_remap_settings_base,ROW=4)
						settings_minpitch=CW_FIELD(ccd_remap_settings_pitch_base,title='MIN PITCH:   ',value=10.,/FLOAT,/ALL_EVENTS)
						settings_maxpitch=CW_FIELD(ccd_remap_settings_pitch_base,title='MAX PITCH:   ',value=90.,/FLOAT,/ALL_EVENTS)
						settings_dpitch=CW_FIELD(ccd_remap_settings_pitch_base,title='DELTA PITCH: ',value=1.,/FLOAT,/ALL_EVENTS)
						settings_remap_levels=CW_FIELD(ccd_remap_settings_pitch_base,title='N LEVELS',value=255,/INT,/ALL_EVENTS)

					ccd_remap_settings_gyr_base=WIDGET_BASE(ccd_remap_settings_base,ROW=3)
						settings_mingyr=CW_FIELD(ccd_remap_settings_gyr_base,title='MIN GYR:   ',value=1.,/FLOAT,/ALL_EVENTS)
						settings_maxgyr=CW_FIELD(ccd_remap_settings_gyr_base,title='MAX GYR:   ',value=10.,/FLOAT,/ALL_EVENTS)
						settings_dgyr=CW_FIELD(ccd_remap_settings_gyr_base,title='DELTA GYR: ',value=0.1,/FLOAT,/ALL_EVENTS)


		ccd_profile_base=WIDGET_BASE(ccd_base,COLUMN=2)

			ccd_gyroradius_profile_base=WIDGET_BASE(ccd_profile_base,COLUMN=2)

				ccd_gyroradius_profile_settings_base=WIDGET_BASE(ccd_gyroradius_profile_base,ROW=5)
					gyroradius_profile_minpitch=CW_FIELD(ccd_gyroradius_profile_settings_base,title='MIN PITCH: ',value=10.,/FLOAT,/ALL_EVENTS)
					gyroradius_profile_maxpitch=CW_FIELD(ccd_gyroradius_profile_settings_base,title='MAX PITCH: ',value=90.,/FLOAT,/ALL_EVENTS)
					gyroradius_profile_mingyr=CW_FIELD(ccd_gyroradius_profile_settings_base,title='MIN GYR:   ',value=1.,/FLOAT,/ALL_EVENTS)
					gyroradius_profile_maxgyr=CW_FIELD(ccd_gyroradius_profile_settings_base,title='MAX GYR:   ',value=10.,/FLOAT,/ALL_EVENTS)
					gyroradius_profile_maxint=CW_FIELD(ccd_gyroradius_profile_settings_base,title='MAX INT:   ',value=1.e7,/FLOAT,/ALL_EVENTS)
				ccd_gyroradius_profile_settings_base2=WIDGET_BASE(ccd_gyroradius_profile_base,COLUMN=1)
					ccd_gyroradius_profile_draw=WIDGET_DRAW(ccd_gyroradius_profile_settings_base2,/button_events,/keyboard_events,xsize=xscr_size*0.2,ysize=xscr_size*0.2)
					ccd_gyroradius_profile_plot_button=WIDGET_BUTTON(ccd_gyroradius_profile_settings_base2,value='REPLOT',frame=1,SENSITIVE=0)



			ccd_pitch_profile_base=WIDGET_BASE(ccd_profile_base,COLUMN=2)

				ccd_pitch_profile_settings_base2=WIDGET_BASE(ccd_pitch_profile_base,COLUMN=1)
					ccd_pitch_profile_draw=WIDGET_DRAW(ccd_pitch_profile_settings_base2,/button_events,/keyboard_events,xsize=xscr_size*0.2,ysize=xscr_size*0.2)
					ccd_pitch_profile_plot_button=WIDGET_BUTTON(ccd_pitch_profile_settings_base2,value='REPLOT',frame=1,SENSITIVE=0)

				ccd_pitch_profile_settings_base=WIDGET_BASE(ccd_pitch_profile_base,ROW=5)
					pitch_profile_minpitch=CW_FIELD(ccd_pitch_profile_settings_base,title='MIN PITCH: ',value=10.,/FLOAT,/ALL_EVENTS)
					pitch_profile_maxpitch=CW_FIELD(ccd_pitch_profile_settings_base,title='MAX PITCH: ',value=90.,/FLOAT,/ALL_EVENTS)
					pitch_profile_mingyr=CW_FIELD(ccd_pitch_profile_settings_base,title='MIN GYR:   ',value=1.,/FLOAT,/ALL_EVENTS)
					pitch_profile_maxgyr=CW_FIELD(ccd_pitch_profile_settings_base,title='MAX GYR:   ',value=10.,/FLOAT,/ALL_EVENTS)
					pitch_profile_maxint=CW_FIELD(ccd_pitch_profile_settings_base,title='MAX INT:   ',value=1.e7,/FLOAT,/ALL_EVENTS)


	phantom_base=WIDGET_BASE(tab_1,title='PHANTOM ANALYSIS',ROW=2)
		phantom_fr_rem_base=WIDGET_BASE(phantom_base,COLUMN=2)

			phantom_frame_base=WIDGET_BASE(phantom_fr_rem_base,COLUMN=1)

				phantom_draw=WIDGET_DRAW(phantom_frame_base,/button_events,/MOTION_EVENTS,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
				phantom_slider=WIDGET_SLIDER(phantom_frame_base,SENSITIVE=0,/drag)
				phantom_info=WIDGET_BASE(phantom_frame_base,COLUMN=5)
					phantom_info_frame=WIDGET_LABEL(phantom_info,value='Frame=',/dynamic_resize)
					phantom_info_time=WIDGET_LABEL(phantom_info,value='Time=',/dynamic_resize)
					phantom_info_xpixel=WIDGET_LABEL(phantom_info,value='',/dynamic_resize)
					phantom_info_ypixel=WIDGET_LABEL(phantom_info,value='',/dynamic_resize)
					phantom_info_intensity=WIDGET_LABEL(phantom_info,value='Intensity=',/dynamic_resize)

				phantom_plot_grid_button=WIDGET_BUTTON(phantom_frame_base,value='PLOT GRID',frame=1,SENSITIVE=0)
				phantom_set_background_button=WIDGET_BUTTON(phantom_frame_base,value='SET BACKGROUND')
				phantom_substract_background_button=WIDGET_BUTTON(phantom_frame_base,value='SUBSTRACT BACKGROUND')
				phantom_calculate_timetrace_button=WIDGET_BUTTON(phantom_frame_base,value='CALCULATE TIMETRACE')
				phantom_time_settings_base=WIDGET_BASE(phantom_frame_base,ROW=2)
					phantom_initial_time_button=CW_FIELD(phantom_time_settings_base,title='Initial time (s)',value=-1.,/FLOAT,/ALL_EVENTS)
					phantom_final_time_button=CW_FIELD(phantom_time_settings_base,title='Final time (s)',value=-1.,/FLOAT,/ALL_EVENTS)

			phantom_remap_base=WIDGET_BASE(phantom_fr_rem_base,COLUMN=1)

				phantom_remap_draw=WIDGET_DRAW(phantom_remap_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size,/ALIGN_CENTER)
				phantom_remap_button=WIDGET_BUTTON(phantom_remap_base,value='REMAP',frame=1,SENSITIVE=0)
				phantom_remap_settings_base=WIDGET_BASE(phantom_remap_base,COLUMN=2)
					phantom_remap_settings_pitch_base=WIDGET_BASE(phantom_remap_settings_base,ROW=4)
						phantom_settings_minpitch=CW_FIELD(phantom_remap_settings_pitch_base,title='MIN PITCH:   ',value=10.,/FLOAT,/ALL_EVENTS)
						phantom_settings_maxpitch=CW_FIELD(phantom_remap_settings_pitch_base,title='MAX PITCH:   ',value=90.,/FLOAT,/ALL_EVENTS)
						phantom_settings_dpitch=CW_FIELD(phantom_remap_settings_pitch_base,title='DELTA PITCH: ',value=1.,/FLOAT,/ALL_EVENTS)
						phantom_settings_remap_levels=CW_FIELD(phantom_remap_settings_pitch_base,title='N LEVELS',value=255,/INT,/ALL_EVENTS)

					phantom_remap_settings_gyr_base=WIDGET_BASE(phantom_remap_settings_base,ROW=3)
						phantom_settings_mingyr=CW_FIELD(phantom_remap_settings_gyr_base,title='MIN GYR:   ',value=1.,/FLOAT,/ALL_EVENTS)
						phantom_settings_maxgyr=CW_FIELD(phantom_remap_settings_gyr_base,title='MAX GYR:   ',value=10.,/FLOAT,/ALL_EVENTS)
						phantom_settings_dgyr=CW_FIELD(phantom_remap_settings_gyr_base,title='DELTA GYR: ',value=0.1,/FLOAT,/ALL_EVENTS)


		phantom_profile_base=WIDGET_BASE(phantom_base,COLUMN=2)


			phantom_gyroradius_profile_base=WIDGET_BASE(phantom_profile_base,COLUMN=2)

				phantom_gyroradius_profile_settings_base=WIDGET_BASE(phantom_gyroradius_profile_base,COLUMN=1)
					phantom_gyroradius_profile_minpitch=CW_FIELD(phantom_gyroradius_profile_settings_base,title='MIN PITCH: ',value=10.,/FLOAT,/ALL_EVENTS)
					phantom_gyroradius_profile_maxpitch=CW_FIELD(phantom_gyroradius_profile_settings_base,title='MAX PITCH: ',value=90.,/FLOAT,/ALL_EVENTS)
					phantom_gyroradius_profile_mingyr=CW_FIELD(phantom_gyroradius_profile_settings_base,title='MIN GYR:   ',value=1.,/FLOAT,/ALL_EVENTS)
					phantom_gyroradius_profile_maxgyr=CW_FIELD(phantom_gyroradius_profile_settings_base,title='MAX GYR:   ',value=10.,/FLOAT,/ALL_EVENTS)
					phantom_gyroradius_profile_maxint=CW_FIELD(phantom_gyroradius_profile_settings_base,title='MAX INT:   ',value=1.e7,/FLOAT,/ALL_EVENTS)
				phantom_gyroradius_profile_settings_base2=WIDGET_BASE(phantom_gyroradius_profile_base,COLUMN=1)
					phantom_gyroradius_profile_draw=WIDGET_DRAW(phantom_gyroradius_profile_settings_base2,/button_events,/keyboard_events,xsize=xscr_size*0.2,ysize=xscr_size*0.2)
					phantom_gyroradius_profile_plot_button=WIDGET_BUTTON(phantom_gyroradius_profile_settings_base2,value='REPLOT',frame=1,SENSITIVE=0)



			phantom_pitch_profile_base=WIDGET_BASE(phantom_profile_base,COLUMN=2)

				phantom_pitch_profile_settings_base2=WIDGET_BASE(phantom_pitch_profile_base,COLUMN=1)
					phantom_pitch_profile_draw=WIDGET_DRAW(phantom_pitch_profile_settings_base2,/button_events,/keyboard_events,xsize=xscr_size*0.2,ysize=xscr_size*0.2)
					phantom_pitch_profile_plot_button=WIDGET_BUTTON(phantom_pitch_profile_settings_base2,value='REPLOT',frame=1,SENSITIVE=0)

				phantom_pitch_profile_settings_base=WIDGET_BASE(phantom_pitch_profile_base,COLUMN=1)
					phantom_pitch_profile_minpitch=CW_FIELD(phantom_pitch_profile_settings_base,title='MIN PITCH: ',value=10.,/FLOAT,/ALL_EVENTS)
					phantom_pitch_profile_maxpitch=CW_FIELD(phantom_pitch_profile_settings_base,title='MAX PITCH: ',value=90.,/FLOAT,/ALL_EVENTS)
					phantom_pitch_profile_mingyr=CW_FIELD(phantom_pitch_profile_settings_base,title='MIN GYR:   ',value=1.,/FLOAT,/ALL_EVENTS)
					phantom_pitch_profile_maxgyr=CW_FIELD(phantom_pitch_profile_settings_base,title='MAX GYR:   ',value=10.,/FLOAT,/ALL_EVENTS)
					phantom_pitch_profile_maxint=CW_FIELD(phantom_pitch_profile_settings_base,title='MAX INT:   ',value=1.e7,/FLOAT,/ALL_EVENTS)

	spectrogram_base=WIDGET_BASE(tab_1,title='SPECTROGRAMS')


	fildsim_base=WIDGET_BASE(tab_1,title='FILDSIM',ROW=2)

	fildsim_base_2=WIDGET_BASE(fildsim_base,title='FILDSIM',COLUMN=2)

		fildsim_plot_base=WIDGET_BASE(fildsim_base_2,COLUMN=1)
			fildsim_input_label=WIDGET_LABEL(fildsim_plot_base,value='INPUT DISTRIBUTION FOR FILDSIM')
			fildsim_input_draw=WIDGET_DRAW(fildsim_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
			fildsim_output_label=WIDGET_LABEL(fildsim_plot_base,value='OUTPUT DISTRIBUTION FROM FILDSIM')
			fildsim_output_draw=WIDGET_DRAW(fildsim_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
			fildsim_run_button=WIDGET_BUTTON(fildsim_plot_base,value='RUN SIMULATION',frame=1)
			fildsim_slider=WIDGET_SLIDER(fildsim_plot_base,SENSITIVE=0,/drag,title='Saturation Level')




		fildsim_settings_base_main=WIDGET_BASE(fildsim_base_2,/COLUMN)


			fildsim_grid_param_base=WIDGET_BASE(fildsim_settings_base_main,COLUMN=1,frame=1)

				fildsim_grid_label=WIDGET_LABEL(fildsim_grid_param_base,value='GRID PARAMETERS')
				fildsim_grid_mingyr=CW_FIELD(fildsim_grid_param_base,title='MIN GYR:    ',value=1.,/FLOAT,/ALL_EVENTS)
				fildsim_grid_maxgyr=CW_FIELD(fildsim_grid_param_base,title='MAX GYR:    ',value=10.,/FLOAT,/ALL_EVENTS)
				fildsim_grid_dg=CW_FIELD(fildsim_grid_param_base,title='DELTA GYR:  ',value=0.1,/FLOAT,/ALL_EVENTS)
				fildsim_grid_minpitch=CW_FIELD(fildsim_grid_param_base,title='MIN PITCH:  ',value=10.,/FLOAT,/ALL_EVENTS)
				fildsim_grid_maxpitch=CW_FIELD(fildsim_grid_param_base,title='MAX PITCH:  ',value=90.,/FLOAT,/ALL_EVENTS)
				fildsim_grid_dp=CW_FIELD(fildsim_grid_param_base,title='DELTA PITCH:',value=1.,/FLOAT,/ALL_EVENTS)
				fildsim_grid_nlev=CW_FIELD(fildsim_grid_param_base,title='NLEV:       ',value=255,/INT,/ALL_EVENTS)


			fildsim_settings_base=WIDGET_BASE(fildsim_settings_base_main,/COLUMN)


				fildsim_code_base=WIDGET_BASE(fildsim_settings_base,/COLUMN,frame=1)

					checkbox_codes=['NONE','ASCOT','ARTIFITIAL DIST.','LOAD INPUT MATRIX']
					checkbox_codes_settings=CW_BGROUP(fildsim_code_base,checkbox_codes,set_value=1,/COLUMN,/EXCLUSIVE,$
							label_top='INPUT CODE')

				fildsim_options_base=WIDGET_BASE(fildsim_settings_base,/COLUMN,frame=1)

					fildsim_checkbox_options=['Change ASCOT pitch sign','Smooth Input','Add Scintillator Efficiency','GAUSSIAN MODEL','SAVE OUTPUT']
					fildsim_checkbox_settings=CW_BGROUP(fildsim_options_base,fildsim_checkbox_options,set_value=[1,0,0,0,0],/COLUMN,/NONEXCLUSIVE)


				fildsim_magnetic_base=WIDGET_BASE(fildsim_settings_base,/COLUMN,frame=1)
					fildsim_shot=CW_FIELD(fildsim_magnetic_base,title='SHOT:       ',value=0,/LONG,/ALL_EVENTS)
					fildsim_time=CW_FIELD(fildsim_magnetic_base,title='TIME:       ',value=0.,/FLOAT,/ALL_EVENTS)
					fildsim_magnetic=CW_FIELD(fildsim_magnetic_base,title='B FIELD (T):',value=1.,/FLOAT,/ALL_EVENTS)
					fildsim_r_fild=CW_FIELD(fildsim_magnetic_base,title='R FILD (m): ',value=2.18,/FLOAT,/ALL_EVENTS)
					fildsim_z_fild=CW_FIELD(fildsim_magnetic_base,title='Z FILD (m): ',value=0.32,/FLOAT,/ALL_EVENTS)
					fildsim_smooth_input_fild=CW_FIELD(fildsim_magnetic_base,title='Smooth Input Dist: ',value=2,/FLOAT,/ALL_EVENTS)


				fildsim_buttons_base=WIDGET_BASE(fildsim_settings_base,/COLUMN)
					fildsim_get_input_file=WIDGET_BUTTON(fildsim_buttons_base,value='GET INPUT FILE',frame=1)
					fildsim_get_aug_bfield=WIDGET_BUTTON(fildsim_buttons_base,value='GET AUG B FIELD',frame=1)
					fildsim_load_fildsim_run=WIDGET_BUTTON(fildsim_buttons_base,value='LOAD FILDSIM RUN',frame=1)


		fildsim_current_input=WIDGET_LABEL(fildsim_base,value='CURRENT INPUT FILENAME: NONE',/dynamic_resize)



	fild_synthetic_frame_base=WIDGET_BASE(tab_1,title='FILD SYNTHETIC FRAME',COLUMN=2)

		fild_synthetic_frame_plot_base=WIDGET_BASE(fild_synthetic_frame_base,/COLUMN)
			fild_synthetic_frame_plot_label_1=WIDGET_LABEL(fild_synthetic_frame_plot_base,value='SYNTHETIC SCINTILLATOR')
			fild_synthetic_frame_plot_draw_1=WIDGET_DRAW(fild_synthetic_frame_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)


			fild_synthetic_frame_plot_label_2=WIDGET_LABEL(fild_synthetic_frame_plot_base,value='SYNTHETIC FRAME')
			fild_synthetic_frame_plot_draw_2=WIDGET_DRAW(fild_synthetic_frame_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)


		fild_synthetic_buttons_base=WIDGET_BASE(fild_synthetic_frame_base,/COLUMN)
			fild_synthetic_frame_run=WIDGET_BUTTON(fild_synthetic_buttons_base,value='RUN SYNTHETIC FRAME',frame=1)
			fild_synthetic_frame_set_input=WIDGET_BUTTON(fild_synthetic_buttons_base,value='SET INPUT',frame=1)
			fild_synthetic_frame_set_strike_map=WIDGET_BUTTON(fild_synthetic_buttons_base,value='SET STRIKE MAP',frame=1)
			fild_synthetic_frame_set_scintillator_file=WIDGET_BUTTON(fild_synthetic_buttons_base,value='SET SCINTILLATOR FILE',frame=1)
			fild_synthetic_frame_set_scintillator_yield=WIDGET_BUTTON(fild_synthetic_buttons_base,value='SET SCINTILLATOR YIELD',frame=1)
			fild_synthetic_frame_load_camera_properties=WIDGET_BUTTON(fild_synthetic_buttons_base,value='LOAD CAMERA PROPERTIES',frame=1)



		fild_synthetic_frame_settings_base_main=WIDGET_BASE(fild_synthetic_frame_base,/COLUMN)

			fild_synthetic_frame_main_param_base=WIDGET_BASE(fild_synthetic_frame_settings_base_main,/COLUMN,frame=1)

				fild_synthetic_frame_main_param_label=WIDGET_LABEL(fild_synthetic_frame_main_param_base,value='PARAMETERS')
				fild_synthetic_frame_input=CW_FIELD(fild_synthetic_frame_main_param_base,title='Input:    ',value='',/STRING,/ALL_EVENTS)
				fild_synthetic_frame_strike_map=CW_FIELD(fild_synthetic_frame_main_param_base,title='Strike Map:    ',value='',/STRING,/ALL_EVENTS)
				fild_synthetic_frame_scintillator_file=CW_FIELD(fild_synthetic_frame_main_param_base,title='Scintillator file:    ',value='',/STRING,/ALL_EVENTS)
				fild_synthetic_frame_scintillator_yield=CW_FIELD(fild_synthetic_frame_main_param_base,title='Scintillator yield:    ',value='',/STRING,/ALL_EVENTS)
				fild_synthetic_frame_geometrical_factor=CW_FIELD(fild_synthetic_frame_main_param_base,title='Geometrical factor:    ',value=1,/FLOAT,/ALL_EVENTS)
				fild_synthetic_frame_exp_time=CW_FIELD(fild_synthetic_frame_main_param_base,title='Exposure Time (s):    ',value=0.001,/FLOAT,/ALL_EVENTS)



			fild_synthetic_camera_base=WIDGET_BASE(fild_synthetic_frame_settings_base_main,/COLUMN,frame=1)
				fild_synthetic_frame_camera_label=WIDGET_LABEL(fild_synthetic_camera_base,value='CAMERA PROPERTIES')
				fild_synthetic_frame_camera_name=CW_FIELD(fild_synthetic_camera_base,title='Camera Name:    ',value='1',/ALL_EVENTS)
				fild_synthetic_frame_camera_nxpixels=CW_FIELD(fild_synthetic_camera_base,title='NX PIXELS:    ',/LONG,/ALL_EVENTS)
				fild_synthetic_frame_camera_nypixels=CW_FIELD(fild_synthetic_camera_base,title='NY PIXELS:    ',/LONG,/ALL_EVENTS)
				fild_synthetic_frame_camera_pixel_xsize=CW_FIELD(fild_synthetic_camera_base,title='PIXEL XSIZE (cm):    ',/FLOAT,/ALL_EVENTS)
				fild_synthetic_frame_camera_pixel_ysize=CW_FIELD(fild_synthetic_camera_base,title='PIXEL YSIZE (cm):    ',/FLOAT,/ALL_EVENTS)
				fild_synthetic_frame_camera_pixel_qefficiency=CW_FIELD(fild_synthetic_camera_base,title='QUANTUM EFF (Normalized):    ',/FLOAT,/ALL_EVENTS)
				fild_synthetic_frame_camera_pixel_AD=CW_FIELD(fild_synthetic_camera_base,title='A/D factor (electrons/count):    ',/FLOAT,/ALL_EVENTS)
				fild_synthetic_frame_camera_pixel_dynamic_range=CW_FIELD(fild_synthetic_camera_base,title='DYNAMIC RANGE (bits):    ',/FLOAT,/ALL_EVENTS)


			fild_synthetic_optics_base=WIDGET_BASE(fild_synthetic_frame_settings_base_main,/COLUMN,frame=1)
				fild_synthetic_frame_optics_label=WIDGET_LABEL(fild_synthetic_optics_base,value='OPTICAL PROPERTIES')
				fild_synthetic_frame_optics_transmission=CW_FIELD(fild_synthetic_optics_base,title='Transmission (normalized):    ',value=0.5,/FLOAT,/ALL_EVENTS)
				fild_synthetic_frame_optics_solid_angle=CW_FIELD(fild_synthetic_optics_base,title='Solid Angle (in sr):    ',value=1.e-5,/FLOAT,/ALL_EVENTS)
				fild_synthetic_frame_optics_magnification=CW_FIELD(fild_synthetic_optics_base,title='Magnification (Beta):    ',value=0,/FLOAT,/ALL_EVENTS)


			fild_synthetic_noise_base=WIDGET_BASE(fild_synthetic_frame_settings_base_main,/COLUMN,frame=1)
				fild_synthetic_frame_noise_label=WIDGET_LABEL(fild_synthetic_noise_base,value='NOISE PROPERTIES')
				fild_synthetic_frame_noise_gaussian_centroid=CW_FIELD(fild_synthetic_noise_base,title='Gaussian noise centroid (counts):    ',value=200,/FLOAT,/ALL_EVENTS)
				fild_synthetic_frame_noise_gaussian_width=CW_FIELD(fild_synthetic_noise_base,title='Gaussian noise width (counts):    ',value=80,/FLOAT,/ALL_EVENTS)

			fild_synthetic_frame_options_base=WIDGET_BASE(fild_synthetic_frame_settings_base_main,/COLUMN,frame=1)
						fild_synthetic_frame_checkbox_label=['SAVE DATA','EXPORT EPS']
						fild_synthetic_frame_checkbox=CW_BGROUP(fild_synthetic_frame_options_base,fild_synthetic_frame_checkbox_label,$
							set_value=[0,0],/COLUMN,/NONEXCLUSIVE,$
							label_top='OPTIONS')



	sm_calculator_base=WIDGET_BASE(tab_1,title='Strike-Map Calculator',COLUMN=2)

	sm_calculator_base_2=WIDGET_BASE(sm_calculator_base,COLUMN=1,frame=1)


		sm_calculator_namelist_base=WIDGET_BASE(sm_calculator_base_2,COLUMN=1,frame=1,y_scroll_size=200,x_scroll_size=350)

			sm_namelist_label=WIDGET_LABEL(sm_calculator_namelist_base,value='FILDSIM NAMELIST')

			sm_namelist_result_dir=WIDGET_TEXT(sm_calculator_namelist_base,value='Result Directory: ',/SCROLL)
			sm_namelist_geom_dir=WIDGET_TEXT(sm_calculator_namelist_base,value='Geometry Directory: ',/SCROLL)
			sm_namelist_scintillator_files=WIDGET_TEXT(sm_calculator_namelist_base,value='Scintillator files: ',/SCROLL)
			sm_namelist_slit_files=WIDGET_TEXT(sm_calculator_namelist_base,value='Slit files: ',/SCROLL)


			sm_namelist_runID=CW_FIELD(sm_calculator_namelist_base,title='runID:  ',value='test',/ALL_EVENTS)
			sm_namelist_backtrace=CW_FIELD(sm_calculator_namelist_base,title='backtrace',value='.false.',/ALL_EVENTS)
			sm_namelist_save_orbits=CW_FIELD(sm_calculator_namelist_base,title='save_orbits',value='0',/LONG,/ALL_EVENTS)
			sm_namelist_N_ions=CW_FIELD(sm_calculator_namelist_base,title='N ions per gyr-pitch:  ',value=1000,/LONG,/ALL_EVENTS)
			sm_namelist_step=CW_FIELD(sm_calculator_namelist_base,title='Step (cm):  ',value=0.01,/FLOAT,/ALL_EVENTS)
			sm_namelist_length=CW_FIELD(sm_calculator_namelist_base,title='Helix Length (cm)',value=10.,/FLOAT,/ALL_EVENTS)
			sm_namelist_gyroradius=CW_FIELD(sm_calculator_namelist_base,title='Gyroradius (cm):  ',$
				value=string('2.,3.'),/ALL_EVENTS)
			sm_namelist_pitch=CW_FIELD(sm_calculator_namelist_base,title='Pitch Angles (deg):  ',$
				value=string('80.,70.'),/ALL_EVENTS)
			sm_namelist_gyrophase=CW_FIELD(sm_calculator_namelist_base,title='Gyrophase Range (radians):  ',$
				value=string('0.,3.'),/ALL_EVENTS)
			sm_namelist_start_x=CW_FIELD(sm_calculator_namelist_base,title='Start X (cm):  ',$
				value=string('-0.2,0.2'),/ALL_EVENTS)
			sm_namelist_start_y=CW_FIELD(sm_calculator_namelist_base,title='Start Y (cm):  ',$
				value=string('-0.02,0.02'),/ALL_EVENTS)
			sm_namelist_start_z=CW_FIELD(sm_calculator_namelist_base,title='Start Z (cm):  ',$
				value=string('-0.02,0.02'),/ALL_EVENTS)
			sm_namelist_alpha=CW_FIELD(sm_calculator_namelist_base,title='Alpha FILD orientation (deg):  ',value=0.,/FLOAT,/ALL_EVENTS)
			sm_namelist_beta=CW_FIELD(sm_calculator_namelist_base,title='Beta FILD orientation (deg):  ',value=-12.,/FLOAT,/ALL_EVENTS)

		sm_calculator_inputs_base=WIDGET_BASE(sm_calculator_base_2,COLUMN=1,frame=1)

			sm_inputs_shot=CW_FIELD(sm_calculator_inputs_base,title='Shot: ',value=30370,/LONG,/ALL_EVENTS)
			sm_inputs_time=CW_FIELD(sm_calculator_inputs_base,title='Time: ',value=2.5,/FLOAT,/ALL_EVENTS)
			sm_inputs_r=CW_FIELD(sm_calculator_inputs_base,title='R (m): ',value=2.,/FLOAT,/ALL_EVENTS)
			sm_inputs_z=CW_FIELD(sm_calculator_inputs_base,title='Z (m): ',value=0.3,/FLOAT,/ALL_EVENTS)
			sm_inputs_bt=CW_FIELD(sm_calculator_inputs_base,title='Bt: ',value=0.,/FLOAT,/ALL_EVENTS)
			sm_inputs_bz=CW_FIELD(sm_calculator_inputs_base,title='Bz: ',value=0.,/FLOAT,/ALL_EVENTS)
			sm_inputs_br=CW_FIELD(sm_calculator_inputs_base,title='Br: ',value=0.,/FLOAT,/ALL_EVENTS)

		sm_calculator_base_3=WIDGET_BASE(sm_calculator_base_2,COLUMN=2)
		sm_calculator_buttons=WIDGET_BASE(sm_calculator_base_3,COLUMN=1,frame=1)

			sm_calculator_calc_b=WIDGET_BUTTON(sm_calculator_buttons,value='CALCULATE B FIELD',frame=1)
			sm_calculator_select_scint_files=WIDGET_BUTTON(sm_calculator_buttons,value='SELECT SCINTILLATOR FILES',frame=1)
			sm_calculator_select_slit_files=WIDGET_BUTTON(sm_calculator_buttons,value='SELECT SLIT FILES',frame=1)
			sm_calculator_select_geom_dir=WIDGET_BUTTON(sm_calculator_buttons,value='SELECT GEOM DIR',frame=1)
			sm_calculator_select_result_dir=WIDGET_BUTTON(sm_calculator_buttons,value='SELECT RESULT DIR',frame=1)
			sm_calculator_write_namelist=WIDGET_BUTTON(sm_calculator_buttons,value='WRITE NAMELIST',frame=1)
			sm_calculator_load_std=WIDGET_BUTTON(sm_calculator_buttons,value='LOAD STD NAMELIST',frame=1)
			sm_calculator_save_std=WIDGET_BUTTON(sm_calculator_buttons,value='SAVE STD NAMELIST',frame=1)
			sm_calculator_contour_sigma_gyr=WIDGET_BUTTON(sm_calculator_buttons,value='CONTOUR SIGMA GYR',frame=1)

		sm_calculator_buttons_2=WIDGET_BASE(sm_calculator_base_3,COLUMN=1,frame=1)

			sm_calculator_plot_strike_map=WIDGET_BUTTON(sm_calculator_buttons_2,value='PLOT STRIKE MAP',frame=1)
			sm_calculator_plot_gyroradius_resolution=WIDGET_BUTTON(sm_calculator_buttons_2,value='PLOT GYR RESOLUTION',frame=1)
			sm_calculator_plot_pitch_resolution=WIDGET_BUTTON(sm_calculator_buttons_2,value='PLOT PITCH RESOLUTION',frame=1)
			sm_calculator_plot_strike_points=WIDGET_BUTTON(sm_calculator_buttons_2,value='PLOT STRIKE POINTS',frame=1)
			sm_calculator_plot_gyroradius_all=WIDGET_BUTTON(sm_calculator_buttons_2,value='PLOT GYR. RES. ALL',frame=1)
			sm_calculator_plot_pitch_all=WIDGET_BUTTON(sm_calculator_buttons_2,value='PLOT PITCH. RES. ALL',frame=1)
			sm_calculator_contour_colfac=WIDGET_BUTTON(sm_calculator_buttons_2,value='CONTOUR COLFAC',frame=1)
			sm_calculator_calc_all_res=WIDGET_BUTTON(sm_calculator_buttons_2,value='CALC. ALL RES.',frame=1)
			sm_calculator_contour_sigma_pitch=WIDGET_BUTTON(sm_calculator_buttons_2,value='CONTOUR SIGMA PITCH',frame=1)


	sm_plot_base=WIDGET_BASE(sm_calculator_base,COLUMN=1)

			sm_plot_label=WIDGET_LABEL(sm_plot_base,value='STRIKE MAP')
			sm_draw_strike_map=WIDGET_DRAW(sm_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
			sm_plot_label_2=WIDGET_LABEL(sm_plot_base,value='RESOLUTIONS')
			sm_draw_resolutions=WIDGET_DRAW(sm_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
			sm_run_simulation=WIDGET_BUTTON(sm_plot_base,value='RUN SIMULATION',frame=1)
			sm_gyro_resolution=CW_FIELD(sm_plot_base,title='Gyroradius: ',value=4.,/FLOAT,/ALL_EVENTS)
			sm_pitch_resolution=CW_FIELD(sm_plot_base,title='Pitch Angle: ',value=70.,/FLOAT,/ALL_EVENTS)
			sm_binsize_resolution=CW_FIELD(sm_plot_base,title='Binsize: ',value=0.1,/FLOAT,/ALL_EVENTS)

	absolute_flux_base=WIDGET_BASE(tab_1,title='Absolute Flux Estimation')

		af_base=WIDGET_BASE(absolute_flux_base,COLUMN=2)

			af_frame_base=WIDGET_BASE(af_base,COLUMN=1)

				af_frame_draw=WIDGET_DRAW(af_frame_base,/button_events,/MOTION_EVENTS,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
				af_frame_slider=WIDGET_SLIDER(af_frame_base,SENSITIVE=0,minimum=1,maximum=350,/drag)
				af_frame_info=WIDGET_BASE(af_frame_base,COLUMN=5)
					af_frame_info_frame= WIDGET_LABEL(af_frame_info,value='Frame=',/dynamic_resize)
					af_frame_info_time= WIDGET_LABEL(af_frame_info,value='Time=',/dynamic_resize)
					af_calib_frame=WIDGET_LABEL(af_frame_info,value='CURRENT CALIB FRAME: NONE',/dynamic_resize)
 					af_efficiency=WIDGET_LABEL(af_frame_info,value='CURRENT EFFICIENCY FILE: NONE',/dynamic_resize)
				af_yield_curve_draw=WIDGET_DRAW(af_frame_base,/button_events,/MOTION_EVENTS,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)

			af_settings_base=WIDGET_BASE(af_base,COLUMN=1)
				af_input_data_options=['CCD DATA','PHANTOM DATA']
				af_input_data_checkbox=CW_BGROUP(af_settings_base,af_input_data_options,set_value=0,/COLUMN,/EXCLUSIVE)
				af_bandpass_filter_widget=CW_FIELD(af_settings_base,title='BANDPASS FILTER:   ',value=100,/LONG,/RETURN_EVENTS)
				af_input_data_options_2=['FILTER','SUBSTRACT BACKGROUND', 'OVERLAY GRID','IGNORE FCOL','ION FLUX','MEAN CALIBRATION FRAME','REMAP FRAMES']
				af_input_data_checkbox_2=CW_BGROUP(af_settings_base,af_input_data_options_2,set_value=[0,0,0,0,1],/COLUMN,/NONEXCLUSIVE)
				af_input_data_options_3=['DEFAULT','SPOT TRACKING', 'ROI METHOD']
				af_input_data_checkbox_3=CW_BGROUP(af_settings_base,af_input_data_options_3,set_value=0,/COLUMN,/EXCLUSIVE)
				af_magnetic_field=CW_FIELD(af_settings_base,title='B FIELD (T):   ',value=1.6,/FLOAT,/RETURN_EVENTS)
				af_exp_time_widget=CW_FIELD(af_settings_base,title='Exposure Time (s):   ',value=0.02,/FLOAT,/RETURN_EVENTS)
				af_pinhole_area_widget=CW_FIELD(af_settings_base,title='Pinhole area (m^2):   ',value=1.e-6,/FLOAT,/RETURN_EVENTS)
				af_A_widget=CW_FIELD(af_settings_base,title='A (proton masses):   ',value=2,/FLOAT,/RETURN_EVENTS)
				af_Z_widget=CW_FIELD(af_settings_base,title='Z (electron charge):   ',value=1,/FLOAT,/RETURN_EVENTS)
				af_integrated_photon_flux_widget=CW_FIELD(af_settings_base,title='Int. Photon Flux (photons/s/m^2):   ',value=7.83e18,/FLOAT,/RETURN_EVENTS)
				af_calibration_exp_time_widget=CW_FIELD(af_settings_base,title='Calibration frame exposure time (s):   ',value=0.003,/FLOAT,/RETURN_EVENTS)
				af_calibration_area_pixel_widget=CW_FIELD(af_settings_base,title='Effective pixel area (m^2):   ',value=3.e-8,/FLOAT,/RETURN_EVENTS)
				af_DEF_ROI_BUTTON=WIDGET_BUTTON(af_settings_base,value='DEFINE ROI',frame=1)
				af_calculate_timetrace=WIDGET_BUTTON(af_settings_base,value='Calculate Abs. Flux Timetrace',frame=1)
				af_estimate_pixel_area=WIDGET_BUTTON(af_settings_base,value='Estimate pixel area',frame=1)
				af_set_mean_calibration_frame=WIDGET_BUTTON(af_settings_base,value='Set mean calibration frame',frame=1)




	tomography_base=WIDGET_BASE(tab_1,title='Tomography')

		tomo_base=WIDGET_BASE(tomography_base,COLUMN=3)

		tomo_plot_base=WIDGET_BASE(tomo_base,COLUMN=1)
			tomo_input_label=WIDGET_LABEL(tomo_plot_base,value='FILD EXPERIMENTAL SCINTILLATOR')
			tomo_input_draw=WIDGET_DRAW(tomo_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
			tomo_output_label=WIDGET_LABEL(tomo_plot_base,value='FILD RETRIEVED PINHOLE')
			tomo_output_draw=WIDGET_DRAW(tomo_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
			tomo_run_button=WIDGET_BUTTON(tomo_plot_base,value='RUN TOMOGRAPHY',frame=1)

		tomo_settings_base_main=WIDGET_BASE(tomo_base,/COLUMN)


			tomo_grid_scint_base=WIDGET_BASE(tomo_settings_base_main,COLUMN=1,frame=1)

				tomo_grid_scint_label=WIDGET_LABEL(tomo_grid_scint_base,value='SCINTILLATOR GRID PARAMETERS')
				tomo_grid_scint_mingyr=CW_FIELD(tomo_grid_scint_base,title='MIN GYR:    ',value=1.,/FLOAT,/ALL_EVENTS)
				tomo_grid_scint_maxgyr=CW_FIELD(tomo_grid_scint_base,title='MAX GYR:    ',value=10.,/FLOAT,/ALL_EVENTS)
				tomo_grid_scint_dg=CW_FIELD(tomo_grid_scint_base,title='DELTA GYR:  ',value=0.1,/FLOAT,/ALL_EVENTS)
				tomo_grid_scint_minpitch=CW_FIELD(tomo_grid_scint_base,title='MIN PITCH:  ',value=10.,/FLOAT,/ALL_EVENTS)
				tomo_grid_scint_maxpitch=CW_FIELD(tomo_grid_scint_base,title='MAX PITCH:  ',value=90.,/FLOAT,/ALL_EVENTS)
				tomo_grid_scint_dp=CW_FIELD(tomo_grid_scint_base,title='DELTA PITCH:',value=1.,/FLOAT,/ALL_EVENTS)

			tomo_grid_pin_base=WIDGET_BASE(tomo_settings_base_main,COLUMN=1,frame=1)

				tomo_grid_pin_label=WIDGET_LABEL(tomo_grid_pin_base,value='PINHOLE GRID PARAMETERS')
				tomo_grid_pin_mingyr=CW_FIELD(tomo_grid_pin_base,title='MIN GYR:    ',value=1.,/FLOAT,/ALL_EVENTS)
				tomo_grid_pin_maxgyr=CW_FIELD(tomo_grid_pin_base,title='MAX GYR:    ',value=10.,/FLOAT,/ALL_EVENTS)
				tomo_grid_pin_dg=CW_FIELD(tomo_grid_pin_base,title='DELTA GYR:  ',value=0.1,/FLOAT,/ALL_EVENTS)
				tomo_grid_pin_minpitch=CW_FIELD(tomo_grid_pin_base,title='MIN PITCH:  ',value=10.,/FLOAT,/ALL_EVENTS)
				tomo_grid_pin_maxpitch=CW_FIELD(tomo_grid_pin_base,title='MAX PITCH:  ',value=90.,/FLOAT,/ALL_EVENTS)
				tomo_grid_pin_dp=CW_FIELD(tomo_grid_pin_base,title='DELTA PITCH:',value=1.,/FLOAT,/ALL_EVENTS)


			tomo_settings_base=WIDGET_BASE(tomo_settings_base_main,/COLUMN,frame=1)
					tomo_settings_label=WIDGET_LABEL(tomo_settings_base,value='GENERAL SETTINGS')
					tomo_exp_frame=CW_FIELD(tomo_settings_base,title='EXPERIMENTAL FRAME:',value='',/STRING,/ALL_EVENTS)
					tomo_strike_map=CW_FIELD(tomo_settings_base,title='STRIKE MAP:',value='',/STRING,/ALL_EVENTS)
					tomo_strike_points=CW_FIELD(tomo_settings_base,title='STRIKE POINTS:',value='',/STRING,/ALL_EVENTS)
					tomo_efficiency_file=CW_FIELD(tomo_settings_base,title='EFFICIENCY FILE:',value='',/STRING,/ALL_EVENTS)
					tomo_b_field=CW_FIELD(tomo_settings_base,title='B FIELD (T):',value=1.9,/FLOAT,/ALL_EVENTS)
					tomo_A=CW_FIELD(tomo_settings_base,title='A:',value=2,/LONG,/ALL_EVENTS)
					tomo_Z=CW_FIELD(tomo_settings_base,title='Z:',value=1,/LONG,/ALL_EVENTS)


			tomo_buttons_base=WIDGET_BASE(tomo_settings_base_main,/COLUMN)
					tomo_load_exp_frame=WIDGET_BUTTON(tomo_buttons_base,value='LOAD CALIBRATED FRAME',frame=1)
					tomo_set_strike_map=WIDGET_BUTTON(tomo_buttons_base,value='SET STRIKE MAP',frame=1)
					tomo_set_strike_points=WIDGET_BUTTON(tomo_buttons_base,value='SET STRIKE POINTS',frame=1)
					tomo_set_efficiency_file=WIDGET_BUTTON(tomo_buttons_base,value='SET EFFICIENCY FILE',frame=1)

		tomo_settings_base_2=WIDGET_BASE(tomo_base,/COLUMN,frame=1)
					tomo_settings_label_2=WIDGET_LABEL(tomo_settings_base_2,value='TOMOGRAPHY SETTINGS')

					tomo_method_base=WIDGET_BASE(tomo_settings_base_2,/COLUMN,frame=1)
						tomo_methods_label=['NONE','T-SVD','TIKHONOV 0th','TIKHONOV 0th NON-NEG CONSTRAINT']
						tomo_methods=CW_BGROUP(tomo_method_base,tomo_methods_label,set_value=0,/COLUMN,/EXCLUSIVE,$
							label_top='INVERSION METHOD')
						tomo_alpha=CW_FIELD(tomo_method_base,title='Alpha:',value=10,/FLOAT,/ALL_EVENTS)



					tomo_model_base=WIDGET_BASE(tomo_settings_base_2,/COLUMN,frame=1)
						tomo_model_label=['SKEW GAUSSIAN','GAUSSIAN']
						tomo_model=CW_BGROUP(tomo_model_base,tomo_model_label,set_value=1,/COLUMN,/EXCLUSIVE,$
							label_top='FILDSIM MODEL')


					tomo_options_base=WIDGET_BASE(tomo_settings_base_2,/COLUMN,frame=1)
						tomo_checkbox_label=['LIMIT FCOL REGION','NON CALIBRATED FRAME','EXPORT BINARY DATA']
						tomo_checkbox=CW_BGROUP(tomo_options_base,tomo_checkbox_label,set_value=[0,1,1],/COLUMN,/NONEXCLUSIVE,$
							label_top='OPTIONS')




	orbit_calc_base=WIDGET_BASE(tab_1,title='ORBIT CALCULATOR',ROW=2)

	orbit_calc_base_2=WIDGET_BASE(orbit_calc_base,title='ORBIT CALCULATOR',COLUMN=2)

		orbit_calc_plot_base=WIDGET_BASE(orbit_calc_base_2,COLUMN=1)
			poloidal_plot_label=WIDGET_LABEL(orbit_calc_plot_base,value='Poloidal plot')
			poloidal_plot_draw=WIDGET_DRAW(orbit_calc_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
			toroidal_plot_label=WIDGET_LABEL(orbit_calc_plot_base,value='Toroidal plot')
			toroidal_plot_draw=WIDGET_DRAW(orbit_calc_plot_base,/button_events,/keyboard_events,xsize=frame_x_size,ysize=frame_y_size)
			orbit_calc_run_button=WIDGET_BUTTON(orbit_calc_plot_base,value='CALCULATE ORBIT',frame=1)


		orbit_settings_base_main=WIDGET_BASE(orbit_calc_base_2,/COLUMN)


			orbit_param_base=WIDGET_BASE(orbit_settings_base_main,COLUMN=1,frame=1)

				orb_param_label=WIDGET_LABEL(orbit_param_base,value='ORBIT PARAMETERS')
				orb_rini=CW_FIELD(orbit_param_base,title='R initial [m]:    ',value=2.181,/FLOAT,/ALL_EVENTS)
				orb_zini=CW_FIELD(orbit_param_base,title='Z initial [m]:    ',value=0.32,/FLOAT,/ALL_EVENTS)
				orb_phiini=CW_FIELD(orbit_param_base,title='Phi initial [deg]:  ',value=0.,/FLOAT,/ALL_EVENTS)
				orb_energy=CW_FIELD(orbit_param_base,title='Energy [keV]  ',value=93.,/FLOAT,/ALL_EVENTS)
				orb_pitch=CW_FIELD(orbit_param_base,title='Pitch [deg]:  ',value=60.,/FLOAT,/ALL_EVENTS)
                orb_mass=CW_FIELD(orbit_param_base,title='A:  ',value=2.,/FLOAT,/ALL_EVENTS)
                orb_charge=CW_FIELD(orbit_param_base,title='Z:  ',value=1.,/FLOAT,/ALL_EVENTS)
                orb_nsteps=CW_FIELD(orbit_param_base,title='N steps:  ',value=1.e4,/FLOAT,/ALL_EVENTS)
                orb_fild_gyro=CW_FIELD(orbit_param_base,title='FILD gyroradius (cm):  ',value=3.,/FLOAT,/ALL_EVENTS)



			orbit_equi_base=WIDGET_BASE(orbit_settings_base_main,/COLUMN)

				orbit_magnetic_base=WIDGET_BASE(orbit_equi_base,/COLUMN,frame=1)
					orb_shot=CW_FIELD(orbit_equi_base,title='SHOT:       ',value=28061L,/LONG,/ALL_EVENTS)
					orb_time=CW_FIELD(orbit_equi_base,title='TIME [s]:       ',value=2.0,/FLOAT,/ALL_EVENTS)
					orb_exp=CW_FIELD(orbit_equi_base,title='EXP: ',value='AUGD',/STRING,/ALL_EVENTS)
					orb_diag=CW_FIELD(orbit_equi_base,title='DIAG: ',value='EQH',/STRING,/ALL_EVENTS)
					orb_ed=CW_FIELD(orbit_equi_base,title='Edition: ',value=0,/LONG,/ALL_EVENTS)

                orbit_options_base=WIDGET_BASE(orbit_equi_base,/COLUMN,frame=1)

					orb_checkbox_options=['Backtrace orbit','Add guiding centre','Add Vessel','Add separatrix','Add flux surfaces','Export eps','Export ascii']
					orb_checkbox_settings=CW_BGROUP(orbit_options_base,orb_checkbox_options,set_value=[1,1,1,1,0,0,0],/COLUMN,/NONEXCLUSIVE)
                    orbit_calc_fild_energy=WIDGET_BUTTON(orbit_options_base,value='Get FILD energy',frame=1)




;********* UVALUE STRUCTURE ***********


uvalue={main_base:main_base,file_menu:file_menu,$
	exit_button:exit_button,open_movie_button:open_movie_button,open_frame_button:open_frame_button,save_image_button:save_image_button,$
	load_color_table_button:load_color_table_button,$
	create_video_phantom_button:create_video_phantom_button,phantom_gyroradius_profile_video:phantom_gyroradius_profile_video,$
	create_video_ccd_button:create_video_ccd_button,phantom_pitch_profile_video:phantom_pitch_profile_video,$
	phantom_remap_shot:phantom_remap_shot,ccd_remap_shot:ccd_remap_shot,$
	grid_cal:grid_cal,get_calibration_button:get_calibration_button,save_calibration_parameters:save_calibration_parameters,select_scintillator_plate:select_scintillator_plate,$
	save_idl:save_idl,save_fildsim_output_button:save_fildsim_output_button,save_fildsim_eps_button:save_fildsim_eps_button,$
	save_idl_strike_map_resolutions:save_idl_strike_map_resolutions,save_interpolated_data:save_interpolated_data,$
	save_remap_vars:save_remap_vars,save_absolute_flux_vars:save_absolute_flux_vars,$
	save_strike_map_pixels:save_strike_map_pixels,export_eps_absolute_flux:export_eps_absolute_flux,$
	export_eps_ccd_remap:export_eps_ccd_remap,EXPORT_EPS_PHANTOM_REMAP:EXPORT_EPS_PHANTOM_REMAP,export_eps_phantom_strike_map:export_eps_phantom_strike_map,$
	export_ccd_timetrace_ascii:export_ccd_timetrace_ascii,export_phantom_timetrace_ascii:export_phantom_timetrace_ascii,$
	plot_ginterp:plot_ginterp,plot_pinterp:plot_pinterp,plot_cinterp:plot_cinterp,select_fildsim_grid:select_fildsim_grid,select_grid_image:select_grid_image,$
	settings_base:settings_base,shot_field:shot_field,fild_field:fild_field,$
	xscale_field:xscale_field,yscale_field:yscale_field,xshift_field:xshift_field,yshift_field:yshift_field,deg_field:deg_field,$
	checkbox_settings:checkbox_settings,checkbox_settings_2:checkbox_settings_2,$
	tabs_base:tabs_base,tab_1:tab_1,ccd_base:ccd_base,$
	ccd_frame_base:ccd_frame_base,ccd_frame_draw:ccd_frame_draw,ccd_frame_slider:ccd_frame_slider,$
	ccd_frame_info_frame:ccd_frame_info_frame,ccd_frame_info_time:ccd_frame_info_time,plot_grid_button:plot_grid_button,set_background_button:set_background_button,$
	substract_background_button:substract_background_button,ccd_gyroradius_profile_base:ccd_gyroradius_profile_base,ccd_pitch_profile_base:ccd_pitch_profile_base,$
	ccd_gyroradius_profile_draw:ccd_gyroradius_profile_draw,gyroradius_profile_minpitch:gyroradius_profile_minpitch,$
	gyroradius_profile_maxpitch:gyroradius_profile_maxpitch,gyroradius_profile_mingyr:gyroradius_profile_mingyr,gyroradius_profile_maxgyr:gyroradius_profile_maxgyr,$
	ccd_gyroradius_profile_plot_button:ccd_gyroradius_profile_plot_button,gyroradius_profile_maxint:gyroradius_profile_maxint,$
	ccd_pitch_profile_draw:ccd_pitch_profile_draw,pitch_profile_minpitch:pitch_profile_minpitch,$
	pitch_profile_maxpitch:pitch_profile_maxpitch,pitch_profile_mingyr:pitch_profile_mingyr,pitch_profile_maxgyr:pitch_profile_maxgyr,$
	ccd_pitch_profile_plot_button:ccd_pitch_profile_plot_button,pitch_profile_maxint:pitch_profile_maxint,$
	ccd_frame_xpixel:ccd_frame_xpixel,ccd_frame_ypixel:ccd_frame_ypixel,ccd_frame_intensity:ccd_frame_intensity,$
	ccd_remap_base:ccd_remap_base,ccd_remap_draw:ccd_remap_draw,ccd_remap_button:ccd_remap_button,ccd_remap_settings_base:ccd_remap_settings_base,$
	ccd_remap_settings_pitch_base:ccd_remap_settings_pitch_base,ccd_remap_settings_gyr_base:ccd_remap_settings_gyr_base,$
	settings_minpitch:settings_minpitch,settings_maxpitch:settings_maxpitch,settings_dpitch:settings_dpitch,$
	settings_mingyr:settings_mingyr,settings_maxgyr:settings_maxgyr,settings_dgyr:settings_dgyr,settings_remap_levels:settings_remap_levels,$
	ccd_timetrace_button:ccd_timetrace_button,ccd_saturation_slider:ccd_saturation_slider,$

	save_ccd_timetrace:save_ccd_timetrace,save_phantom_timetrace:save_phantom_timetrace,$

	phantom_base:phantom_base,phantom_initial_time_button:phantom_initial_time_button,phantom_final_time_button:phantom_final_time_button,phantom_draw:phantom_draw,$
	phantom_slider:phantom_slider,phantom_info_frame:phantom_info_frame,phantom_info_time:phantom_info_time,phantom_plot_grid_button:phantom_plot_grid_button,$
	phantom_set_background_button:phantom_set_background_button,phantom_substract_background_button:phantom_substract_background_button,$
	phantom_remap_base:phantom_remap_base,phantom_remap_draw:phantom_remap_draw,phantom_remap_button:phantom_remap_button,phantom_remap_settings_base:phantom_remap_settings_base,$
	phantom_remap_settings_pitch_base:phantom_remap_settings_pitch_base,phantom_remap_settings_gyr_base:phantom_remap_settings_gyr_base,$
	phantom_settings_minpitch:phantom_settings_minpitch,phantom_settings_maxpitch:phantom_settings_maxpitch,phantom_settings_dpitch:phantom_settings_dpitch,$
	phantom_settings_mingyr:phantom_settings_mingyr,phantom_settings_maxgyr:phantom_settings_maxgyr,phantom_settings_dgyr:phantom_settings_dgyr,$
	phantom_settings_remap_levels:phantom_settings_remap_levels,$

	phantom_fr_rem_base:phantom_fr_rem_base,$
	phantom_info_xpixel:phantom_info_xpixel,phantom_info_ypixel:phantom_info_ypixel,phantom_info_intensity:phantom_info_intensity,$
	phantom_calculate_timetrace_button:phantom_calculate_timetrace_button,$


	phantom_gyroradius_profile_base:phantom_gyroradius_profile_base,phantom_pitch_profile_base:phantom_pitch_profile_base,$
	phantom_gyroradius_profile_draw:phantom_gyroradius_profile_draw,phantom_gyroradius_profile_minpitch:phantom_gyroradius_profile_minpitch,$
	phantom_gyroradius_profile_maxpitch:phantom_gyroradius_profile_maxpitch,phantom_gyroradius_profile_mingyr:phantom_gyroradius_profile_mingyr,$
	phantom_gyroradius_profile_maxgyr:phantom_gyroradius_profile_maxgyr,$
	phantom_gyroradius_profile_plot_button:phantom_gyroradius_profile_plot_button,phantom_gyroradius_profile_maxint:phantom_gyroradius_profile_maxint,$
	phantom_pitch_profile_draw:phantom_pitch_profile_draw,phantom_pitch_profile_minpitch:phantom_pitch_profile_minpitch,$
	phantom_pitch_profile_maxpitch:phantom_pitch_profile_maxpitch,phantom_pitch_profile_mingyr:phantom_pitch_profile_mingyr,$
	phantom_pitch_profile_maxgyr:phantom_pitch_profile_maxgyr,$
	phantom_pitch_profile_plot_button:phantom_pitch_profile_plot_button,phantom_pitch_profile_maxint:phantom_pitch_profile_maxint,$

	spectrogram_base:spectrogram_base,$
	fildsim_base:fildsim_base,fildsim_plot_base:fildsim_plot_base,fildsim_settings_base:fildsim_settings_base,fildsim_run_button:fildsim_run_button,$
	fildsim_input_draw:fildsim_input_draw,fildsim_output_draw:fildsim_output_draw,fildsim_slider:fildsim_slider,$
	fildsim_code_base:fildsim_code_base,fildsim_magnetic_base:fildsim_magnetic_base,fildsim_current_input:fildsim_current_input,$
	checkbox_codes_settings:checkbox_codes_settings,fildsim_magnetic:fildsim_magnetic,fildsim_shot:fildsim_shot,$
	fildsim_time:fildsim_time,fildsim_r_fild:fildsim_r_fild,fildsim_z_fild:fildsim_z_fild,fildsim_smooth_input_fild:fildsim_smooth_input_fild,$
	fildsim_get_input_file:fildsim_get_input_file,fildsim_get_aug_bfield:fildsim_get_aug_bfield,fildsim_load_fildsim_run:fildsim_load_fildsim_run,$
	fildsim_grid_mingyr:fildsim_grid_mingyr,fildsim_grid_maxgyr:fildsim_grid_maxgyr,fildsim_grid_dg:fildsim_grid_dg,$
	fildsim_grid_minpitch:fildsim_grid_minpitch,fildsim_grid_maxpitch:fildsim_grid_maxpitch,fildsim_grid_dp:fildsim_grid_dp,fildsim_grid_nlev:fildsim_grid_nlev,$
	fildsim_options_base:fildsim_options_base,fildsim_checkbox_options:fildsim_checkbox_options,fildsim_checkbox_settings:fildsim_checkbox_settings,$

    fild_synthetic_frame_base:fild_synthetic_frame_base,fild_synthetic_frame_plot_base:fild_synthetic_frame_plot_base,$
	fild_synthetic_frame_plot_label_1:fild_synthetic_frame_plot_label_1,fild_synthetic_frame_plot_draw_1:fild_synthetic_frame_plot_draw_1,$
	fild_synthetic_frame_plot_label_2:fild_synthetic_frame_plot_label_2,fild_synthetic_frame_plot_draw_2:fild_synthetic_frame_plot_draw_2,$
	fild_synthetic_frame_settings_base_main:fild_synthetic_frame_settings_base_main,fild_synthetic_frame_main_param_base:fild_synthetic_frame_main_param_base,$
	fild_synthetic_frame_main_param_label:fild_synthetic_frame_main_param_label,fild_synthetic_frame_input:fild_synthetic_frame_input,$
	fild_synthetic_frame_strike_map:fild_synthetic_frame_strike_map,fild_synthetic_frame_scintillator_file:fild_synthetic_frame_scintillator_file,$
	fild_synthetic_frame_scintillator_yield:fild_synthetic_frame_scintillator_yield,fild_synthetic_frame_geometrical_factor:fild_synthetic_frame_geometrical_factor,$
	fild_synthetic_frame_exp_time:fild_synthetic_frame_exp_time,$
	fild_synthetic_camera_base:fild_synthetic_camera_base,fild_synthetic_frame_camera_label:fild_synthetic_frame_camera_label,$
	fild_synthetic_frame_camera_name:fild_synthetic_frame_camera_name,fild_synthetic_frame_camera_nxpixels:fild_synthetic_frame_camera_nxpixels,$
	fild_synthetic_frame_camera_nypixels:fild_synthetic_frame_camera_nypixels,fild_synthetic_frame_camera_pixel_xsize:fild_synthetic_frame_camera_pixel_xsize,$
	fild_synthetic_frame_camera_pixel_ysize:fild_synthetic_frame_camera_pixel_ysize,fild_synthetic_frame_camera_pixel_qefficiency:fild_synthetic_frame_camera_pixel_qefficiency,$
	fild_synthetic_frame_camera_pixel_AD:fild_synthetic_frame_camera_pixel_AD,fild_synthetic_frame_camera_pixel_dynamic_range:fild_synthetic_frame_camera_pixel_dynamic_range,$
	fild_synthetic_optics_base:fild_synthetic_optics_base,fild_synthetic_frame_optics_label:fild_synthetic_frame_optics_label,$
	fild_synthetic_frame_optics_transmission:fild_synthetic_frame_optics_transmission,fild_synthetic_frame_optics_solid_angle:fild_synthetic_frame_optics_solid_angle,$
	fild_synthetic_frame_optics_magnification:fild_synthetic_frame_optics_magnification,$
	fild_synthetic_noise_base:fild_synthetic_noise_base,fild_synthetic_frame_noise_label:fild_synthetic_frame_noise_label,$
	fild_synthetic_frame_noise_gaussian_centroid:fild_synthetic_frame_noise_gaussian_centroid,$
	fild_synthetic_frame_noise_gaussian_width:fild_synthetic_frame_noise_gaussian_width,$
	fild_synthetic_buttons_base:fild_synthetic_buttons_base,fild_synthetic_frame_set_input:fild_synthetic_frame_set_input,$
	fild_synthetic_frame_set_strike_map:fild_synthetic_frame_set_strike_map,fild_synthetic_frame_set_scintillator_file:fild_synthetic_frame_set_scintillator_file,$
	fild_synthetic_frame_set_scintillator_yield:fild_synthetic_frame_set_scintillator_yield,fild_synthetic_frame_load_camera_properties:fild_synthetic_frame_load_camera_properties,$
	fild_synthetic_frame_run:fild_synthetic_frame_run,fild_synthetic_frame_options_base:fild_synthetic_frame_options_base,$
	fild_synthetic_frame_checkbox_label:fild_synthetic_frame_checkbox_label,fild_synthetic_frame_checkbox:fild_synthetic_frame_checkbox,$



	sm_calculator_base:sm_calculator_base,sm_calculator_base2:sm_calculator_base_2,sm_calculator_namelist_base:sm_calculator_namelist_base,$
	sm_namelist_runID:sm_namelist_runID,sm_namelist_result_dir:sm_namelist_result_dir,sm_namelist_geom_dir:sm_namelist_geom_dir,$
	sm_namelist_backtrace:sm_namelist_backtrace,sm_namelist_save_orbits:sm_namelist_save_orbits,$
	sm_namelist_scintillator_files:sm_namelist_scintillator_files,sm_namelist_slit_files:sm_namelist_slit_files,$
	sm_namelist_N_ions:sm_namelist_N_ions,sm_namelist_step:sm_namelist_step,$
	sm_namelist_length:sm_namelist_length,sm_namelist_gyroradius:sm_namelist_gyroradius,sm_namelist_pitch:sm_namelist_pitch,$
	sm_namelist_gyrophase:sm_namelist_gyrophase,sm_namelist_start_x:sm_namelist_start_x,sm_namelist_start_y:sm_namelist_start_y,$
	sm_namelist_start_z:sm_namelist_start_z,sm_namelist_alpha:sm_namelist_alpha,sm_namelist_beta:sm_namelist_beta,sm_namelist_label:sm_namelist_label,$

	sm_calculator_inputs_base:sm_calculator_inputs_base,$
	sm_inputs_shot:sm_inputs_shot,sm_inputs_time:sm_inputs_time,sm_inputs_r:sm_inputs_r,sm_inputs_z:sm_inputs_z,$
	sm_inputs_bt:sm_inputs_bt,sm_inputs_bz:sm_inputs_bz,sm_inputs_br:sm_inputs_br,$

	sm_calculator_buttons:sm_calculator_buttons,sm_calculator_calc_b:sm_calculator_calc_b,sm_calculator_load_std:sm_calculator_load_std,$
	sm_calculator_select_scint_files:sm_calculator_select_scint_files,sm_calculator_select_slit_files:sm_calculator_select_slit_files,$
	sm_calculator_select_geom_dir:sm_calculator_select_geom_dir,sm_calculator_select_result_dir:sm_calculator_select_result_dir,$
	sm_calculator_write_namelist:sm_calculator_write_namelist,sm_calculator_save_std:sm_calculator_save_std,$

	sm_calculator_base_3:sm_calculator_base_3,sm_calculator_buttons_2:sm_calculator_buttons_2,$
	sm_calculator_plot_strike_map:sm_calculator_plot_strike_map,sm_calculator_plot_gyroradius_resolution:sm_calculator_plot_gyroradius_resolution,$
	sm_calculator_plot_pitch_resolution:sm_calculator_plot_pitch_resolution,sm_calculator_plot_gyroradius_all:sm_calculator_plot_gyroradius_all,$
	sm_calculator_plot_pitch_all:sm_calculator_plot_pitch_all,sm_calculator_contour_colfac:sm_calculator_contour_colfac,$
	sm_calculator_calc_all_res:sm_calculator_calc_all_res,sm_calculator_contour_sigma_pitch:sm_calculator_contour_sigma_pitch,$
	sm_calculator_contour_sigma_gyr:sm_calculator_contour_sigma_gyr,$

	sm_plot_base:sm_plot_base,sm_plot_label:sm_plot_label,sm_plot_label_2:sm_plot_label_2,sm_draw_strike_map:sm_draw_strike_map,$
	sm_draw_resolutions:sm_draw_resolutions,sm_run_simulation:sm_run_simulation,sm_gyro_resolution:sm_gyro_resolution,$
	sm_pitch_resolution:sm_pitch_resolution,sm_binsize_resolution:sm_binsize_resolution,$
	sm_calculator_plot_strike_points:sm_calculator_plot_strike_points,$

	absolute_flux:absolute_flux,af_print_info_button:af_print_info_button,af_select_calibration_frame_button:af_select_calibration_frame_button,$
	af_select_efficiency_file_button:af_select_efficiency_file_button,$
	absolute_flux_base:absolute_flux_base,af_base:af_base,af_frame_base:af_frame_base,$
	af_frame_draw:af_frame_draw,af_frame_slider:af_frame_slider,af_frame_info:af_frame_info,af_frame_info_frame:af_frame_info_frame,$
	af_frame_info_time:af_frame_info_time,af_settings_base:af_settings_base,af_input_data_checkbox:af_input_data_checkbox,$
	af_input_data_options_3:af_input_data_options_3,af_input_data_checkbox_3:af_input_data_checkbox_3,$
	af_yield_curve_draw:af_yield_curve_draw,$
	af_bandpass_filter_widget:af_bandpass_filter_widget,af_magnetic_field:af_magnetic_field,af_calib_frame:af_calib_frame,af_efficiency:af_efficiency,$
	af_input_data_checkbox_2:af_input_data_checkbox_2,af_exp_time_widget:af_exp_time_widget,af_pinhole_area_widget:af_pinhole_area_widget,$
	af_A_widget:af_A_widget,af_Z_widget:af_Z_widget,af_DEF_ROI_BUTTON:af_DEF_ROI_BUTTON,af_calculate_timetrace:af_calculate_timetrace,$
	af_estimate_pixel_area:af_estimate_pixel_area,af_integrated_photon_flux_widget:af_integrated_photon_flux_widget,$
	af_calibration_exp_time_widget:af_calibration_exp_time_widget,af_calibration_area_pixel_widget:af_calibration_area_pixel_widget,$
	af_set_mean_calibration_frame:af_set_mean_calibration_frame,$


	tomography_base:tomography_base,tomo_base:tomo_base,$
	tomo_plot_base:tomo_plot_base,tomo_input_label:tomo_input_label,tomo_input_draw:tomo_input_draw,tomo_output_label:tomo_output_label,$
	tomo_output_draw:tomo_output_draw,tomo_run_button:tomo_run_button,$
	tomo_settings_base_main:tomo_settings_base_main,$
	tomo_grid_scint_base:tomo_grid_scint_base,tomo_grid_scint_label:tomo_grid_scint_label,tomo_grid_scint_mingyr:tomo_grid_scint_mingyr,$
	tomo_grid_scint_maxgyr:tomo_grid_scint_maxgyr,tomo_grid_scint_dg:tomo_grid_scint_dg,$
	tomo_grid_scint_minpitch:tomo_grid_scint_minpitch,tomo_grid_scint_maxpitch:tomo_grid_scint_maxpitch,tomo_grid_scint_dp:tomo_grid_scint_dp,$
	tomo_grid_pin_base:tomo_grid_pin_base,tomo_grid_pin_label:tomo_grid_pin_label,tomo_grid_pin_mingyr:tomo_grid_pin_mingyr,$
	tomo_grid_pin_maxgyr:tomo_grid_pin_maxgyr,tomo_grid_pin_dg:tomo_grid_pin_dg,$
	tomo_grid_pin_minpitch:tomo_grid_pin_minpitch,tomo_grid_pin_maxpitch:tomo_grid_pin_maxpitch,tomo_grid_pin_dp:tomo_grid_pin_dp,$
	tomo_settings_base:tomo_settings_base,tomo_settings_label:tomo_settings_label,tomo_exp_frame:tomo_exp_frame,tomo_strike_map:tomo_strike_map,$
	tomo_strike_points:tomo_strike_points,tomo_efficiency_file:tomo_efficiency_file,tomo_b_field:tomo_b_field,tomo_A:tomo_A,tomo_Z:tomo_Z,$
	tomo_buttons_base:tomo_buttons_base,tomo_load_exp_frame:tomo_load_exp_frame,tomo_set_strike_map:tomo_set_strike_map,$
	tomo_set_strike_points:tomo_set_strike_points,tomo_set_efficiency_file:tomo_set_efficiency_file,$
	tomo_settings_base_2:tomo_settings_base_2,tomo_settings_label_2:tomo_settings_label_2,tomo_method_base:tomo_method_base,$
	tomo_methods_label:tomo_methods_label,tomo_methods:tomo_methods,tomo_alpha:tomo_alpha,tomo_model_base:tomo_model_base,$
	tomo_model_label:tomo_model_label,tomo_model:tomo_model,$
	tomo_options_base:tomo_options_base,tomo_checkbox_label:tomo_checkbox_label,tomo_checkbox:tomo_checkbox,$

    orbit_calc_base:orbit_calc_base,orbit_calc_base_2:orbit_calc_base_2,orbit_calc_plot_base:orbit_calc_plot_base,$
    poloidal_plot_label:poloidal_plot_label,poloidal_plot_draw:poloidal_plot_draw,toroidal_plot_draw:toroidal_plot_draw,toroidal_plot_label:toroidal_plot_label,$
    orbit_calc_run_button:orbit_calc_run_button,orbit_settings_base_main:orbit_settings_base_main,orbit_param_base:orbit_param_base,$
    orb_param_label:orb_param_label,orb_rini:orb_rini,orb_zini:orb_zini,orb_phiini:orb_phiini,orb_energy:orb_energy,orb_pitch:orb_pitch,orb_mass:orb_mass,orb_charge:orb_charge,orb_nsteps:orb_nsteps,$
    orbit_equi_base:orbit_equi_base,orbit_options_base:orbit_options_base,orb_checkbox_options:orb_checkbox_options,orb_checkbox_settings:orb_checkbox_settings,$
    orbit_magnetic_base:orbit_magnetic_base,orb_shot:orb_shot,orb_time:orb_time,orb_exp:orb_exp,orb_diag:orb_diag,orb_ed:orb_ed,$
    orbit_calc_fild_energy:orbit_calc_fild_energy,orb_fild_gyro:orb_fild_gyro}


;******** COMMON BLOCKS ****************


;****************************************
COMMON SETTINGS,settings

FILD=INT(0)
SHOT=LONG(0)
FILTER=INT(0)
CAL_TYPE=INT(1)
OVERLAY_GRID=INT(0)
REMAP=INT(0)
PROFILES=INT(0)
CAMERA=INT(0)

ccd_data_loaded=INT(0)
phantom_data_loaded=INT(0)

HOME_path=getenv('WORK')
GUI_routines_path=getenv('FILD_GUI_PATH')
Results_path=strtrim(GUI_routines_path,1)+'/RESULTS/'
FILDSIMf90_path=getenv('FILDSIMf90_PATH')
FILD_data_path=getenv('FILD_DATA_PATH')


calibration_file=strtrim(GUI_routines_path)+'/config/calibration_database_new.txt'
grid_image=strtrim(GUI_routines_path)+'/config/grid_fild_1_nice.bmp'
default_scintillator_plate=strtrim(FILDSIMf90_path,1)+'/geometry/aug_fild1_scint.pl'

settings={fild:fild,shot:shot,filter:filter,cal_type:cal_type,overlay_grid:overlay_grid,remap:remap,profiles:profiles,camera:camera,$
	HOME_path:HOME_path,GUI_routines_path:GUI_routines_path,RESULTS_path:RESULTS_path,FILDSIMf90_path:FILDSIMf90_path,FILD_data_path:FILD_data_path,$
	grid_image:grid_image,calibration_file:calibration_file,$
	default_scintillator_plate:default_scintillator_plate,ccd_data_loaded:ccd_data_loaded,phantom_data_loaded:phantom_data_loaded}

;****************************************

COMMON FILD_GRID_INFO,fild_grid_info

FILD_NAME=''
STRIKE_MAP_FILENAME=''
STRIKE_POINTS_FILENAME=''

fild_grid_info={fild_name:fild_name,strike_map_filename:strike_map_filename,STRIKE_POINTS_FILENAME:STRIKE_POINTS_FILENAME}

;****************************************

COMMON CCD,ccd_current_image,ccd_ccd_file_path,ccd_ccd_shot_num,ccd_ccd_frame_num,ccd_background_image,ccd_draw_size_x,ccd_draw_size_y,ccd_ccd_txt_frames,ccd_ccd_txt_times,ccd_interpolated_gyro,ccd_interpolated_pitch,ccd_interpolated_fcol,ccd_interpolated_angle,ccd_interpolated_gyrophase,ccd_frames,ccd_times

ccd_current_image=fltarr(640,480)
ccd_ccd_file_path=''
ccd_ccd_shot_num=string('0',format='(I5)')
ccd_ccd_frame_num=string('0',format='(I3)')
ccd_background_image=fltarr(640,480)

ccd_draw_size_x=frame_x_size
ccd_draw_size_y=frame_y_size

ccd_ccd_txt_frames=fltarr(350)
ccd_ccd_txt_times=fltarr(350)

ccd_interpolated_gyro=fltarr(640,480)
ccd_interpolated_pitch=fltarr(640,480)
ccd_interpolated_fcol=fltarr(640,480)
ccd_interpolated_angle=fltarr(640,480)
ccd_interpolated_gyrophase=fltarr(640,480)

ccd_frames=fltarr(1)
ccd_times=fltarr(1)

;ccd={ccd_current_image:ccd_current_image,ccd_ccd_file_path:ccd_ccd_file_path,ccd_ccd_shot_num:ccd_ccd_shot_num,$
;	ccd_ccd_frame_num:ccd_ccd_frame_num,ccd_background_image:ccd_background_image,$
;	ccd_draw_size_x:ccd_draw_size_x,ccd_draw_size_y:ccd_draw_size_y,$
;	ccd_ccd_txt_frames:ccd_ccd_txt_frames,ccd_ccd_txt_times:ccd_ccd_txt_times,$
;	ccd_interpolated_gyro:ccd_interpolated_gyro,ccd_interpolated_pitch:ccd_interpolated_pitch,ccd_interpolated_fcol:ccd_interpolated_fcol,$
;	ccd_interpolated_angle:ccd_interpolated_angle,ccd_interpolated_gyrophase:ccd_interpolated_gyrophase}

;****************************************

COMMON PHANTOM,phantom_current_frame,phantom_initial_time,phantom_final_time,phantom_frames,phantom_times,phantom_frame_num,phantom_background_frame,phantom_interpolated_gyro,phantom_interpolated_pitch,phantom_interpolated_fcol,phantom_interpolated_angle,phantom_interpolated_gyrophase

phantom_current_frame=fltarr(640,480)
phantom_initial_time=-1
phantom_final_time=-1
phantom_background_frame=fltarr(640,480)

phantom_interpolated_gyro=fltarr(640,480)
phantom_interpolated_pitch=fltarr(640,480)
phantom_interpolated_fcol=fltarr(640,480)
phantom_interpolated_angle=fltarr(640,480)
phantom_interpolated_gyrophase=fltarr(640,480)


phantom_frames=fltarr(1)
phantom_times=fltarr(1)
phantom_frame_num=0

;****************************************

COMMON TIMETRACE,ccd_timetrace,ccd_timetrace_roi,phantom_timetrace,phantom_timetrace_roi

ccd_timetrace=0.
ccd_timetrace_roi=0.
phantom_timetrace=0.
phantom_timetrace_roi=0.

;****************************************

COMMON PHANTOM_remap,phantom_remap_frame,phantom_remap_gyroradius,phantom_remap_pitch


phantom_remap_frame=fltarr(640,480)
phantom_remap_gyroradius=fltarr(1)
phantom_remap_pitch=fltarr(1)

;****************************************

COMMON CCD_remap,ccd_remap_frame,ccd_remap_gyroradius,ccd_remap_pitch


ccd_remap_frame=fltarr(640,480)
ccd_remap_gyroradius=fltarr(1)
ccd_remap_pitch=fltarr(1)



;****************************************

COMMON GRID_PARAMS,grid_params

xscale_im=float(1) & yscale_im=float(1)
xshift_im=float(0) & yshift_im=float(0)
deg_im=float(0)

xscale_pix=float(1) & yscale_pix=float(1)
xshift_pix=float(0) & yshift_pix=float(0)
deg_pix=float(0)

grid_params={xscale_im:xscale_im,yscale_im:yscale_im,xshift_im:xshift_im,yshift_im:yshift_im,deg_im:deg_im,$
	xscale_pix:xscale_pix,yscale_pix:yscale_pix,xshift_pix:xshift_pix,yshift_pix:yshift_pix,deg_pix:deg_pix}

;****************************************

COMMON REMAP_SETTINGS,remap_settings

mingyr=1.
maxgyr=10.

minpitch=10.
maxpitch=90.

dgyr=0.1
dpitch=1.0

nlev=255

profile_gyro_minpitch=10.
profile_gyro_maxpitch=90.
profile_gyro_mingyr=1.
profile_gyro_maxgyr=10.
profile_gyro_maxint=1.e7

profile_pitch_minpitch=10.
profile_pitch_maxpitch=90.
profile_pitch_mingyr=1.
profile_pitch_maxgyr=10.
profile_pitch_maxint=1.e7


remap_settings={mingyr:mingyr,maxgyr:maxgyr,minpitch:minpitch,maxpitch:maxpitch,dgyr:dgyr,dpitch:dpitch,nlev:nlev,$
		profile_gyro_minpitch:profile_gyro_minpitch,profile_gyro_maxpitch:profile_gyro_maxpitch,$
		profile_gyro_mingyr:profile_gyro_mingyr,profile_gyro_maxgyr:profile_gyro_maxgyr,profile_gyro_maxint:profile_gyro_maxint,$
		profile_pitch_minpitch:profile_pitch_minpitch,profile_pitch_maxpitch:profile_pitch_maxpitch,$
		profile_pitch_mingyr:profile_pitch_mingyr,profile_pitch_maxgyr:profile_pitch_maxgyr,profile_pitch_maxint:profile_pitch_maxint}

;****************************************

COMMON FILDSIM_SETTINGS,fildsim_settings

shot=LONG(0)
time=0.
b_field=1.
r_fild=2.18
z_fild=0.32

input_filename=''
input_code=LONG(0)
strike_map_filename=''
strike_points_filename=''


mingyr=1.
maxgyr=10.
dg=0.1
minpitch=10.
maxpitch=90.
dp=1.
nlev=255
main_grid={mingyr:mingyr,maxgyr:maxgyr,dg:dg,minpitch:minpitch,maxpitch:maxpitch,dp:dp,nlev:nlev}


fildsim_settings={shot:shot,time:time,b_field:b_field,r_fild:r_fild,z_fild:z_fild,$
		input_filename:input_filename,input_code:input_code,main_grid:main_grid,$
		strike_map_filename:strike_map_filename,strike_points_filename:strike_points_filename}

;****************************************

COMMON FILDSIM_RESULTS,fildsim_gyr,fildsim_pitch,fildsim_output_dist,fildsim_input_dist

fildsim_gyr=0.
fildsim_pitch=0.
fildsim_output_dist=0.
fildsim_input_dist=0.

;****************************************

COMMON PHYSIC_CONSTANTS,physic_constants

e_charge=1.6021765e-19 	; in C
proton_mass=938.272e6 	; in MeV/c^2
c=3.e8			; in m/s


physic_constants={e_charge:e_charge,proton_mass:proton_mass,c:c}

;****************************************

COMMON STRIKE_MAP_CALCULATOR,sm_settings

nm_runID='test'
nm_result_dir=''
nm_backtrace='.false.'
nm_N_gyroradius=INT(2)
nm_N_pitch=INT(2)
nm_save_orbits=INT(0)
nm_N_ions=INT(1000)
nm_step=0.01
nm_length=10.
nm_gyrophase=fltarr(2)
nm_start_x=fltarr(2)
nm_start_y=fltarr(2)
nm_start_z=fltarr(2)
nm_theta=0.
nm_phi=-12.
nm_geometry_dir=''
nm_N_scintillator=INT(1)
nm_N_slits=INT(5)

nm_alpha=0.
nm_beta=0.
nm_shot=LONG(30370)
nm_time=2.5
nm_bt=0.
nm_bz=0.
nm_br=0.
nm_r=2.
nm_z=0.3

nm_namelist_filename=''

nm_strike_map_filename=''
nm_strike_points_filename=''

fildsimf90_executable='.'+strtrim(FILDSIMf90_path,1)+'/bin/fildsim.exe'
machine='toki04'


sm_settings={nm_runID:nm_runID,nm_result_dir:nm_result_dir,nm_N_gyroradius:nm_N_gyroradius,nm_N_pitch:nm_N_pitch,nm_N_ions:nm_N_ions,nm_step:nm_step,$
	nm_save_orbits:nm_save_orbits,nm_backtrace:nm_backtrace,$
	nm_length:nm_length,nm_gyrophase:nm_gyrophase,nm_start_x:nm_start_x,$
	nm_start_y:nm_start_y,nm_start_z:nm_start_z,nm_theta:nm_theta,nm_phi:nm_phi,nm_geometry_dir:nm_geometry_dir,$
	nm_N_scintillator:nm_N_scintillator,nm_N_slits:nm_N_slits,$
	nm_alpha:nm_alpha,nm_beta:nm_beta,nm_shot:nm_shot,nm_time:nm_time,nm_bt:nm_bt,nm_br:nm_br,nm_bz:nm_bz,nm_r:nm_r,nm_z:nm_z,$
	nm_namelist_filename:nm_namelist_filename,nm_strike_map_filename:nm_strike_map_filename,$
	nm_strike_points_filename:nm_strike_points_filename,fildsimf90_executable:fildsimf90_executable,machine:machine}

;****************************************

COMMON STRIKE_MAP_CALCULATOR_FILENAMES,nm_scintillator_files,nm_slit_files

nm_scintillator_files=strarr(1)
nm_slit_files=strarr(5)

;****************************************

COMMON STRIKE_MAP_DATA,nm_strike_points,nm_strike_map,nm_resolutions

nm_strike_points=0.
nm_strike_map=0.
nm_resolutions=0.

;****************************************

COMMON ABSOLUTE_FLUX,af_bandpass_filter,af_bfield,af_filter,af_background_substraction,af_overlay_grid,af_ignore_fcol,af_A,af_Z,af_exp_time,af_pinhole_area,$
	af_calibration_frame,af_integrated_photon_flux,af_calibration_exp_time,af_calibration_area_pixel,$
	af_input_path,af_efficiency_curve_file,af_calibration_file,af_timetrace,af_mean_calibration_frame

af_bandpass_filter=100.
af_bfield=1.6
af_filter=INT(0)
af_background_substraction=INT(0)
af_overlay_grid=INT(0)
af_ignore_fcol=INT(0)
af_A=INT(2)
af_Z=INT(1)
af_exp_time=0.02
af_pinhole_area=1.e-6

af_calibration_frame=FLTARR(640,480)
af_integrated_photon_flux=7.83e18	; in photons/s/m^2
af_calibration_exp_time=0.003		; in s		;0.02
af_calibration_area_pixel=3.e-8		; in m^2

af_input_path=strtrim(GUI_routines_path)+'/config/'
af_efficiency_curve_file='TG_Green_yield/tg_green_yield_deuterium_9um.dat'
af_calibration_file='Integrating_sphere_calibrations/00714-002.png'

af_mean_calibration_frame=0.

;af={af_bandpass_filter:af_bandpass_filter,af_bfield:af_bfield,af_filter:af_filter,af_background_substraction:af_background_substraction,af_ignore_fcol:af_ignore_fcol,$
;	af_overlay_grid:af_overlay_grid,af_calibration_file:af_calibration_file,af_calibration_frame:af_calibration_frame,af_input_path:af_input_path,$
;	af_efficiency_curve_file:af_efficiency_curve_file,af_A:af_A,af_Z:af_Z,af_exp_time:af_exp_time,af_pinhole_area:af_pinhole_area,$
;	af_integrated_photon_flux:af_integrated_photon_flux,af_calibration_exp_time:af_calibration_exp_time,$
;	af_calibration_area_pixel:af_calibration_area_pixel}

;******************************************

COMMON AF_ROI_VARS,af_roi,af_flux_frame,af_heat_load_frame,af_flux,af_heat_load,af_photon_flux_frame,af_photon_flux

af_roi=0.
af_flux_frame=fltarr(640,480)
af_heat_load_frame=fltarr(640,480)
af_photon_flux_frame=fltarr(640,480)
af_flux=0.
af_heat_load=0.
af_photon_flux=0.

;****************************************
;
COMMON ORBIT,oc_shot,oc_time,oc_rini,oc_zini,oc_phiini,oc_energy,oc_pitch,oc_exp,oc_diag,oc_ed,$
    oc_r,oc_z,oc_phi,oc_mass,oc_charge,oc_nsteps,oc_fild_gyro

oc_shot=28061L
oc_time=2.0
oc_rini=2.18
oc_zini=0.32
oc_phiini=0.
oc_energy=93.e3
oc_pitch=60.
oc_exp='AUGD'
oc_diag='EQH'
oc_ed=0
oc_r=0.
oc_z=0.
oc_phi=0.
oc_mass=2.
oc_charge=1.
oc_nsteps=1.e4
oc_fild_gyro=3.

;******************************************

print,'************************'
print,'Check FILD GUI config'
print,'************************'
print,'Home path: ',home_path
print,'GUI routines path: ',gui_routines_path
print,'Results path: ',results_path
print,'FILDSIMf90 path: ',fildsimf90_path
print,'FILDSIM executable: ',fildsimf90_executable
print,'************************'


;***Realize the menu
WIDGET_CONTROL, main_base, /REALIZE
WIDGET_CONTROL,main_base, SET_UVALUE=uvalue


;*** Register the GUI (Hand control to X-Manager):***
XMANAGER, 'fild_gui',main_base


END
