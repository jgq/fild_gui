pro FILDSIM_read_strike_map,output,filename=filename

;*****************************************
; J. Galdon               19/01/2016
; University of Seville
; Email: jgaldon@us.es
;*****************************************
;****************************************************************
; Routine to read the strike-map filename given by fildsim.f90
;****************************************************************


IF keyword_set(filename) EQ 0 THEN BEGIN
	print,'You must set an input filename... Returning'
	return
ENDIF

dumm=''
gyroradius=0.
pitch_angle=0.
average_initial_gyrophase=0.
xx=0.
yy=0.
zz=0.
n_strike_points=0.
collimator_factor=0.
average_incidence_angle=0.

openr,lun,filename,/get_lun

readf,lun,dumm
readf,lun,dumm
readf,lun,dumm

WHILE NOT EOF(lun) DO BEGIN

	readf,lun,g1,p1,xx1,yy1,zz1,i1,n1,c2,i2

	gyroradius=[gyroradius,g1]
	pitch_angle=[pitch_angle,p1]
	average_initial_gyrophase=[average_initial_gyrophase,i1]
	xx=[xx,xx1]
	yy=[yy,yy1]
	zz=[zz,zz1]
	n_strike_points=[n_strike_points,n1]
	collimator_factor=[collimator_factor,c2]
	average_incidence_angle=[average_incidence_angle,i2]

ENDWHILE

close,lun
free_lun,lun

gyroradius=gyroradius(1:*)
pitch_angle=pitch_angle(1:*)
average_initial_gyrophase=average_initial_gyrophase(1:*)
xx=xx(1:*)
yy=yy(1:*)
zz=zz(1:*)
n_strike_points=n_strike_points(1:*)
collimator_factor=collimator_factor(1:*)
average_incidence_angle=average_incidence_angle(1:*)


output={gyroradius:gyroradius,pitch_angle:pitch_angle,average_initial_gyrophase:average_initial_gyrophase, $
	xx:xx,yy:yy,zz:zz,n_strike_points:n_strike_points,collimator_factor:collimator_factor,average_incidence_angle:average_incidence_angle}

return
end
