function get_vpara_FILD,pitch,B


;==========================================================================
;                   J. Galdon               16/01/2017
;==========================================================================
;
; From the definition:
;
; INPUTS:
;
;	pitch = in deg, as given by FILD strike map
;
;==========================================================================
; jgq 30.10.2020: currently this routine just converts pitch from FILd units [deg] into v_para/v
;==========================================================================

vpara_v=cos(pitch*2.*!pi/360.)

return,vpara
end
