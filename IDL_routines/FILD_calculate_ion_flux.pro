@get_energy_FILD.pro

pro FILD_calculate_ion_flux,output,photon_flux_frame,$
	efficiency_energy,efficiency_yield,b_field,band_pass_filter,$
	interpolated_gyro,interpolated_fcol,roi,$
	IGNORE_FCOL=IGNORE_FCOL,A=A,Z=Z,METHOD=METHOD


;*****************************************
; J. Galdon               15/03/2017
; University of Seville
; Email: jgaldon@us.es
;*****************************************
;****************************************************************
; Routine to convert photons/s into ions/s in a FILD frame
;
; photon_flux_frame --> photons/(s*m^2)
; efficiency_energy --> eV
; efficiency_yield --> Photons/ion
; b_field --> T
; interpolated_gyro --> cm
; interpolated_fcol --> %  
;
; absolute_flux_frame --> ions/(s*m^2) in the pinhole
; absolute_heat_load --> W/m^2 integrated
; absolute_heat_load_frame --> W/m^2
;****************************************************************

echarge=1.602e-19


IF keyword_set(IGNORE_FCOL) THEN BEGIN

	int_fcol=interpolated_fcol*0.+1.
	print,'Ignoring collimator factor --> Calculating Ion Flux at the scintillator not the pinhole!!!'

ENDIF ELSE BEGIN

	int_fcol=interpolated_fcol/100.

	ww=where(int_fcol LT 0.005,count)

	IF count GT 0 THEN int_fcol(ww)=0.		; We remove interpolated values being lower than 0.5%. Since fcol goes in the denominator of the formula,
							; very low values lead to diverging heat loads which are nonsense
ENDELSE


IF max(interpolated_gyro) EQ 0 THEN BEGIN

	print,'Interpolated matrix is calculated ONLY after a remap operation...Returning'

	return

ENDIF

;*******************************
; CALCULATE INTERPOLATED ENERGY
;*******************************

interpolated_energy=get_energy_FILD(interpolated_gyro,b_field,A=A,Z=Z)


;***********************************
; CALCULATE INTERPOLATED EFFICIENCY
;***********************************

efficiency_frame=interpol(efficiency_yield,efficiency_energy,interpolated_energy)
efficiency_frame>=1.

print,'WARNING!!! EFFICIENCY FRAME IS BEING FAKED CURRENTLY!! Forced to be > 1.'


;*******************************
; GET THE SPOTS REGIONS
;*******************************

spots_frame=photon_flux_frame
dumm_spots=bpass(photon_flux_frame,1,band_pass_filter)
ww=where(dumm_spots EQ 0,count)

IF count GT 0 THEN spots_frame(ww)=0.


IF keyword_set(SPOT_TRACKING) THEN BEGIN

	spot_track=feature(spots_frame,band_pass_filter,iterate=1)

ENDIF ELSE BEGIN

	spot_track=0.

ENDELSE

;*******************************
; APPLY MODEL
;*******************************


CASE method OF

	1: BEGIN

	;*******************************
	; GET THE SPOTS REGIONS (i.e: filter noise regions, to avoid artifacts due to low fcol)
	;*******************************

	spots_frame=photon_flux_frame
	dumm_spots=bpass(photon_flux_frame,1,band_pass_filter)
	ww=where(dumm_spots EQ 0,count)
	IF count GT 0 THEN spots_frame(ww)=0.
	absolute_flux_frame=spots_frame/(efficiency_frame)
	
	fcol_0_pos=where(int_fcol EQ 0,c1)
	fcol_finite_pos=where(int_fcol GT 0,c2)
	absolute_flux_frame(fcol_finite_pos)=absolute_flux_frame(fcol_finite_pos)/int_fcol(fcol_finite_pos)
	IF c1 GT 0 THEN absolute_flux_frame(fcol_0_pos)=0.


	absolute_flux=total(absolute_flux_frame,/DOUBLE,/NAN)
	absolute_heat_load_frame=absolute_flux_frame*interpolated_energy*echarge
	absolute_heat_load=total(absolute_heat_load_frame,/DOUBLE,/NAN)

	spot_track=0.

	END


	2:BEGIN


	;*******************************
	; GET THE SPOTS REGIONS
	;*******************************

	spots_frame=photon_flux_frame
	dumm_spots=bpass(photon_flux_frame,1,band_pass_filter)
	ww=where(dumm_spots EQ 0,count)

	IF count GT 0 THEN spots_frame(ww)=0.
	spot_track=feature(spots_frame,band_pass_filter,iterate=1)
	dumm=max(spot_track(2,*),bright_spot)

	print,'Using spot tracking algorithm to find mean collimator factor'

	absolute_flux_frame=spots_frame/(efficiency_frame)
	absolute_flux_frame=absolute_flux_frame/int_fcol(spot_track(0,bright_spot),spot_track(1,bright_spot))

	print,'Mean Collimator Factor: ',int_fcol(spot_track(0,bright_spot),spot_track(1,bright_spot))

	absolute_flux=total(absolute_flux_frame,/DOUBLE,/NAN)
	absolute_heat_load_frame=absolute_flux_frame*interpolated_energy*echarge
	absolute_heat_load=total(absolute_heat_load_frame,/DOUBLE,/NAN)

	END


	3:BEGIN

	print,'Using ROI method'

	roi_region=ROI
	help,roi_region

	spots_frame=photon_flux_frame
	absolute_flux_frame=photon_flux_frame*0.
	absolute_flux_frame(roi_region)=spots_frame(roi_region)/(efficiency_frame(roi_region))
	
	absolute_flux_frame=absolute_flux_frame/mean(int_fcol(roi_region))
	absolute_heat_load_frame=absolute_flux_frame*mean(interpolated_energy(roi_region))*echarge

	print,'Mean Collimator Factor: ',mean(int_fcol(roi_region))
	print,'Mean Energy (eV): ',mean(interpolated_energy(roi_region))

	absolute_flux=total(absolute_flux_frame,/DOUBLE,/NAN)
	absolute_heat_load=total(absolute_heat_load_frame,/DOUBLE,/NAN)

	spot_track=0.


	END

	ELSE: BEGIN

		print,'Select a method for the spot detection:'
		print,'1 --> Integrate whole frame --> Uses f_col and energy of each pixel individually'
		print,'2 --> Spot tracking method'
		print,'3 --> ROI method --> Uses mean f_col and energy values'
	END

ENDCASE

;*******************************


output={absolute_flux_frame:absolute_flux_frame,$
	absolute_flux:absolute_flux,$
	absolute_heat_load_frame:absolute_heat_load_frame,$
	absolute_heat_load:absolute_heat_load,$
	spot_track:spot_track,roi:roi}

return
end
