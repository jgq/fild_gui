@FILDSIM_build_weight_matrix
@FILDSIM_read_strike_points
@FILDSIM_read_strike_map

pro FILDSIM_tomography,output,measurement_file,pin_grid_params,scint_grid_params,strike_map_file,strike_points_file,efficiency_file,$
	LIMIT_REGION_FCOL=LIMIT_REGION_FCOL,A=A,Z=Z,BFIELD=BFIELD,GRID_ALIGNMENT_PARAMETERS=GRID_ALIGNMENT_PARAMETERS,$
	INVERSION_METHOD=INVERSION_METHOD,TSVD_VALUE=TSVD_VALUE,GAUSS_MODEL=GAUSS_MODEL,NON_CALIBRATED_FRAME=NON_CALIBRATED_FRAME,$
	EXPORT_BINARY=EXPORT_BINARY,OUTPUT_FILENAME=OUTPUT_FILENAME


;=====================================================================================================
;                   J. Galdon               24/03/2017
;=====================================================================================================
;
; Measurement_file --> Calibrated Frame (in photons/s) calculated with FILD_calculate_photon_flux and exported
;			by save_absolute_flux_vars in fild_gui_event.pro
;
; pin_grid --> {mingyr,maxgyr,dgyr,minpitch,maxpitch,dpitch}
;
; output frames are given in 1/(cm * deg) --> i.e: per unit velocity space
;=====================================================================================================

;***************************
; READ THE MEASUREMENT
;***************************


IF keyword_set(NON_CALIBRATED_FRAME) THEN BEGIN

	measurement_frame=read_png(measurement_file) 	

	measurement_frame=median(measurement_frame,3)		; apply filter for raw .png frames

	print,'Applying median filter to raw frame'

	print,max(measurement_frame),min(measurement_frame)	


ENDIF ELSE BEGIN

	restore,measurement_file,/verb
	measurement_frame=absolute_flux_vars.af_photon_flux_frame

ENDELSE

;***************************
; REMAP MEASUREMENT
;***************************

print,'Remember to set the grid alignment parameters correctly!'

FILD_remap_grid,remap,REF_FRAME=measurement_frame,STRIKE_MAP_FILENAME=strike_map_file,$
			GRID_PARAMS=GRID_ALIGNMENT_PARAMETERS,REMAP_set=scint_grid_params,OLD_ALGORITHM=0

measurement_2D=transpose(remap.h)
measurement_gyroradius=remap.gir
measurement_pitch=remap.pitch

print,max(measurement_2D),min(measurement_2d)


;*********************
; DEFINE PINHOLE GRID
;*********************

rpin=findgen((pin_grid_params.maxgyr-pin_grid_params.mingyr)/pin_grid_params.dgyr+1)*pin_grid_params.dgyr+pin_grid_params.mingyr
ppin=findgen((pin_grid_params.maxpitch-pin_grid_params.minpitch)/pin_grid_params.dpitch+1)*pin_grid_params.dpitch+pin_grid_params.minpitch

pin_grid={nr_pin:n_elements(rpin),np_pin:n_elements(ppin),rpin:rpin,ppin:ppin}

;****************************
; DEFINE SCINTILLATOR GRID
;****************************

rscint=findgen((scint_grid_params.maxgyr-scint_grid_params.mingyr)/scint_grid_params.dgyr+1)*scint_grid_params.dgyr+scint_grid_params.mingyr
pscint=findgen((scint_grid_params.maxpitch-scint_grid_params.minpitch)/scint_grid_params.dpitch+1)*scint_grid_params.dpitch+scint_grid_params.minpitch

scint_grid={nr_scint:n_elements(rscint),np_scint:n_elements(pscint),rscint:rscint,pscint:pscint}

;********************************************
; READ THE STRIKE MAP AND STRIKE POINTS FILE
;********************************************

print,'Reading FILDSIM data'
FILDSIM_read_strike_points,fildsim_strike_points,filename=strike_points_file
FILDSIM_read_strike_map,fildsim_strike_map,filename=strike_map_file
print,'FILDSIM data read'


;*******************************************
; LIMIT PINHOLE VEL SPACE REGION TO FCOL>0
;*******************************************

IF keyword_set(LIMIT_REGION_FCOL) THEN BEGIN

	print,'Grid Definition --> Limiting to regions where FCOL>0'

	ww=where(fildsim_strike_map.collimator_factor GT 0,count)
	dumm_gyro=fildsim_strike_map.gyroradius(ww)
	dumm_pitch=fildsim_strike_map.pitch_angle(ww)

	dumm_gyro = dumm_gyro[UNIQ(dumm_gyro, SORT(dumm_gyro))]
	dumm_pitch = dumm_pitch[UNIQ(dumm_pitch, SORT(dumm_pitch))]

	minr=min(dumm_gyro)
	maxr=max(dumm_gyro)

	minp=min(dumm_pitch)
	maxp=max(dumm_pitch)

	wg=where(rpin GE minr AND rpin LE maxr,ng)
	wp=where(ppin GE minp AND ppin LE maxp,np)
	
	pin_grid={nr_pin:ng,np_pin:np,rpin:rpin(wg),ppin:ppin(wp)}


ENDIF 

print,'****************************************'
print,'PINHOLE GRID'
print,strtrim(pin_grid.nr_pin,1)+' x '+strtrim(pin_grid.np_pin,1)
print,'Gyroradius (cm): ',min(pin_grid.rpin),max(pin_grid.rpin)
print,'Pitch Angle (�): ',min(pin_grid.ppin),max(pin_grid.ppin)
print,'****************************************'
print,'SCINTILLATOR GRID'
print,strtrim(scint_grid.nr_scint,1)+' x '+strtrim(scint_grid.np_scint,1)
print,'Gyroradius (cm): ',min(scint_grid.rscint),max(scint_grid.rscint)
print,'Pitch Angle (�): ',min(scint_grid.pscint),max(scint_grid.pscint)
print,'****************************************'


;***************************
; BUILD TRANSFER FUNCTION
;***************************

eff_file=efficiency_file
eff={A:A,Z:Z,B:BFIELD,efficiency_file:eff_file}

ww=FILDSIM_build_weight_matrix(fildsim_strike_map,fildsim_strike_points,scint_grid.rscint,scint_grid.pscint,pin_grid.rpin,pin_grid.ppin,EFFICIENCY=eff)


IF keyword_set(GAUSS_MODEL) THEN BEGIN

	weight_matrix_4D=ww.weight_matrix
	print,'Using the Gaussian model ...'

ENDIF ELSE BEGIN

	weight_matrix_4D=ww.weight_matrix_skew
	print,'Using the skew Gaussian model ...'

ENDELSE

;******************************
; COLLAPSE MEASUREMENT INTO 1D
;******************************

measurement_1D=dblarr(scint_grid.nr_scint*scint_grid.np_scint)

FOR ii=0,scint_grid.nr_scint-1 DO BEGIN
	FOR jj=0,scint_grid.np_scint-1 DO BEGIN
		
		measurement_1D(ii*scint_grid.np_scint+jj)=measurement_2D(ii,jj)
		
	ENDFOR
ENDFOR

weight_matrix_2D=dblarr(scint_grid.nr_scint*scint_grid.np_scint,pin_grid.nr_pin*pin_grid.np_pin)

;************************************
; COLLAPSE TRANSFER MATRIX INTO 2D
;************************************

FOR ii=0,scint_grid.nr_scint-1 DO BEGIN
	FOR jj=0,scint_grid.np_scint-1 DO BEGIN
		FOR kk=0,pin_grid.nr_pin-1 DO BEGIN
			FOR ll=0,pin_grid.np_pin-1 DO BEGIN
				
				weight_matrix_2D(ii*scint_grid.np_scint+jj,kk*pin_grid.np_pin+ll)=weight_matrix_4D(ii,jj,kk,ll)
				
			ENDFOR	
		ENDFOR	
	ENDFOR
ENDFOR


;********************
; PERFORM INVERSION 
;********************

CASE INVERSION_METHOD OF

	0: BEGIN

		print,'No inversion method selected ...'
		measurement_inversion_1D=-1
		measurement_inversion_2D=-1

	END

	1: BEGIN

		print,'***PERFORMING T-SVD INVERSION***'

		tic=systime(/seconds)
		print,'Computing SVD'

		LA_SVD,weight_matrix_2D,svd_w,svd_u,svd_v,/DOUBLE,status=stat,/DIVIDE_CONQUER

		print,'Status: ',stat
		print,'SVD computed'

		wfiltered=1./svd_w

		; filter w

		IF keyword_set(TSVD_VALUE) THEN BEGIN

			wfiltered(TSVD_value:*)=0.
			print,'keeping only the first SVD values:',tsvd_value

		ENDIF

		inv_res_matrix_2D=svd_v##diag_matrix(wfiltered)##transpose(svd_u)

		print,'Matrix inverted'

		measurement_inversion_1D=inv_res_matrix_2D#measurement_1D

		;*************************
		; RESHAPE MATRIX
		;*************************

		measurement_inversion_2D=dblarr(pin_grid.nr_pin,pin_grid.np_pin)
		
		FOR hh=0,pin_grid.nr_pin*pin_grid.np_pin-1 DO BEGIN

			measurement_inversion_2D(FLOOR(hh/pin_grid.np_pin),hh MOD pin_grid.np_pin)=measurement_inversion_1D(hh)

		ENDFOR

		measurement_inversion_2D>=0.

		toc=systime(/seconds)
		print,'It took (s):',toc-tic

	END

	ELSE: BEGIN

		print,'No inversion method selected ...'
		measurement_inversion_1D=-1
		measurement_inversion_2D=-1

	END


ENDCASE


;***************************
; EXPORT VARIABLES
;***************************

IF keyword_set(EXPORT_BINARY) THEN BEGIN

	pin_grid_gyro=FLOAT(pin_grid.rpin)
	pin_grid_pitch=FLOAT(pin_grid.ppin)

	scint_grid_gyro=FLOAT(scint_grid.rscint)
	scint_grid_pitch=FLOAT(scint_grid.pscint)

	pin_ngyro=n_elements(pin_grid_gyro)
	pin_npitch=n_elements(pin_grid_pitch)

	scint_ngyro=n_elements(scint_grid_gyro)
	scint_npitch=n_elements(scint_grid_pitch)

	weight_matrix_4D=FLOAT(weight_matrix_4D)
	weight_matrix_2D=FLOAT(weight_matrix_2D)

	
	measurement_2D=FLOAT(measurement_2D)
	measurement_1D=FLOAT(measurement_1D)

	binary_filename=strtrim(output_filename,1)+'.bin'

	print,'Writing info into: ',binary_filename

	openw,lun,binary_filename,/get_lun

	writeu,lun,pin_ngyro,pin_npitch
	writeu,lun,scint_ngyro,scint_npitch

	writeu,lun,pin_grid_gyro,pin_grid_pitch
	writeu,lun,scint_grid_gyro,scint_grid_pitch

	writeu,lun,weight_matrix_2D

	writeu,lun,measurement_2D
	writeu,lun,measurement_1D

	close,lun
	free_lun,lun

	print,'Finished'
ENDIF

;***************************
; CHECKS
;***************************

print,'Total flux of photons (ions/s) (in raw frame): ',total(measurement_frame)
print,'Total flux of photons (ions/(s*cm*deg))(after remap): ',total(measurement_2D)
print,'Total flux of photons (ions/s)(after remap): ',total(measurement_2D)*abs(rscint(1)-rscint(0))*abs(pscint(1)-pscint(0))
print,'Difference (%): ',(total(measurement_frame)-total(measurement_2D)*abs(rscint(1)-rscint(0))*abs(pscint(1)-pscint(0)))/total(measurement_frame)*100.

;***************************

output={pin_grid:pin_grid,scint_grid:scint_grid,measurement_2d:measurement_2D,measurement_1D:measurement_1D,weight_matrix_4D:ww.weight_matrix,$
	weight_matrix_2D:weight_matrix_2D,$
	measurement_inversion_2D:measurement_inversion_2D,measurement_inversion_1D:measurement_inversion_1D}


return
end