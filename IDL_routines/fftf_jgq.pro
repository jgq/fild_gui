function fftf_jgq,xx,yy,HANN=HANN,PLOT=PLOT

;=====================================================================================================
;                   J. Galdon               17/07/2020
;=====================================================================================================
;
; My own routine to calculate FFT in IDL. Based on fftf from unknown origin.
;
;=====================================================================================================

nxx=n_elements(xx)
dxx=(double(xx(nxx-1))-double(xx(0)))/(nxx-1.)

; Set up frequencies

dummy_xx=findgen((nxx-1)/2.)+1.

is_N_even=(nxx MOD 2) EQ 0

IF (is_N_even) THEN BEGIN

    freq=[0.,dummy_xx,nxx/2,-nxx/2+dummy_xx]/(nxx*dxx)

ENDIF ELSE BEGIN

    freq=[0.,dummy_xx,-(nxx/2+1)+dummy_xx]/(nxx*dxx)

ENDELSE

; Apply FFT

IF KEYWORD_SET(HANN) THEN BEGIN

    hann_yy=yy*hanning(n_elements(yy))
    fft_result=abs(fft(hann_yy))

    print,'Using Hanning window'

ENDIF ELSE BEGIN

    fft_result=abs(fft(yy))

ENDELSE

; PLOTTING

IF KEYWORD_SET(PLOT) THEN BEGIN

    !P.Multi=[0,1,2]
    window,0
    plot,xx,yy,xtitle='Signal x [a.u.]',ytitle='Signal y [a.u.]',charsize=2.,$
        color=0,background=255

    plot,freq,abs(fft_result),xtitle='Freq.',ytitle='Amplitude',charsize=2.,$
        color=0,background=255

    !P.Multi=0


ENDIF

; Outputs

output={freq:freq,fft_result:fft_result}

return,output
end
