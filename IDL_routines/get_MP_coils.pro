function get_MP_coils,shot

;=====================================================================================================
;                   J. Galdon               29/06/2021
;=====================================================================================================
;
; Routine to get MP coils currents
;
;=====================================================================================================

diag='MAW'
ncoils=8
signal_upper=strarr(ncoils)
signal_lower=strarr(ncoils)

phi=[45.,90.,135.,180.,225.,270.,315.,360.]

FOR kk=0,ncoils-1 DO BEGIN

    signal_upper(kk)='IBu'+strtrim(kk+1,1)
    signal_lower(kk)='IBl'+strtrim(kk+1,1)

ENDFOR

; Read signals

FOR kk=0,ncoils-1 DO BEGIN

    read_signal,ier,shot,diag,signal_upper(kk),tu,du,pu,edition=0
    read_signal,ier,shot,diag,signal_lower(kk),tl,dl,pl,edition=0

    IF kk EQ 0 THEN BEGIN


        I_upper=fltarr(ncoils,n_elements(tu))
        I_lower=fltarr(ncoils,n_elements(tu))
        time=tu

    ENDIF

    I_upper(kk,*)=du
    I_lower(kk,*)=dl

ENDFOR

output={shot:shot,signal_upper:signal_upper,signal_lower:signal_lower,$
    i_upper:i_upper,i_lower:i_lower,time:time,phi:phi}

return,output
end
