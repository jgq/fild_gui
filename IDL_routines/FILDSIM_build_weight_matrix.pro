@FILDSIM_calculate_all_resolutions
@get_energy_FILD
@FILD_read_scintillator_efficiency

function FILDSIM_build_weight_matrix,fildsim_strike_map,fildsim_strike_points,rscint,pscint,rpin,ppin,EFFICIENCY=EFFICIENCY,PLOT=PLOT

;=====================================================================================================
;                   J. Galdon               31/3/2017
;=====================================================================================================
; Routine to build the FILD weight matrix as defined in the FILDSIM model.
; The Weight matrix has the form: W_ijkl=T_ijkl * Eff_k
; where T is the transfer matrix (Probability) and Eff is the efficiency function (ions/photon).
;
; By default this routine builds the TRANSFER function. If you want to build the WEIGHT FUNCTION
; use the keyword EFFICIENCY.
;
; EFFICIENCY=structure:
;		A: Ion species mass
;		Z: Ion species charge
;		B: Magnetic field
;		efficiency_file: String
;=====================================================================================================

loadct,5
print,'Calculating FILD weight matrix'

;***********************
; LOAD STRIKE POINTS
;***********************

strike_map=fildsim_strike_map
strike_points=fildsim_strike_points

strike_map.collimator_factor/=100.

;***********************
; SCINT GRID
;***********************
nr_scint=n_elements(rscint)
np_scint=n_elements(pscint)

dr_scint=abs(rscint(1)-rscint(0))
dp_scint=abs(pscint(1)-pscint(0))

;***********************
; PINHOLE GRID
;***********************

nr_pin=n_elements(rpin)
np_pin=n_elements(ppin)
dr_pin=abs(rpin(1)-rpin(0))
dp_pin=abs(ppin(1)-ppin(0))

;********************************
; CALCULATE GAUSSIAN PARAMETERS
;********************************

res=FILDSIM_calculate_all_resolutions(strike_points,BINSIZE=dr_scint,STRIKE_POINTS_CALCULATED=1)

;********************************
; BUILD RESOLUTION AND FCOL MATRIX
;********************************

rpin_aux=strike_points.gyroradius[uniq(strike_points.gyroradius,sort(strike_points.gyroradius))]
ppin_aux=strike_points.pitch_angle[uniq(strike_points.pitch_angle,sort(strike_points.pitch_angle))]

nr_pin_aux=n_elements(rpin_aux)
np_pin_aux=n_elements(ppin_aux)
dr_pin_aux=abs(rpin_aux(1)-rpin_aux(0))
dp_pin_aux=abs(ppin_aux(1)-ppin_aux(0))


sigmar_aux=dblarr(nr_pin_aux,np_pin_aux)
sigmap_aux=dblarr(nr_pin_aux,np_pin_aux)
fcol_aux=dblarr(nr_pin_aux,np_pin_aux)
centroidr_aux=dblarr(nr_pin_aux,np_pin_aux)
centroidp_aux=dblarr(nr_pin_aux,np_pin_aux)

skewr_aux=dblarr(nr_pin_aux,np_pin_aux)
skewr_sigmar_aux=dblarr(nr_pin_aux,np_pin_aux)
skewr_centroidr_aux=dblarr(nr_pin_aux,np_pin_aux)

FOR kk=0,nr_pin_aux-1 DO BEGIN
	FOR ll=0,np_pin_aux-1 DO BEGIN

		dumm=where(res(0,*) EQ rpin_aux(kk) AND res(1,*) EQ ppin_aux(ll),count)
		dumm_fcol=where(strike_map.gyroradius EQ rpin_aux(kk) AND strike_map.pitch_angle EQ ppin_aux(ll))

		IF count GT 0 THEN BEGIN
			sigmar_aux(kk,ll)=res(2,dumm)
			sigmap_aux(kk,ll)=res(3,dumm)
			fcol_aux(kk,ll)=strike_map.collimator_factor(dumm_fcol)
			centroidr_aux(kk,ll)=res(4,dumm)
			centroidp_aux(kk,ll)=res(5,dumm)
			skewr_aux(kk,ll)=res(8,dumm)
			skewr_sigmar_aux(kk,ll)=res(10,dumm)
			skewr_centroidr_aux(kk,ll)=res(9,dumm)


		ENDIF

	ENDFOR
ENDFOR

;***************************************
; INTERPOLATE RESOLUTION AND FCOL MATRIX
;***************************************

ind_r=interpol(findgen(nr_pin_aux),rpin_aux,rpin)
ind_p=interpol(findgen(np_pin_aux),ppin_aux,ppin)

sigmar=interpolate(sigmar_aux,ind_r,ind_p,/grid)
sigmap=interpolate(sigmap_aux,ind_r,ind_p,/grid)
fcol=interpolate(fcol_aux,ind_r,ind_p,/grid,missing=0)

centroidr=interpolate(centroidr_aux,ind_r,ind_p,/grid)
centroidp=interpolate(centroidp_aux,ind_r,ind_p,/grid)


skewr=interpolate(skewr_aux,ind_r,ind_p,/grid)
skewr_sigmar=interpolate(skewr_sigmar_aux,ind_r,ind_p,/grid)
skewr_centroidr=interpolate(skewr_centroidr_aux,ind_r,ind_p,/grid)

;stop

;***************************************
; PLOT SIGMAS
;***************************************

IF KEYWORD_SET(PLOT) THEN BEGIN

    window,/free
    image,sigmar,rpin,ppin,title='Sigma gyro',xtitle='Gyroradius (cm)',ytitle='Pitch Angle (deg)',charsize=2.

    window,/free
    image,sigmap,rpin,ppin,title='Sigma pitch',xtitle='Gyroradius (cm)',ytitle='Pitch Angle (deg)',charsize=2.

ENDIF

;***************************************
; DEFINE EFFICIENCY VECTOR
;***************************************

IF keyword_set(EFFICIENCY) THEN BEGIN

	raw_eff=FILD_read_scintillator_efficiency(efficiency.efficiency_file)			; in photons/ion
	energy=get_energy_FILD(rpin,efficiency.B,A=efficiency.A,Z=efficiency.Z)	; in eV

	eff=interpol(raw_eff.yield,raw_eff.energy,energy)

	print,'Including scintillator yield in the Weight function'
	print,'Reading yield info from: ',efficiency.efficiency_file
    help,efficiency,/str

ENDIF ELSE BEGIN

	eff=fltarr(nr_pin)+1.
	print,'Not including scintillator yield --> Computing only FILD Transfer function'

ENDELSE

;***************************************
; BUILD THE MATRIX
;***************************************

tstart=systime(/seconds)

res_matrix=dblarr(nr_Scint,np_scint,nr_pin,np_pin)
res_matrix_skew=dblarr(nr_Scint,np_scint,nr_pin,np_pin)

print,'Computing weight matrix via SKEW GAUSSIAN MODEL'
print,'Computing weight matrix via GAUSSIAN MODEL'


FOR ii=0,nr_scint-1 DO BEGIN
	FOR jj=0,np_scint-1 DO BEGIN
		FOR kk=0,nr_pin-1 DO BEGIN
			FOR ll=0,np_pin-1 DO BEGIN

				dumm_fcol=fcol(kk,ll)

				IF dumm_fcol GT 0 THEN BEGIN

					res_matrix_skew(ii,jj,kk,ll)=eff(kk)*fcol(kk,ll)*dr_scint*dp_scint*$
						1./(!pi*skewr_sigmar(kk,ll)*sigmap(kk,ll))*$
						exp(-(rscint(ii)-skewr_centroidr(kk,ll))^2/(2.*skewr_sigmar(kk,ll)^2))*$
						0.5*(1+erf((skewr(kk,ll)*(rscint(ii)-skewr_centroidr(kk,ll))/skewr_sigmar(kk,ll))/sqrt(2)))*$
						exp(-(pscint(jj)-centroidp(kk,ll))^2/(2.*sigmap(kk,ll)^2))

					res_matrix(ii,jj,kk,ll)=eff(kk)*fcol(kk,ll)*dr_scint*dp_scint*$
							1./(2.*!pi*sigmar(kk,ll)*sigmap(kk,ll))*$
							exp(-(rscint(ii)-centroidr(kk,ll))^2/(2.*sigmar(kk,ll)^2))*$
							exp(-(pscint(jj)-centroidp(kk,ll))^2/(2.*sigmap(kk,ll)^2))

				ENDIF ELSE BEGIN

					res_matrix_skew(ii,jj,kk,ll)=0.
					res_matrix(ii,jj,kk,ll)=0.


				ENDELSE
			ENDFOR
		ENDFOR
	ENDFOR
ENDFOR

;*********************************************
; Keep only finite terms in weight matrix
;*********************************************

ff_skew=finite(res_matrix_skew)
dumm=where(ff_skew EQ 0,count)
IF count GT 0 THEN res_matrix_skew(dumm)=0.

ff=finite(res_matrix)
dumm=where(ff EQ 0,count)
IF count GT 0 THEN res_matrix(dumm)=0.

;**********************************************

tfinish=systime(/seconds)

print,'Weight matrix calculated'
print,'It took (s): ',tfinish-tstart


output={weight_matrix:res_matrix,weight_matrix_skew:res_matrix_skew,rpin:rpin,ppin:ppin,rscint:rscint,pscint:pscint,$
	sigmar:sigmar,sigmap:sigmap,fcol:fcol,centroidr:centroidr,centroidp:centroidp,$
	skewr:skewr,skewr_centroidr:skewr_centroidr,skewr_sigmar:skewr_sigmar}

return,output
end
