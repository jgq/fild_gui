pro FILD_remap_grid,o,REF_FRAME=REF_FRAME,STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME,$
			GRID_PARAMS=GRID_PARAMS,REMAP_set=REMAP_set,OLD_ALGORITHM=OLD_ALGORITHM


;==========================================================================
;                   J. Galdon               10/07/2014
;==========================================================================
;
; This routine remaps the strike-map into the orthogonal phase-space of the
; escaping ions.
;
; GRID_PARAMS --> Parameters for the grid alignment
; REMAP_SET --> Settings for the remap (grid size)
;==========================================================================

REF_FRAME=REF_FRAME
STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME
GRID_PARAMS=GRID_PARAMS
REMAP_SET=REMAP_SET

tic=systime(/seconds)

;***********************
;GET GRID INFORMATION
;***********************

FILDSIM_read_strike_map,strike_map,filename=strike_map_filename

IF n_tags(strike_map) EQ 0 THEN return

w=where(strike_map.collimator_factor GT 0,count)
ygrid=strike_map.yy(w)
zgrid=strike_map.zz(w)
gyr_grid=strike_map.gyroradius(w)
pitch_grid=strike_map.pitch_angle(w)
colfac=strike_map.collimator_factor(w)
ang=strike_map.average_incidence_angle(w)
gyrophase=strike_map.average_initial_gyrophase(w)

;*****************************
;GET CALIBRATION PARAMETERS
;*****************************

cal_params_pix=grid_params
IF n_tags(cal_params_pix) LT 2 THEN return

;************************************************
; LOOK FOR THE POSITION IN PIXELS OF THE POINTS
;************************************************

grid_pixel=FILD_get_pixel_coord(ygrid,zgrid,$
                               cal_params_pix.xscale_pix,cal_params_pix.yscale_pix,$
                               cal_params_pix.xshift_pix,cal_params_pix.yshift_pix,$
                               cal_params_pix.deg_pix)

;*******************************************************
;INTERPOLATION TO ASSOCIATE GYR AND PITCH TO EACH PIXEL
;*******************************************************

grid_interp=FILD_interp_grid(grid_pixel.xpixel,grid_pixel.ypixel,gyr_grid,pitch_grid,colfac,ang,gyrophase,ref_frame)

ginterp=grid_interp.ginterp
pinterp=grid_interp.pinterp
cinterp=grid_interp.cinterp
anginterp=grid_interp.anginterp
gyrophaseinterp=grid_interp.gyrophaseinterp

;***************
;PLOT HISTOGRAM
;***************

	maxgir=remap_set.maxgyr
	mingir=remap_set.mingyr         				 
	dg=remap_set.dgyr            						
	maxpitch=remap_set.maxpitch
	minpitch=remap_set.minpitch					
	dp=remap_set.dpitch	 					
	

	gir=findgen((maxgir-mingir)/dg+1)*dg+mingir
	pitch=findgen((maxpitch-minpitch)/dp+1)*dp+minpitch

	h=fltarr(n_elements(pitch),n_elements(gir))
	t=0
	dif=dg/10000.	;0.0001

	
IF keyword_set(OLD_ALGORITHM) THEN BEGIN

	FOR g=mingir,maxgir,dg DO BEGIN
      		FOR p=minpitch,maxpitch,dp DO BEGIN     

      			w1=where(ginterp GT g-dg/2 AND ginterp LE g+dg/2 AND pinterp GT p-dp/2 AND pinterp LE p+dp/2,count)

      			wg=where(gir GT g-dif AND gir LT g+dif)
      			wp=where(pitch GT p-dif AND pitch LT p+dif)         

               		if count gt 0 then begin
               			t=total(ref_frame(w1))  
               			h(wp,wg)=t
               		endif else begin
               		h(wp,wg)=0
               		endelse

      		ENDFOR
	ENDFOR

	sframe=size(ref_frame,/dimensions)
	remap_matrix=fltarr(n_elements(pitch),n_elements(gir),sframe(0),sframe(1))

ENDIF ELSE BEGIN

	sframe=size(ref_frame,/dimensions)
	remap_matrix=fltarr(n_elements(pitch),n_elements(gir),sframe(0),sframe(1))

	print,'Using Matrix Method for the remapping'

	FOR ii=0,sframe(0)-1 DO BEGIN
		FOR jj=0,sframe(1)-1 DO BEGIN

			wg=where(gir-dg/2 LT ginterp(ii,jj) AND gir+dg/2 GT ginterp(ii,jj),c1)
      			wp=where(pitch-dp/2 LT pinterp(ii,jj) AND pitch+dp/2 GT pinterp(ii,jj),c2)  
	
			IF c1 GT 0 AND c2 GT 0 THEN remap_matrix(wp,wg,ii,jj)=1
		
		ENDFOR
	ENDFOR

	;help,max(remap_matrix),min(remap_matrix)

tic2=systime(/seconds)

	FOR ii=0,sframe(0)-1 DO BEGIN
		FOR jj=0,sframe(1)-1 DO BEGIN
		
			dumm=remap_matrix(*,*,ii,jj)*ref_frame(ii,jj)
			h+=dumm
		ENDFOR
	ENDFOR

toc2=systime(/seconds)

print,'Matrix multiplication took (s): ',toc2-tic2

ENDELSE

h=h/(dg*dp)		; To get the output in units of counts/(deg * cm)

o={h:h,gir:gir,pitch:pitch,ginterp:ginterp,cinterp:cinterp,pinterp:pinterp,anginterp:anginterp,gyrophaseinterp:gyrophaseinterp,remap_matrix:remap_matrix}


toc=systime(/seconds)
elapsed_time=toc-tic
print,'It took (s): ',elapsed_time

return
end
