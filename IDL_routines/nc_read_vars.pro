function nc_read_vars,cdf_id,var_names

;=====================================================================================================
;                   J. Galdon               28/11/2021
;=====================================================================================================
;
; Routine to import variables from a netCDF file
;
;=====================================================================================================

nvars=n_elements(vars)

FOR kk=0,nvars-1 DO BEGIN

	ncdf_varget,cdf_id,vars(kk),dumm

ENDFOR

output={variables:variables}

return,output
end
