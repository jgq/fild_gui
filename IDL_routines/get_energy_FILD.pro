function get_energy_FILD,gyroradius,B,A=A,Z=Z


;==========================================================================
;                   J. Galdon               16/01/2017
;==========================================================================
;
; From the definition:
;
;
; INPUTS:
;	gyroradius= Particle Larmor radius as taken from FILD strike map (in cm)
;	B= Magnetic field (in T)
;
;==========================================================================

m_proton=938.272e6		; in eV/c^2
cc=3.e8

IF keyword_set(Z) THEN BEGIN

	Z=Z

ENDIF ELSE BEGIN

	Z=1
	print,'Using by default Z=1'

ENDELSE


IF keyword_set(A) THEN BEGIN
	A=A
ENDIF ELSE BEGIN
	A=2
	print,'A not specified, assuming deuterium A=2...'
ENDELSE

mass=m_proton*A		; in eV/c^2


energy=0.5*(gyroradius/100.*Z*B)^2/mass*cc^2	; in eV

return,energy
end
