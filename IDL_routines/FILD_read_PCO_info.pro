function FILD_read_PCO_info,ref_file

;=====================================================================================================
;                   J. Galdon               6/07/2015
;=====================================================================================================
;
;=====================================================================================================


ref_file=ref_file

	openr,lun,ref_file,/GET_LUN

	frame=0L & frameo=0L
	series=0L & serieso=0L
	exp_time=0. & exp_timeo=0.
	exp_start=0. & exp_start=0.
	header_1='' & header_2=''
	readf,lun,header_1,format='(A30)'
	readf,lun,header_2,format='(A30)'

	WHILE NOT EOF(lun) DO BEGIN
		readf,lun,frameo,serieso,exp_timeo,exp_starto
		frame=[frame,frameo]
		series=[series,serieso]
		exp_time=[exp_time,exp_timeo]
		exp_start=[exp_start,exp_starto]
	ENDWHILE

	frame=frame(1:*)
	series=series(1:*)
	exp_time=exp_time(1:*)
	exp_start=exp_start(1:*)
	
	close,lun
	FREE_LUN,lun

nframes=n_elements(frame)

output={frame:frame,series:series,exp_time:exp_time,exp_start:exp_start,nframes:nframes}

return,output
end