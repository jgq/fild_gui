@FILD_calculate_absolute_calibration_frame

pro FILD_calculate_absolute_flux_whole_shot_phantom,output,frames,phantom_times,phantom_background_frame,calibration_frame,roi_region,$
	efficiency_energy,efficiency_yield,b_field,band_pass_filter,interpolated_gyro,interpolated_fcol,$
	PINHOLE_AREA=PINHOLE_AREA,EXPOSURE_TIME=EXPOSURE_TIME,$
	CALIB_EXPOSURE_TIME=CALIB_EXPOSURE_TIME,PIXEL_AREA_COVERED=PIXEL_AREA_COVERED,$
	INT_PHOTON_FLUX=INT_PHOTON_FLUX,METHOD=METHOD,$
	IGNORE_FCOL=IGNORE_FCOL,A=A,Z=Z,$
	FILTER=FILTER,BACKGROUND_SUBSTRACTION=BACKGROUND_SUBSTRACTION,$
	REMAP_FRAMES=REMAP_FRAMES,STRIKE_MAP_FILENAME=strike_map_filename,$
	GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS



;*****************************************
; J. Galdon               02/04/2018
; University of Seville
; Email: jgaldon@us.es
;*****************************************
;
;
; Phantom frames = [Nx,Ny,Time]
;
;
;****************************************************************
; Routine to convert counts into photons/s in a FILD frame
;
; Int_photon_flux --> Photons/(s*m^2)
; Pixel_area_covered --> m^2
; Calib_exposure_time --> s
; exposure_time --> s
; pinhole_area --> m^2
; Calibration frame (input) --> Counts
;
; Raw Frame --> Counts
; Calibration frame (output) --> Counts*s*m^2/photons
; Photon flux frame --> Photons/(s*m^2)
;****************************************************************

;****************************************************************
; Routine to convert photons/s into ions/s in a FILD frame
;
; photon_flux_frame --> photons/(s*m^2)
; efficiency_energy --> eV
; efficiency_yield --> Photons/ion
; b_field --> T
; interpolated_gyro --> cm
; interpolated_fcol --> %  
;
; absolute_flux_frame --> ions/(s*m^2) in the pinhole
; absolute_heat_load --> W/m^2 integrated
; absolute_heat_load_frame --> W/m^2
;****************************************************************


inputs={efficiency_energy:efficiency_energy,efficiency_yield:efficiency_yield,$
	band_pass_filter:band_pass_filter,pinhole_area:pinhole_area,exposure_time:exposure_time,$
	calibration_exposure_time:calib_exposure_time,pixel_area_covered:pixel_area_covered,$
	method:method,ignore_fcol:ignore_fcol,A:A,Z:Z,FILTER:FILTER,BACKGROUND_SUBSTRACTION:BACKGROUND_SUBSTRACTION}


;***************************
; CHECK FRAMES SIZE
;***************************
s1=size(frames,/dimensions)
s2=size(calibration_frame,/dimensions)

nframes=s1(2)

IF n_elements(s2) GT 1 THEN BEGIN

	IF s1(0) NE s2(0) OR s1(1) NE s1(1) THEN BEGIN

		print,'Size of data frame and calibration frame not matching!!'
		print,'Use mean calibration frame method instead!!'
		return
	ENDIF

ENDIF ELSE BEGIN

	print,'Using mean calibration frame method --> Using single (mean) value instead of 2D array'

ENDELSE
			

;*******************************
; CALIBRATION FRAME
;*******************************

calibration_frame=FILD_calculate_absolute_calibration_frame(calibration_frame,exposure_time,pinhole_area,calib_exposure_time,int_photon_flux,pixel_area_covered)

;*******************************


echarge=1.602e-19


IF keyword_set(IGNORE_FCOL) THEN BEGIN

	int_fcol=interpolated_fcol*0.+1.
	print,'Ignoring collimator factor --> Calculating Ion Flux at the scintillator not the pinhole!!!'

ENDIF ELSE BEGIN

	int_fcol=interpolated_fcol/100.

	ww=where(int_fcol LT 0.005,count)

	IF count GT 0 THEN int_fcol(ww)=0.		; We remove interpolated values being lower than 0.5%. Since fcol goes in the denominator of the formula,
							; very low values lead to diverging heat loads which are nonsense
ENDELSE


IF max(interpolated_gyro) EQ 0 THEN BEGIN

	print,'Interpolated matrix is calculated ONLY after a remap operation...Returning'

	return

ENDIF

;*******************************
; CALCULATE INTERPOLATED ENERGY
;*******************************

interpolated_energy=get_energy_FILD(interpolated_gyro,b_field,A=A,Z=Z)


;***********************************
; CALCULATE INTERPOLATED EFFICIENCY
;***********************************

efficiency_frame=interpol(efficiency_yield,efficiency_energy,interpolated_energy)
efficiency_frame>=1.

print,'WARNING!!! EFFICIENCY FRAME IS BEING FAKED CURRENTLY!! Forced to be > 1.'


;*******************************
; APPLY MODEL
;*******************************

photon_flux_frames=frames*0.
photon_flux=fltarr(nframes)
	
absolute_flux_frames=photon_flux_frames
absolute_flux=photon_flux

absolute_heat_load_frames=photon_flux_frames
absolute_heat_load=photon_flux


CASE method OF

	1: BEGIN
	

	print,'Using full frame method: Collimator factor and energy interpolation applied to each pixel independently'

	FOR kk=0,nframes-1 DO BEGIN

		print,'Frame '+strtrim(kk+1,1)+' out of '+strtrim(nframes,1)

		current_frame=reform(frames(*,*,kk))

		IF keyword_set(FILTER) THEN BEGIN

			current_frame=median(current_frame,3)


		ENDIF

		IF keyword_set(BACKGROUND_SUBSTRACTION) THEN BEGIN

			current_frame=LONG(current_frame)-LONG(phantom_background_frame)
			current_frame>=0

		ENDIF


		photon_flux_frame=current_frame/calibration_frame
		dumm_photon_flux=total(photon_flux_frame,/DOUBLE,/NAN)


		spots_frame=photon_flux_frame
		dumm_spots=bpass(photon_flux_frame,1,band_pass_filter)
		ww=where(dumm_spots EQ 0,count)
		IF count GT 0 THEN spots_frame(ww)=0.
		absolute_flux_frame=spots_frame/(efficiency_frame)
	
		fcol_0_pos=where(int_fcol EQ 0,c1)
		fcol_finite_pos=where(int_fcol GT 0,c2)
		absolute_flux_frame(fcol_finite_pos)=absolute_flux_frame(fcol_finite_pos)/int_fcol(fcol_finite_pos)
		IF c1 GT 0 THEN absolute_flux_frame(fcol_0_pos)=0.


		dumm_absolute_flux=total(absolute_flux_frame,/DOUBLE,/NAN)
		
		absolute_heat_load_frame=absolute_flux_frame*interpolated_energy*echarge
		dumm_absolute_heat_load=total(absolute_heat_load_frame,/DOUBLE,/NAN)

		photon_flux_frames(*,*,kk)=photon_flux_frame
		photon_flux(kk)=dumm_photon_flux


		absolute_flux_frames(*,*,kk)=absolute_flux_frame
		absolute_flux(kk)=dumm_absolute_flux

		absolute_heat_load_frames(*,*,kk)=absolute_heat_load_frame
		absolute_heat_load(kk)=dumm_absolute_heat_load

		

		ENDFOR

	END


	2:BEGIN


	print,'Spot tracking method not working... '
	return
	
	END


	3:BEGIN

	print,'Using ROI method: Mean collimator factor and energy of the ROI are calculated'

	roi_region=roi_region
	help,roi_region

	FOR kk=0,nframes-1 DO BEGIN

		print,'Frame '+strtrim(kk+1,1)+' out of '+strtrim(nframes,1)

		current_frame=reform(frames(*,*,kk))


		IF keyword_set(FILTER) THEN BEGIN

			current_frame=median(current_frame,3)

		ENDIF

		IF keyword_set(BACKGROUND_SUBSTRACTION) THEN BEGIN


			current_frame=LONG(current_frame)-LONG(phantom_background_frame)
			current_frame>=0

		ENDIF


		photon_flux_frame=current_frame/calibration_frame
		dumm_photon_flux=total(photon_flux_frame,/DOUBLE,/NAN)

		spots_frame=photon_flux_frame
		absolute_flux_frame=photon_flux_frame*0.
		absolute_flux_frame(roi_region)=spots_frame(roi_region)/(efficiency_frame(roi_region))
	
		absolute_flux_frame=absolute_flux_frame/mean(int_fcol(roi_region))
		absolute_heat_load_frame=absolute_flux_frame*mean(interpolated_energy(roi_region))*echarge

		dumm_absolute_flux=total(absolute_flux_frame,/DOUBLE,/NAN)
		dumm_absolute_heat_load=total(absolute_heat_load_frame,/DOUBLE,/NAN)


		photon_flux_frames(*,*,kk)=photon_flux_frame
		photon_flux(kk)=dumm_photon_flux


		absolute_flux_frames(*,*,kk)=absolute_flux_frame
		absolute_flux(kk)=dumm_absolute_flux

		absolute_heat_load_frames(*,*,kk)=absolute_heat_load_frame
		absolute_heat_load(kk)=dumm_absolute_heat_load



	ENDFOR

	END

	ELSE: BEGIN

		print,'Select a method for the spot detection:'
		print,'1 --> Integrate whole frame'
		print,'2 --> Spot tracking method (Disabled at the moment)'
		print,'3 --> ROI method'
	END

ENDCASE

;*******************************
; REMAP FRAMES
;*******************************

IF keyword_set(REMAP_FRAMES) THEN BEGIN

	print,'Remapping absolutely calibrated frames ...'

	FOR kk=0,nframes-1 DO BEGIN

		print,'Frame '+strtrim(kk+1,1)+' out of '+strtrim(nframes,1)

		IF kk EQ 0 THEN BEGIN
	
			FILD_remap_grid,remap,REF_FRAME=reform(absolute_heat_load_frame(*,*,kk)),STRIKE_MAP_FILENAME=strike_map_filename,$
					GRID_PARAMS=GRID_PARAMS,REMAP_SET=REMAP_SETTINGS,OLD_ALGORITHM=0


			remapped_photon_flux_frames=fltarr(n_elements(remap.pitch),n_elements(remap.gir),nframes)
			remapped_ion_flux_frames=fltarr(n_elements(remap.pitch),n_elements(remap.gir),nframes)
			remapped_heat_load_frames=fltarr(n_elements(remap.pitch),n_elements(remap.gir),nframes)

		ENDIF


		FOR ii=0,s1(0)-1 DO BEGIN
			FOR jj=0,s1(1)-1 DO BEGIN
		
				dumm1=remap.remap_matrix(*,*,ii,jj)*photon_flux_frames(ii,jj,kk)
				remapped_photon_flux_frames(*,*,kk)+=dumm1

				dumm2=remap.remap_matrix(*,*,ii,jj)*absolute_flux_frames(ii,jj,kk)
				remapped_ion_flux_frames(*,*,kk)+=dumm2

				dumm3=remap.remap_matrix(*,*,ii,jj)*absolute_heat_load_frames(ii,jj,kk)
				remapped_heat_load_frames(*,*,kk)+=dumm3

			ENDFOR
		ENDFOR


	ENDFOR


	dg=abs(remap.gir(1)-remap.gir(0))
	dp=abs(remap.pitch(1)-remap.pitch(0))


	remapped_photon_flux_frames=remapped_photon_flux_frames/(dg*dp)		; in Photons/(s*m2*deg*cm)
	remapped_ion_flux_frames=remapped_ion_flux_frames/(dg*dp)		; in Ions/(s*m2*deg*cm)
	remapped_heat_load_frames=remapped_heat_load_frames/(dg*dp)		; in w/(m2*deg*cm)


	remapped_frames={remapped_photon_flux_frames:remapped_photon_flux_frames,$
			remapped_ion_flux_frames:remapped_ion_flux_frames,$
			remapped_heat_load_frames:remapped_heat_load_frames,$
			gyroradius:remap.gir,pitch:remap.pitch,strike_map_filename:strike_map_filename}


ENDIF ELSE BEGIN

	remapped_frames=-1

ENDELSE


;*******************************


output={photon_flux_frames:photon_flux_frames,$
	photon_flux:photon_flux,$
	absolute_flux_frames:absolute_flux_frames,$
	absolute_flux:absolute_flux,$
	absolute_heat_load_frames:absolute_heat_load_frames,$
	absolute_heat_load:absolute_heat_load,$
	roi:roi_region,time:phantom_times,inputs:inputs,remapped_frames:remapped_frames}


return
end
