pro plot_aug_walls,g,COL=COL


IF keyword_set(COL) THEN col=col ELSE col=0

drlim=((g.lim(0,*)-shift(g.lim(0,*),1))^2+(g.lim(1,*)-shift(g.lim(1,*),1))^2)^.5
wglim=where(drlim gt 0.5,ncc)


IF ncc gt 2 THEN BEGIN   		;asdex limiter is defined in noncontiguous pieces so plotting each individually

	FOR i=1,ncc-1 DO BEGIN

		oplot,g.lim(0,wglim(i-1):wglim(i)-1),g.lim(1,wglim(i-1):wglim(i)-1),color=col

	ENDFOR

ENDIF

return
end
