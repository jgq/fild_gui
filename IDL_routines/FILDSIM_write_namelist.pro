pro FILDSIM_write_namelist,runID,result_dir,backtrace,N_gyroradius,N_pitch,save_orbits,$
	N_ions,step,helix_length,gyroradius,pitch_angle,gyrophase_range,start_x,start_y,start_z,theta,phi,$
	geometry_dir,N_scintillator,N_slits,$
	scintillator_files,slit_files,FILENAME=FILENAME

;*****************************************
; J. Galdon               19/01/2016
; University of Seville
; Email: jgaldon@us.es
;*****************************************
;****************************************************************
; Routine to write a namelist to run a fildsim.f90 simulation
; INPUTS:
; runID: String 
; result_dir: String
; backtrace: String
; N_gyroradius: Integer
; N_pitch: Integer
; Save_orbits: Integer
; N_ions: Integer
; Step: Float
; Helix_length: Float
; Gyroradius: Float array (N_gyroradius)
; Pitch_angle: Float array (N_pitch)
; Gyrophase_range: Float array (2)
; Start_x : Float array (2)
; Start_y : Float array (2)
; Start_z : Float array (2)
; Theta: Float
; Phi: Float
; Geometry_dir: String
; N_scintillator: Integer
; N_slits: Integer
; Scintillator_files: String array (N_scintillator)
; Slit_files: String array (N_slits)
;
; TEST CALL:
; write_fildsim_namelist,'test','test_dir',4,6,100,0.01,12.,[2.,3.,6.],[10.,55.],[0.,3.1],[0.02,-0.02],[1.,1.],[0.,0.],0.,76.,'test_dir',1,1,['test_plate.pl'],['test_slit.pl']
;******************************************************************

IF keyword_set(FILENAME) EQ 0 THEN BEGIN

	print,'Filename not given... Saving NAMELIST as namelist_test.cfg'
	filename='namelist_test.cfg'

ENDIF

s=size(gyroradius,/dimensions)
gyroradius_string=''
FOR kk=0,s(0)-1 DO BEGIN
	gyroradius_string=gyroradius_string+' '+string(gyroradius(kk))+','
ENDFOR

s=size(pitch_angle,/dimensions)
pitch_string=''
FOR kk=0,s(0)-1 DO BEGIN
	pitch_string=pitch_string+' '+string(pitch_angle(kk))+','
ENDFOR

s=size(scintillator_files,/dimensions)
scintillator_files_string=''
FOR kk=0,s(0)-1 DO BEGIN
	scintillator_files_string=scintillator_files_string+" '"+scintillator_files(kk)+"',"
ENDFOR

s=size(slit_files,/dimensions)
slit_files_string=''
FOR kk=0,s(0)-1 DO BEGIN
	slit_files_string=slit_files_string+" '"+slit_files(kk)+"',"
ENDFOR


openw,lun,filename,/get_lun

printf,lun,'&config'
printf,lun,"runID='"+string(runID)+"',"
printf,lun,"result_dir='"+string(result_dir)+"',"
printf,lun,'backtrace='+strtrim(backtrace,1)+','
printf,lun,'N_gyroradius='+string(N_gyroradius,format='(I3)')+','
printf,lun,'N_pitch='+string(N_pitch,format='(I3)')+','
printf,lun,'save_orbits='+string(save_orbits,format='(I3)')+','
printf,lun,'/'

printf,lun,''

printf,lun,'&input_parameters'
printf,lun,'N_ions='+string(N_ions,format='(I10)')+','
printf,lun,'step='+string(step,format='(f12.7)')+','
printf,lun,'helix_length='+string(helix_length,format='(f12.7)')+','
printf,lun,'gyroradius='+gyroradius_string
printf,lun,'pitch_angle='+pitch_string
printf,lun,'gyrophase_range='+string(gyrophase_range(0),format='(f12.7)')+', ',+string(gyrophase_range(1),format='(f12.7)')+','
printf,lun,'start_x='+string(start_x(0),format='(f12.7)')+', ',+string(start_x(1),format='(f12.7)')+','
printf,lun,'start_y='+string(start_y(0),format='(f12.7)')+', ',+string(start_y(1),format='(f12.7)')+','
printf,lun,'start_z='+string(start_z(0),format='(f12.7)')+', ',+string(start_z(1),format='(f12.7)')+','
printf,lun,'theta='+string(theta,format='(f12.7)')+','
printf,lun,'phi='+string(phi,format='(f12.7)')+','
printf,lun,'/'

printf,lun,''

printf,lun,'&plate_setup_cfg'
printf,lun,"geometry_dir='"+geometry_dir+"',"
printf,lun,'N_scintillator='+string(N_scintillator,format='(I3)')+','
printf,lun,'N_slits='+string(N_slits,format='(I3)')+','
printf,lun,'/'

printf,lun,''

printf,lun,'&plate_files'
printf,lun,"scintillator_files="+scintillator_files_string
printf,lun,'slit_files='+slit_files_string
printf,lun,'/'

close,lun
free_lun,lun


return
end
