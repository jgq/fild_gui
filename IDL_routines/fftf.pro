pro fftf,t,s,fo,fftn,hann=hann,plotff=plotff

if n_elements(hann) lt 1 then hann=0
if n_elements(plotff) lt 1 then plotff=0
nnf=n_elements(t)
;dt=(double(t(10))-double(t(0)))/10.
nnf1=nnf*1.
dt=(double(t(nnf-1))-double(t(0)))/(nnf1-1.)

fo = LINDGEN(nnf/2.+1.) / (nnf*dt)


if hann eq 1 then begin
ss=s*hanning(n_elements(s))
fftn=abs(fft(ss))
endif else begin
fftn=abs(fft(s))
endelse

if plotff then begin
plot,fo,fftn,/ylog,charsize=2.,xtitle='Freq (Hz)',ytitle='Amp (a.u.)'
endif

end
