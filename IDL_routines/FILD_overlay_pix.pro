pro	FILD_overlay_pix,ref_image,output_image,xscale,yscale,xshift,yshift,deg,filter=filter,$
		FILENAME=FILENAME,SCINTILLATOR_PLATE=SCINTILLATOR_PLATE,EXPORT_VARS=EXPORT_VARS

;==========================================================================
;                   J. Galdon               1/10/2014
;==========================================================================
;
;==========================================================================

;***************
;READ GRID INFO
;***************

IF keyword_set(FILENAME) THEN BEGIN
	strike_map_filename=filename
ENDIF ELSE BEGIN
	print,'==============================================================';
	print,'Need to Select a FILDSIM_strike_map.dat... Returning '
	print,'=============================================================='
	return
ENDELSE


IF keyword_set(SCINTILLATOR_PLATE) THEN BEGIN

	scintillator_filename=scintillator_plate

ENDIF ELSE BEGIN

	scintillator_filename='/afs/ipp-garching.mpg.de/home/j/jgq/CODES_TEST/FILDSIM_v2/geometry/AUG/aug_fild1_scint.pl'

ENDELSE

;******************
;READ GRID POINTS
;******************

FILDSIM_read_strike_map,strike_map,filename=strike_map_filename

x=strike_map.yy
y=strike_map.zz
gyr_grid=strike_map.gyroradius
pitch_grid=strike_map.pitch_angle
colfac=strike_map.collimator_factor

FILDSIM_read_plate,plate,filename=scintillator_filename

;************
;READ FRAME
;************

ss=size(ref_image)

IF ss(0) EQ 0 THEN BEGIN
	Image=read_IMAGE(ref_image)
ENDIF ELSE BEGIN
	Image=ref_image
ENDELSE

Image=bytscl(image,max=max(image))
IF keyword_set(filter) THEN BEGIN
        filter=3
        Image=median(image,filter)        
ENDIF
                  
s=size(image,/dimensions)
im_size_x=s(0)
im_size_y=s(1)

;*********************************************
;PARAMETERS FOR GRID POSITIONING AND SCALING
;*********************************************

xscale=xscale
yscale=yscale
xshift=xshift
yshift=yshift
deg=deg
alfa=deg*!pi/180

;**************
;OVERLAY GRID
;**************

STR_MAP_POINTS=FILD_get_pixel_coord(x,y,xscale,yscale,xshift,yshift,deg)

x=str_map_points.xpixel
y=str_map_points.ypixel

PLATE_POINTS=FILD_get_pixel_coord(plate.y,plate.z,xscale,yscale,xshift,yshift,deg)
xplate=plate_points.xpixel
yplate=plate_points.ypixel

;******************
; EXPORT_VARS
;******************

IF keyword_set(EXPORT_VARS) THEN BEGIN

	export_vars_filename=export_vars
	strike_map_pixels={x:x,y:y}
	scintillator_pixels={x:xplate,y:yplate}
	strike_map_info={gyroradius:gyr_grid,pitch_angle:pitch_grid,colfac:colfac}
	save,filename=export_vars_filename,strike_map_pixels,scintillator_pixels,strike_map_info
	print,'Variables saved into: ',export_vars_filename
	return

ENDIF


;*************
;PLOT GRID
;*************

gir=gyr_grid[UNIQ(gyr_grid, SORT(gyr_grid))]
pitch=pitch_grid[UNIQ(pitch_grid,SORT(pitch_grid))]

set_plot,'Z'
device,set_resolution=[im_size_x,im_size_y]


tvscl,image
plots,xplate,yplate,/device
plots,x,y,psym=3,/device


for ii=0,n_elements(gir)-1 DO BEGIN
df=0.0001
tmp=where(abs(gyr_grid-gir(ii)) LT df,count)
vecx=x(tmp)
vecy=y(tmp)
plots,vecx,vecy,thick=1,/device
endfor

for ii=0,n_elements(pitch)-1 DO BEGIN
df=0.0001
tmp=where(abs(pitch_grid-pitch(ii)) LT df,count)
vecx=x(tmp)
vecy=y(tmp)
plots,vecx,vecy,thick=1,/device
endfor

output_image=tvrd()

set_plot,'X'


return
end
