function FILD_read_calibration_database,database

;=====================================================================================================
;                   J. Galdon               6/07/2015
;=====================================================================================================
;
; INPUT: 
; DATABASE: string containing the name of the database file. 
;=====================================================================================================

database=database

openr,lun,database,/GET_LUN

CAL_ID=LONG(0)
CAM_TYPE=''
PULSE_I=LONG(0)
PULSE_F=LONG(0)
XSHIFT=FLOAT(0)
YSHIFT=FLOAT(0)
XSCALE=FLOAT(0)
YSCALE=FLOAT(0)
DEG=FLOAT(0)
CAL_TYPE=''
FILD_ID=INT(0)

dumm=strarr(1)

readf,lun,dumm
readf,lun,dumm
readf,lun,dumm
readf,lun,dumm
readf,lun,dumm

WHILE NOT(EOF(LUN)) DO BEGIN

	dumm=''
	readf,lun,dumm
	dummy=strsplit(dumm,/EXTRACT)
	
	CAL_ID=[CAL_ID,dummy(0)]
	CAM_TYPE=[CAM_TYPE,dummy(1)]
	PULSE_I=[PULSE_I,dummy(2)]
	PULSE_F=[PULSE_F,dummy(3)]
	XSHIFT=[XSHIFT,dummy(4)]
	YSHIFT=[YSHIFT,dummy(5)]
	XSCALE=[XSCALE,dummy(6)]
	YSCALE=[YSCALE,dummy(7)]
	DEG=[DEG,dummy(8)]
	CAL_TYPE=[CAL_TYPE,dummy(9)]
	FILD_ID=[FILD_ID,dummy(10)]


ENDWHILE

close,lun
FREE_LUN,lun

CAL_ID=CAL_ID(1:*)
CAM_TYPE=CAM_TYPE(1:*)
PULSE_I=PULSE_I(1:*)
PULSE_F=PULSE_F(1:*)
XSHIFT=XSHIFT(1:*)
YSHIFT=YSHIFT(1:*)
XSCALE=XSCALE(1:*)
YSCALE=YSCALE(1:*)
DEG=DEG(1:*)
CAL_TYPE=CAL_TYPE(1:*)
FILD_ID=FILD_ID(1:*)

output={CAL_ID:CAL_ID,CAM_TYPE:CAM_TYPE,PULSE_I:PULSE_I,PULSE_F:PULSE_F,XSHIFT:XSHIFT,YSHIFT:YSHIFT,XSCALE:XSCALE,YSCALE:YSCALE,DEG:DEG,CAL_TYPE:CAL_TYPE,FILD_ID:FILD_ID}

return,output
end
