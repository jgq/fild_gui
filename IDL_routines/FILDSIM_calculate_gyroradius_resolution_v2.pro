pro exp_gauss,X,A,F,pder


;a(0)=mu
;a(1)=sigma
;a(2)=lambda
;a(3)=Normalization

F=a(3)*exp(a(2)/2*(2*a(0)+a(2)*a(1)^2-2*x))*erfc((a(0)+a(2)*a(1)^2-x)/(a(1)*sqrt(2.)))


 IF N_PARAMS() GE 5 THEN $
    pder = [[f], [A[0] * X * f], [replicate(1.0, N_ELEMENTS(X))]]


end

pro power_normal,X,B,F,pder

;b(0)=p
;b(1)=mu
;b(2)=sigma
;b(3)=Normalization


pdf=b(3)*exp(-((x-b(1))/b(2))^2/2)

cdf=0.5*(1+erf((-(x-b(1))/b(2))/sqrt(2)))

F=b(0)*pdf*cdf^(b(0)-1)

 IF N_PARAMS() GE 4 THEN $
    pder = [[f], [A[0] * X * f], [replicate(1.0, N_ELEMENTS(X))]]


end

pro skew_normal,X,C,F,pder

;*******************************************************************
; Skew Normal function is normalized to 1 when C(3)=sqrt(2./!pi)*1./c(2)
;*******************************************************************

;C(0)=alpha
;C(1)=mu
;C(2)=sigma
;C(3)=Normalization


pdf=C(3)*exp(-((x-C(1))/C(2))^2/2)

cdf=0.5*(1+erf((c(0)*(x-c(1))/c(2))/sqrt(2)))	;27.3.2017


F=pdf*cdf

 IF N_PARAMS() GE 4 THEN $
    pder = [[f], [A[0] * X * f], [replicate(1.0, N_ELEMENTS(X))]]


end




pro FILDSIM_calculate_gyroradius_resolution_v2,remap_gyro,output,BINSIZE=BINSIZE,PLOT=PLOT,PRINT_INFO=PRINT_INFO

;==========================================================================
;                   J. Galdon               19/01/2016
;==========================================================================
; Routine to calculate the gyroradius resolution of FILD from the data
; given by fildsim.f90
; Given a set of strike points, a histogram in gyroradius is calculated
; and fitted to a gaussian.
; INPUTS:
; remap_gyro: The remapped gyroradius of each strike point as given by fildsim.f90
;	The strike-points must correspond to only one pair Gyroradius-Pitch angle
; KEYWORDS:
; BINSIZE: Select the size of the histogram bins.
; PLOT: IF 1, then the histogram points are plotted together with the gaussian fit.
;==========================================================================


IF n_elements(remap_gyro) LE 5 THEN BEGIN

	print,'Number of strike points too low to perform resolution calculation...Returning'
	return

ENDIF

IF keyword_set(BINSIZE) EQ 0 THEN BEGIN

	BINSIZE=0.1

ENDIF ELSE BEGIN

	BINSIZE=BINSIZE

ENDELSE

h=histogram(remap_gyro,binsize=binsize,locations=x_hist,min=0.,max=20.)
n_strikes=total(h)
dh=x_hist(2)-x_hist(1)
h=h		;/(n_strikes)

;help,h,dh
;print,'dh'
;print,'Histogram Integral: ',total(h*dh)


cdf=TOTAL(h,/CUMULATIVE)/N_elements(remap_gyro)



;*****************************************
; GAUSSIAN FIT
;*****************************************

fitt=gaussfit(x_hist,h,coeff,NTERMS=3,CHISQ=chi_gauss)

; COMPUTE SKEWNESS
sk=skewness(h)


;*****************************************
; EXP NORMAL FIT
;*****************************************

;A=[coeff(1),coeff(2),1.,coeff(0)]
;weights=fltarr(n_elements(h))+1.
;print,'A before: ', a
;yfit=curvefit(x_hist,h,weights,A,FUNCTION_NAME='exp_gauss',status=stat,/NODERIVATIVE,chisq=chi,TOL=1.e-10,ITER=iterations,ITMAX=100)
;print,'Fit status: ',stat
;print,'Number of iterations performed: ',iterations
;print,'Exp Gaussian Fit parameters',a
;print,'Reduced Chi squared is: ',chi

;*****************************************
; POWER NORMAL FIT
;*****************************************

;B=[1.,coeff(1),coeff(2),coeff(0)]
;weights=fltarr(n_elements(h))+1.
;yfit2=curvefit(x_hist,h,weights,B,FUNCTION_NAME='power_normal',status=stat,/NODERIVATIVE,chisq=chi,TOL=1.e-8,ITER=iterations,ITMAX=100,$
		;FITA=[1.,1.,1.,1.])
;print,'Fit status: ',stat
;print,'Number of iterations performed: ',iterations
;print,'POWER NORMAL Fit parameters',b
;print,'Reduced Chi squared is: ',chi


;*****************************************
; SKEW NORMAL FIT
;*****************************************

weights=fltarr(n_elements(h))+1.
C=[1.,coeff(1),coeff(2),coeff(0)]
yfit2=curvefit(x_hist,h,weights,C,FUNCTION_NAME='skew_normal',status=stat,/NODERIVATIVE,chisq=chi_skew,TOL=1.e-8,ITER=iterations,ITMAX=100)

IF STAT NE 0 THEN BEGIN			; if no convergence is achieved then set parameters equal to normal gaussian

	c(0)=0
	c(1)=coeff(1)
	c(2)=coeff(2)
	c(3)=coeff(0)
ENDIF


;*************************************
; PLOT
;*************************************

IF keyword_set(PLOT) THEN BEGIN

	xx=findgen(1000)/999*20.

	pdf_=C(3)*exp(-((xx-C(1))/C(2))^2/2)
	cdf_=0.5*(1+erf((c(0)*(xx-c(1))/c(2))/sqrt(2)))

	maxh=max([cdf_*pdf_,coeff(0)])

	plot,x_hist,h,color=0,background=255,psym=10,xtitle='Gyroradius (cm)',ytitle='N strikes /N total',charsize=2.0,$
		xrange=[coeff(1)-5,coeff(1)+5],yrange=[0,maxh]

	xx=findgen(1000)/999*20.

	; NORMAL
	oplot,xx,coeff(0)*exp(-((xx-coeff(1))/coeff(2))^2/2),color=50,psym=-3


	; SKEW NORMAL
	oplot,xx,pdf_*cdf_,color=100,psym=-3


	xyouts,0.2,0.85,'Gaussian',/normal,charsize=1.5,color=50
    xyouts,0.2,0.8,'Gaussian sigma: '+strtrim(coeff(2),1),/normal,charsize=1.5,color=50
	xyouts,0.2,0.75,'Skew Normal',/normal,charsize=1.5,color=100
    xyouts,0.2,0.70,'Skew Normal sigma: '+strtrim(c(2),1),/normal,charsize=1.5,color=100

ENDIF

;***********************************
; PRINT INFO
;***********************************

IF keyword_set(PRINT_INFO) THEN BEGIN

	print,'********FIT TO GAUSSIAN****************************'

	print,'Fitted to gaussian'
	print,'Centroid: ',coeff(1)
	print,'Sigma: ',coeff(2)
	print,'Normalization: ',coeff(0)
	print,'Reduced Chi Squared is: ',chi_gauss
	print,'Skewness: ',sk

	print,'********FIT TO SKEW NORMAL*************************'

	print,'SKEW Fit status: ',stat
	print,'Number of iterations performed: ',iterations
	print,'skew: ',c(0)
	print,'Centroid: ',c(1)
	print,'Sigma: ',c(2)
	print,'Normalization: ',c(3)
	print,'Reduced Chi squared is: ',chi_skew


ENDIF


output={h:h,x_hist:x_hist,coeff:coeff,skew_normal:c}


return
end
