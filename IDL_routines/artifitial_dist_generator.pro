pro artifitial_dist_generator,energy,pitch,delta_energy,delta_pitch,n_markers,$
        A=A,Z=Z,WEIGHT=WEIGHT,OUTPUT_NAME=OUTPUT_NAME

;==========================================================================
;                   J. Galdon               20/05/2016
;==========================================================================
; Routine to generate an artifitial velocity space distribution of ions
; INPUTS
;	ENERGY (in eV)
;	PITCH (in v_para/v_tot)
;
; i.e: artifitial_dist_generator,6.e4,0.5,5.e3,0.005,1.e4,A=2,WEIGHT=3.8e+12
; i.e: (Tomography sensitivity test) artifitial_dist_generator,90.e3,0.34,0,0.1,5.e3,A=2,Z=1,output_name='artifitial_spot_140keV_pitch_65-75.dat'
;==========================================================================

;******************
; READ INPUTS
;******************

IF keyword_set(A) THEN BEGIN

	A=A

ENDIF ELSE BEGIN

	print,'No ion-mass specified. Using A=1 .'

	A=1
ENDELSE

IF keyword_set(Z) THEN BEGIN

	Z=Z

ENDIF ELSE BEGIN

	print,'No Z specified. Using Z=1.'

	Z=1

ENDELSE

IF keyword_set(WEIGHT) THEN BEGIN

	WEIGHT=weight

ENDIF ELSE BEGIN

	print,'No marker weight specified (in ions/s). Using weight=1.'

	weight=1.

ENDELSE


IF n_markers LT 10 THEN BEGIN

	print,'Number of markers selected is too small ... Make it larger than 10 at least...'
	return
ENDIF

print,'Total number of ions/s in the distributions will be: ',n_markers*weight

;**********************************
; GENERATE SYNTHETIC DISTRIBUTION
;**********************************

energy_dist=fltarr(n_markers)
pitch_dist=fltarr(n_markers)

; GAUSSIAN

energy_dist=energy+(randomu(seed,n_markers,/normal))*delta_energy
pitch_dist=pitch+(randomu(seed,n_markers,/normal))*delta_pitch

;********************
; PLOT
;********************

energy_histogram=histogram(energy_dist,locations=energy_values_hist,nbins=100)
pitch_histogram=histogram(pitch_dist,locations=pitch_values_hist,nbins=100)

window,0

!P.Multi=[0,1,2]

plot,energy_values_hist,energy_histogram,psym=-4,color=0,background=255,$
	xtitle='Energy (eV)',ytitle='N_markers',charsize=2.

plot,pitch_values_hist,pitch_histogram,psym=-4,color=0,background=255,$
	xtitle='Pitch Angle (v_para/v_tot)',ytitle='N_markers',charsize=2.


!P.Multi=0


;******************************************
; WRITE FILENAME FOR FILD SYNTHETIC INPUT
;******************************************

output_filename='test_artifitial_distribution'+strcompress(systime(),/remove_all)+'.dat'

IF keyword_set(OUTPUT_NAME) then output_filename=output_name

openw,lun,output_filename,/get_lun

	printf,lun,'Artifitial energy-pitch distribution created by artifitial_dist_generator.pro on '+systime()
	printf,lun,'Energy (eV)','Pitch (v_para/vtot)','A (mass number)','Z (charge number)','Weight (ions/s)'

	FOR kk=0L,n_markers-1 DO BEGIN

		printf,lun,energy_dist(kk),pitch_dist(kk),A,Z,weight,format='(e11.5,2x,f11.6,2x,I3,2x,I3,2x,e11.5)'

	ENDFOR

close,lun
free_lun,lun


return
end
