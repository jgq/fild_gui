pro FILDSIM_calculate_fild_orientation,bt,br,bz,alpha,beta,theta,phi,phi2

;*****************************************
; J. Galdon               19/01/2016
; University of Seville
; Email: jgaldon@us.es
;*****************************************
; Routine to calculate the magnetic field orientation with respect
; to FILD.
; INPUTS:
; Bt,Br,Bz: Magnetic field components in the standard reference.
; alpha: Poloidal orientation of FILD. Given in deg
; beta: Pitch orientation of FILD. Given in deg
; OUTPUTS:
; theta: Euler angle to use as input in fildsim.f90. Given in deg.
; phi: Euler angle to use as input in fildsim.f90. Given in deg.
;*****************************************

;***************************************************************
; In AUG the magnetic field orientation is counter current.
; FILDSIM.f90 works with the co-current reference
;***************************************************************

IF bt LT 0 THEN BEGIN

	bt=-bt
	br=-br
	bz=-bz

ENDIF

alpha=alpha*!pi/180.
beta=beta*!pi/180.

;*********************************************************************************************
; REFER THE BFIELD TO THE ORIENTATION OF THE FILD
; 2D ROTATION in R-Z. ALPHA is measured from R(positive getting away from axis) to Z(positive)
;*********************************************************************************************

bt1=bt
br1=br*cos(alpha)-bz*sin(alpha)
bz1=br*sin(alpha)+bz*cos(alpha)

;*********************************************************************************************
; ROTATE TO THE PITCH ORIENTATION OF FILD.
; 2D ROTATION in TOROIDAL-Z. BETA is measured from Toroidal (co-going) to Z (positive)
;*********************************************************************************************

br2=br1
bt2=bt1*cos(beta)-bz1*sin(beta)
bz2=bt1*sin(beta)+bz1*cos(beta)
btot2=sqrt(bt2^2+bz2^2+br2^2)

; NOW BT2,BZ2 and BR2 are the coordinates referred to the local FILD reference.
; According to FILDSIM.f90
; BT2 ---> Y component
; BR2 ---> X component
; BZ2 ---> Z component
; THEN theta and phi are:
; PHI --> Euler Angle measured from y(positive) to x(positive)
; THETA --> Euler Angle measured from x(positive) to z(negative)


theta=atan(-bz2/bt2)*180./!pi
phi=asin(br2/btot2)*180./!pi


print,'Bt, Bz, Br are: ',bt,bz,br,sqrt(bt^2+bz^2+br^2)
print,'FILD orientation is (alpha,beta)= ',alpha*180./!pi,beta*180./!pi
print,'Alpha rotation',bt1,bz1,br1,sqrt(bt1^2+bz1^2+br1^2)
print,'Bx By Bz in FILDSIM are: ',br2,bt2,bz2,btot2
print,'Euler angles are (phi,theta): ',phi,theta

return
end
