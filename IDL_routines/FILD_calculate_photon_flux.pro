@FILD_calculate_absolute_calibration_frame

pro FILD_calculate_photon_flux,output,raw_frame,calibration_frame,roi_region,$
	PINHOLE_AREA=PINHOLE_AREA,EXPOSURE_TIME=EXPOSURE_TIME,$
	CALIB_EXPOSURE_TIME=CALIB_EXPOSURE_TIME,PIXEL_AREA_COVERED=PIXEL_AREA_COVERED,$
	INT_PHOTON_FLUX=INT_PHOTON_FLUX,ROI=ROI


;*****************************************
; J. Galdon               15/03/2017
; University of Seville
; Email: jgaldon@us.es
;*****************************************
;****************************************************************
; Routine to convert counts into photons/s in a FILD frame
;
; Int_photon_flux --> Photons/(s*m^2)
; Pixel_area_covered --> m^2
; Calib_exposure_time --> s
; exposure_time --> s
; pinhole_area --> m^2
; Calibration frame (input) --> Counts
;
; Raw Frame --> Counts
; Calibration frame (output) --> Counts*s*m^2/photons
; Photon flux frame --> Photons/(s*m^2)
;****************************************************************


;***************************
; CHECK FRAMES SIZE
;***************************
s1=size(raw_frame,/dimensions)
s2=size(calibration_frame,/dimensions)

IF n_elements(s2) GT 1 THEN BEGIN

	IF s1(0) NE s2(0) OR s1(1) NE s1(1) THEN BEGIN

		print,'Size of data frame and calibration frame not matching!!'
		print,'Use mean calibration frame method!!'
		return
	ENDIF

ENDIF ELSE BEGIN

	print,'Using mean calibration frame method --> Using single (mean) value instead of 2D array'

ENDELSE

;*******************************
; CALIBRATION FRAME
;*******************************

calibration_frame=FILD_calculate_absolute_calibration_frame(calibration_frame,exposure_time,pinhole_area,calib_exposure_time,int_photon_flux,pixel_area_covered)

;*******************************
; CALIBRATE RAW FRAME
;*******************************

photon_flux_frame=raw_frame/calibration_frame
photon_flux=total(photon_flux_frame,/DOUBLE,/NAN)


;*******************************
; SELECT ROI
;*******************************

IF ROI EQ 3 THEN BEGIN

	print,'USING ROI METHOD FOR PHOTON FRAME CALCULATION'

	dumm_frame=photon_flux_frame*0.
	dumm_frame(roi_region)=photon_flux_frame(roi_region)

	photon_flux_frame=dumm_frame
	photon_flux=total(photon_flux_frame,/DOUBLE,/NAN)

ENDIF

;*******************************

output={photon_flux_frame:photon_flux_frame,$
	photon_flux:photon_flux,$
	calibration_frame:calibration_frame,$
	exposure_time:exposure_time,pinhole_Area:pinhole_area}

return
end
