function get_IDX_profile,shot,dd,ss,ee,PLOT=PLOT,PROF=PROF

;=====================================================================================================
;                   J. Galdon               28/11/2019
;=====================================================================================================
;
; Routine to read any IDX (IDA/IDI) profile
; read_signal from /afs/ipp/u/augidl/idl/user_contrib/mrm/readdata/read_signal.pro
;=====================================================================================================

IF KEYWORD_SET(PROF) THEN prof_=prof ELSE prof_=0

CASE PROF_ OF

    1: BEGIN

        diag='IDA'
        signal='ne'
        unc='ne_unc'

    END

    2: BEGIN

        diag='IDA'
        signal='Te'
        unc='Te_unc'

    END

    3: BEGIN

        diag='IDI'
        signal='vt'
        unc='vt_unc'

    END

    4: BEGIN

        diag='IDI'
        signal='vtdat'
        unc='vtdatunc'

    END

    5: BEGIN

        diag='IDI'
        signal='Ti'
        unc='Ti_unc'

    END

    6: BEGIN

        diag='IDI'
        signal='Tidat'
        unc='Tidatunc'

    END


    7: BEGIN

        diag='IDA'
        signal='pe'
        unc='pe_unc'

    END

    8: BEGIN

        diag='IDZ'
        signal='Zeff'
        unc='Zeff_unc'

    END

    
    ELSE: BEGIN

        diag=dd
        signal=ss
        unc=ee
    END

ENDCASE


print,'Using diag: ',diag
print,'Using signal: ',signal

;***************
; LOAD IDA
;***************

IF keyword_set(TIME_WINDOW) THEN BEGIN

  ti=time_window(0)
  tf=time_window(1)

ENDIF ELSE BEGIN

  ti=0.
  tf=10.

ENDELSE

read_signal,ier,shot,diag,signal,time,data,phys,edition=0,area_base=rhop
read_signal,ier,shot,diag,unc,time,error,phys,edition=0,area_base=rhop

IF ier NE 0 THEN BEGIN

  time=-1
  data=-1
  phys=-1

  print,'Did not read profiles'
  return,-1


ENDIF ELSE BEGIN

  dummy=size(rhop,/dim)
  ntime=dummy(0)
  nrho=dummy(1)

  data=data(*,0:nrho-1)
  error=error(*,0:nrho-1)

  print,'Read profiles correctly'

ENDELSE

;***************
; PLOT
;***************

IF KEYWORD_SET(PLOT) THEN BEGIN

    plot,rhop,data,color=0,background=255,/nodata,xtitle='Rhopol',ytitle=strtrim(signal,1)+' ['+strtrim(phys,2)+']',charsize=2.

    FOR kk=0,n_elements(time)-1 DO BEGIN

        oplot,rhop(kk,*),data(kk,*),psym=-4,color=kk*200./n_elements(time)

    ENDFOR


ENDIF

output={time:time,data:data,error:error,rhop:rhop,phys:phys}

return,output
end
