pro readpulse,pulse=pulse,time=time,dtime=dtime,tdelay=tdelay, frames=frames,cintime=cintime,PIXEL_DEPTH_8=pixel_depth_8,error

pulse=LONG(pulse)
;print,'pulse:',pulse

error=0

if not keyword_set(tdelay) then begin 
tdelay = 0.0
if pulse lt 24326L then begin
 print,''
 print,'WARNING!!! pulses before 24326L usually are recorded with a time delay'
 print,'lookup the delay in:'
 print,'/u/augd/rawfiles/FIT/pulse_journal_22XXX.txt'
 print,'/u/augd/rawfiles/FIT/pulse_journal_23XXX.txt and' 
 print,'/u/augd/rawfiles/FIT/pulse_journal_24XXX.txt' 
endif
endif


;cinfile='/afs/ipp-garching.mpg.de/u/augd/rawfiles/FIT/'+$

;hhost=getenv('HOSTNAME')
;dumm='toki01.bc.rzg.mpg.de'

;IF hhost EQ dumm THEN BEGIN
;	print,'We are in toki01 ... searching for the correct FIT path'
;	cinfile='/p/IPP/AUG/rawfiles/FIT/'+$
;	STRING(pulse/1000,FORMAT='(I2)')+'/'+STRING(pulse, FORMAT='(I5)')+'_v710.cin'
;
;ENDIF

;IF pulse GT 32070L THEN BEGIN

IF pulse GT 0L THEN BEGIN		; All shots already moved to /gpfs/ ... For some reason function CINreadHeaderInfo does not work
					; in .cin files stored under /u/augd/rawfiles/... but do work for files stored under /gpfs/...

print,'For shot# GT 32070 Phantom camera videos are stored under /gpfs/aug/rawfiles/FIT'
print,'To access these you must me logged in sxaug20/ 22 / 24 / 26 OR sxidl29'

	cinfile='/gpfs/aug/rawfiles/FIT/'+$
	STRING(pulse/1000,FORMAT='(I2)')+'/'+STRING(pulse, FORMAT='(I5)')+'_v710.cin'

	cinfile='/p/IPP/AUG/rawfiles/FIT/'+$
	STRING(pulse/1000,FORMAT='(I2)')+'/'+STRING(pulse, FORMAT='(I5)')+'_v710.cin'

ENDIF

print, 'Opening pulse', pulse, ' // Cinfile: ',cinfile
ierr=CINreadHeaderInfo(cinfile, CinHeaderInfo,verbose=1)

if ierr ne 0 then begin 
 print,'Error opening file ',cinfile
error=1
 return
endif

cintime=CINHeaderInfo.timebase+tdelay

dx=cinheaderinfo.size_x
dy=cinheaderinfo.size_y

f0=-1L
f1=-1L
for i=0L,n_elements(cintime)-2 do begin
 if (cintime[i] le time) and (cintime[i+1] gt time) then f0=i
 if (cintime[i] le time+dtime) and (cintime[i+1] gt time+dtime) then f1=i 
endfor

if (f0 lt 0) or (f1 lt 0) then begin
 print,'the specified time range is not valid'
error=1
 return
endif

IF keyword_set(PIXEL_DEPTH_8) THEN BEGIN
	print, 'Loading frames',f0,' to ',f1,' (',time,'s to ',time+dtime,' s)'
	ierr=CINreadFrames(cinfile,CinHeaderInfo,Frames,OFFSET=f0,NFRAMES=f1-f0,PIXEL_DEPTH_8=1)
	cintime=cintime[f0:f1]
ENDIF ELSE BEGIN
	print, 'Loading frames',f0,' to ',f1,' (',time,'s to ',time+dtime,' s)'
	ierr=CINreadFrames(cinfile,CinHeaderInfo,Frames,OFFSET=f0,NFRAMES=f1-f0)
	cintime=cintime[f0:f1]
ENDELSE

end
