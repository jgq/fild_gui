pro FILD_video_ccd,FILE_PATH=FILE_PATH,$
	PULSE=PULSE,TIME=TIME,FRAMES=FRAMES,RES_PATH=RES_PATH,$
	xshift,yshift,xscale,yscale,alfa,grid_image_file,STRIKE_MAP_FILENAME=STRIKE_MAP_FILENAME,$
	OVERLAY_GRID=OVERLAY_GRID

;==========================================================================
;                   J. Galdon               13/03/2016
;==========================================================================
; 
;==========================================================================

PULSE=PULSE
TIME=TIME
FRAMES=FRAMES
OUTPUT_PATH=RES_PATH

;**************
;LOAD FRAMES
;**************

n_frames=n_elements(frames)

;******************
;READ FIRST FRAME
;******************

jj=frames(0)

frame_num=string('000')		
num=string(jj,format='(I3)')
num=strcompress(num,/remove_all)
l=strlen(num)
strput,frame_num,num,3-l
		
image_filename=file_path+str(pulse)+'-'+str(frame_num)+'.png'
image=read_png(image_filename)

s=size(image,/dimensions)
xsize=s(0)
ysize=s(1)


DATA=FLTARR(xsize,ysize,n_frames)

print,'Reading frames into memory... Can take a while'
kk=0
FOR jj=frames(0),frames(n_frames-1) DO BEGIN

		frame_num=string('000')		
		num=string(jj,format='(I3)')
		num=strcompress(num,/remove_all)
		l=strlen(num)
		strput,frame_num,num,3-l
		
		image_filename=file_path+str(pulse)+'-'+str(frame_num)+'.png'
		image=read_png(image_filename)

		data(*,*,kk)=image
		kk+=1
ENDFOR

data=bytscl(data,max=max(data))     
s=size(data,/dimensions)
xsize=s(0)
ysize=s(1)

;**********************
;GRID PARAMETERS
;**********************

gridImage=read_bmp(grid_image_file)

gridSize=size(gridImage)

xGRIDSize=gridsize[1]
yGRIDsize=gridsize[2]

gridInd=max(gridImage)

print,xgridsize,ygridsize

;*****************************
;CALIBRATION PARAMETERS
;*****************************

xshift=xshift
yshift=yshift
xscale=xscale
yscale=yscale
alfa=alfa*!pi/180

print,xshift,yshift,xscale,yscale,alfa,xsize,ysize


;**************
;CREATE MPEG
;**************

Filename_save=OUTPUT_PATH+strtrim(pulse,2)+'_'+strtrim(time(0),2)+'_'+strtrim(time(n_elements(time)-1),2)+'_ccd.mpeg'
mpegID = MPEG_Open([xsize, ysize], Filename=filename_save,quality=100)


set_plot,'X'

FOR j=0,n_frames-1 DO BEGIN
      print,j,' out of ',n_frames,'  frames loaded.'

;******************
;OVERLAY GRID
;******************

IF keyword_set(OVERLAY_GRID) THEN BEGIN

	ref_image=reform(data(*,*,j))

	FILD_overlay_pix,ref_image,output_image,xscale,yscale,xshift,yshift,alfa,filter=3,$
		FILENAME=strike_map_filename,SCINTILLATOR_PLATE=0,EXPORT_VARS=0
	data_plot=output_image

ENDIF ELSE BEGIN

	data_plot=median(reform(data(*,*,j)),3)

ENDELSE

;********************
;ROTATE WHOLE IMAGE
;********************

	IF abs(alfa) GT 140 THEN BEGIN

		data_plot=rotate(data_plot,2)

	ENDIF

;*******************************
; USE Z BUFFER TO OVERLAY TIME
;*******************************

	set_plot,'Z'
	device,set_pixel_depth=24,decomposed=0,set_resolution=[xsize,ysize],set_colors=256          ; resolution sets the size of the buffer
	tvscl,data_plot

	XYOUTS, 10, 10, 't='+strtrim(string(time[j],format='(f6.3)'))+' s', color=255,charsize=1 ,/device       ; here we can write anything to add to the frames
	XYOUTS,100,10,STRING('Pulse: ',pulse,format='(A,I0)'),color=255,charsize=1 ,/device

	data_2=TVRD(/true)                        

	MPEG_Put, mpegID, image=data_2, Frame=j,/ORDER,/color   


ENDFOR

set_plot,'X'

print,'Saving Mpeg video into '+OUTPUT_PATH+'      ... can take a while'
MPEG_Save, mpegID
MPEG_Close, mpegID



return
end
