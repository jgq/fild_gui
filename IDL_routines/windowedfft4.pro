pro windowedfft4,sig_raw,t_raw,nff,tmin,tmax,overlap,fsmot,ampsp,times,freq,output,$
	hann=hann,power=power,crosspower=crosspower,sigb_raw=sigb_raw,tb_raw=tb_raw,complexcp=complexcp,$
	detrend=detrend,crosscoh=crosscoh,coherence=coherence,conflev=conflev,phase=phase,$
	fsmotafter=fsmotafter,freqmin=freqmin,freqmax=freqmax,plott=plott

;***************************************************************************************
;REMOVES THE AVERAGE OF EACH SUB WINDOW
;sig = data series
;t = time series
;nff = number of points in fft, power of 2 for sure
;tmin= starting time
;tmax= ending time
;overlap = % window overlap  use multiple of 10 (i.e. 50 for 50% overlap)
;fsmot = number of kHz to smooth over
;ampsp = returned amplitude spectrum
;times = returned array of center times for windows
;freq = frequencies for ampsp
;output= output structure
;hann ne 0 means use hanning window on each subseries
;power ne 0 means return power spectrum
;crosspower ne 0 means return cross-power spectrum - must also set sig2
;complexcp means return complex cross-power no smoothing done, must also set crosspower
;detrend means it will fit a line to each time series and remove that
;****************************************************************************************

if n_elements(hann) lt 1 then hann=0
if n_elements(power) lt 1 then power=0
if n_elements(crosspower) lt 1 then crosspower=0
if n_elements(complexcp) lt 1 then complexcp=0
if n_elements(coherence) lt 1 then coherence=0
if n_elements(crosscoh) lt 1 then crosscoh=0
if n_elements(detrend) lt 1 then detrend=0
if n_elements(phase) lt 1 then phase=0
if n_elements(fsmotafter) lt 1 then fsmotafter=0
if n_elements(freqmax) lt 1 then freqmax=0.
if n_elements(freqmin) lt 1 then freqmin=0.
if n_elements(plott) lt 1 then plott=0
hanvar=hann

print, 'Min/Max Time (s) = ',min(t_raw),max(t_raw)
print,'Requested ',tmin,' ',tmax

nff=long(nff)
nts=n_elements(t_raw)
dtt=(t_raw(nts-1)-t_raw(0))/(nts-1)
nwin=LONG(1./(1.-overlap*.01)*(tmax-tmin)/(dtt*nff)+1)
times=fltarr(nwin)


;****GET SIGNAL IN DESIRED TIME INTERVAL********
mec=min(abs(tmin-t_raw),n0)
mec=min(abs(tmax-t_raw),n1)
sig=sig_raw(n0-long(nff):n1+long(nff))
t=t_raw(n0-long(nff):n1+long(nff))
mec=min(abs(tmin-t),n0)
mec=min(abs(tmax-t),n1)
;***********************************************

IF n_elements(sigb_raw) gt 1 THEN BEGIN
	ntsb=n_elements(tb_raw)*1.
	dttb=(tb_raw(ntsb-1)-tb_raw(0))/(ntsb-1)
	nwinb=LONG(1./(1.-overlap*.01)*(tmax-tmin)/(dttb*nff)+1)
	timesb=fltarr(nwinb)

	;****GET SIGNAL IN DESIRED TIME INTERVAL********
	mec=min(abs(tmin-tb_raw),n0b)
	mec=min(abs(tmax-tb_raw),n1b)
	sigb=sigb_raw(n0b-long(nff):n1b+long(nff))
	tb=tb_raw(n0b-long(nff):n1b+long(nff))
	mec=min(abs(tmin-tb),n0b)
	mec=min(abs(tmax-tb),n1b)
	;***********************************************

	t_ratio=dtt/dttb

	CASE 1 OF 							;interpolate to make the same time basis to both signals
		(t_ratio GT 1.0): BEGIN
					print,'Interpolating signal 1'
					sig=interpolate(sig,findgen(n_elements(tb))/(n_elements(tb)-1)*(n_elements(t)-1),cubic=-0.1)
					t=interpolate(t,findgen(n_elements(tb))/(n_elements(tb)-1)*(n_elements(t)-1))
					nwin=LONG(1./(1.-overlap*.01)*(tmax-tmin)/(dttb*nff)+1)
					n0=n0b & n1=n1b & dtt=dttb

				  END
		ELSE: BEGIN
			sigb=interpolate(sigb,findgen(n_elements(t))/(n_elements(t)-1)*(n_elements(tb)-1),cubic=-0.1)
			tb=interpolate(tb,findgen(n_elements(t))/(n_elements(t)-1)*(n_elements(tb)-1))
			nwin=LONG(1./(1.-overlap*.01)*(tmax-tmin)/(dtt*nff)+1)
		      END
	ENDCASE



ENDIF

ampsp=fltarr(nwin,nff/2+1)
ampsp_s1=ampsp
ampsp_s2=ampsp
times=fltarr(nwin)



IF detrend THEN PRINT,'Doing linear detrending'
IF hann THEN PRINT,'Using Hanning window'
print,'Number of windows: ',nwin

;*******************************************************
;************* BEGIN LOOP OVER WINDOWS******************
;*******************************************************

FOR i=0,nwin-1 DO BEGIN

sig2=sig(n0-long(nff)/2:n0+long(nff)/2-1)  					;get elements from the sigan corresponding to the window n_i
;sig2=sig((nff-n0)/2-1:(n0+nff/2))						; changed by JGQ 7/1/2015

sig2=sig2-avg(sig2)  								;removing average


;*********Detrending w/ linear detrending if requested***********

IF detrend THEN BEGIN

	;tshor=t((nff-n0)/2-1:n0+nff/2)
	tshor=t(n0-nff/2:n0+nff/2-1)    					; JGQ 7/1/2015

	adt=poly_fit(tshor,sig2,1,yfit=dtfit)
	sig2=sig2-dtfit
ENDIF

IF n_elements(sigb_raw) gt 1 THEN BEGIN
	       					; consider a second signal
	sig2b=sigb(n0-nff/2:n0+nff/2-1)
	;sig2b=sigb((nff-n0)/2-1:n0+nff/2)

	sig2b=sig2b-avg(sig2b)  						;removing average

	IF detrend THEN BEGIN
		adt=poly_fit(tshor,sig2b,1,yfit=dtfitb)
		sig2b=sig2b-dtfitb
	ENDIF

;*******Mulitplying by Hanning window if requested***************

	IF hann ne 0 THEN sig2b=sig2b*hanning(n_elements(sig2b))
ENDIF



IF hann ne 0 THEN sig2=sig2*hanning(n_elements(sig2))

;****************************************************************


tsig2=t(n0-nff/2:n0+nff/2-1)    						;time of window is just average of timebase

;tsig2=t((nff-n0)/2-1:(n0+nff/2))   						; changed by JGQ 7/1/2015

t0=avg(tsig2)


IF i eq 0 THEN typp='AutoAmp'
IF i eq 0 and power ne 0 THEN typp='AutoPower w/ units'

;*********Using fftf routine to get frequency base**************

fftf,tsig2,sig2,fo,fftn,hann=hanvar
fftn=abs(fftn)
nsmot=fix(fsmot/(fo(1)-fo(0)))  						;checking to see how many to smooth over


IF power ne 0 THEN BEGIN    							;If requesting actual power in real units
	pow=fft2ps(fftn,sig2,dtt)
	pow=abs(pow)
ENDIF

fftn=abs(fftn)

IF nsmot gt 2 THEN BEGIN
	fftn=smooth(fftn,nsmot)
	IF power ne 0 THEN pow=smooth(pow,nsmot)
ENDIF

IF n_elements(sigb_raw) gt 1 THEN BEGIN

	fftf,tsig2,sig2b,fob,fftnb,hann=hanvar
	fftnb=abs(fftnb)
	IF power ne 0 THEN BEGIN    							;If requesting actual power in real units
		pow2=fft2ps(fftnb,sig2b,dtt)
		pow2=abs(pow2)
	ENDIF
	fftnb=abs(fftnb)

	IF nsmot gt 2 THEN BEGIN
		fftnb=smooth(fftnb,nsmot)
		IF power ne 0 THEN pow2=smooth(pow2,nsmot)
	ENDIF

ampsp_s2(i,*)=fftnb(0:nff/2)*2.^.5

ENDIF


times(i)=t0
ampsp(i,*)=fftn(0:nff/2)*2.^.5
ampsp_s1(i,*)=fftn(0:nff/2)*2.^.5 							;added 2.^.5

IF power ne 0 THEN BEGIN
	ampsp(i,*)=pow(0:nff/2)*2. &  ampsp_s1(i,*)=pow(0:nff/2)*2.    				;will be off if hanning is used
	IF n_elements(sigb) gt 1 THEN ampsp_s2(i,*)=pow2(0:nff/2)*2.
ENDIF

;****************************************************
;*****CROSSPOWER CALCULATION*************************
;****************************************************
IF crosspower THEN BEGIN
	IF i eq 0 THEN typp='Crosspower'

	fftc1=fft(sig2)
	fftc2=fft(sig2b)
	cpower=abs(fftc1*conj(fftc2))


	IF nsmot gt 2 THEN cpower=smooth(cpower,nsmot)

;Formally this is not correct it should be the expression below
;I think this amounts to just |power1|*|power2|
;if no freq. smoothing is used.
;cpower=abs(smooth(fftc1*conj(fftc2),nsmot,/edge_truncate,/nan))


	ampsp(i,*)=cpower(0:nff/2)*2.

ENDIF

;****************************************************
;******COHERENCE CACULATION**************************
;****************************************************
IF coherence THEN BEGIN
	IF i eq 0 THEN typp='Coherence'
	fftc1=fft(sig2)
	fftc2=fft(sig2b)

	cxy=smooth(fftc1*conj(fftc2),nsmot,/edge_truncate,/nan)
	cxx=smooth(fftc1*conj(fftc1),nsmot,/edge_truncate,/nan)
	cyy=smooth(fftc2*conj(fftc2),nsmot,/edge_truncate,/nan)

	pxy=abs(cxy)
	pxx=abs(cxx)
	pyy=abs(cyy)

	coh=pxy/(pxx*pyy)^.5
	conf=tanh(1.96/(2.*nsmot-2.)^.5) 					;95% confidence level for coherence

	ampsp(i,*)=coh(0:nff/2)

ENDIF

;****************************************************
;*****CROSS-COHERENCE CALCULATION********************                           I think this is just the crosspower multiplied by coherence^2
;****************************************************
IF crosscoh THEN BEGIN
	IF i eq 0 THEN typp='Crosscoherence'
	fftc1=fft(sig2)
	fftc2=fft(sig2b)

	;cpower=abs(fftc1*conj(fftc2))
	;if nsmot gt 2 then cpower=smooth(cpower,nsmot)
	;ampsp(i,*)=cpower(0:nff/2)*2.

	cxy=smooth(fftc1*conj(fftc2),nsmot,/edge_truncate,/nan)
	cxx=smooth(fftc1*conj(fftc1),nsmot,/edge_truncate,/nan)
	cyy=smooth(fftc2*conj(fftc2),nsmot,/edge_truncate,/nan)

	pxy=abs(cxy)
	pxx=abs(cxx)
	pyy=abs(cyy)

	coh=pxy/(pxx*pyy)^.5
	conf=tanh(1.96/(2.*nsmot-2.)^.5) 					;95% confidence level for coherence

	ampsp(i,*)=pxx(0:nff/2)*coh(0:nff/2)^2

ENDIF

;****************************************************
;*****PHASE OPTION***********************************
;****************************************************
IF phase THEN BEGIN

	IF n_elements(sig2b) le 1 THEN BEGIN   					;if only 1 signal then doing phase relative to timebase
		IF i eq 0 THEN typp='AutoPhase'
		fftc1=fft(sig2)
		phassig=atan(imaginary(fftc1),real(fftc1))
		ampsp(i,*)=real(phassig(0:nff/2))
	ENDIF ELSE BEGIN

		IF i eq 0 THEN typp='Crossphase'
		fftc1=fft(sig2)
		fftc2=fft(sig2b)
		;frcp=fftc1*conj(fftc2)

		frcp=smooth(fftc1*conj(fftc2),nsmot,/edge_truncate,/nan)
		phassig=-atan(imaginary(frcp),real(frcp)) ;atan(real(fftc1),imaginary(fftc1))-atan(real(fftc2),imaginary(fftc2))
		;could calculate the phase either way. using the crosspower or phase of 1 - phase of other

		ampsp(i,*)=real(phassig(0:nff/2))
	ENDELSE
ENDIF
;****************************************************

n0=n0+nff*(1.-overlap*.01)
t0=t(n0)

ENDFOR

;****************************************************
;***********End of multiwindow loop******************
;****************************************************



freq=fo

IF coherence THEN BEGIN
	print,'**************************************'
	print,'95% confidence level for coherence is:'
	print, tanh(1.96/(2.*nsmot-2.)^.5) 					;95% confidence level for coherence
	print,'**************************************'
ENDIF

conflev=tanh(1.96/(2.*nsmot-2.)^.5)


print,'Analysis type is: ',typp
print,'Smoothed over:',nsmot,' freq elements, df=',fsmot



nsmotaf=fix(fsmotafter/(fo(1)-fo(0)))  						;checking to see how many to smooth over after analysis if at all

IF nsmotaf gt 0 THEN BEGIN
	print,'Smoothing after analysis by:',nsmotaf,'freq elements, df=',fsmotafter
	ampsp=smooth(ampsp,[1,nsmotaf],/edge_truncate,/nan)
ENDIF

;***************************************************
;********Cutting down freq. range*******************
;***************************************************

IF freqmin ge 0 and freqmax gt 0 THEN BEGIN

	temp=min(abs(fo-freqmin),nfqm)
	temp=min(abs(fo-freqmax),nfqx)

	ampsp=ampsp(*,nfqm:nfqx)
	ampsp_s1=ampsp_s1(*,nfqm:nfqx)
	IF n_elements(sigb_raw) GT 1 THEN ampsp_s2=ampsp_s2(*,nfqm:nfqx)
	freq=fo(nfqm:nfqx)
ENDIF
;***************************************************

;***************************************************
;********PLOTTING***********************************
;***************************************************

IF plott THEN BEGIN
	shade_surf,ampsp,times,freq,ax=90,az=0,shades=bytscl(ampsp),xtitle='Time',ytitle='frequency',xstyle=1,ystyle=1,$
	charsize=1.5

	;contour,ampsp,times,freq,/fill,nlevels=255,xtitle='Time',ytitle='frequency',xstyle=1,ystyle=1,$
	;charsize=1.5

ENDIF


output={ampsp:ampsp,ampsp_s1:ampsp_s1,ampsp_s2:ampsp_s2,freq:freq,times:times}


END
