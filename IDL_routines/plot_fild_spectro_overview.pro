@calculate_spectro.pro

pro plot_fild_spectro_overview,FILD_number=FILD_number,SHOT=SHOT,TIME=TIME,N_FFT=N_FFT,$
    SMOOTH=SMOOTH,OVERLAP=OVERLAP,FREQ_MIN=FREQ_MIN,FREQ_MAX=FREQ_MAX,SAVE_EPS=SAVE_EPS


;=====================================================================================================
;                   J. Galdon               13.07.2021
;=====================================================================================================
;
; Routine to plot FILD overview spectrograms.
;
;=====================================================================================================

;======================================
; DEFAULT PARAMETERS
;======================================
def_shot=30810
def_time_initial=1.5
def_time_final=8.0
def_n_fft=2048.
def_smooth=50.
def_overlap=10.
def_freq_min=1000.
def_freq_max=200000.
;======================================

IF keyword_set(SHOT) THEN shot=shot ELSE shot=def_shot

IF keyword_set(TIME) THEN BEGIN

	time_initial=time(0)
	time_final=time(1)

ENDIF ELSE BEGIN

	time_initial=def_time_initial
	time_final=def_time_final

ENDELSE

IF keyword_set(N_FFT) THEN n_fft=n_fft ELSE n_fft=def_n_fft
IF keyword_set(SMOOTH) THEN smooth=smooth ELSE smooth=def_smooth
IF keyword_set(OVERLAP) THEN overlap=overlap ELSE overlap=def_overlap
IF keyword_set(FREQ_MIN) THEN freq_min=freq_min ELSE freq_min=def_freq_min
IF keyword_set(FREQ_MAX) THEN freq_max=freq_max ELSE freq_max=def_freq_max


IF keyword_set(FILD_number) EQ 0 THEN BEGIN

	print,'Must set a fild number: FILD_number=1 or FILD_number=2 ... Returning'
	return

ENDIF

CASE FILD_number OF

1: BEGIN
	diagname='FHC'
	signal_name='FILD3_'
    nchannels=20

END

2: BEGIN
	diagname='FHA'
	signal_name='FIPM_'
    nchannels=20

END

3: BEGIN
	diagname='HBD'
	signal_name='LAMBDA1:'
    nchannels=20

END


4: BEGIN
	diagname='FHD'
	signal_name='Chan-'
    nchannels=32

END


5: BEGIN
	diagname='FHE'
	signal_name='Chan-'
    nchannels=64

END

ELSE: BEGIN
	print,'Must set a fild number: FILD_number=1 or FILD_number=2 ... Returning'
	return

END

ENDCASE

window,/free,xsize=1400,ysize=1000

nxx=4
nyy=CEIL(nchannels/nxx)
!P.Multi=[0,nxx,nyy]

IF keyword_set(save_eps) THEN BEGIN

		filnm=string(shot)+'_'+string(diagname)+'_overview.eps'
		set_plot,'ps'
		device,filename=filnm,/color,bits_per_pixel=8,font_size=4.,xsize=20.,ysize=10.

		print,'Plotting data to '+filnm
ENDIF


FOR kk=1,nchannels DO BEGIN

	pmtnum=string(kk,format='(I2)')
	pmtnum=strcompress(pmtnum,/remove_all)

	num0='00'
	l=strlen(pmtnum)
	strput,num0,pmtnum,2-l

	signame=signal_name+num0

	print,signame

	calculate_spectro,SHOT,diagname,signame,time_initial,time_final,n_fft,o,$
        overlap=overlap,fsmoth=smooth,freqmin=freq_min,freqmax=freq_max,$
        zlog=0,zsqrt=1,color_table=5,cont_plot=1

;    stop

ENDFOR


IF keyword_set(save_eps) THEN BEGIN
	device,/close_file
	set_plot,'x'
ENDIF

!P.Multi=0

return
end
