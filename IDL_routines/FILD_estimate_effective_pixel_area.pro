function   FILD_estimate_effective_pixel_area,xscale,yscale

;=====================================================================================================
;                   J. Galdon               02/04/2018
;=====================================================================================================
;
; Routine to estimate effective area of the scintillator covered by one pixel
;
; Area_covered_by_1_pixel: A_omega=Area_scint/# pixels inside scintillator
;				Area_scint=Lx_scint*Ly_scint
;				# pixels inside scint=L'x_scint*L'y_scint=Lx_scint*xscale*Ly_scint*yscale
; xscale and yscale are in units of : #pixels/cm
;
; So A_omega can be approximated by: A_omega=1/(xscale*yscale) [cm^2]
;
; 
;=====================================================================================================



output=abs(1./(xscale*yscale)*1.e-4) 	; in m^2

return,output
end
