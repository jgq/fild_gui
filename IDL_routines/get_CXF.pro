function get_CXF,shot,output,PLOT=PLOT

;=====================================================================================================
;                   J. Galdon               20/07/2021
;=====================================================================================================
;
; Routine to read NPA data from CXF diagnostic
; read_signal from /afs/ipp/u/augidl/idl/user_contrib/mrm/readdata/read_signal.pro
;
; Energy is in keV
; Time is in s
; Flux is in XX
;=====================================================================================================

diag='CXF'

rh_energy=-1
lh_energy=-1
rd_energy=-1
ld_energy=-1

; Read H flux
; Right analyzer

read_signal,ier,shot,diag,'RfluxH',time,rfluxh,pp,area_base=rh_energy

IF ier NE 0 THEN rfluxh=-1

; Left analyzer

read_signal,ier,shot,diag,'LfluxH',time,lfluxh,pp,area_base=lh_energy

IF ier NE 0 THEN lfluxh=-1

; Read D flux
; Right analyzer

read_signal,ier,shot,diag,'RfluxD',time,rfluxd,pp,area_base=rd_energy

IF ier NE 0 THEN rfluxd=-1

; Left analyzer

read_signal,ier,shot,diag,'LfluxD',time,lfluxd,pp,area_base=ld_energy

IF ier NE 0 THEN lfluxd=-1

; Background levels

read_signal,ier,shot,diag,'RctsBG',time,rctsbg,pp

IF ier NE 0 THEN rctsbg=-1

read_signal,ier,shot,diag,'LctsBG',time,lctsbg,pp

IF ier NE 0 THEN lctsbg=-1



; HtoD ratio

read_signal,ier,shot,diag,'H2HD',th2hd,h2hd,pp

; Plot

IF KEYWORD_SET(PLOT) THEN BEGIN

    IF n_elements(size(rfluxh,/dim)) GT 1 THEN BEGIN

        window,0
        image,rfluxh,time,rh_energy,$
            xtitle='Time (s)',ytitle='Energy H (keV)',title='Hydrogen Flux Right analyzer',$
            charsize=2.

    ENDIF

    IF n_elements(size(lfluxh,/dim)) GT 1 THEN BEGIN

        window,1
        image,lfluxh,time,lh_energy,$
            xtitle='Time (s)',ytitle='Energy H (keV)',title='Hydrogen Flux Left analyzer',$
            charsize=2.

    ENDIF

    IF n_elements(size(rfluxd,/dim)) GT 1 THEN BEGIN

        window,2
        image,rfluxd,time,rd_energy,$
            xtitle='Time (s)',ytitle='Energy D (keV)',title='Deuterium Flux Right analyzer',$
            charsize=2.

    ENDIF

    IF n_elements(size(lfluxd,/dim)) GT 1 THEN BEGIN

        window,3
        image,lfluxd,time,ld_energy,$
            xtitle='Time (s)',ytitle='Energy D (keV)',title='Deuterium Flux Left analyzer',$
            charsize=2.

    ENDIF

    window,4
    plot,th2hd,h2hd,color=0,background=255,psym=4,$
        xtitle='Time (s)',ytitle='H/(H+D) ratio at 1.5 keV',$
        charsize=2.

ENDIF

; Output

output={time:time,th2hd:th2hd,$
    h2hd:h2hd,$
    rfluxh:rfluxh,rh_energy:rh_energy,$
    lfluxh:lfluxh,lh_energy:lh_energy,$
    rfluxd:rfluxd,rd_energy:rd_energy,$
    lfluxd:lfluxd,ld_energy:ld_energy,$
    lctsbg:lctsbg,rctsbg:rctsbg}



return,output
end
