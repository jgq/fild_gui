pro FILDSIM_plot_strike_map,plate_filename=plate_filename,$
	strike_map_filename=strike_map_filename,strike_point_filename=strike_point_filename

;*****************************************
; J. Galdon               19/01/2016
; University of Seville
; Email: jgaldon@us.es
;*****************************************

;*************************************************************
; Routine to plot the strike-map calculated with fildsim.f90
;*************************************************************


IF keyword_set(plate_filename) EQ 0 THEN BEGIN

	plate_filename='/afs/ipp-garching.mpg.de/home/j/jgq/CODES_TEST/FILDSIM_v1/geometry/aug_fild1_scint.pl'
	print,'Using default plate: ',plate_filename

ENDIF

IF keyword_set(strike_map_filename) EQ 0 THEN BEGIN

	strike_map_filename='/afs/ipp-garching.mpg.de/home/j/jgq/CODES_TEST/FILDSIM_v1/results/TEST_v1_strike_map.dat'
	print,'Using default strike map: ',strike_map_filename

ENDIF

IF keyword_set(strike_points_filename) EQ 0 THEN BEGIN

	strike_points_filename='/afs/ipp-garching.mpg.de/home/j/jgq/CODES_TEST/FILDSIM_v1/results/TEST_v1_strike_points.dat'
	print,'Using default strike points: ',strike_points_filename

ENDIF

;**************************
; READ SCINTILLATOR PLATE
;**************************

FILDSIM_read_plate,plate,filename=plate_filename

;*******************
; READ STRIKE MAP
;*******************

FILDSIM_read_strike_map,strike_map,filename=strike_map_filename

;*******
; PLOT
;*******

window,/free

plot,plate.y,plate.z,psym=-3,color=0,background=255,/iso,xtitle='Y (cm)',ytitle='Z (cm)',charsize=2.0
oplot,strike_map.yy,strike_map.zz,psym=1,color=0

;*************
;PLOT GRID
;*************


gir=strike_map.gyroradius[UNIQ(strike_map.gyroradius, SORT(strike_map.gyroradius))]
pitch=strike_map.pitch_angle[UNIQ(strike_map.pitch_angle,SORT(strike_map.pitch_angle))]



for ii=0,n_elements(gir)-1 DO BEGIN
df=0.0001
tmp=where(abs(strike_map.gyroradius-gir(ii)) LT df,count)
vecx=strike_map.yy(tmp)
vecy=strike_map.zz(tmp)
plots,vecx,vecy,thick=1,color=0
endfor

for ii=0,n_elements(pitch)-1 DO BEGIN
df=0.0001
tmp=where(abs(strike_map.pitch_angle-pitch(ii)) LT df,count)
vecx=strike_map.yy(tmp)
vecy=strike_map.zz(tmp)
plots,vecx,vecy,thick=1,color=0
endfor

return
end
