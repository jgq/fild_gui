pro rewrite_ASCOT_input,A=A,Q=Q,WEIGHT=WEIGHT


;=====================================================================================================
;                   J. Galdon               23/12/2015
;=====================================================================================================
; Routine to rewrite ASCOT input. It reads ASCOT filename given as 5 columns R,Phi,Z,Energy,Pitch and adds 3 additional columns A,Z,Weight
; R,Z in meters
; Phi in deg
; Pitch in v||/v
; E in eV
; A in units of proton mass
; WEIGHT in Number of Ions per MC marker
;
; i.e.:rewrite_ASCOT_input,A=1,Q=1,WEIGHT=1
;=====================================================================================================


IF keyword_set(A) THEN BEGIN

	A=A

ENDIF ELSE BEGIN

	print,'You need to specify the mass keyword (A=??)... Returning'
	return

ENDELSE

IF keyword_set(Q) THEN BEGIN

	Q=Q

ENDIF ELSE BEGIN

	print,'You need to specify the charge keyword (Q=??)... Returning '
	return

ENDELSE

IF keyword_set(WEIGHT) THEN BEGIN

	WEIGHT=WEIGHT

ENDIF ELSE BEGIN

	WEIGHT=1.

ENDELSE


filename= DIALOG_PICKFILE(/READ,PATH='/afs/ipp-garching.mpg.de/u/jgq/IDL/FILD/AUG/')



openr,lun,filename,/get_lun

R=double(0)
Z=double(0)
Phi=double(0)
Energy=double(0)
Pitch=double(0)


WHILE NOT EOF(lun) DO BEGIN

	readf,lun,Ro,Phio,Zo,Energyo,Pitcho            
	R=[R,Ro]
	Z=[Z,Zo]
	Phi=[Phi,Phio]
	Energy=[Energy,Energyo]
	Pitch=[Pitch,Pitcho]
	

ENDWHILE

close,lun
free_lun,lun


R=R(1:*)
Z=Z(1:*)
Phi=Phi(1:*)
Energy=Energy(1:*)
Pitch=Pitch(1:*)

output_filename=DIALOG_PICKFILE(/WRITE,PATH='/afs/ipp-garching.mpg.de/u/jgq/IDL/FILD/AUG/')


openw,lun,output_filename,/get_lun

FOR kk=0,n_elements(R)-1 DO BEGIN
	printf,lun,R(kk),Phi(kk),Z(kk),Energy(kk),Pitch(kk),A,Q,WEIGHT
ENDFOR

close,lun
free_lun,lun


return
end
