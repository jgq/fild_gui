pro FILD_overlay_image,ref_image,image_grid,xscale,yscale,xshift,yshift,deg,grid_image,filter=filter

;==========================================================================
;                   J. Galdon               1/10/2014
;==========================================================================
;
; This routine overlays strike-map from an image file into a given image.
; PARAMETERS AND KEYWORDS:
; ref_image: image (2D array)
; image_grid: 2-D array containing output image with overlayed grid 
; xscale/yscale/xshift/yshift/deg: parameters to overlay grid
; FILTER: keyword to use median filter.
; PLOT: keyword to plot image_grid. 
;==========================================================================

FILTER=FILTER
PLOT=PLOT

;******************************
;READ STRIKE-MAP IMAGE
;******************************

gridImage=grid_image
gridImage=read_bmp(gridImage)

;*********************************
;READ FRAME
;*********************************

ss=size(ref_image)

IF ss(0) EQ 0 THEN BEGIN
	Imagen=read_IMAGE(ref_image)
ENDIF ELSE BEGIN
	Imagen=ref_image
ENDELSE


IF keyword_set(filter) THEN BEGIN
        filter=filter
        Imagen=median(imagen,filter)        
ENDIF

Imagen=bytscl(imagen,max=max(imagen))

                  
s=size(imagen,/dimensions)
im_size_x=s(0)
im_size_y=s(1)

;*********************************************
;PARAMETERS FOR GRID POSITIONING AND SCALING
;*********************************************

xscale=xscale
yscale=yscale
xshift=xshift
yshift=yshift
deg=deg
alfa=deg*!pi/180

;***************
;OVERLAY GRID
;***************

gridSize=size(gridImage)
        if gridSize[0] ne 2 then begin
        print,'cannot add grid because expecting 2-D data'
        print,gridsize
        return
        endif

xSize=gridsize[1]
ysize=gridsize[2]

gridInd=max(gridImage)				;   change min/max depending on the grid color (black or white)
gridvalue=make_array(1,1,/UINT)
gridvalue[0,0]=!d.table_size-1


for x=0,xsize-1 do begin
   for y=0,ysize-1 do begin
        if gridImage[x,y] eq gridInd then begin

				round1=ROUND((cos(alfa)*x-sin(alfa)*y)*xscale+xshift)
				round2=ROUND((sin(alfa)*x+cos(alfa)*y)*yscale+yshift)
                           	IF round1 LT im_size_x AND round1 GE 0 AND round2 LT im_size_y AND round2 GE 0 THEN BEGIN
	
					imagen(round1,round2)=GRIDVALUE

                           	ENDIF
       
        endif
   endfor
endfor

image_grid=imagen

return
end
