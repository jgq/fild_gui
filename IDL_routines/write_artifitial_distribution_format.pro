pro write_artifitial_distribution_format,filename,header1,header2,energy,pitch,A,Z,weight

;=====================================================================================================
;                   J. Galdon               20/05/2015
;=====================================================================================================
;
; energy in eV
; pitch in deg
; A in uma
; Z in echarge
;=====================================================================================================

print,'Writing distribution file'
help,energy,pitch,A,Z,weight

filename=filename

openw,lun,filename,/get_lun

printf,lun,header1
printf,lun,header2

FOR kk=0L,n_elements(energy)-1 DO BEGIN

	printf,lun,energy(kk),pitch(kk),A(kk),Z(kk),weight(kk)

ENDFOR

close,lun
free_lun,lun

ON_IOERROR,NULL


return
end
