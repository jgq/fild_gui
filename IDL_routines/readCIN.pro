; ---------------------------------------------------------
; -- Programs for reading data from phantom V7 camera
; -- in .cin format.
; -- Adapted from Bill Davis 31-May-2006 (NSTX)
; -- Original program written by Ricky Maqueda (?)
; --
; -- Mirko Ramisch 23-Febr-2007
; ---------------------------------------------------------



FUNCTION CINreadHeaderInfo, filename, CinHeaderInfo, VERBOSE=verbose
; -- INPUT	: STRING filename
; -- OUTPUT	: STRUCT CinHeaderInfo
; -- KEYWORD	: VERBOSE prints header info
; -- RETURN	: error code unequal zero if error occurs

	first_frame = 0UL
	sequence_length = 0UL
	pointers = ULONARR(3)
	ttrigg = ULONARR(2)

	size_x = 0UL
	size_y = 0UL

	toffset = 0U
	tsize = 0UL
	type = 0U

	frame_rate = 0UL
	exposure = 0UL



	GET_LUN, U
	OPENR, U, filename, ERROR=err, /swap_if_big_endian
	IF(err NE 0) THEN BEGIN
        	PRINT, 'Error opening file '+filename + '!'
        	RETURN, err
	ENDIF

	; -- read specific info from header
	; -- skip first 16 bytes
	POINT_LUN, U, 16		; - [0]
					; - [0] I16 id ?
					; - [2] I16 pointer0 ?
					; - [4]

	READU, U, first_frame		; - [16] read pos of first frame
	READU, U, sequence_length	; - [20] read number of frames
	READU, U, pointers		; - [24] read pointers
	READU, U, ttrigg		; - [36] ?
					; - [42] U32 pointer0-4 ?
					; - [46] U32 size x ?
					; - [50] U32 size y ?
					; - [54] U64 ?
					; - [62] U32 frame size ?
					; - [66]

	; -- jump to pointer 0 and skip 4 bytes
	POINT_LUN, U, pointers[0]+4	; - [0]
	READU, U, size_x		; - [p0+4] x size of frame
	READU, U, size_y		; - [p0+8] y size of frame
					; - [p0+12]

	; -- jump to pointer 1 and skip 142 bytes
	POINT_LUN, U, pointers[1]+142	; - [0]
	READU, U, toffset		; - [p1+142] offset for timebase
					; - [p1+144]

	; -- jump to pointer 1 and skip 768 bytes
	POINT_LUN, U, pointers[1]+768	; - [0]
	READU, U, frame_rate		; - [p1+768] frames per second
	READU, U, exposure		; - [p1+772] ...
					; - [p1+776]

	; -- jump to pointer 1 and skip toffset
	POINT_LUN, U, pointers[1]+toffset	; - [0]
	READU, U, tsize, type		; - [p1+to] size of timebase and type
					; - [p1+to+6]

	; -- read timebase
	IF ((pointers[2] GT pointers[1]+toffset) $
				AND (type EQ 1002)) THEN BEGIN
		tmpt = ULONARR(2, sequence_length)

		; -- jump to pointer 1 and skip toffset+8
		POINT_LUN, U, pointers[1]+toffset+8
		READU, U, tmpt

		timebase = REFORM((tmpt[0,*]/2.D^32-ttrigg[0]/2.D^32)+ $
				 (tmpt[1,*]-ttrigg[1]), sequence_length)

originaltimebase=timebase		 

		dt = timebase[1:sequence_length-1] - $
			timebase[0:sequence_length-2]

		idx = WHERE(ABS(dt*frame_rate - 1) GT 0.1)

		IF idx[0] NE -1 THEN BEGIN
			frame_rate = idx[0]/(timebase[idx[0]]-timebase[0])
			timebase = timebase[0]+FINDGEN(sequence_length)/frame_rate
		ENDIF
		frame_rate = (sequence_length-1)/(timebase[sequence_length-1]-timebase[0])
		IF ABS(frame_rate*timebase[0]-first_frame) GT 2 THEN BEGIN
		      timebase = (timebase-timebase[0]) + first_frame/frame_rate
		ENDIF
	ENDIF ELSE BEGIN
		; -- alternate timebase
		timebase = (FINDGEN(sequence_length)+first_frame)/frame_rate
	ENDELSE

	; -- jump to pointer 2
	POINT_LUN, U, pointers[2]	; - [0]
	frame_pos_list = ULON64ARR(sequence_length)
	READU, U, frame_pos_list	; - [p2]
					; - [p2+8*frames]

	FREE_LUN, U

timebase=originaltimebase

	CinHeaderInfo = CREATE_STRUCT(NAME='CinHeaderInfo', $
			'first_frame', first_frame, $
			'num_frames', sequence_length, $
			'pointers', pointers, $
			'ttrigg', ttrigg, $
			'size_x', size_x, $
			'size_y', size_y, $
			'toffset', toffset, $
			'frame_rate', frame_rate, $
			'exposure', exposure, $
			't_size', tsize, $
			'type', type, $
			'timebase', timebase, $
			'frame_pos', frame_pos_list)

	IF NOT KEYWORD_SET(verbose) THEN RETURN, 0

	PRINT, '1st Frame: ', first_frame
	PRINT, '#frames  : ', sequence_length
	PRINT, 'pointers : ', pointers[0]
	PRINT, '           ', pointers[1]
	PRINT, '           ', pointers[2]
	PRINT, 'Ttrigg	 : ', ttrigg[0]
	PRINT, '           ', ttrigg[1]
	PRINT, 'Size X   : ', size_x
	PRINT, 'Size Y   : ', size_y
	PRINT, 't offset : ', toffset
	PRINT, 'FPS      : ', frame_rate
	PRINT, 'Exposure : ', exposure
	PRINT, 't size   : ', tsize
	PRINT, 'Type     : ', type
	PRINT, 'Timebase : ', timebase[0]
	PRINT, 'Frame Pos: ', n_elements(frame_pos_list)

	RETURN, 0
END










FUNCTION CINreadFrames, filename, CinHeaderInfo, Frames, $
		OFFSET=offset, NFRAMES=nframes,PIXEL_DEPTH_8=pixel_depth_8
; -- INPUT	: STRING filename
; -- OUTPUT	: STRUCT CinHeaderInfo
; --		: ARRAY  Frames [x, y, t]
; -- KEYWORD	: OFFSET 0 if not set
; -- 		: NFRAMES all frames if not set
; -- RETURN	: error code unequal zero if error occurs


	; -- read header info first

	err = CINreadHeaderInfo(filename, CinHeaderInfo)
	IF (err NE 0) THEN RETURN, err


	; -- preparations

	frametot = CinHeaderInfo.num_frames
	frame1	 = CinHeaderInfo.first_frame

	IF NOT KEYWORD_SET(offset) THEN offset=0
	fstart = ULONG((offset LT 0)?0:offset)

	IF NOT KEYWORD_SET(nframes) THEN $
		fend=frametot-1UL ELSE $
		fend=fstart+nframes-1UL

	IF (fend GE frametot) THEN BEGIN
		PRINT, 'Warning: number of frames are truncated to upper limit!'
		fend = frametot-1UL
	ENDIF

	IF (fstart GT fend) THEN BEGIN
		PRINT, 'Offset exceeds total number of frames. Aborting!'
		RETURN, -1
	ENDIF


	; -- allocate memory

	tnframes = fend - fstart + 1UL
        IF keyword_set(Pixel_depth_8) THEN BEGIN
                Frames = BYTARR(CinHeaderInfo.size_x, CinHeaderInfo.size_y, tnframes)
                tframe = BYTARR(CinHeaderInfo.size_x, CinHeaderInfo.size_y)
        ENDIF ELSE BEGIN
	Frames = UINTARR(CinHeaderInfo.size_x, CinHeaderInfo.size_y, tnframes)             ; use UINTARR when discharge is saved in 12 bits
	tframe = UINTARR(CinHeaderInfo.size_x, CinHeaderInfo.size_y)                       ; use BYTARR when discharge is saved in 8 bits
        ENDELSE
	; -- open file

	GET_LUN, U
	OPENR, U, filename, ERROR=err , /swap_if_big_endian
	IF(err NE 0) THEN BEGIN
        	PRINT, 'Error opening file '+filename + '!'
        	RETURN, err
	ENDIF


	foff = 0UL

	FOR i=fstart, fend DO BEGIN
		POINT_LUN, U, CinHeaderInfo.frame_pos[i]
		READU, U, foff
		POINT_LUN, U, CinHeaderInfo.frame_pos[i]+foff
		READU, U, tframe
		Frames[*,*,i-fstart] = tframe
	ENDFOR

	FREE_LUN, U

	RETURN, 0

END








FUNCTION CINreadSingleFrame, filename, framenum, Frame
; -- INPUT	: STRING filename
; --		: ULONG framenum frame number (start@0)
; -- OUTPUT	: 2D-ARRAY  Frame
; -- RETURN	: error code unequal zero if error occurs


	sequence_length = 0UL
	pointers = ULONARR(3)

	size_x = 0UL
	size_y = 0UL


	GET_LUN, U
	OPENR, U, filename, ERROR=err , /swap_if_big_endian
	IF(err NE 0) THEN BEGIN
        	PRINT, 'Error opening file '+filename + '!'
        	RETURN, err
	ENDIF

	; -- read specific info from header
	; -- skip first 20 bytes
	POINT_LUN, U, 20		; - [0]

	READU, U, sequence_length	; - [20] read number of frames
	READU, U, pointers		; - [24] read pointers

	; -- jump to pointer 0 and skip 4 bytes
	POINT_LUN, U, pointers[0]+4	; - [0]
	READU, U, size_x		; - [p0+4] x size of frame
	READU, U, size_y		; - [p0+8] y size of frame

	; -- jump to pointer 2
	POINT_LUN, U, pointers[2]	; - [0]
	frame_pos_list = ULON64ARR(sequence_length)
	READU, U, frame_pos_list	; - [p2]

	; -- checks
	IF ((framenum LT 0) OR (framenum GE sequence_length)) THEN BEGIN
		PRINT, 'Frame index out of range. Aborting!'
		FREE_LUN, U
		RETURN, -1
	ENDIF

	; -- read frame
	foff = 0UL
	Frame = UINTARR(size_x, size_y)

	POINT_LUN, U, frame_pos_list[framenum]
	READU, U, foff
	POINT_LUN, U, frame_pos_list[framenum]+foff
	READU, U, Frame

	FREE_LUN, U

	RETURN, 0

END







FUNCTION CINreadSubFrames, filename, pix_lowleft, pix_upright, SubFrames, $
	NPOINTS=npoints
; -- INPUT	: STRING filename
; --		: UINTARR(2) pix_lowleft, pix_upright corner pixels, indices (start@0)
; -- OUTPUT	: UINTARR(subx, suby, npoints) SubFrames
; -- KEYWORD	: NPOINTS number of points to be read (per pixel)
; -- RETURN	: error code unequal zero if error occurs

	sequence_length = 0UL
	pointers = ULONARR(3)

	size_x = 0UL
	size_y = 0UL


	GET_LUN, U
	OPENR, U, filename, ERROR=err , /swap_if_big_endian
	IF(err NE 0) THEN BEGIN
        	PRINT, 'Error opening file '+filename + '!'
        	RETURN, err
	ENDIF

	; -- read specific info from header
	; -- skip first 20 bytes
	POINT_LUN, U, 20		; - [0]

	READU, U, sequence_length	; - [20] read number of frames
	READU, U, pointers		; - [24] read pointers

	; -- jump to pointer 0 and skip 4 bytes
	POINT_LUN, U, pointers[0]+4	; - [0]
	READU, U, size_x		; - [p0+4] x size of frame
	READU, U, size_y		; - [p0+8] y size of frame

	; -- jump to pointer 2
	POINT_LUN, U, pointers[2]	; - [0]
	frame_pos_list = ULON64ARR(sequence_length)
	READU, U, frame_pos_list	; - [p2]

	; -- checks
	IF ((pix_lowleft[0] LT 0) OR $
		(pix_upright[0] GE size_x) OR $
		(pix_lowleft[1] LT 0) OR $
		(pix_upright[1] GE size_y)) THEN BEGIN
		PRINT, 'Offset indices out of range. Aborting!'
		FREE_LUN, U
		RETURN, -1
	ENDIF

	; -- start offset in frame
	infoff = (pix_lowleft[1]*ULONG(size_x) + pix_lowleft[0])*2UL
	nextrowstep = ULONG(size_x)*2UL

	IF NOT KEYWORD_SET(npoints) THEN npoints=sequence_length

	IF (npoints GT sequence_length) THEN BEGIN
		PRINT, 'Warning: trace length has been truncated.'
		npoints = sequence_length
	ENDIF

	; -- read subframes
	subx_size = ULONG(pix_upright[0] - pix_lowleft[0] + 1)
	suby_size = ULONG(pix_upright[1] - pix_lowleft[1] + 1)
	foff = 0UL
	SubFrames = UINTARR(subx_size, suby_size, npoints)
	SubRow = UINTARR(subx_size)

	FOR i=0UL, npoints-1UL DO BEGIN
		POINT_LUN, U, frame_pos_list[i]
		READU, U, foff
		FOR j=0UL, suby_size-1UL DO BEGIN
			POINT_LUN, U, frame_pos_list[i]+foff+infoff+j*nextrowstep
			READU, U, SubRow
			SubFrames[*,j,i] = SubRow
		ENDFOR
	ENDFOR


	FREE_LUN, U

	RETURN, 0

END









FUNCTION CINreadSingleTrace, filename, xoff, yoff, Trace, $
	NPOINTS=npoints
; -- INPUT	: STRING filename
; --		: UINT xoff, yoff channel indices (start@0)
; -- OUTPUT	: ARRAY  Trace
; -- KEYWORD	: NPOINTS number of points to be read (per pixel)
; -- RETURN	: error code unequal zero if error occurs

	sequence_length = 0UL
	pointers = ULONARR(3)

	size_x = 0UL
	size_y = 0UL


	GET_LUN, U
	OPENR, U, filename, ERROR=err, /swap_if_big_endian
	IF(err NE 0) THEN BEGIN
        	PRINT, 'Error opening file '+filename + '!'
        	RETURN, err
	ENDIF

	; -- read specific info from header
	; -- skip first 20 bytes
	POINT_LUN, U, 20		; - [0]

	READU, U, sequence_length	; - [20] read number of frames
	READU, U, pointers		; - [24] read pointers

	; -- jump to pointer 0 and skip 4 bytes
	POINT_LUN, U, pointers[0]+4	; - [0]
	READU, U, size_x		; - [p0+4] x size of frame
	READU, U, size_y		; - [p0+8] y size of frame

	; -- jump to pointer 2
	POINT_LUN, U, pointers[2]	; - [0]
	frame_pos_list = ULON64ARR(sequence_length)
	READU, U, frame_pos_list	; - [p2]

	; -- checks
	IF ((xoff LT 0) OR (xoff GE size_x) $
			OR (yoff LT 0) OR (yoff GE size_y)) THEN BEGIN
		PRINT, 'Offset indices out of range. Aborting!'
		FREE_LUN, U
		RETURN, -1
	ENDIF

	; -- offset in frame
	infoff = (yoff*ULONG(size_x) + xoff)*2UL

	IF NOT KEYWORD_SET(npoints) THEN npoints=sequence_length

	IF (npoints GT sequence_length) THEN BEGIN
		PRINT, 'Warning: trace length has been truncated.'
		npoints = sequence_length
	ENDIF

	; -- read trace
	foff = 0UL
	Trace = UINTARR(npoints)
	value = 0U

	FOR i=0UL, npoints-1UL DO BEGIN
		POINT_LUN, U, frame_pos_list[i]
		READU, U, foff
		POINT_LUN, U, frame_pos_list[i]+foff+infoff
		READU, U, value
		Trace[i] = value
	ENDFOR


	FREE_LUN, U

	RETURN, 0

END







FUNCTION CINreadRows, filename, yoff, Rows, $
	NPOINTS=npoints
; -- INPUT	: STRING filename
; --		: UINT yoff row index (start@0)
; -- OUTPUT	: ARRAY  Rows [x, t]
; -- KEYWORD	: NPOINTS number of points to be read (per pixel)
; -- RETURN	: error code unequal zero if error occurs

	sequence_length = 0UL
	pointers = ULONARR(3)

	size_x = 0UL
	size_y = 0UL


	GET_LUN, U
	OPENR, U, filename, ERROR=err, /swap_if_big_endian
	IF(err NE 0) THEN BEGIN
        	PRINT, 'Error opening file '+filename + '!'
        	RETURN, err
	ENDIF

	; -- read specific info from header
	; -- skip first 20 bytes
	POINT_LUN, U, 20		; - [0]

	READU, U, sequence_length	; - [20] read number of frames
	READU, U, pointers		; - [24] read pointers

	; -- jump to pointer 0 and skip 4 bytes
	POINT_LUN, U, pointers[0]+4	; - [0]
	READU, U, size_x		; - [p0+4] x size of frame
	READU, U, size_y		; - [p0+8] y size of frame

	; -- jump to pointer 2
	POINT_LUN, U, pointers[2]	; - [0]
	frame_pos_list = ULON64ARR(sequence_length)
	READU, U, frame_pos_list	; - [p2]

	; -- checks
	IF ((yoff LT 0) OR (yoff GE size_y)) THEN BEGIN
		PRINT, 'Row index out of range. Aborting!'
		FREE_LUN, U
		RETURN, -1
	ENDIF

	; -- offset in frame
	infoff = yoff*ULONG(size_x)*2UL

	IF NOT KEYWORD_SET(npoints) THEN npoints=sequence_length

	IF (npoints GT sequence_length) THEN BEGIN
		PRINT, 'Warning: trace lengths have been truncated.'
		npoints = sequence_length
	ENDIF

	; -- read trace
	foff = 0UL
	Rows = UINTARR(size_x, npoints)
	srow = UINTARR(size_x)

	FOR i=0UL, npoints-1UL DO BEGIN
		POINT_LUN, U, frame_pos_list[i]
		READU, U, foff
		POINT_LUN, U, frame_pos_list[i]+foff+infoff
		READU, U, srow
		Rows[*,i] = srow
	ENDFOR


	FREE_LUN, U

	RETURN, 0

END

