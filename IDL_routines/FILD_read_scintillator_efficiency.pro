function FILD_read_scintillator_efficiency,filename,PLOT=PLOT

;*****************************************
; J. Galdon               15/03/2017
; University of Seville
; Email: jgaldon@us.es
;*****************************************
;****************************************************************
; 
;****************************************************************

nlines_header=8
header=strarr(nlines_header)
npoints=0L
dumm=''
dumme=0.
dummy=0.
comm=''

openr,lun,filename,/get_lun

FOR kk=0,nlines_header-1 DO BEGIN
	readf,lun,dumm
	header(kk)=dumm
ENDFOR

readf,lun,npoints

energy=dblarr(npoints)
yield=dblarr(npoints)

FOR kk=0,npoints-1 DO BEGIN

	readf,lun,dumme,dummy
	energy(kk)=dumme
	yield(kk)=dummy

ENDFOR

Comments=''

WHILE NOT EOF(lun) DO BEGIN

	readf,lun,comm
	comments=[comments,comm]
ENDWHILE

close,lun
free_lun,lun

;print,'Data file read succesfully: ',filename

IF keyword_set(PLOT) THEN BEGIN

	window,/free
	plot,energy,yield,xtitle='Energy (eV)',ytitle='Yield (photons/s)',charsize=2.,color=0,background=255,psym=-4

ENDIF

output={header:header,energy:energy,yield:yield,comments:comments}

return,output
end
