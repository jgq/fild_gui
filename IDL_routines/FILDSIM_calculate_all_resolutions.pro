@FILDSIM_calculate_pitch_resolution
@FILDSIM_calculate_gyroradius_resolution_v2

function FILDSIM_calculate_all_resolutions,strike_points_filename,BINSIZE=BINSIZE,STRIKE_POINTS_CALCULATED=STRIKE_POINTS_CALCULATED

;==========================================================================
;                   J. Galdon               19/01/2016
;==========================================================================
; 
;==========================================================================


; READ STRIKE POINTS
widget_control,/HOURGLASS

IF keyword_set(BINSIZE) THEN bin_size=binsize ELSE bin_size=0

IF keyword_set(STRIKE_POINTS_CALCULATED) THEN BEGIN
	strike_points=strike_points_filename
ENDIF ELSE BEGIN
	FILDSIM_read_strike_points,strike_points,filename=strike_points_filename
	print,'Strike Points Filename read: ',strike_points_filename
ENDELSE

nm_strike_points=strike_points	
	
		
U_gyro=nm_strike_points.gyroradius[UNIQ(nm_strike_points.gyroradius,SORT(nm_strike_points.gyroradius))]
U_pitch=nm_strike_points.pitch_angle[UNIQ(nm_strike_points.pitch_angle,SORT(nm_strike_points.pitch_angle))]


nm_resolutions=fltarr(12,n_elements(U_gyro)*n_elements(U_pitch))

; FOR EACH PAIR PITCH-GYRO CALCULATE THE RESOLUTION

counter=0

FOR kk=0,n_elements(U_gyro)-1 DO BEGIN
	FOR jj=0,n_elements(U_pitch)-1 DO BEGIN

		ww=where(nm_strike_points.gyroradius EQ U_gyro(kk) AND nm_strike_points.pitch_angle EQ U_pitch(jj),count)

		sigma_gir=0.
		sigma_pitch=0.
		centroid_gir=0.
		centroid_pitch=0.
		amplitude_gir=0.
		amplitude_pitch=0.
		skew_alpha=0.
		skew_mu=0.
		skew_sigma=0.
		skew_norm=0.

			
		IF COUNT GT 0 THEN BEGIN

			remap_gyro=nm_strike_points.remap_gyro(ww)
			remap_pitch=nm_strike_points.remap_pitch(ww)

			FILDSIM_calculate_pitch_resolution,remap_pitch,o2,BINSIZE=bin_size

			IF size(o2,/TYPE) EQ 8 THEN BEGIN

				sigma_pitch=o2.coeff(2)
				centroid_pitch=o2.coeff(1)
				amplitude_pitch=o2.coeff(0)

			
			ENDIF

			FILDSIM_calculate_gyroradius_resolution_v2,remap_gyro,o1,BINSIZE=bin_size,PRINT_INFO=0,PLOT=0

			IF size(o1,/TYPE) EQ 8 THEN BEGIN

				sigma_gir=o1.coeff(2)
				centroid_gir=o1.coeff(1)
				amplitude_gir=o1.coeff(0)
				skew_alpha=o1.skew_normal(0)
				skew_mu=o1.skew_normal(1)
				skew_sigma=o1.skew_normal(2)
				skew_norm=o1.skew_normal(3)

			ENDIF	

		ENDIF

		nm_resolutions(0,counter)=U_gyro(kk)
		nm_resolutions(1,counter)=U_pitch(jj)
		nm_resolutions(2,counter)=sigma_gir
		nm_resolutions(3,counter)=sigma_pitch
		nm_resolutions(4,counter)=centroid_gir
		nm_resolutions(5,counter)=centroid_pitch
		nm_resolutions(6,counter)=amplitude_gir
		nm_resolutions(7,counter)=amplitude_pitch
		nm_resolutions(8,counter)=skew_alpha
		nm_resolutions(9,counter)=skew_mu
		nm_resolutions(10,counter)=skew_sigma
		nm_resolutions(11,counter)=skew_norm
			
		counter=counter+1

	ENDFOR
ENDFOR
	
print,'Gyro /','Pitch /','Fitted Gyro Centroid /','Fitted Pitch Centroid /','Fitted Gyro centroid skew /','Skew alpha'

FOR kk=0,n_elements(U_gyro)*n_elements(U_pitch)-1 DO BEGIN

	print,nm_resolutions(0,kk),nm_resolutions(1,kk),nm_resolutions(4,kk),nm_resolutions(5,kk),nm_resolutions(9,kk),nm_resolutions(8,kk)

ENDFOR


return,nm_resolutions
end
