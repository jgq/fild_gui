pro FILDSIM_calculate_gyroradius_resolution,remap_gyro,output,BINSIZE=BINSIZE,PLOT=PLOT

;*****************************************
; J. Galdon               19/01/2016
; University of Seville
; Email: jgaldon@us.es
;*****************************************

;**************************************************************************
; Routine to calculate the gyroradius resolution of FILD from the data
; given by fildsim.f90
; Given a set of strike points, a histogram in gyroradius is calculated
; and fitted to a gaussian.
; INPUTS:
; remap_gyro: The remapped gyroradius of each strike point as given by fildsim.f90
;	The strike-points must correspond to only one pair Gyroradius-Pitch angle
; KEYWORDS:
; BINSIZE: Select the size of the histogram bins.
; PLOT: IF 1, then the histogram points are plotted together with the gaussian fit.
;***************************************************************************


IF n_elements(remap_gyro) LE 5 THEN BEGIN

	print,'Number of strike points too low to perform resolution calculation...Returning'
	return

ENDIF

IF keyword_set(BINSIZE) EQ 0 THEN BEGIN

	BINSIZE=0.1

ENDIF ELSE BEGIN

	BINSIZE=BINSIZE

ENDELSE

h=histogram(remap_gyro,binsize=binsize,locations=x_hist,min=0.,max=15.)

; FIT TO GAUSSIAN
; Should also try procedure curvefit.pro to fit to a SKEW NORMAL DISTRIBUTION (Assymetric gaussian)

fitt=gaussfit(x_hist,h,coeff,NTERMS=3,CHISQ=chi)
;print,'Fitted to gaussian'
;print,'Centroid / Sigma :',coeff(1),coeff(2)
;print,'Reduced Chi Squared is: ',chi

IF keyword_set(plot) THEN BEGIN

	window,/free
	plot,x_hist,h,color=0,background=255,psym=4,xtitle='Gyroradius (cm)',ytitle='N strikes',charsize=2.0,$
		xrange=[1.,12.]

	xx=findgen(1000)/999*12
	oplot,xx,coeff(0)*exp(-((xx-coeff(1))/coeff(2))^2/2),color=50,psym=-3
ENDIF

output={h:h,x_hist:x_hist,coeff:coeff}

return
end
