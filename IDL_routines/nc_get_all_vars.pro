pro nc_get_all_vars,cdf_filename,output,QUIET=QUIET

;=====================================================================================================
;                   J. Galdon               28/11/2021
;=====================================================================================================
;
; Routine to read all variables from a netCDF file
;
;=====================================================================================================

cdf_id=NCDF_open(cdf_filename,/nowrite)
print,'Reading file: ',cdf_filename


info= NCDF_INQUIRE(cdf_id)

names=strarr(info.nvars)
datatype=strarr(info.nvars)
ndims=lonarr(info.nvars)
natts=lonarr(info.nvars)
dimens=lonarr(info.nvars)


IF keyword_set(QUIET) EQ 0 THEN print,'VARNAME   ','DATATYPE   ','NDIMS   ','NATTS   ','DIM'

FOR jj=0,info.nvars-1 DO BEGIN

	var = ncdf_varinq( cdf_id, jj )
  	ncdf_control, cdf_id, /NOVERBOSE
  	names(jj)=var.name
	datatype(jj)=var.datatype
	ndims(jj)=var.ndims
	natts(jj)=var.natts
	;dimens(jj)=var.dim
	ncdf_control, cdf_id, /VERBOSE

	IF keyword_set(QUIET) EQ 0 THEN BEGIN
		print,string(var.name,format='(A15)'),'   ',strtrim(string(var.datatype),1),'   ',strtrim(string(var.ndims),1),'   ',$
			strtrim(string(var.natts),1),'   ',strtrim(string(var.dim),1)
	ENDIF

ENDFOR

NCDF_close,cdf_id

output={info:info,names:names,datatype:datatype,ndims:ndims,natts:natts}

return
end
