
pro convert_sav_into_png,filename

restore,filename,/verb

ss=size(vec,/dim)

mean_im=total(vec,3)/ss(2)

mean_im=UINT(mean_im)


output_filename=filename+'.png'
write_png,output_filename,mean_im


return
end