# FILD_GUI

This is the GUI used for analysis of FILD signals in ASDEX Upgrade. 
Written in IDL by J.Galdon-Quiroga (jgq@ipp.mpg.de)

# Configuration of the GUI

The configuration of the GUI is now automatically done in the start_fild_gui.sh file. Here you can define the path of your FILDSIM and FILD_GUI codes.

Anyway, one can change the default configuration to his/her own convenience in the file fild_gui.pro, under the COMMON SETTINGS block.

Some additional configuration is possible:

- calibration file --> to automatically get the strike-map parameters
- grid image --> to overlay a strike-map from an image. Not really used anymore.
- default scintillator plate --> format input is the same as for FILDSIMf90. By default set to the FILDSIM/geometry directory.

COMMON STRIKE MAP CALCULATOR BLOCK

fildsimf90_executable --> give here the path for the FILDSIM.exe file
machine --> name of the machine where you want to submit the runs. USE WITH RESPONSIBILITY!!


# Additional information

# 22.01.2024 

Setting up configuration in the new zsh. 

In order to start the GUI type:
> source ./start_fild_gui.sh 

---------------------------------------------

The readg.pro routine needs to be adapted to the new shares. Any function of the GUI relying on this will not work. 

# Pre 2024

In order to start the GUI type

> ./start_fild_gui.sh 

In order to be able to load videos from the phantom camera, you should run the GUI from toki01 machine

> ssh user@toki01

In order to be able to use the "Calculate B field buttons" you should load the NAG libraries in advance

> module load nag_flib/mk26

(In principle, this should be automatically loaded by start_fild_gui.sh)


Some generic strike maps can be found in:

> /u/jgq/PUBLIC/FILDSIM_strike_maps/

In order to update your FILD_GUI version do the following:

In your fild_gui/ directory type:

> git fetch

with this command you will be fetching changes in the remote repository. Then:

> git pull

with this command you pull the changes to your local version