# Shell script to start fild_gui with correct configuration
# In zsh execute this file as: source ./start_fild_gui.sh (otherwise it will not load the env variables properly).  

module load idl

# To make this file executable type: chmod +x start_fild_gui.sh

export FILD_GUI_PATH=${work}/fild_gui
export FILDSIMf90_PATH=${work}/FILDSIM
export FILD_DATA_PATH=/afs/ipp-garching.mpg.de/home/a/augd/rawfiles/FIL/

# Clean user settings and define IDL path needed

unset IDL_PATH
unset IDL_STARTUP

# export IDL_PATH=+${HOME}:+shares/software/aug-dv/moduledata/idl_user_contrib:+${FILD_GUI_PATH}:+${FILD_GUI_PATH}/IDL_routines/
export IDL_PATH=+/shares/software/aug-dv/moduledata/idl_user_contrib:"<IDL_DEFAULT>":+${FILD_GUI_PATH}:+${FILD_GUI_PATH}/IDL_routines/

# Load modules

export name=${HOST}

# # switch ("$name")
#        case tok*:
#        	    echo "We are in $name"
# 	    module load intel
# 	    module load nag_flib/mk26
# 	    breaksw

# 	    default:
# 		echo "We are in $name"
# 		module load nag_flib/mk26
# 		breaksw

# # endsw
 


# Start fild_gui

 idl ${FILD_GUI_PATH}/IDL_routines/start_fild_gui.pro
